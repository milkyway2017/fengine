#include "Camera.h"
#include "Interpolation.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Fengine
{
	Camera::Camera()
	{
	}


	Camera::~Camera()
	{
	}

	void Camera::NewRes()
	{
		screen_wh_ = Point2F(Graphics::GetInstance()->GetWindowSize());

		camera_ortho_matrix_ = glm::ortho((float)0, screen_wh_.x, screen_wh_.y, (float)0);
	}

	void Camera::Update()
	{
		//update position by calling get_position_
		if(get_position_)
			get_position_(&camera_position_.x, &camera_position_.y);

		Apply();
	}

	void Camera::SetPosition(glm::vec2 position)
	{
		this->camera_position_ = position;

		Apply();
	}

	float Camera::GetScale() const
	{
		return camera_scale_;
	}

	void Camera::Init(GLuint ProgramID, std::string NameInProgram)
	{
		screen_wh_ = Point2F(Graphics::GetInstance()->GetWindowSize());
		camera_scale_ = 1.0f;
		camera_position_ = glm::vec2(.0f, .0f);

		this->shader_program_ = ProgramID;
		this->camera_location_ = glGetUniformLocation(ProgramID, NameInProgram.c_str());
		camera_ortho_matrix_ = glm::ortho((float)0, screen_wh_.x, screen_wh_.y, (float)0);
	}

	Rect4F Camera::GetFieldOfView()
	{
		float field_w = screen_wh_.x / camera_scale_, field_h = screen_wh_.y / camera_scale_;
		return Rect4F(camera_position_.x - (field_w / 2), camera_position_.y - (field_h / 2), field_w, field_h);
	}

	void Camera::SetPositionCallback(PositionSetter get_position_function)
	{
		get_position_ = get_position_function;
	}

	glm::vec2 Camera::GetPostion()
	{
		return camera_position_;
	}

	void Camera::ZoomToScale(float to_scale, float from_scale, Uint32 time, const Functor & callback)
	{
		if (from_scale == -1.f)
			from_scale = camera_scale_;
		Interpolation::AddInterpolationToPool(
			new SineInterpolation(&camera_scale_, from_scale, to_scale, time, callback)
		);
	}

	void Camera::MoveTo(const Point2F & position, Uint32 time, const Functor& callback)
	{
		this->SetPositionCallback({});
		Interpolation::AddInterpolationToPool(new SineInterpolation((float*)&camera_position_.x, camera_position_.x, position.x, time, callback));
		Interpolation::AddInterpolationToPool(new SineInterpolation((float*)&camera_position_.y, camera_position_.y, position.y, time, callback));
	}

	void Camera::Apply()
	{

		//transform
		glm::vec3 Translate(-camera_position_.x + (screen_wh_.x / 2), -camera_position_.y + (screen_wh_.y / 2), 0.0f);
		camera_matrix_ = glm::translate(camera_ortho_matrix_, Translate);

		//scale
		glm::vec3 scale(camera_scale_, camera_scale_, 0.0f);
		camera_matrix_ = glm::scale(glm::mat4(1.0f), scale) * camera_matrix_;

		//Apply our Camera Matrix
		Graphics::GetInstance()->BindProgram(shader_program_);
		glUniformMatrix4fv(camera_location_, 1, GL_FALSE, glm::value_ptr(camera_matrix_));
	}
}
