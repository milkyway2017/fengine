#include "Player.h"
#include "Input.h"
#include "Map.h"

namespace Fengine
{

	Player::Player() :
		Object(PLAYER_OBJ),
		max_inventory_size_(10)
	{
		item_list_.resize(max_inventory_size_, nullptr);
	}

	Player::~Player()
	{
	}

	void Player::Respawn()
	{
		//move to spawn position and set to visible
		MoveObjectCenterTo(spawn_position_);
		SetVisible(true);

		//set stats back to max!
		Health = maxHealth;
		Stamina = maxStamina;
	}

	void Player::DecideDirection(Input & input)
	{
		auto key_press = input.GetPlayerMovementKeySet();
		if(key_press.empty())
		{
			StopWalking();
			return;
		}
		Point2F overall_dir(0, 0);
		for(auto i : key_press)
		{
			overall_dir += ScancodeToVector(i);
		}

		if (overall_dir.x > 0 && overall_dir.y > 0)
		{
			SetMovementState(NORTH_EAST);
		}
		else if (overall_dir.x < 0 && overall_dir.y > 0)
		{
			SetMovementState(NORTH_WEST);
		}
		else if (overall_dir.x < 0 && overall_dir.y < 0)
		{
			SetMovementState(SOUTH_WEST);
		}
		else if (overall_dir.x > 0 && overall_dir.y < 0)
		{
			SetMovementState(SOUTH_EAST);
		}
		else if (overall_dir.x == 0 && overall_dir.y > 0)
		{
			SetMovementState(NORTH);
		}
		else if (overall_dir.x == 0 && overall_dir.y < 0)
		{
			SetMovementState(SOUTH);
		}
		else if (overall_dir.x > 0 && overall_dir.y == 0)
		{
			SetMovementState(EAST);
		}
		else if (overall_dir.x < 0 && overall_dir.y == 0)
		{
			SetMovementState(WEST);
		}
		else if (!overall_dir)
		{
			if (key_press[0] == SDL_SCANCODE_A)
			{
				SetMovementState(WEST);
			}
			else if (key_press[0] == SDL_SCANCODE_D)
			{
				SetMovementState(EAST);
			}
			else if (key_press[0] == SDL_SCANCODE_S)
			{
				SetMovementState(SOUTH);
			}
			else if (key_press[0] == SDL_SCANCODE_W)
			{
				SetMovementState(NORTH);
			}
		}
	}

	Point2F Player::ScancodeToVector(SDL_Scancode code)
	{
		switch (code)
		{
		case SDL_SCANCODE_A:
			return Point2F(-1.f, 0.f);
		case SDL_SCANCODE_D:
			return Point2F(1.f, 0.f);
		case SDL_SCANCODE_W:
			return Point2F(0.f, 1.f);
		case SDL_SCANCODE_S:
			return Point2F(0.f, -1.f);
		default:
			return Point2F(0.f, 0.f);
		}
	}

	void Player::Update(int time_passed, void * Data)
	{
		//this is a player object, therefore <Data> would contain user_input
		//cast the void* to Input*
		Input* user_input = (Input*)Data;

		if(user_input != nullptr)
		{
			//logic when start walking
			DecideDirection(*user_input);

			if (user_input->GetLButtonState()==BUTTON_PRESSED)            //if left-clicked, try to attack!
			{
				//attack!
				//find the object the player is clicking on!
				auto ObjAttacked = map->GetObjectByLocation(map->GetCursorLocInMap());

				//if there is an object, determine damage and attack!
				if (ObjAttacked && ObjAttacked!=this)
				{
					//check if damage can be made
					float attackDamage =
						CanMakeDamage(ObjAttacked) ? this->Strength*baseDamage /*get actual dmg by multiplying the two*/
												   : 0;

					SendAttack(ObjAttacked, attackDamage);
				}
			}
			if (user_input->IsKeyReleased(SDL_SCANCODE_F))
			{
				if (item_selected_ < item_list_.size() && item_list_[item_selected_]!=nullptr)
				{
					item_list_[item_selected_]->Toggle();
				}
			}
			if (user_input->IsKeyPressed(SDL_SCANCODE_G))
			{
				//if can drop an item (that means the player currently has a selected item)
				if (DropItem())
				{
				}
					//if not see if can pick one up near by
				else
				{
					//find the closest item!
					auto temp = map->GetItemInRange(body_.GetCenter(), 70, 1);
					if (!temp.empty())
					{
						//if there is an item!
						//add it
						PickUpItem(temp.front(), item_selected_);
					}
				}
			}
		}
		//update stats
		UpdateStats(time_passed);

		//Update the object
		Object::Update(time_passed, nullptr);
	}
	void Player::Init(Map * map, Point2F position, GLuint shader_program, int id)
	{
		//calculate SpriteRect and collision_rect_ using ObjDef properties & Position given
		//set Object::sprite_rect_
		sprite_rect_ = def_SpriteRect;
		sprite_rect_.SetCenter(position);				//set the center to Position
		Rect4F collision_rect = sprite_rect_ + def_SpriteToBody;

		body_.motion_.GetSpeed()->AddSpeedComponent("default", def_Speed);

		//initialize body animation, hardcoded animation refresh rate
		InitAnimation(50);

		//set random facing direction
		SetMovementState(RANDOM_IDLE_DIRECTION, IDLE);

		//initialize base class
		Object::Init_Obj(map,
			collision_rect,
			shader_program,
			id);

		//set stats
		Health = def_Health;
		Stamina = def_Stamina;
		Agility = def_Agility;
		Strength = def_Strength;
		Stealth = def_Stealth;

		maxHealth = def_Health;
		maxStamina = def_Stamina;

		HealthRegen = def_HealthRegen;
		StaminaRegen = def_StaminaRegen;

		//record the last spawn position
		spawn_position_ = position;
	}

	void Player::UpdateStats(int timeElapsed)
	{
		if (IsAlive())
		{
			if (Health < maxHealth)
			{
				Health += HealthRegen * timeElapsed;
			}
			else if (Health > maxHealth)
			{
				Health = maxHealth;
			}
			if (Stamina < maxStamina)
			{
				Stamina += StaminaRegen * timeElapsed;
			}
			else if (Stamina > maxStamina)
			{
				Stamina = maxStamina;
			}
		}
	}

	bool Player::CanMakeDamage(Object* other)
	{
		//find the distance between 2 objects, if they are with in 100 px from each other, return true
		Point2F playerPos = GetCenter();
		Point2F enemyPos = other->GetCenter();

		return Dist(playerPos, enemyPos) < 100;
	}

	void Player::ReceiveAttack(int Damage)
	{
		Health -= Damage;

		if(Health <= 0)
		{
			//Die
			Die();

			//wait 3 seconds to respawn, this can be done using Scheduler
			scheduled_timers_.push_back(Timer::AddTimer(3000, Functor(this, &Player::Respawn), nullptr));
		}
	}

	void Player::Die()
	{
		//reset relevant stats to 0
		//unlike in other classes, Player must reset stats because it is never really deleted by Map
		Health = 0;
		Stamina = 0;

		//set self to invisible
		SetVisible(false);

		//call base Die()
		Object::Die();
	}

	Obj_ptr Player::Clone(Point2F Location)
	{
		Player* newObj = new Player();
		newObj->Init(map, Location, shader_program_, id_);
		return Obj_ptr(newObj);
	}

	void Player::InitAnimation(int TimeToUpdate)
	{	
		//initialize body animation
		body_animation_.Init("slime.png", TimeToUpdate, &sprite_rect_);

		//add actions to the animation
		body_animation_.AddAction(IDLE + SOUTH.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + SOUTH.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + WEST.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + WEST.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + EAST.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + EAST.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + NORTH.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + NORTH.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + SOUTH_WEST.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + SOUTH_WEST.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + SOUTH_EAST.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + SOUTH_EAST.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + NORTH_WEST.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + NORTH_WEST.GetDegree(), 32, 0, 32, 32, 9);

		body_animation_.AddAction(IDLE + NORTH_EAST.GetDegree(), 0, 0, 32, 32, 1);
		body_animation_.AddAction(WALKING + NORTH_EAST.GetDegree(), 32, 0, 32, 32, 9);
	}
	void Player::LoadDef(const std::string &DefPath)
	{
		if (!def_Loaded)
		{
			using json = nlohmann::json;
			//loads the ObjDef file
			std::ifstream defFile(DefPath);

			//find the Def ObjectGroup
			json Def;
			defFile>>Def;
			Def = Def["layers"];

			bool defFound = false;
			for (auto i : Def)
			{//loop through and find the layer with the name "Def"
				if (i["name"] == "Def")
				{
					Def = i;
					defFound = true;
					break;
				}
			}
			if (!defFound)
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Player Def Loading", "failed to find def objectgroup" }, false);
			}

			json defProperties = Def["properties"];

			//check if the ObjType property is actually PLAYER
			if (defProperties["ObjType"] != "PLAYER")
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Player Def Loading", "incorrect ObjType" }, false);
			}

			//get SpriteRect("Sprite" in map editor) and collisionrect("Body" in map editor)
			Rect4F Body;
			for (auto i : Def["objects"])
			{
				if (i["name"] == "Body")
				{
					Body.x = i["x"];
					Body.y = i["y"];
					Body.h = i["height"];
					Body.w = i["width"];
				}
				else
				{
					def_SpriteRect.x = i["x"];
					def_SpriteRect.y = i["y"];
					def_SpriteRect.w = i["width"];
					def_SpriteRect.h = i["height"];
				}
			}
			//check if the body and sprite are actually loaded
			if (Body.IsDefault() || def_SpriteRect.IsDefault())
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Player Def Loading", "missing body and sprite" }, false);
			}
			//find out def_SpriteToBody
			def_SpriteToBody.x = Body.x - def_SpriteRect.x;
			def_SpriteToBody.y = Body.y - def_SpriteRect.y;
			def_SpriteToBody.w = Body.w - def_SpriteRect.w;
			def_SpriteToBody.h = Body.h - def_SpriteRect.h;

			try
			{
				//load stats and skill points
				def_Health = defProperties["STAT_Health"];
				def_Stamina = defProperties["STAT_Stamina"];
				def_Agility = defProperties["SKILL_POINT_Agility"];;
				def_Stealth = defProperties["SKILL_POINT_Stealth"];
				def_Strength = defProperties["SKILL_POINT_Strength"];
				def_StaminaRegen = (float)defProperties["STAT_StaminaRegen"]/1000.f;
				def_HealthRegen = (float)defProperties["STAT_HealthRegen"]/1000.f;
				def_Speed = (float)defProperties["STAT_Speed"]/1000.f;
			}
			catch (const std::exception &e)
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Player Def Loading", "failed to load stats/skillpoints", e.what() }, false);
			}
			def_Loaded = true;
		}
	}

	bool Player::IsPlayer(Object* obj)
	{
		//this can be done using C++'s dynamic_cast
		if (dynamic_cast<Player*>(obj))
		{
			return true;
		}
		return false;
	}

	bool Player::IsAlive() const
	{
		return Health != 0;
	}

	Rect4F Player::def_SpriteToBody = Rect4F();
	Rect4F Player::def_SpriteRect = Rect4F();
	bool Player::def_Loaded = false;
	float Player::def_Health = 0;
	float Player::def_Stamina = 0;
	float Player::def_StaminaRegen = 0;
	float Player::def_Agility = 0;
	float Player::def_Stealth = 0;
	float Player::def_Strength = 0;
	float Player::def_HealthRegen = 0;
	float Player::def_Speed = 0;

	void Player::Respawn(void* v)
	{
		Respawn();
		//also remove the timer in scheduled_timers
		Functor f(this, &Player::Respawn);
		for (auto i = scheduled_timers_.begin(); i!= scheduled_timers_.end();)
		{
			if ((*i)->f_ == f)
			{
				scheduled_timers_.erase(i);
				break;

			}
			i++;
		}
	}

	int Player::GetHealth()
	{
		return Health;
	}

	int Player::GetMaxHealth()
	{
		return maxHealth;
	}

	int Player::GetStamina()
	{
		return Stamina;
	}

	int Player::GetMaxStamina()
	{
		return maxStamina;
	}
}
