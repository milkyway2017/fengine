#include "Utility.h"

namespace Fengine
{

	template<typename Out>
	void split(const std::string & s, char delim, Out result) {
		std::stringstream ss;
		ss.str(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			if (!item.empty())
				*(result++) = item;
		}
	}
	int RectCollide(const Rect4F& obj1, const Rect4F& obj2)
	{
		int result_in_x = 0, result_in_y = 0, result = 0;
		if (
			/*if colliding on x axis*/
			((obj1.x < obj2.x + obj2.w) &&
			(obj1.x + obj1.w > obj2.x))
			&&
			/*if also colliding on y axis*/
			((obj1.y < obj2.y + obj2.h) &&
			(obj1.y + obj1.h > obj2.y))
			)
			//it is colliding
		{
			result_in_x = CollideOnX(obj1, obj2);

			result_in_y = CollideOnY(obj1, obj2);

			if (result_in_x == 3 && result_in_y == 3)	//one is inside another
			{
				return 5;
			}

			if (result_in_x == 2 && result_in_y == 2)	//obj2 in at the top left direction of obj1
			{
				return 6;
			}

			if (result_in_x == 2 && result_in_y == 1)	//obj2 in at the bottom left direction of obj1
			{
				return 7;
			}

			if (result_in_x == 1 && result_in_y == 2)	//obj2 in at the top right direction of obj1
			{
				return 8;
			}

			if (result_in_x == 1 && result_in_y == 1)		//ob2 is in the bottom right direction of obj1
			{
				return 9;
			}

			if (result_in_x == 3 && result_in_y == 2)		//obj2 is at the top of obj1
			{
				return 1;
			}

			if (result_in_x == 3 && result_in_y == 1)			//obj2 is at the bottom of obj1
			{
				return 2;
			}

			if (result_in_y == 3 && result_in_x == 1)		//obj2 is at the left of obj1
			{
				return 4;
			}

			if (result_in_y == 3 && result_in_x == 2)		//obj2 is at the right of obj1
			{
				return 3;
			}
		}
		return 0;
	}

	int CollideOnX(const Rect4F & obj1, const Rect4F & obj2)
	{
		if (((obj1.x >= obj2.x) && (obj1.x + obj1.w <= obj2.x + obj2.w)) ||/*obj1's x range is within obj2's x range*/
			((obj2.x >= obj1.x) && (obj2.x + obj2.w <= obj1.x + obj1.w)))/*obj2's x range is within obj1's x range*/
		{
			return 3;
		}
		if ((obj1.x < obj2.x) && (obj1.x + obj1.w > obj2.x) && (obj1.x + obj1.w < obj2.x + obj2.w))	//obj1 | obj2
		{
			return 1;
		}
		if ((obj2.x < obj1.x) && (obj2.x + obj2.w > obj1.x) && (obj2.x + obj2.w < obj1.x + obj1.w))	//obj2 | obj1
		{
			return 2;
		}
		return 0;		//no collision
	}

	int CollideOnY(const Rect4F & obj1, const  Rect4F & obj2)
	{
		if ((obj1.y >= obj2.y) && (obj1.y + obj1.h <= obj2.y + obj2.h) ||/*obj1's y range is within obj2's y range*/
			(obj2.y >= obj1.y) && (obj2.y + obj2.h <= obj1.y + obj1.h))/*obj2's y range is within obj1's y range*/
		{
			return 3;
		}

		if ((obj1.y < obj2.y) && (obj1.y + obj1.h > obj2.y) && (obj1.y + obj1.h < obj2.y + obj2.h))	//obj1 / obj2
		{
			return 1;
		}
		if ((obj2.y < obj1.y) && (obj2.y + obj2.h > obj1.y) && (obj2.y + obj2.h < obj1.y + obj1.h))	//obj2 / obj1
		{
			return 2;
		}
		return 0;		//no collision
	}

	Rect4F MergeRect(const Rect4F & r1, const Rect4F & r2)
	{
		//if top left line up vertically
		if (r1.x == r2.x && r1.w == r2.w)
		{
			if (r1.y + r1.h == r2.y)
			{
				return Rect4F(r1.GetXY(), r1.w, r1.h + r2.h);
			}
			if (r2.y + r2.h == r1.y)
			{
				return Rect4F(r2.GetXY(), r1.w, r1.h + r2.h);
			}
		}
		//if top left line up horizontally
		else if(r1.y == r2.y && r1.h == r2.h)
		{
			if(r1.x + r1.w == r2.x)
			{
				return Rect4F(r1.GetXY(), r1.w + r2.w, r1.h);
			}
			if(r2.x + r2.w == r1.x)
			{
				return Rect4F(r2.GetXY(), r1.w + r2.w, r1.h);
			}
		}
		return Rect4F();
	}

	bool operator<(const Point2F & t, const Point2F & other)
	{
		return (t.x < other.x);
	}

	float GetTop(const Rect4F & r)
	{
		return r.y;
	}

	float GetLeft(const Rect4F & r)
	{
		return r.x;
	}

	float GetBottom(const Rect4F & r)
	{
		return r.y + r.h;
	}

	float GetRight(const Rect4F & r)
	{
		return r.x + r.w;
	}

	bool operator<(const Line & t, const Line & other)
	{
		return FindAngle(t.P1 - t.P2) < FindAngle(other.P1 - other.P2);
	}

	GLShape GenShape(const Rect4F &rect, bool GenNewVAO)
	{
		GLShape Rect;
		if (GenNewVAO)
		{
			glGenVertexArrays(1, &Rect.vao_id);
		}
		Rect.vertices.reserve(6);		//6 vertices are needed for a rectangle

		Rect.vertices.push_back(GLVertex(Point2F(rect.x, rect.y)));
		Rect.vertices.push_back(GLVertex(Point2F(rect.x, rect.y + rect.h)));
		Rect.vertices.push_back(GLVertex(Point2F(rect.x + rect.w, rect.y)));			//first triangle

		Rect.vertices.push_back(GLVertex(Point2F(rect.x, rect.y + rect.h)));
		Rect.vertices.push_back(GLVertex(Point2F(rect.x + rect.w, rect.y)));
		Rect.vertices.push_back(GLVertex(Point2F(rect.x + rect.w, rect.y + rect.h)));	//second triangle
		if (GenNewVAO)
		{																				//generate buffer and buffer data
			Rect.vbo_id = 0;		//VBO
			glGenBuffers(1, &Rect.vbo_id);
			glBindBuffer(GL_ARRAY_BUFFER, Rect.vbo_id);

			glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(GLVertex), &Rect.vertices[0], GL_STREAM_DRAW);//buff in data
			glBindVertexArray(Rect.vao_id);

			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::position));
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::color));
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::texture_coordinate));//tell OpenGL how to read our ST buffer	

			glEnableVertexAttribArray(VAO_POSITION);	//enable the first buffer in array for vertex position
			glEnableVertexAttribArray(VAO_COLOR);	//enable the second buffer in array for vertex color
			glEnableVertexAttribArray(VAO_TEXTURE_COORD);	//enable the third buffer in array for vertex texture coordinates
		}
		return Rect;
	}

	GLShape GenShape(const Line & line, bool new_vao)
	{
		GLShape Line;
		if (new_vao)
		{
			glGenVertexArrays(1, &Line.vao_id);
		}
		Line.vertices.reserve(6);		//6 vertices are needed for a rectangle

		Line.vertices.push_back(GLVertex(line.P1));
		Line.vertices.push_back(GLVertex(line.P2));

		if (new_vao)
		{																				//generate buffer and buffer data
			Line.vbo_id = 0;		//VBO
			glGenBuffers(1, &Line.vbo_id);
			glBindBuffer(GL_ARRAY_BUFFER, Line.vbo_id);

			glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(GLVertex), &Line.vertices[0], GL_STREAM_DRAW);//buff in data
			glBindVertexArray(Line.vao_id);

			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::position));
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::color));
			glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::texture_coordinate));//tell OpenGL how to read our ST buffer	

			glEnableVertexAttribArray(VAO_POSITION);	//enable the first buffer in array for vertex position
			glEnableVertexAttribArray(VAO_COLOR);	//enable the second buffer in array for vertex color
			glEnableVertexAttribArray(VAO_TEXTURE_COORD);	//enable the third buffer in array for vertex texture coordinates
		}
		return Line;
	}

	void SubRect(GLShape & Target, const Rect4F & Rect)
	{
		if (Target.vertices.size() < 6)
			Target.vertices.resize(6);
		Target.vertices[0].position = Point2F(Rect.x, Rect.y);
		Target.vertices[1].position = Point2F(Rect.x, Rect.y + Rect.h);
		Target.vertices[2].position = Point2F(Rect.x + Rect.w, Rect.y);
		Target.vertices[3].position = Point2F(Rect.x, Rect.y + Rect.h);
		Target.vertices[4].position = Point2F(Rect.x + Rect.w, Rect.y);
		Target.vertices[5].position = Point2F(Rect.x + Rect.w, Rect.y + Rect.h);
	}

	void SubColor(GLShape & Target, const GLColor& color)
	{
		for (auto &i : Target.vertices)
		{
			i.color = color;
		}
	}

	void SubRectTexCoord(GLShape & Target, const SDL_Rect& textureRect, int width, int height, int offset)
	{
		Rect4F TextureRect(textureRect.x + 0.1f, textureRect.y + 0.1f, textureRect.w - 0.2f, textureRect.h - 0.2f);
		if (Target.vertices.size() < 6)
			Target.vertices.resize(6);
		Target.vertices[offset + 0].texture_coordinate = Point2F(TextureRect.x / width, 1 - (TextureRect.y / height));
		Target.vertices[offset + 1].texture_coordinate = Point2F(TextureRect.x / width, 1 - ((TextureRect.y + TextureRect.h) / height));
		Target.vertices[offset + 2].texture_coordinate = Point2F((TextureRect.x + TextureRect.w) / width, 1 - (TextureRect.y / height));
		Target.vertices[offset + 3].texture_coordinate = Point2F(TextureRect.x / width, 1 - ((TextureRect.y + TextureRect.h) / height));
		Target.vertices[offset + 4].texture_coordinate = Point2F((TextureRect.x + TextureRect.w) / width, 1 - (TextureRect.y / height));
		Target.vertices[offset + 5].texture_coordinate = Point2F((TextureRect.x + TextureRect.w) / width, 1 - ((TextureRect.y + TextureRect.h) / height));

	}

	void SubRectTexCoord(GLShape & Target, int offset)
	{
		Target.vertices[offset + 0].texture_coordinate = Point2F(.0f, 1.0f);
		Target.vertices[offset + 1].texture_coordinate = Point2F(.0f, .0f);
		Target.vertices[offset + 2].texture_coordinate = Point2F(1.0f, 1.0f);
		Target.vertices[offset + 3].texture_coordinate = Point2F(.0f, .0f);
		Target.vertices[offset + 4].texture_coordinate = Point2F(1.0f, 1.0f);
		Target.vertices[offset + 5].texture_coordinate = Point2F(1.0f, .0f);
	}

	Rect4F GenRectFromShape(const GLShape &Target)
	{
		Point2F XY;
		float W, H;
		XY = Target.vertices[0].position;
		W = Target.vertices[2].position.x - Target.vertices[0].position.x;
		H = Target.vertices[1].position.y - Target.vertices[0].position.y;

		return Rect4F(XY, W, H);
	}

	void RegisterShape(GLShape & Target)
	{
		//VBO											
		Target.vbo_id = 0;
		glGenBuffers(1, &Target.vbo_id);
		glBindBuffer(GL_ARRAY_BUFFER, Target.vbo_id);

		//VAO
		glGenVertexArrays(1, &Target.vao_id);
		glBindVertexArray(Target.vao_id);

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::position));
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::color));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, GLVertex::texture_coordinate));//tell OpenGL how to read our ST buffer	

		glEnableVertexAttribArray(VAO_POSITION);	//enable the first buffer in array for vertex position
		glEnableVertexAttribArray(VAO_COLOR);	//enable the second buffer in array for vertex color
		glEnableVertexAttribArray(VAO_TEXTURE_COORD);	//enable the third buffer in array for vertex texture coordinates
	}

	void UnregisterShape(GLShape & Target)
	{
		glDeleteBuffers(1, &Target.vbo_id);
		glDeleteVertexArrays(1, &Target.vao_id);
	}

	void ResizeShapeBuffer(GLShape & target, unsigned int vertex_anticipated)
	{
		if (vertex_anticipated == 0 && !target.vertices.empty())
		{
			//expand the buffer
			glBufferData(GL_ARRAY_BUFFER, target.vertices.size() * sizeof(GLVertex), &target.vertices[0], GL_STREAM_DRAW);//buff in data
		}
		else if(vertex_anticipated)
		{
			target.vertices.resize(vertex_anticipated);
			glBufferData(GL_ARRAY_BUFFER, vertex_anticipated * sizeof(GLVertex), &target.vertices[0], GL_STREAM_DRAW);//buff in data
		}
	}

	template<typename T>
	inline T Det(T a, T b, T c, T d)
	{

		return a * d - b * c;

	}

	float GetSlope(const Line & a)
	{
		return (a.P2.y - a.P1.y) / (a.P2.x - a.P1.x);
	}

	Point2F * DoLinesIntersect(Line l1, Line l2)
	{
		//By Andrew Feng, 2018-01-30

		//check if share points
		auto p = l1 ^ l2;
		if (p)
		{
			return new Point2F(p->x, p->y);
		}

		//By Wolfram Mathworld
		///Calculate intersection of two lines.

		///\return true if found, false if not found or error

			//http://mathworld.wolfram.com/Line-LineIntersection.html
		double detL1 = Det(l1.P1.x, l1.P1.y, l1.P2.x, l1.P2.y);
		double detL2 = Det(l2.P1.x, l2.P1.y, l2.P2.x, l2.P2.y);
		double x1mx2 = l1.P1.x - l1.P2.x;
		double x3mx4 = l2.P1.x - l2.P2.x;
		double y1my2 = l1.P1.y - l1.P2.y;
		double y3my4 = l2.P1.y - l2.P2.y;

		double xnom = Det(detL1, x1mx2, detL2, x3mx4);
		double ynom = Det(detL1, y1my2, detL2, y3my4);
		double denom = Det(x1mx2, y1my2, x3mx4, y3my4);

		if (denom == 0.0)//Lines are colinear or parallel
		{
			return nullptr;
		}

		Point2F * result = new Point2F;

		result->x = (float)(xnom / denom);
		result->y = (float)(ynom / denom);

		if (!std::isfinite(result->x) || !std::isfinite(result->y)) //Probably a numerical issue
			return nullptr;

		return result; //All OK

	}

	Point2F * DoSegmentsIntersect(const Line & l1, const Line & l2)
	{
		using namespace std;
		Point2F* LineIntersection = DoLinesIntersect(l1, l2);
		if (LineIntersection == nullptr)
		{
			return nullptr;
		}
		if (OnSegment(*LineIntersection, l1) && OnSegment(*LineIntersection, l2))
		{
			return LineIntersection;
		}
		delete LineIntersection;
		return nullptr;
	}

	Point2F* DoSegmentandRayIntersect(Line l1, Line ray)
	{
		Point2F* LineIntersection = DoLinesIntersect(l1, ray);
		if (LineIntersection == nullptr)
		{
			return nullptr;
		}
		else
		{
			if (OnSegment(*LineIntersection, l1) && OnRay(*LineIntersection, ray))
			{
				return LineIntersection;
			}
			else
			{
				delete LineIntersection;
				return nullptr;
			}
		}
	}

	float Floor1Digits(float n)
	{
		return floorf(n * 10) / 10;
	}

	float Floor3Digits(float n)
	{
		return floorf(n * 1000) / 1000;
	}

	bool IsVertical(Line l)
	{
		return (l.P1.x == l.P2.x);
	}

	bool IsHorizontal(Line l)
	{
		return (l.P1.y == l.P2.y);
	}

	bool OnSegment(Point2F p, Line l)
	{
		using namespace std;
		//round to closest tenth to avoid error
		if (IsHorizontal(l))
		{//if horizontal test x normally and test y approximately
			return (Floor3Digits(p.x) >= Floor3Digits(min(l.P1.x, l.P2.x))) && (Floor3Digits(p.x) <= Floor3Digits(max(l.P1.x, l.P2.x))) && (abs(l.P1.y - p.y) < 0.2f);
		}
		else if (IsVertical(l))
		{//if horizontal test y normally and test x approximately
			return (abs(l.P1.x - p.x) < 0.2f) && (Floor3Digits(p.y) >= Floor3Digits(min(l.P1.y, l.P2.y))) && (Floor3Digits(p.y) <= Floor3Digits(max(l.P1.y, l.P2.y)));
		}
		else
		{
			return (Floor3Digits(p.x) >= Floor3Digits(min(l.P1.x, l.P2.x))) && (Floor3Digits(p.x) <= Floor3Digits(max(l.P1.x, l.P2.x))) && (Floor3Digits(p.y) >= Floor3Digits(min(l.P1.y, l.P2.y))) && (Floor3Digits(p.y) <= Floor3Digits(max(l.P1.y, l.P2.y)));
		}
	}

	template <typename valueType>
	bool SameSign(valueType x, valueType y)
	{
		return (x >= 0) ^ (y < 0);
	}

	bool OnRay(Point2F p, Line l)
	{
		using namespace std;
		return SameSign((l.P2 - l.P1).x, p.x - l.P1.x) && SameSign((l.P2 - l.P1).y, p.y - l.P1.y);
	}

	std::vector<Point2F> FindVerticesandIntersections(std::vector<Line> segments)
	{
		std::vector<Line> Segments = segments;
		std::vector<Point2F> ReturnList;

		for (auto i = Segments.begin(); i != Segments.end();)
		{
			if (std::find(ReturnList.begin(), ReturnList.end(), i->P1) == ReturnList.end())
			{//add only if the Point does not already exist
				ReturnList.push_back(i->P1);
			}
			if (std::find(ReturnList.begin(), ReturnList.end(), i->P2) == ReturnList.end())
			{//add only if the Point does not already exist
				ReturnList.push_back(i->P2);
			}
			for (unsigned int k = 0; k < Segments.size(); k++)
			{
				if (!(*i^Segments[k]))
				{
					Point2F* Intersection = DoSegmentsIntersect(*i, Segments[k]);
					if (Intersection != nullptr)
					{
						if (std::find(ReturnList.begin(), ReturnList.end(), *Intersection) == ReturnList.end())
						{//add only if the Point does not already exist
							ReturnList.push_back(*Intersection);
						}
						//delete the pointer
						delete Intersection;
					}
				}
			}
			//delete the element because all of its intersection have been stored
			i = Segments.erase(i);
		}

		return ReturnList;
	}

	bool DoSegmentandRectIntersect(const Rect4F& Shape, const Line& s)
	{
		int x1 = (int) s.P1.x, x2 = (int)s.P2.x, y1 = (int)s.P1.y, y2 = (int)s.P2.y;

		SDL_Rect temprect = (SDL_Rect)Shape;
		return SDL_IntersectRectAndLine(&temprect, &x1, &y1, &x2, &y2);
	}

	std::vector<std::string> GetEnclosedSubStrings(const std::string & s, const std::string & delimpair, unsigned int count)
	{
		std::vector<std::string> return_list;

		if (delimpair.size() != 2)
		{
			return std::vector<std::string>();
		}
		size_t first_pos_ = s.find(delimpair[0]);
		if ((first_pos_ == std::string::npos) || (s.find(delimpair[1], first_pos_ + 1) == std::string::npos))
		{
			return std::vector<std::string>();
		}

		unsigned long begin = 0;
		while (true)
		{
			if(count != 0 && return_list.size() == count)
			{
				break;
			}
			if (s.find(delimpair[0], begin) == std::string::npos)
			{
				break;
			}
			if (begin == s.size())				//break out if it is at the end
			{
				break;
			}
			size_t first_pos = s.find(delimpair[0], begin);
			return_list.push_back(s.substr(first_pos + 1, s.find(delimpair[1], first_pos + 1) - first_pos - 1));		//pushback one string
			begin = s.find(delimpair[1], begin) + 1;		//increase begin 
		}
		return return_list;
	}

	std::string GetSubStrBeforeChar(const std::string & s, char c)
	{
		return s.substr(0, s.find(c));
	}

	std::vector<Point2F> FindABIntersections(const std::vector<Line>& A, const std::vector<Line>& B)
	{
		std::vector<Point2F> ReturnList;

		for (unsigned int i = 0; i < B.size(); i++)
		{
			for (unsigned int k = 0; k < A.size(); k++)
			{
				Point2F* Intersection = DoSegmentsIntersect(B[i], A[k]);
				if (Intersection != nullptr)
				{
					if (std::find(ReturnList.begin(), ReturnList.end(), *Intersection) == ReturnList.end())
					{//add only if the Point does not already exist
						ReturnList.push_back(*Intersection);
					}
					//delete the pointer
					delete Intersection;
				}
			}
		}

		return ReturnList;
	}

	void VirticesInRange(float r, Point2F loc, const std::vector<Point2F>& Points, std::vector<Point2F>& PointsInRange)
	{
		PointsInRange.clear();
		for (auto i : Points)
		{
			if (Dist(i, loc) <= r)
			{
				PointsInRange.push_back(i);
			}
		}
	}

	std::vector<Point2F> VerticesInRect(Rect4F rect, std::vector<Point2F>& Points)
	{
		std::vector<Point2F> ReturnList;

		for (unsigned int i = 0; i < Points.size(); i++)
		{
			if (PointInRect(&Points[i], &rect))
			{
				ReturnList.push_back(Points[i]);
			}
		}

		return ReturnList;
	}

	void FindDirectVertices(Point2F loc, const std::vector<Line> lines, std::vector<Point2F>& DirectPoints)
	{
		std::vector<Point2F> InputPoints = DirectPoints;
		DirectPoints.clear();

		for (auto InputPoint : InputPoints)
		{
			Line Temp = { loc, InputPoint};
			bool success = true;
			for (const auto& line : lines)
			{
				if (Temp^ line)	//if they are connected then continue because it does not count as intersecting in this case
				{
					continue;
				}
				auto intersection = DoSegmentsIntersect(Temp, line);
				if (intersection != nullptr)
				{
					success = false;
					delete intersection;
					break;
				}
			}
			if (success)
			{
				DirectPoints.push_back(InputPoint);
			}
		}
	}

	std::vector<std::pair<Angle, Angle>> GenerateAngleRangeList(std::vector<Line> segments, Point2F p)
	{
		std::vector<std::pair<Angle, Angle>> ReturnList;
		for (auto &i : segments)
		{
			ReturnList.push_back(
				std::pair<Angle, Angle>(
					FindAngle(i.P1 - p)
					, FindAngle(i.P2 - p)
					)
			);
		}
		return ReturnList;
	}


	float Dist(Point2F p1, Point2F p2)
	{
		using namespace std;
		return sqrtf(powf(p1.x - p2.x, 2.0f) + powf(p1.y - p2.y, 2.f));
	}

	bool ComparePoints(Point2F a, Point2F b)
	{
		return FindAngle(a) < FindAngle(b);
	}

	void ExtendRayInSys(Line & l, std::vector<Line>& sys, std::vector<std::pair<Angle, Angle>> AngleRangeList)
	{
		//store the direction of l so that it doesn't need to be calculated twice
		Angle ray_angle = FindAngle(l.P2 - l.P1);
		std::vector<Point2F> intersection_points;
		for (unsigned int i = 0; i < sys.size(); i++)
		{
			if (sys[i] ^ l)
			{	
				//if they share points then that must be an intersection
				//add it to the list
				intersection_points.push_back(l.P2);
				continue;
			}
			if (IsWithin(ray_angle, AngleRangeList[i].first, AngleRangeList[i].second))
				//only executed if the angle range of the line segment contains the ray <l>'s angle
			{
				auto intersection_point = DoSegmentandRayIntersect(sys[i], l);
				if (intersection_point != nullptr)
				{
					intersection_points.push_back(*intersection_point);
					delete intersection_point;
				}
			}
			else
			{
				continue;
			}
		}

		//todo: optimize
		if (!intersection_points.empty())
		{
			//find the closest point and return
			int ClosestPoint = 0;				//stores the index of the closest point
			float ShortestDist = 1000.f;

			for (unsigned int i = 0; i < intersection_points.size(); i++)
			{
				auto t = Dist(l.P1, intersection_points[i]);
				if (t < ShortestDist)
				{
					ShortestDist = t;
					ClosestPoint = i;
				}
			}

			//extension
			l.P2 = intersection_points.at(ClosestPoint);
		}
	}

	Angle FindAngle(Point2F v, bool inner)
	{
		auto return_val = Angle(std::atan2(v.x,v.y));
		if(inner)
			return_val = Angle(2*(float)pi) - return_val;
		return return_val;
	}

	std::vector<std::string> SplitStringByDelim(const std::string &s, char delim)
	{
		std::vector<std::string> elems;
		split(s, delim, std::back_inserter(elems));
		return elems;
	}

	bool FileExist(const std::string & file_path)
	{
		struct stat buffer;
		return (stat(file_path.c_str(), &buffer) == 0);

	}

	std::string ReadJson(nlohmann::json & json_object, const std::string file_path)
	{
		//read the .json GameWorld file
		std::ifstream file(file_path);

		try
		{
			file >> json_object;
		}
		catch (const std::exception & e)
		{
			return e.what();
		}
		return "";
	}

	Rect4F operator+(const Rect4F &t, const Rect4F &other)
	{
		return Rect4F(t.x+other.x,t.y+other.y,t.w+other.w,t.h+other.h);
	}

	Rect4F operator-(const Fengine::Rect4F t, const Fengine::Rect4F &other)
	{
		return Rect4F(t.x-other.x,t.y-other.y,t.w-other.w,t.h-other.h);
	}

	Rect4F operator+(const Rect4F rect, const Point2F& move)
	{
		return Rect4F(rect.GetXY()+move,rect.w,rect.h);
	}
	std::string GetSubStrAfterChar(const std::string &str, char c)
	{
		unsigned long pos = str.find(c);
		return str.substr(pos + 1, str.length()-pos);
	}
	std::string ToUpperString(const std::string &str)
	{
		std::string return_str = str;
		const std::locale loc;
		for (auto & c: return_str) c = std::toupper(c, loc);
		return return_str;
	}
	std::string ToLowerString(const std::string &str)
	{
		std::string return_str = str;
		const std::locale loc;
		for (auto & c: return_str) c = std::tolower(c, loc);
		return return_str;
	}
	std::string TrimString(const std::string &str)
	{
		//find the first non space character
		size_t starting_pos = str.find_first_not_of(" \t\n\v\f\r");
		//trim insignificant space
		size_t ending_pos = str.find_last_not_of(" \t\n\v\f\r");
		if(starting_pos == str.npos || ending_pos == str.npos)
		{
			return "";			//return empty string
		}
		else
		{
			return 	str.substr(starting_pos,ending_pos - starting_pos + 1);
		}
	}

	std::vector<Line> GenLines(const GLShape& shape, bool is_rect, bool shape_enclosed)
	{
		std::vector<Line> ReturnList;
		if (is_rect)
		{
			for (unsigned int i = 0; i < shape.vertices.size(); i += 6)
			{
				ReturnList.emplace_back(shape.vertices[i + 0].position, shape.vertices[i + 1].position);
				ReturnList.emplace_back(shape.vertices[i + 0].position, shape.vertices[i + 2].position);
				ReturnList.emplace_back(shape.vertices[i + 5].position, shape.vertices[i + 3].position);
				ReturnList.emplace_back(shape.vertices[i + 5].position, shape.vertices[i + 4].position);
			}
		}
		else
		{
			for (unsigned int i = 0; i < shape.vertices.size() - 1; i++)
			{
				ReturnList.emplace_back(shape.vertices[i].position, shape.vertices[i + 1].position);
			}
			if (shape_enclosed)
				ReturnList.emplace_back(shape.vertices.back().position, shape.vertices.front().position);
		}
		return ReturnList;
	}

	bool PointInRect(const Point2F *p, const Rect4F *r)
	{
		return ((p->x >= r->x) && (p->x < (r->x + r->w)) &&
			(p->y >= r->y) && (p->y < (r->y + r->h))) ? SDL_TRUE : SDL_FALSE;
	}

	bool FindAndReplaceString(std::string &string, const std::string &find, const std::string &replace)
	{
		bool return_val = false;

		size_t pos = 0;
		size_t find_size = find.length();
		while((pos = string.find(find, pos)) != string.npos)
		{
			string.replace(pos, find_size, replace);
		}

		return return_val;
	}
}