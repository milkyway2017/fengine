#include "Command.h"
#include "Utility.h"

namespace Fengine
{

	Command::Command(const std::string & command_string)
	{
		try
		{
			//get the command_level_
			command_level_ = (CommandLevel)CommandLevelToEnum(GetEnclosedSubStrings(command_string, "[]")[0]);

			//if command_level_ is NO_LEVEL, this command is not valid, throw
			switch (command_level_)
			{
			case NO_LEVEL:
				//todo:get rid of this throw
				throw std::runtime_error("invalid command level");
			case PROGRAM:
				command_type_ = StringToEnum(GetEnclosedSubStrings(command_string, "<>")[0], ProgramCommandTypeMap);
				break;
			case FRAME:
				command_type_ = StringToEnum(GetEnclosedSubStrings(command_string, "<>")[0], FrameCommandTypeMap);
				break;
			case LEVEL:
				command_type_ = StringToEnum(GetEnclosedSubStrings(command_string, "<>")[0], LevelCommandTypeMap);
				break;
			case GAMEWORLD:
				command_type_ = StringToEnum(GetEnclosedSubStrings(command_string, "<>")[0], GameWorldCommandTypeMap);
				break;
			case MAP:
				command_type_ = StringToEnum(GetEnclosedSubStrings(command_string, "<>")[0], MapCommandTypeMap);
				break;
			case MAP_EVENTMANAGER:
				command_type_ = StringToEnum(GetEnclosedSubStrings(command_string, "<>")[0], MapEventManagerEventTypeMap);
				break;
			}

			commander_ = "Unknown";
			resolved_ = false;
			irresolvable_ = false;
			in_heap_ = false;

			param_string_list_ = GetEnclosedSubStrings(command_string, "()");
		}
		catch (const std::exception & e)
		{
			//todo:get rid of this throw
			throw std::runtime_error("invalid command: error parsing command\n" + std::string(e.what()));
		}
	}

	Command::Command(CommandLevel command_level, int command_type, const std::string & command, std::vector<void*> param_list, std::string param_string) :
		command_level_(command_level),
		command_type_(command_type),
		commander_(command),
		param_list_(param_list),
		resolved_(0),
		irresolvable_(0),
		in_heap_(0)
	{
		param_string_list_ = GetEnclosedSubStrings(param_string, "()");
	}

	Command::Command(CommandLevel command_level, int command_type, const std::string & commander, void* target) :
		command_level_(command_level),
		command_type_(command_type),
		commander_(commander),
		target_(target),
		resolved_(0),
		irresolvable_(0),
		in_heap_(0)
	{
	}

	Command::Command(CommandLevel command_level, int command_type, const std::string & commander, const char* param_string) :
		command_level_(command_level),
		command_type_(command_type),
		commander_(commander),
		resolved_(0),
		irresolvable_(0),
		in_heap_(0)
	{
		param_string_list_ = GetEnclosedSubStrings(param_string,"()");
	}

	void Command::Resolve()
	{
		resolved_ = 1;
		//see if this Command is InHeap
		if (in_heap_)
		{
			delete this;
		}
	}
	void Command::CannotResolve()
	{
		irresolvable_ = 1;
		if (in_heap_)
		{
			delete this;
		}
	}
	void Command::CannotResolve(Logger * log)
	{
		log->log_ += "[font='DejaVuSans-12'][colour='FFFF0000']invalid command";
		log->log_ += '\n';
		CannotResolve();
	}

	std::string Command::ToString(bool with_commander)	const
	{
		std::string command_type;
		std::string command_level;
		switch (command_level_)
		{
		default:
			command_level = "[NO_LEVEL]";
			command_type = "<NO_COMMAND>";
			break;
		case NO_LEVEL:
			command_level = "[NO_LEVEL]";
			command_type = "<NO_COMMAND>";
			break;
		case PROGRAM:
			command_level = "[PROGRAM]";
			command_type = EnumToString(command_type_, ProgramCommandTypeMap);
			break;
		case FRAME:
			command_level = "[FRAME]";
			command_type = EnumToString(command_type_, FrameCommandTypeMap);
			break;
		case LEVEL:
			command_level = "[LEVEL]";
			command_type = EnumToString(command_type_, LevelCommandTypeMap);
			break;
		case GAMEWORLD:
			command_level = "[GAMEWORLD]";
			command_type = EnumToString(command_type_, GameWorldCommandTypeMap);
			break;
		case MAP:
			command_level = "[MAP]";
			command_type = EnumToString(command_type_, MapCommandTypeMap);
			break;
		case MAP_EVENTMANAGER:
			command_level = "[MAP_EVENTMANAGER]";
			command_type = EnumToString(command_type_, MapEventManagerEventTypeMap);
			break;
		}
		std::string commander = {with_commander ? "commander: " + commander_: ""};

		std::string param_string;
		for(auto i : param_string_list_)
		{
			param_string += "(" + i + ")";
		}

		return command_level + command_type + param_string + " " + commander;
	}

	Command::Command():
		command_level_(NO_LEVEL),
		command_type_(0),
		commander_(""),
		resolved_(false),
		irresolvable_(false),
		in_heap_(false)
	{}

	Command Command::Clone()
	{
		Command new_command;
		if (target_)
		{
			new_command = Command(command_level_,command_type_,commander_,target_);
		}
		else
		{
			new_command = Command(command_level_, command_type_, commander_, param_list_);
		}

		//add paramStringList
		new_command.param_string_list_ = this->param_string_list_;
		return new_command;
	}

	Command & Command::operator=(const Command& other)
	{
		if (other.in_heap_)			//no heap allocated command is allowed to be copied
		{
			throw("Command is not copyable");
		}
		else
		{
			this->commander_ = other.commander_;
			this->command_type_ = other.command_type_;
			this->command_level_ = other.command_level_;
			this->param_list_ = other.param_list_;
			this->param_string_list_ = other.param_string_list_;
			this->target_ = other.target_;
			this->resolved_ = 0;
			this->irresolvable_ = 0;
			this->in_heap_ = 0;
		}
		return *this;
	}

	Command::Command(const Command & other)
	{
		if (other.in_heap_)			//no heap allocated command is allowed to be copied
		{
			throw("Command is not copyable");
		}
		else
		{
			this->commander_ = other.commander_;
			this->command_type_= other.command_type_;
			this->command_level_ = other.command_level_;
			this->param_list_ = other.param_list_;
			this->param_string_list_ = other.param_string_list_;
			this->target_ = other.target_;
			this->resolved_ = 0;
			this->irresolvable_ = 0;
			this->in_heap_ = 0;
		}
	}

	Command::~Command()
	{
		if (resolved_)
		{
			CommandDumpLog::GetInstance()->logger_.AddLine("Command Resolved: CommandLevel: " + CommandLevelToString(command_level_) + ", CommandType: " + std::to_string(command_type_) + ", Commander: "+ commander_);
		}
		if (irresolvable_)
		{
			CommandDumpLog::GetInstance()->logger_.AddLine("Command Cannot Be Resolved: CommandLevel: " + CommandLevelToString(command_level_) + ", CommandType: " + std::to_string(command_type_) + ", Commander: " + commander_);
		}
	}
}