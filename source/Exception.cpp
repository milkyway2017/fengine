#include "Exception.h"
#include "Command.h"

namespace Fengine
{
	Exception::Exception(const Command & command, bool is_fatal) noexcept:
		is_fatal_(is_fatal)
	{
		message_queue_.push_back("Exception Thrown");			//first line of exception
		message_queue_.push_back("Failed Command: ");			//explain the cause
		message_queue_.push_back("Command: "+command.ToString(true));		//the command that causes

		JoinMessage();
	}

	Exception::Exception(std::initializer_list<std::string> msg, bool is_fatal) noexcept:
		is_fatal_(is_fatal)
	{
		message_queue_.push_back("Exception Thrown");			//first line of execution
		message_queue_.push_back("Error Message");
		message_queue_.insert(message_queue_.end(),msg);

		JoinMessage();
	}

	Exception::Exception(const Exception & other) noexcept:
		message_queue_(other.message_queue_),
		joined_message_(other.joined_message_),
		is_fatal_(other.is_fatal_)
	{
	}

	const char* Exception::what() const noexcept
	{
		return joined_message_.c_str();
	}

	void Exception::JoinMessage()
	{
		//join the message and return
		for(auto & message : message_queue_)
		{
			joined_message_ += message + "\n";
		}
	}
}