#include "SlidingDoor.h"
#include "Map.h"

namespace Fengine
{

	SlidingDoor::SlidingDoor(const Rect4F &collisionRect, const Point2F & ClosedLoc, const Point2F & OpenedLoc, const Point2F & WH, const std::string & imagepath, const SDL_Rect & SrcRect, int Duration, Map * map, int ActivateArea, GLuint ShaderProgram, int id) :
		Figure(collisionRect, 1, false, id, map),
		ClosedLoc(ClosedLoc),
		OpenedLoc(OpenedLoc),
		ClosedLoc_Fig(collisionRect.GetXY()),
		ActivateAreaID(ActivateArea),
		ShaderProgram(ShaderProgram),
		SrcRect(SrcRect),
		map(map),
		status(CLOSED),
		Duration(Duration),
		other(nullptr)
	{
		/*calculate openedloc_fig*/
		OpenedLoc_Fig = ClosedLoc_Fig + (OpenedLoc - ClosedLoc);
		sprite.Init(imagepath, Rect4F(ClosedLoc, WH));
		current_sprite_pos_ = Rect4F(ClosedLoc, WH);
		sprite.SetReferenceDestinationRect(&current_sprite_pos_);
		ConfigureZOrder();

		GetBody()->Attach(&current_sprite_pos_.GetXY());
		GetBody()->motion_.GetSpeed()->AddSpeedComponent("default_speed", (OpenedLoc - ClosedLoc).Norm()/(float)Duration);

		//subscribe to all activearea events
		event_manager_->SubscribeEvent(int(MapEventManagerEventType::ACTIVEAREA_EMPTIED), Functor(this, &SlidingDoor::HandleAreaEvent));
		event_manager_->SubscribeEvent(int(MapEventManagerEventType::ACTIVEAREA_FILLED), Functor(this, &SlidingDoor::HandleAreaEvent));
	}

	SlidingDoor::~SlidingDoor()
	{
	}

	void SlidingDoor::HandleAreaEvent(void* e)
	{
		Event areastatus = *static_cast<Event*>(e);
		//check if it is the correct activate area
		if (((ActiveArea*)areastatus.target_)->id_== ActivateAreaID)
		{
			DoorStatus LastFrame = status;
			bool StatusChanged = false;
			if (areastatus.command_type_ == int(MapEventManagerEventType::ACTIVEAREA_EMPTIED))
			{		//if the area is empty, close if not already closed
				status = CLOSING;
				body_.motion_.SetAcceleration((ClosedLoc - OpenedLoc) / (float)Duration);
			}
			else if (areastatus.command_type_ == int(MapEventManagerEventType::ACTIVEAREA_FILLED))
			{		//if the area is filled, open if not already opened
				status = OPENING;
				body_.motion_.SetAcceleration((OpenedLoc - ClosedLoc) / (float)Duration);
			}

		}
	}

	void SlidingDoor::Update(int TimeElapsed)
	{
		if (other.get())
			other->Update(TimeElapsed);
		switch (status)
		{
		case CLOSED:
		case OPENED:
			return;
		case CLOSING:
		case OPENING:
		{
			InfoLabel::GetInstance()->DisplayString("slid", "progress: " + std::to_string(IsComplete()));
			if (IsComplete())
			{
				body_.motion_.StopWithAcceleration();
				if (status == CLOSING)
					status = CLOSED;
				else if (status == OPENING)
					status = OPENED;
			}
		}
		}
		return;
	}

	void SlidingDoor::Draw()
	{/*
		Graphics::GetInstance()->DrawLines(shader_program_, sprite.Shape);*/
		if (isDebugging)		//if the debug mode is on, highlight the body
		{
			Draw_Fig_Debug(GLColor(Graphics::GetInstance()->GetColor("carnatIOn PINk")).ToTransparent(0.3f),
				ShaderProgram);
		}
		sprite.Draw(ShaderProgram, SrcRect);
		if (other.get())
			other->Draw();
	}

	void SlidingDoor::LoadDef(const std::string &defPath)
	{
		//since there can be more than one kind of SlidingDoor Def
		//we need to...
		//get the file name component of this path, and make it the name of this Def
		int begin = defPath.rfind('/') + 1;
		int end = defPath.rfind("Def");
		std::string defName = defPath.substr(begin, end - begin);

		//check if this def already exists, if exists return
		if (defMap.find(defName) != defMap.end())
			return;

		SlidingDoorDef defStruct;							//the structure that holds the information loaded from this ObjDef

		using json = nlohmann::json;
		std::ifstream defFile(defPath);

		//find the Def ObjectGroup
		json Def;
		defFile >> Def;
		Def = Def["layers"];

		bool defFound = false;
		for (auto i : Def)
		{//loop through and find the layer with the name "Def"
			try
			{
				using namespace std;
				string name = i["name"];
				int x = i["x"];
			}
			catch (const std::exception & e)
			{
				std::cout << e.what() << std::endl;
			}
			if (i["name"] == "Def")
			{
				Def = i;
				defFound = true;
				break;
			}
		}
		if (!defFound)
		{
			//throw
			throw Exception({ "SlidingDoor Def Loading", "failed to find def objectgroup" }, false);
		}

		try
		{
			//read properties
			json defProperties = Def["properties"];

			if (defProperties["ObjType"] != "SLIDINGDOOR")
				throw Exception({ "SlidingDoor Def Loading", "incorrect ObjType set in ObjDef" }, false);

			//set movement direction
			defStruct.isHorizontal = defProperties["Axis"] == "X";

			//set open distance
			defStruct.distance = defProperties["Distance"];

			//set open/close duration
			defStruct.duration_ms = int((float)defProperties["Duration"] * 1000.f)/*multiply by 1000 to get time in millisecond*/;

			//set texture path
			defStruct.texture_path = defProperties["Image"];

			//----------------------------------------------------------------//

			//read all the rects and format them
			std::map<std::string/*type*/, Rect4F/*rect*/> rects;
			for (auto defObject : Def["objects"])
			{
				std::string type = defObject["type"];
				Rect4F rect(defObject["x"], defObject["y"], defObject["width"], defObject["height"]);
				rects.insert(std::pair<std::string, Rect4F>(type, rect));

				//get texture coordinates
				if (type == "-")
					GetTextureCoord(defObject["properties"], &defStruct.source_rect_1);
				else if(type == "+")
					GetTextureCoord(defObject["properties"], &defStruct.source_rect_2);
			}

			//check if is single or double door
			//here '-' denotes the door that goes in negative direction in the map's coordinate system (visually left & up)
			bool firstFound = rects.find("-") != rects.end(), secondFound = rects.find("+") != rects.end();
			if (firstFound && secondFound)
			{
				//the Def contains both + and -, this is a double door
				defStruct.isDouble = true;
				defStruct.spriteRect = rects["-"];
				defStruct.delta = rects["+"].GetXY() - rects["-"].GetXY();
				defStruct.spriteToBody1 = rects["-Body"] - rects["-"];
				defStruct.spriteToBody2 = rects["+Body"] - rects["-"];
				defStruct.spriteToActiveArea = rects["ActiveArea"] - rects["-"];
			}
			else if (firstFound)
			{
				//the Def contains -, this is a single door
				defStruct.isDouble = false;
				defStruct.isNegative = true;
				defStruct.spriteRect = rects["-"];
				defStruct.spriteToBody1 = rects["-Body"] = rects["-"];
				defStruct.spriteToActiveArea = rects["ActiveArea"] - rects["-"];
			}
			else if (secondFound)
			{
				//the Def contains +, this is a single door
				defStruct.isDouble = false;
				defStruct.isNegative = false;
				defStruct.spriteRect = rects["+"];
				defStruct.spriteToBody1 = rects["+Body"] = rects["-"];
				defStruct.spriteToActiveArea = rects["ActiveArea"] - rects["+"];
			}
			else
			{
				//nothing's found, this is an invalid Def
				throw Exception({ "SlidingDoor Def Loading", "no door found in Def" }, false);
			}

			//done loading, save the data loaded
			defMap.insert(std::pair<std::string, const SlidingDoorDef>(defName, defStruct));
		}
		catch (const std::exception & e)
		{
			throw Exception({ "SlidingDoor Def Loading", "error reading json file", e.what() }, false);
		}

	}

	void SlidingDoor::GetTextureCoord(const nlohmann::json & source, Rect4F * source_rect)
	{
		source_rect->x = source["SpriteX"];
		source_rect->y = source["SpriteY"];
		source_rect->w = source["SpriteW"];
		source_rect->h = source["SpriteH"];
	}

	Body * SlidingDoor::GetOtherBody()
	{
		if (!other.get())
			return nullptr;
		return other->GetBody();
	}

	SlidingDoor::SlidingDoor(const std::string & type, Point2F location, GLuint shaderProgram, Map* map, int id)
	{
		//check if the associated def is loaded
		if (defMap.find(type) == defMap.end())
			throw Exception({ "Sliding door constructor exception", "cannot find loaded ObjDef" }, true);

		const SlidingDoorDef & def = defMap[type];
		Rect4F actualSpriteRect = def.spriteRect; actualSpriteRect.GetXY() = location - Point2F(0, def.spriteRect.h);		//the sprite rect at the desired <location> passed in
		Rect4F collisionRect = actualSpriteRect + def.spriteToBody1;

		//initialize figure
		Figure::Init(map, GenShape(collisionRect, true), RECT_SHAPE, id, true, false);

		//set ClosedLoc, OpenedLoc, and ...
		ClosedLoc = actualSpriteRect.GetXY();
		//figure out the opened loc by checking which way the door opens
		if (def.isDouble)
		{
			//since this is a pair double door this door would have to be '-'
			OpenedLoc = ClosedLoc - Point2F((def.isHorizontal ? (float)def.distance : 0), (def.isHorizontal ? 0 : (float)def.distance));
		}
		else if (def.isNegative)
		{
			OpenedLoc = ClosedLoc - Point2F((def.isHorizontal ? (float)def.distance : 0), (def.isHorizontal ? 0 : (float)def.distance));
		}
		else if (!def.isNegative)
		{
			OpenedLoc = ClosedLoc + Point2F((def.isHorizontal ? (float)def.distance : 0), (def.isHorizontal ? 0 : (float)def.distance));
		}
		ClosedLoc_Fig = collisionRect.GetXY();
		OpenedLoc_Fig = ClosedLoc_Fig + (OpenedLoc - ClosedLoc);

		//set shader program
		ShaderProgram = shaderProgram;

		//set duration
		Duration = def.duration_ms;

		//set source rect of the texture
		SrcRect = (SDL_Rect)def.source_rect_1;

		//initialize sprite
		sprite.Init(def.texture_path, actualSpriteRect);
		current_sprite_pos_ = actualSpriteRect;
		sprite.SetReferenceDestinationRect(&current_sprite_pos_);

		//configure z-order
		ConfigureZOrder();

		//subscribe to all activearea events
		event_manager_->SubscribeEvent(int(MapEventManagerEventType::ACTIVEAREA_EMPTIED), Functor(this, &SlidingDoor::HandleAreaEvent));
		event_manager_->SubscribeEvent(int(MapEventManagerEventType::ACTIVEAREA_FILLED), Functor(this, &SlidingDoor::HandleAreaEvent));

		//make the whole class subsribe to debugmode
		if (!SubsribedToDebugMode)
			SubcribeToDebugMode(map->GetEventManager());

		static int unique_area_id = -1;			//do this since the active area that comes with sliding door is anonymous
		//add the ActiveArea to map
		Area_ptr area = std::make_shared<ActiveArea>(actualSpriteRect + def.spriteToActiveArea, map, unique_area_id--);
		map->AddActiveArea(area);

		//set active area
		this->ActivateAreaID = area->id_;

		//bind sprite movement to body movement
		GetBody()->Attach(&current_sprite_pos_.GetXY());
		GetBody()->motion_.GetSpeed()->AddSpeedComponent("default_speed", (float)def.distance / def.duration_ms);

		if (def.isDouble)
		{
			//configure stuff for the second SlidingDoor
			//since this is a double door, the "other" door would be the "+" door

			Rect4F collisionRect = actualSpriteRect + def.spriteToBody2;
			Point2F closedLoc = actualSpriteRect.GetXY() + def.delta;
			Point2F openedLoc = closedLoc + Point2F((def.isHorizontal ? (float)def.distance : 0),
				(def.isHorizontal ? 0 : (float)def.distance));
			Point2F WH = Point2F(actualSpriteRect.w, actualSpriteRect.h);

			std::string imagePath = def.texture_path;
			SDL_Rect srcRect = (SDL_Rect)def.source_rect_2;
			int duration = def.duration_ms;

			other = std::make_unique<SlidingDoor>(collisionRect, closedLoc, openedLoc, WH, imagePath, srcRect, duration, map, area->id_, ShaderProgram, id);
		}


	}
	void SlidingDoor::CleanUp()
	{
		event_manager_->UnsubscribeEvent(int(MapEventManagerEventType::ACTIVEAREA_EMPTIED), Functor(this, &SlidingDoor::HandleAreaEvent));
		event_manager_->UnsubscribeEvent(int(MapEventManagerEventType::ACTIVEAREA_FILLED), Functor(this, &SlidingDoor::HandleAreaEvent));
		if (isDouble())
			other->CleanUp();
	}

	void SlidingDoor::ToggleDebugMode(void * e)
	{
		if (stoi(((Event*)e)->param_string_list_[0]))
			isDebugging = true;
		else
			isDebugging = false;
	}

	void SlidingDoor::SubcribeToDebugMode(EventManager * eventManager)
	{
		//subscribe to debugmode events
		eventManager->SubscribeEvent(int(MapEventManagerEventType::DEBUGMODE), Functor(&SlidingDoor::ToggleDebugMode));
		SubsribedToDebugMode = true;
	}

	bool SlidingDoor::SubsribedToDebugMode = false;
	bool SlidingDoor::isDebugging = false;

	std::map<std::string, const SlidingDoorDef> SlidingDoor::defMap = std::map<std::string, const SlidingDoorDef>();
}