
#include <StatusBar.h>

#include "StatusBar.h"

namespace Fengine
{
	StatusBar::StatusBar()
	{

	}

	StatusBar::StatusBar(const std::string & filler_path, const std::string & frame_path,
								  Point2F location, LabelStyle label_style):
								  label_style_(label_style),
								  label_(Label("", location, label_style)),
								  scale_(1)
	{
		bar_frame_.Init(frame_path, Rect4F());
		bar_frame_wh_ = Graphics::GetInstance()->GetTextureWH(bar_frame_.sprite_sheet_);
		bar_frame_src_rect_ = Rect4F(0, 0, bar_frame_wh_.x, bar_frame_wh_.y);
		bar_frame_dest_rect_ = Rect4F(location, bar_frame_wh_.x, bar_frame_wh_.y);

		bar_.Init(filler_path, Rect4F());
		bar_wh_ = Graphics::GetInstance()->GetTextureWH(bar_.sprite_sheet_);
		bar_src_rect_ = Rect4F(0, 0, bar_wh_.x, bar_wh_.y);
		bar_dest_rect_ = Rect4F(location, bar_frame_wh_.x, bar_frame_wh_.y);

		SetValue(0);
	}

	void StatusBar::Draw(GLuint shader_program)
	{
		bar_.Draw(shader_program, (SDL_Rect)bar_src_rect_, bar_dest_rect_);
		bar_frame_.Draw(shader_program, (SDL_Rect)bar_frame_src_rect_, bar_frame_dest_rect_);
		label_.Draw(shader_program);
	}

	void StatusBar::SetText(const std::string &text)
	{
		label_.SetText(text);
	}

	void StatusBar::SetScale(float scale)
	{
		//scale only if the status bar is initialized
		if(bar_.IsDrawable() && bar_frame_.IsDrawable())
		{
			scale_ = scale;

			bar_dest_rect_.w = (value_) * scale_ * (float)bar_frame_wh_.x;
			bar_dest_rect_.h = scale_ * (float)bar_frame_wh_.y;

			bar_frame_dest_rect_.w = scale_ * (float)bar_frame_wh_.x;
			bar_frame_dest_rect_.h = scale_ * (float)bar_frame_wh_.y;
		}
	}

	void StatusBar::SetValue(float value)
	{
		value_ = value;

		//update the rects for drawing
		bar_src_rect_.w = (value_) * (float)bar_wh_.x;
		bar_dest_rect_.w = (value_) * scale_ * (float)bar_frame_wh_.x;
	}
}
