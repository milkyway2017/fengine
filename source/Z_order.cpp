#include "Z_order.h"


namespace Fengine
{
	void Z_order::Init(float z_pos, std::function<void()> draw_function, float z_offset)
	{
		is_ref_used = false;
		z_pos_ = z_pos;
		draw_function_ = draw_function;
		z_offset_ = z_offset;
	}

	void Z_order::Init(float * z_pos_ref, std::function<void()> draw_function, float z_offset)
	{
		is_ref_used = true;
		z_pos_ref_ = z_pos_ref;
		draw_function_ = draw_function;
		z_offset_ = z_offset;
	}

	float Z_order::GetZPosition()	const
	{
		if(is_ref_used)
		{
			return *z_pos_ref_ + z_offset_;
		}
			return z_pos_ + z_offset_;
	}

	void Z_order::Draw()	const
	{
		draw_function_();
	}

	bool CompareZOrder(Z_order* a, Z_order* b)
	{
		return (a->GetZPosition() < b->GetZPosition());
	}
}