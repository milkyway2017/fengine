#include "Map.h"
#include "Interpolation.h"
#include <chrono>

namespace Fengine
{
	void ReplaceString(std::string& str, std::string& oldStr, std::string& newStr)
	{
		size_t pos = 0u;
		while ((pos = str.find(oldStr, pos)) != std::string::npos) {
			str.replace(pos, oldStr.length(), newStr);
			pos += newStr.length();
		}
	}

	Map::Map() :
		player_(nullptr),
		event_manager_(&logger_),
		path_solver_(this, 500, 8)
	{}

	Map::Map(const std::string & path, Logger* systemLog, bool is_active) :
		player_(new Player()),
		event_manager_(&logger_),
		logger_system_(systemLog),
		path_solver_(this, 500, 8),
		player_has_control_(true),
		is_active_(is_active),
		is_fading_(0),
		transparency_(0)
	{
		//initialize shader program
		Graphics::GetInstance()->GenShaderProgram("Resources/Shaders/Map Rendering/white.vert", "Resources/Shaders/Map Rendering/white.frag", shader_program_, "Game Rendering");
		camera_.Init(shader_program_, "CameraMatrix");

		Uint32 begin_time = SDL_GetTicks();
		this->logger_system_->AddLine("*************loading map*************");
		LoadMap(path);
		this->logger_system_->AddLine("time taken: " + std::to_string(SDL_GetTicks() - begin_time));
		begin_time = SDL_GetTicks();

		this->logger_system_->AddLine("*******generating nodes*************");
		GenIntersectionPoints();
		this->logger_system_->AddLine("time taken: " + std::to_string(SDL_GetTicks() - begin_time));
		begin_time = SDL_GetTicks();

		this->logger_system_->AddLine("*********initializing components*************");
		InitLabels();
		this->logger_system_->AddLine("time taken: " + std::to_string(SDL_GetTicks() - begin_time));
		begin_time = SDL_GetTicks();

		this->logger_system_->AddLine("*********subscribing events*************");
		SubscribeEvents();
		this->logger_system_->AddLine("time taken: " + std::to_string(SDL_GetTicks() - begin_time));
		begin_time = SDL_GetTicks();

		this->logger_system_->AddLine("*************map loaded*************");

		//successfully created, add this map to maps_
		maps_.push_back(this);
		SetCameraToPlayer();
		//fade in if this map is active
		if(is_active_)
		{
			FadeMap(0.01f, true);
		}
	}


	Map::~Map()
	{
		//delete this from maps_
		auto iter = std::find(maps_.begin(), maps_.end(), this);
		if(iter != maps_.end())
		{
			maps_.erase(iter);
		}
	}

	void Map::HandleDieEvent(void *e)
	{
		Event event = *((Event*)e);
		Object* dyingObj = (Object*)event.target_;
		//check if the dying object is player, if so, just do nothing (Player itself decides what happens)
		if (Player::IsPlayer(dyingObj))
		{
			return;
		}

		//remove the dead object
		RemoveObject(dyingObj);
	}

	void Map::GenIntersectionPoints()
	{

		//gather all the lines of shapes
		for (auto &s : figures_)
		{
			std::vector<Line> Temp = GenLines(s->GetBody()->GetShape(),s->GetBody()->type_ == RECT_SHAPE, true);

			//insert
			if (lines_.size() == 0)
			{
				lines_ = Temp;
			}
			else
			{
				lines_.insert(lines_.end(), Temp.begin(), Temp.end());
			}
		}

		this->intersect_points_ = FindVerticesandIntersections(lines_);
	}

	std::vector<Command> Map::CommandProc()
	{

		COMMANDPROCBEGIN
		if (i.command_level_ < MAP)
		{		//if the level is less than MAP, this means that it belongs to levels above
			ReturnList.push_back(i);
			continue;
		}
		else if (i.command_level_ > MAP)
		{		//if the level is greater than MAP, this means that it belongs to eventmanager
			event_manager_.FireEvent(i);
			continue;
		}
		else
		{
			switch ((MapCommandType)i.command_type_)
			{
			case MapCommandType::NEW_RES:
				camera_.NewRes();		//update camera
				i.Resolve();
				break;
			default:
				i.CannotResolve(logger_system_);
				continue;
			}
		}
		COMMANDPROCEND
	}

	std::vector<Object*> Map::GetObjectAtLoc(Point2F location)
	{
		return std::vector<Object*>();
	}

	TileLayer * Map::GetGroundLayer()
	{
		for (auto& i : layers_)
		{
			if (i.is_ground_layer_)
				return &i;
		}
		return nullptr;
	}

	Tile * Map::GetInvolvedTile(Point2F p)
	{
		SDL_Point point = (SDL_Point)p;
		if (!SDL_PointInRect(&point, &map_rect_int_))				//check if the selected point is inside the map
			return nullptr;			//return nullptr if not
		int column, row, index;
		column = div((int)p.x, tile_width_).quot;
		row = div((int)p.y, tile_height_).quot;
		index = (map_width_*row) + column;
		if (IsWithin(index, 0, (int)GetGroundLayer()->TileList.size()))
			return &GetGroundLayer()->TileList[(map_width_*row) + column];
		else
			return nullptr;
	}

	Object* Map::GetPlayer()
	{
		if (player_->IsAlive())
			return player_;
		else
			return nullptr;
	}

	Object * Map::GetObjectByLocation(const Point2F & location)
	{
		//loop through the list and see if the cursor is on top of any object
		//if so return that object
		for (auto &i : objects_)
		{
			if (i->IsPointInSprite(GetCursorLocInMap()))
			{
				return i.get();
			}
		}
		return nullptr;
	}

	Object * Map::GetObjectByID(int id)
	{
		for(auto &i : objects_)
		{
			if (i->id_ == id)
				return i.get();
		}
		return nullptr;
	}

	GameItem * Map::GetItemByID(int id)
	{
		for (auto &i : items_)
		{
			if (i->id_ == id)
				return i.get();
		}
		return nullptr;
	}

	Figure * Map::GetFigureByID(int id)
	{
		for (auto &i : figures_)
		{
			if (i->id_ == id)
				return i.get();
		}
		return nullptr;
	}

	Camera * Map::GetCamera()
	{
		return &camera_;
	}

	RenderSorter * Map::GetRenderSorter()
	{
		return &render_sorter_;
	}

	std::vector<Tile*> Map::GetInvolvedTiles(Line l)
	{
		using namespace std;
		std::vector<Tile*> return_list;

		Tile* a, *b;
		a = GetInvolvedTile(l.P1);
		b = GetInvolvedTile(l.P2);
		if(!(a && b))
		{
			return return_list;
		}
		int x = a->tile_id_, y = b->tile_id_;
		int x_col, x_row, y_col, y_row;
		x_col = x % map_width_;
		y_col = y % map_width_;
		if (x_col > y_col) Swap(&x_col, &y_col);
		x_row = div(x, map_width_).quot;
		y_row = div(y, map_width_).quot;
		if (x_row > y_row) Swap(&x_row, &y_row);

		std::vector<Tile*> TestList;
		for (int i = x_col; i <= y_col; i++)
		{
			auto a = GetTileColumn(i, x_row, y_row);
			TestList.insert(TestList.end(), a.begin(), a.end());
		}
		//go over TestList and test collision between segment and rect
		for (auto & i : TestList)
		{
			if (DoSegmentandRectIntersect(Rect4F(i->dest_rect_), l))
			{
				return_list.push_back(i);
			}
		}
		return return_list;
	}

	

	std::vector<Tile*> Map::GetTileColumn(int columnNum, int begin, int end)
	{
		std::vector<Tile*> ReturnList;
		int beginTileID = (begin*map_width_) + columnNum;
		int endTileID = (end*map_width_) + columnNum;
		for (int TileID = beginTileID; TileID <= endTileID; TileID += map_width_)
		{
			ReturnList.push_back(&GetGroundLayer()->TileList[TileID]);
		}
		return ReturnList;
	}

	unsigned int Map::FindTileSet(unsigned int gid)
	{
		for (unsigned int i = 0; i < tilesets_.size(); i++)
		{
			if (IsWithin(gid, tilesets_.at(i).first_gid, tilesets_.at(i).first_gid + tilesets_.at(i).tile_count - 1))
			{
				return i;
			}
		}
		return -1;
	}

	void Map::AddGameItem(GameItem* item)
	{
		try
		{
			//add the item to our list
			items_.emplace_back(Item_ptr(item));
			//add to render sorter
			render_sorter_.AddZOrder(item->GetZOrder());
		}
		catch (...)
		{
			logger_.log_ += "SpawnItem exception!\n";
		}
	}

	void Map::RemoveGameItem(int i)
	{
		auto it = items_.begin();
		std::advance(it, i);
		render_sorter_.RemoveZOrder((*it)->GetZOrder());
		items_.erase(it);
	}

	void Map::RemoveGameItem(GameItem * item)
	{
		for (unsigned int i =0; i < items_.size(); i++)
		{
			if (items_[i].get() == item)
			{
				RemoveGameItem(i);
				return;
			}
		}
	}

	void Map::AddObject(ObjectType type, Point2F Location, int id)
	{
		switch (type)
		{
		case PLAYER_OBJ:
		{
			//init a player
			Player * p = new Player();
			//assign the player in Map
			player_ = p;
			//add player to objects_
			objects_.push_back(Obj_ptr(p));
			p->Init(this, Location, shader_program_, id);
			world_.AddBody(p->GetBody());
			render_sorter_.AddZOrder(p->GetZOrder());
			break;
		}
		case LYCAN_OBJ:
		{
			//init a lycan
			Lycan * l = new Lycan();
			objects_.push_back(Obj_ptr(l));
			l->Init(this, Location, shader_program_, id);
			world_.AddBody(l->GetBody());
			render_sorter_.AddZOrder(l->GetZOrder());
			break;
		}
		}
	}

	void Map::RemoveObject(int index)
	{
		//get the iterator of this index
		auto it = objects_.begin();
		std::advance(it, index);

		//remove the object's event subscription
		Object* o = (*it).get();
		for (auto i : o->subscribedEvents)
		{	//loop through the events and unsubscribe them
			event_manager_.UnsubscribeEvent(i.first, i.second);
		}

		//remove its Body from World
		world_.RemoveBody(o->GetBody());

		//remove its Z_order
		render_sorter_.RemoveZOrder(o->GetZOrder());
		
		//objects_.erase(it); dont do this because this will change the objects_ while looping through it
		//instead add it to dead_objects
		dead_objects_.push_back(o);
	}

	void Map::RemoveObject(Object * obj)
	{
		for (unsigned int i = 0; i < objects_.size(); i++)
		{
			if (objects_[i].get() == obj)
			{
				RemoveObject(i);
				return;
			}
		}
	}

	void Map::AddFigure(Figure * figure, bool drawable)
	{
		//!< @todo deprecate marking Tiles as available/unavailable
		//set all tiles that contains the lines to unavailable (for pathfinding)
		//except for SlidingDoors because the doors should not be an obstacle of pathfinding
		if(!(bool)(dynamic_cast<SlidingDoor*>(figure)))
		{
			auto LineList = GenLines(figure->GetBody()->GetShape(), figure->GetBody()->type_ == RECT_SHAPE, true);
			for (auto &i : LineList)
			{
				auto TileList = GetInvolvedTiles(i);
				for (auto &t : TileList)
				{    //set to unavailable for path finding
					t->no_collision_ = 0;
				}
			}
		}
		if(drawable)
			//add to render sorter
			render_sorter_.AddZOrder(figure->GetZOrder());

		//add body to world
		world_.AddBody(figure->GetBody());
		//if the it is a double sliding door, add the second body as well
		if ((bool)(dynamic_cast<SlidingDoor*>(figure)) && (dynamic_cast<SlidingDoor*>(figure)->isDouble()))
			world_.AddBody((dynamic_cast<SlidingDoor*>(figure)->GetOtherBody()));

		figures_.push_back(Figure_ptr(figure));
	}

	void Map::RemoveFigure(unsigned int index)
	{
		//get the iterator of this index
		auto it = figures_.begin();
		std::advance(it, index);

		//remove the object's event subscription
		Figure* f = (*it).get();

		//remove its Body from World
		world_.RemoveBody(f->GetBody());

		//remove its Z_order
		render_sorter_.RemoveZOrder(f->GetZOrder());

		figures_.erase(it); 
	}

	void Map::FindPath(const Point2F & StartPos, const Point2F & EndPos, std::vector<Tile*>* path)
	{
		std::vector<void*> VoidPath;
		
		float totalCost;

		//first check if both startPos and endPos are inside the map
		if (IsInsideMap(StartPos) && IsInsideMap(EndPos))
		{

			//solve for path
			path_solver_.Solve((void*)(GetInvolvedTile(StartPos)),
				(void*)(GetInvolvedTile(EndPos)),
				&VoidPath,
				&totalCost);

			path->clear();
			for (auto& i : VoidPath)
			{
				path->push_back((Tile*)(i));
			}
		}
	}

	std::vector<Tile*> Map::FindPath(const Point2F & StartPos, const Point2F & EndPos)
	{
		std::vector<Tile*> ReturnList;
		FindPath(StartPos, EndPos, &ReturnList);
		return ReturnList;
	}

	Tile * Map::GetAdjacentTile(Tile * t, Angle Direction)
	{
		return GetGroundLayer()->GetAdjacentTile(t, Direction);
	}

	void Map::Draw()
	{
		using namespace std::chrono;
		for (auto &i : layers_)
		{
			//i.render_method_ = (LayerRenderMethod)temp_render_mode;
			i.Draw();
		}
		render_sorter_.SortAndDraw();

		//draw any label overlay
		dialogue_.Draw(Graphics::GetInstance()->GetShaderProgram("Normal Rendering"));

		//draw hint texts
		for(auto & text_label : hint_texts_)
		{
			text_label.second.Draw(Graphics::GetInstance()->GetShaderProgram("Normal Rendering"));
		}

		//draw fade mask on top if the map is in the process of fading
		if(is_fading_)
		{
			Graphics::GetInstance()->DisplayDebugShape({0,0,1280,720},
				GLColor(Graphics::GetInstance()->GetColor("Black")).ToTransparent(1-transparency_),
				Graphics::GetInstance()->GetShaderProgram("Normal Rendering"));
		}
	}

	std::vector<Command> Map::Update(Input & input, int time_passed)
	{
		//sync ipt_
		this->input_ = &input;

		//fire key press event
		if(input_->is_key_pressed_)
		{
			event_manager_.FireEvent(Event(MAP_EVENTMANAGER,(int)MapEventManagerEventType::KEY_PRESSED, "Map", &input_));
		}

		auto returnedVal = std::to_string(GetCursorLocInMap().x) + ", " + std::to_string(GetCursorLocInMap().y);
		InfoLabel::GetInstance()->DisplayString("cursor position in map", returnedVal,
				Graphics::GetInstance()->GetFont("lazy.ttf", 20));

		//add/destroy a Lycan at where the player right clicked
		if (this->input_->GetRButtonState() == BUTTON_PRESSED)
		{
			if (IsInsideMap(GetCursorLocInMap()))
				AddObject(LYCAN_OBJ, GetCursorLocInMap());
			else
				this->logger_system_->AddLine("cannot add object outside of the map's defined area, request ignored");
		}
		auto r = input_->GetWheelEvent();
		if (r > 0)
		{
			camera_.SetScale(camera_.GetScale()*1.1f);
		}
		else if(r < 0)
			camera_.SetScale(camera_.GetScale()/1.1f);

		//update
		for (auto &i : areas_)
		{
			i->Update();
		}
		for (auto &i : figures_)
		{
			i->Update(time_passed);
		}
		//update Object
		for (auto & i : objects_)
		{
			if (std::find(dead_objects_.begin(), dead_objects_.end(), i.get()) != dead_objects_.end())
			{
				continue;
			}
			//check if the object is player, if so, pass in the Input class as second param
			if (Player::IsPlayer(i.get()) && player_has_control_)
			{
				i->Update(time_passed, &input);
			}
			else if(i)			//check if the object is not pointing to nullptr (this can happen because the size of objects_ may change)
				i->Update(time_passed, nullptr);
		}
		for (auto & i : layers_)
		{
			i.Update(&input);
		}

		world_.Update(time_passed);

		//remote dead objects
		for (auto &i : dead_objects_)
		{
			for (auto j = objects_.begin(); j != objects_.end();)
			{
				if ((*j).get() == i)
				{
					j = objects_.erase(j);
					continue;
				}
				j++;
			}
		}
		dead_objects_.clear();

		//check scripts if they should be executed
		for(auto & script : scripts_)
		{
			if(!script.IsRunning() && script.ShouldRunScript())
			{
				script.Run();
			}
		}

		HandlePendingHints();

		//camera update
		camera_.Update();

		//this is going to fade the map if the map is not yet completely opaque/transparent
		FadeMap((float)time_passed/1000.f);

		return CommandProc();
	}

	void Map::SetCameraToPlayer()
	{
		camera_.SetPositionCallback([this](float *x, float *y)
									{
										*x = player_->GetCenter().x;
										*y = player_->GetCenter().y;
									});
	}

	std::vector<GameItem*> Map::GetItemInRange(Point2F Location, float range, int itemcount)
	{
		std::map<float, GameItem*>DistItemMap;
		std::vector<GameItem*> ReturnList;

		for (auto &i : items_)
		{
			//UnrootedDist is dx^2 + dy^2
			float UnrootedDist = powf(i->GetLocation().x - Location.x, 2.0f) + powf(i->GetLocation().y - Location.y, 2.f);
			if (Dist(i->GetLocation(), Location) <= range)
			{
				DistItemMap[UnrootedDist] = i.get();
			}
			if (ReturnList.size() == itemcount)
			{
				break;
			}
		}
		std::transform(DistItemMap.begin(), DistItemMap.end(), std::back_inserter(ReturnList), [](std::pair<const float, GameItem*> p) {return p.second; });
		return ReturnList;
	}

	std::vector<Object*> Map::GetObjectInRange(Point2F Location, float range, int objectcount, Object* exclude)
	{
		float pseudo_range = range * range;
		std::map<float, Object*>DistItemMap;
		std::vector<Object*> ReturnList;

		for (auto &i : objects_)
		{
			//UnrootedDist is dx^2 + dy^2
			float pseudo_dist = (i->GetCenter() - Location).PsuedoNorm();
			if (pseudo_dist <= pseudo_range && i.get() != exclude)
			{
				DistItemMap[pseudo_dist] = i.get();
			}
			if (ReturnList.size() == objectcount)
			{
				break;
			}
		}
		std::transform(DistItemMap.begin(), DistItemMap.end(), std::back_inserter(ReturnList), [](std::pair<const float, Object*> p) {return p.second; });
		return ReturnList;
	}

	std::vector<Object*> Map::GetObjectInDirection(Object* caller, float DistRange, Angle direction, Angle AngleRange)
	{
		Angle start_angle = direction - (AngleRange * 0.5f);
		Angle end_angle = start_angle + AngleRange;
		Point2F Location = caller->GetCenter();
		std::vector<Object*> ReturnList;
		std::vector<Object*> object_in_range = GetObjectInRange(Location,DistRange, 100/*todo: change GetObjectInRange to not take object count*/);
		for (auto &i : object_in_range)
		{
			//cannot get self 
			if (i == caller)
			{
				continue;
			}
			//check angle
			Point2F target_center = i->GetCenter();
			if (IsWithin(
				FindAngle(target_center - Location).FlipX(),
				start_angle,
				end_angle))
			{		/*within dist*/					    /*within angle range*/
				ReturnList.push_back(i);
				continue;
			}
		}

		return ReturnList;
	}

	Point2F Map::GetCursorLocInMap()
	{
		auto returnVal = Point2F(camera_.GetPostion().x, camera_.GetPostion().y)
			- (Point2F(Graphics::GetInstance()->GetWindowSize())*0.5f)*(1 / camera_.GetScale())
			+ Point2F(input_->cursor_loc_)*(1 / camera_.GetScale());
		return returnVal;
	}

	int Map::ObjectsInArea(const Rect4F & area)
	{
		int count = 0;
		for (auto& i : objects_)
		{
			if (RectCollide(i->GetBody()->GetBound(), area) != 0)
			{
				count++;
			}
		}
		return count;
	}

	bool Map::IsInsideMap(Point2F p)
	{
		SDL_Point point = (SDL_Point)p;
		return SDL_PointInRect(&point,&map_rect_int_);
	}

	void Fengine::Map::FadeFigure(Figure* figure, float target_amount, int time)
	{
		auto * transparency = figure->GetSprite()->GetTransparencyPtr();
		Interpolation::AddInterpolationToPool(
			new LinearInterpolation(transparency, *transparency, target_amount, time, Functor()));
	}

	void Map::ZoomCamera(float scale, int time)
	{
		camera_.ZoomToScale(scale, camera_.GetScale(), time);
	}

	void Map::TranslateCamera(const Point2F & position, int time)
	{
		camera_.MoveTo(position, time);
	}

	float Map::LeastCostEstimate(void * stateStart, void * stateEnd)
	{
		return 0/*return 0 so that this is a A * algorithm with h(p) = 0, a.k.a. Dijkstra algorithm*/;
		//read more about this at https://brilliant.org/wiki/a-star-search/
	}

	void Map::AdjacentCost(void * state, std::vector<micropather::StateCost>* adjacent)
	{
		std::vector<micropather::StateCost> ResultList;

		TileLayer & ground = *GetGroundLayer();
		Tile * t = (Tile*)state;
		Tile * result = nullptr;
		bool NAvai = false, EAvai = false, SAvai = false, WAvai = false;
		//N
		result = ground.GetAdjacentTile(t, NORTH);
		if (result && result->no_collision_)
		{
			ResultList.push_back({ (void*)result,10 });
			NAvai = true;
		}

		//E
		result = ground.GetAdjacentTile(t, EAST);
		if (result && result->no_collision_)
		{
			ResultList.push_back({ (void*)result,10 });
			EAvai = true;
		}

		//S
		result = ground.GetAdjacentTile(t, SOUTH);
		if (result && result->no_collision_)
		{
			ResultList.push_back({ (void*)result,10 });
			SAvai = true;
		}

		//W
		result = ground.GetAdjacentTile(t, WEST);
		if (result && result->no_collision_)
		{
			ResultList.push_back({ (void*)result,10 });
			WAvai = true;
		}
		//we finished NESW, time for diagonal directions
		if (NAvai && EAvai)
		{
			result = ground.GetAdjacentTile(t, NORTH_EAST);
			if (result && result->no_collision_)
			{
				ResultList.push_back({ (void*)result, 14 });
			}
		}

		if (SAvai && EAvai)
		{
			result = ground.GetAdjacentTile(t, SOUTH_EAST);
			if (result && result->no_collision_)
			{
				ResultList.push_back({ (void*)result,14 });
			}
		}

		if (SAvai && WAvai)
		{
			result = ground.GetAdjacentTile(t, SOUTH_WEST);
			if (result && result->no_collision_)
			{
				ResultList.push_back({ (void*)result,14 });
			}
		}

		if (NAvai && WAvai)
		{
			result = ground.GetAdjacentTile(t, NORTH_WEST);
			if (result && result->no_collision_)
			{
				ResultList.push_back({ (void*)result,14 });
			}
		}

		*adjacent = ResultList;
	}

	void Map::LoadMap(const std::string & path)
	{
		//find the directory of this map file, save for later use
		std::string file_directory;
		{
			auto temp = SplitStringByDelim(path, '/');
			for (unsigned int i = 0; i < temp.size() - 1; i++)
			{
				file_directory += temp[i] + '/';
			}
		}

		this->file_path_ = path;

		//read map file into JSON object
		using json = nlohmann::json;
		json defaultsJSON;
		std::string error = ReadJson(defaultsJSON, path);
		if(!error.empty())
		{
			throw Exception(error, true);
		}

		//get the camera scale of this map, if not found, default 1
		if(defaultsJSON.count("properties") && defaultsJSON["properties"].count("CameraScale"))
		{
			camera_.SetScale((float)defaultsJSON["properties"]["CameraScale"]);
			this->logger_system_->AddLine("camera scaled to " + std::to_string(camera_.GetScale()) + "x");
		}
		else
		{
			this->logger_system_->AddLine("map property CameraScale not found, auto scaled to 1x");
		}

		//get the width and height of the map and tile width and tile height
		map_width_ = defaultsJSON["width"];
		map_height_ = defaultsJSON["height"];
		tile_width_ = defaultsJSON["tilewidth"];
		tile_height_ = defaultsJSON["tileheight"];

		//allocate layer size
		layers_.reserve(defaultsJSON["layers"].size());	

		map_rect_ = Rect4F(0, 0, (float) map_width_*tile_width_, (float) map_height_*tile_height_);
		map_rect_int_ = (SDL_Rect)map_rect_;

		//initialize world with map_rect_
		world_ = World(map_rect_);

		//read in all tilesets
		for(auto tilesetJSON : defaultsJSON["tilesets"])
		{
			TileSet Ts;
			std::string tileset_path = tilesetJSON["source"];				//uses independent tileset only from now on!
			std::string tileset_abs_path = file_directory + tileset_path;
			std::string tileset_dir = tileset_abs_path.substr(0, tileset_abs_path.rfind('/') + 1);

			//read tile set file
			std::ifstream tileset_file(file_directory + tileset_path);
			json tile_set_json;
			tileset_file >> tile_set_json;
			Ts.first_gid = tilesetJSON["firstgid"];
			Ts.tile_width = tile_set_json["tilewidth"];
			Ts.tile_height = tile_set_json["tileheight"];
			Ts.column_count = tile_set_json["columns"];
			Ts.tile_count = tile_set_json["tilecount"];
			Ts.row_count = Ts.tile_count/Ts.column_count;
			std::string image_path = tile_set_json["image"];
			Ts.image_path = tileset_dir + image_path;
			Ts.texture = Graphics::GetInstance()->GetTexture(Ts.image_path);

			//read tile properties of this tile set if there is any
			if (tile_set_json.count("tileproperties"))
			{
				json tile_properties = tile_set_json["tileproperties"];
				for (unsigned int gid = 0; gid < Ts.tile_count; gid++)
				{
					if(tile_properties.count(std::to_string(gid)))
					{
						//read z_order
						TileProperties new_tile_property;
						if (tile_properties[std::to_string(gid)].count("Z_Order"))
						{
							float z_order_offset = tile_properties[std::to_string(gid)]["Z_Order"];
							new_tile_property.z_order_offset = z_order_offset;
						}

						//read other properties if exist...
						Ts.tiles_properties.insert({ gid, new_tile_property});
					}
				}		
			}
			//read if any tiles in the tile set contain collision bodies
			if(tile_set_json.count("tiles"))
			{
				json tiles = tile_set_json["tiles"];
				for (unsigned int gid = 0; gid < Ts.tile_count; gid++)
				{
					if(tiles.count(std::to_string(gid)))
					{
						if(!Ts.tiles_properties.count(gid))
						{
							TileProperties new_tile_property;
							Ts.tiles_properties.insert({gid, new_tile_property});
						}
						for(auto& object: tiles[std::to_string(gid)]["objectgroup"]["objects"])
						{
							Figure new_figure(Rect4F(object["x"], object["y"], object["width"], object["height"]),true, false);
							Ts.tiles_properties[gid].collision_bodies.push_back(new_figure);
						}
					}
				}
			}

			this->tilesets_.push_back(Ts);
		}


		//read all layers (objectlayer & tilelayer)
		for(auto layerJSON : defaultsJSON["layers"])
		{
			try
			{
				//check whether this is a objectlayer or tilelayer
				if (layerJSON["type"]=="tilelayer")
				{
					//since this is a tilelayer, it corresponds to a TileLayer object in our game
					//push back a tile layer in layers_
					layers_.push_back(TileLayer(this));

					bool isGround = false;
					if (layerJSON["properties"]["Type"]
						=="ground")            //the "Type" property tells if this layer is the ground layer
						isGround = true;

					//tell the layer whether it is ground and what shaderProgram to use
					layers_.back().Init(shader_program_, map_width_, isGround, (unsigned int)map_width_ * map_height_);

					//read all tiles in the tilelayer
					unsigned int tileCount = 0;                                //count the tile number
					for (unsigned int
							gid : layerJSON["data"])                //gid tracks the tile's corresponding texture in tilesets
					{
						SDL_Rect SourceRect, DestRect;

						//calculate destRect for current tile
						DestRect.x = (tileCount%map_width_)*tile_width_;
						DestRect.w = tile_width_;
						DestRect.y = (tileCount/map_width_)*tile_height_;
						DestRect.h = tile_height_;

						unsigned int TSIndex = 0;
						if (gid==0)
						{
							//a gid of 0 means
							//no texture for this tile, add blank tile and keep looping
							layers_.back().AddTile({}, DestRect, gid, 0);

							tileCount++;
							continue;
						}
						TSIndex = FindTileSet(gid);
						auto tileset_gid = gid - tilesets_[TSIndex].first_gid;
						SourceRect = tilesets_[TSIndex].GetSourceRect(tileset_gid);

						//if there is any collision body on this gid
						if(tilesets_[TSIndex].tiles_properties.count(tileset_gid)&& !tilesets_[TSIndex].tiles_properties[tileset_gid].collision_bodies.empty())
						{
							for(auto & i: tilesets_[TSIndex].tiles_properties[tileset_gid].collision_bodies)
							{
								Figure* new_figure = new Figure(i);
								new_figure->MoveTo(Point2F((float)DestRect.x, (float)DestRect.y) + new_figure->GetPosition());
								//adding too many figures in map would result in extremely laggy gameplay
								//to resolve this, we can merge shapes that are adjacent to each other
								for(unsigned int index = 0; index < figures_.size(); index++)
								{
									if (figures_[index]->IsSolid())
									{
										Rect4F new_rect = MergeRect(figures_[index]->GetBody()->collision_rect_, new_figure->GetBody()->collision_rect_);
										if (!new_rect.IsDefault())
										{
											//remove the old figure
											RemoveFigure(index);

											//update the shape of the new figure to the merged version
											new_figure->Init(this, GenShape(new_rect, false), RECT_SHAPE, new_figure->id_, true, false);
											new_figure->SetSolidity(true);
											break;
										}
									}
								}
								AddFigure(new_figure, false);
							}
						}

						//check z order offset of the tile
						float z_order_offset = 0;
						if (tilesets_[TSIndex].tiles_properties.count(tileset_gid))
						{
							z_order_offset = tilesets_[TSIndex].tiles_properties[tileset_gid].z_order_offset;
						}

						//Find the sourceRect of tile
						layers_.back().AddTile(SourceRect, DestRect, tilesets_.at(TSIndex).texture, gid, z_order_offset);

						tileCount++;
					}
					layers_.back().Activate();        //activate the layer
				}
				else if (layerJSON["type"] == "objectgroup")
				{
					//loop through all objects and...
					for(auto objJSON : layerJSON["objects"])
					{
						//every object in Tiled translates into an EntityInfo
						EntityInfo new_entity;

						//retrieve the unique object id
						int id = objJSON["id"];
						new_entity.id = id;

						//get the xy coordinates of this object
						Point2F location(objJSON["x"],objJSON["y"]);

						//get the type of this object
						std::string type = objJSON["type"];
						new_entity.type = type;

						//get the name of this object
						new_entity.name = objJSON["name"];

						if (type.empty())
						{
							this->logger_system_->AddLine("missing object type for object " + std::to_string(id) + ", object ignored");
							continue;
						}
						//append "Def.json" and get the ObjDef file this object uses, load it
						std::string defPath = "Resources/Game/ObjDef/"+type+"Def.json";
						LoadObjDef(defPath,logger_system_);
						
						//check type
						if(type == "Player")
						{
							AddObject(PLAYER_OBJ, location, id);
							new_entity.object = objects_.back().get();
						}
						else if(type == "Lycan")
						{
							AddObject(LYCAN_OBJ,location, id);
							new_entity.object = objects_.back().get();
						}
						else if(type == "Wall")
						{
							//the wall is a polyline/polygon
							if(objJSON.count("polyline"))
							{
								Figure * new_figure = new Figure();
								GLShape shape;
								for(auto & vertex : objJSON["polyline"])
								{
									shape.AddVertex({ Point2F(vertex["x"], vertex["y"]) + location});
								}
								new_figure->Init(this, shape, LINE_SHAPE, id, true, false);	
								AddFigure(new_figure);
							}
							else if(objJSON.count("polygon"))
							{
								Figure * new_figure = new Figure();
								GLShape shape;
								for (auto & vertex : objJSON["polygon"])
								{
									shape.AddVertex({ Point2F(vertex["x"], vertex["y"]) + location });
								}
								new_figure->Init(this, shape, POLYGON_SHAPE, id, true, false);
								AddFigure(new_figure);
							}
							else
							{
								//the wall is a rectangle
								AddFigure(new Figure(Rect4F(location, objJSON["width"], objJSON["height"]), true, false, id, this));
							}
							new_entity.object = figures_.back().get();
						}
						else if(type == "SlidingDoorH")
						{
							AddFigure(new SlidingDoor(type, location, shader_program_, this), true);
							new_entity.object = figures_.back().get();
						}
						else if(type == "Flashlight1")
						{
							AddGameItem(new Flashlight("Flashlight1", location, shader_program_, this, id,nullptr));
							new_entity.object = items_.back().get();
						}
						else if(type == "ActiveArea")
						{
							AddActiveArea(std::make_shared<ActiveArea>(Rect4F(location, Point2F(objJSON["width"], objJSON["height"])), this, id));
							new_entity.object = areas_.back().get();
							//check if this active area triggers teleportation
							if(objJSON["properties"].count("NextMap"))
							{
								areas_.back()->AddSpell({objJSON["properties"]["NextMap"]});
							}
						}
						else if(type == "Image")
						{
							unsigned int gid = objJSON["gid"];
							unsigned int tileset_index = FindTileSet(gid);
							unsigned int tileset_gid = gid - tilesets_[tileset_index].first_gid;
							//since the given xy is bottom left coord, we need to minus height from y
							float height = objJSON["height"];
							AddFigure(new Figure(Rect4F(location.x, location.y - height,objJSON["width"],objJSON["height"]),
								false, false, id, this,
								tilesets_[tileset_index].image_path,
								tilesets_[tileset_index].GetSourceRect(tileset_gid)
								), true);

							//set visibility
							if (!(bool)objJSON["visible"])
								figures_.back()->GetSprite()->SetTransparency(0.f);

							//add offset
							if (tilesets_[tileset_index].tiles_properties.count(tileset_gid))
							{
								figures_.back()->GetZOrder()->z_offset_ += tilesets_[tileset_index].tiles_properties[tileset_gid].z_order_offset;
							}

							new_entity.object = figures_.back().get();
						}
						else
						{
							this->logger_system_->AddLine("object type " + type + " is not recognized!");
							continue;
						}

						//finish loading, add this entity to entities if a Fengine object has been created for it
						if(new_entity.object)
							entities_.push_back(new_entity);

						//check if the object comes with a script, only supports ActiveArea as a script carrier
						if(objJSON.count("properties") && objJSON["properties"].count("ScriptPath"))
						{
							std::string script_name = objJSON["properties"]["ScriptPath"];
							std::string script_path = "Resources/Game/Scripts/" + script_name;

							if(type == "ActiveArea")
							{
								//load script
								this->scripts_.emplace_back(this, script_path, [=](){
									if(this->GetPlayer())
									{
										auto player_rect = GetPlayer()->GetBody()->GetBound();
										auto area_rect = this->GetActiveAreaByID(id)->GetArea();
										return RectCollide(player_rect, area_rect) != 0;
									}
									return false;
								});
							}
							else if(type == "Image")
							{
								//load script
								this->scripts_.emplace_back(this, script_path, [=]() {
									return (input_->GetLButtonState() == BUTTON_PRESSED) &&
										GetFigureByID(id)->GetSprite()->IsPointInShape(this->GetCursorLocInMap());
								});
							}
						}
					}
				}
			}
			catch(const std::exception & e)
			{
				//throw fatal error and abort map loading
				throw Exception({"Map Loading Exception", "Error reading layers", e.what(), "Loading Failed"}, true);
			}

		}

		if (player_ && player_->is_init_)
			SetCameraToPlayer();
		else 
			throw Exception({ "Map Loading Exception","Player is not initialized.","Loading Failed" }, true);
	}
	void Map::AddActiveArea(Area_ptr activeArea)
	{
		areas_.push_back(activeArea);
	}

	ActiveArea* Map::GetActiveAreaByID(int area_id)
	{
		for(auto i: areas_)
		{
			if(i->id_ == area_id)
				return i.get();
		}
		return nullptr;
	}

	void Map::HandleAttackEvent(void *e)
	{
		//TODO: the event should be logged with more detail than this! There should be a ToString() for every Object, then we can log which object attacked which
		this->logger_.log_ += "ATTACK event fired\n";

		//get the attacked Object and inform that Object
		//according to CommandDef, the attacked Object is at param_list_[1] of the event
		Object* attackedObject = (Object*)((Command*)e)->param_list_[1];
		if (attackedObject == nullptr)
			return;
		//the attack damage is paramStringList[0], get that string and cast it into int
		int attackDamage = std::stoi(((Command*)e)->param_string_list_[0]);

		//do the attack
		attackedObject->ReceiveAttack(attackDamage);

		//done!
	}
	void Map::LoadObjDef(const std::string& DefPath, Logger* systemLog)
	{
		//check if the file exists
		if(!FileExist(DefPath))
		{
			systemLog->log_ += std::string("cannot find the ObjDef file specified: \"") + DefPath + "\n";
			return;
		}
		using json=nlohmann::json;
		std::ifstream defFile(DefPath);
		json Def; defFile >> Def;

		try
		{
			bool defFound = false;
			//detect the type
			for (auto i:Def["layers"])
			{//loop through and find the layer with the name "Def"
				if (i["name"]=="Def")
				{
					Def = i;
					defFound = true;
					break;
				}
			}
			if (!defFound)
			{
				systemLog->log_+=std::string("generic ObjDef Loading, ")+"failed to find def objectgroup\n";
				return;
			}
			std::string type = Def["properties"]["ObjType"];

			try
			{
				//use the type to determine which class to call
				if (type=="SLIDINGDOOR")
					SlidingDoor::LoadDef(DefPath);
				else if (type=="PLAYER")
					Player::LoadDef(DefPath);
				else if (type=="LYCAN")
					Lycan::LoadDef(DefPath);
				else if (type=="FLASHLIGHT")
					Flashlight::LoadDef(DefPath);
			}
			catch(const Exception & e)
			{	//catch any custom exception thrown by LoadDef(), and log content
				for(auto s : e.message_queue_)
					systemLog->log_.append(s);
			}
		}
		catch(const std::exception & e)
		{		//do nothing
			systemLog->log_.append("generic ObjDef loading error", e.what() + '\n');
			return;
		}
	}

	void Map::TakeOverPlayerControl(bool on_off)
	{
		this->player_has_control_ = !on_off;
		player_->StopWalking();
	}

	void Map::CharacterSpeak(Object *object_speaking, const std::string &speech)
	{
		dialogue_.SetText(speech);
	}
	void Map::InitLabels()
	{
		//dialogue stuff
		dialogue_style_.color = Graphics::GetInstance()->GetColor("navy blue");
		dialogue_style_.font = Graphics::GetInstance()->GetFont("manaspc.ttf",20);
		dialogue_style_.fade_in_time = 0;
		dialogue_style_.fade_out_time = 0;
		dialogue_ = Label("",Point2F(0.f, 400.f), dialogue_style_);

		//hint text stuff
		hint_style_.color = Graphics::GetInstance()->GetColor("red");
		hint_style_.font = Graphics::GetInstance()->GetFont("manaspc.ttf",20);
		hint_style_.fade_in_time = 500;
		hint_style_.fade_out_time = 1000;
	}
	void Map::SubscribeEvents()
	{
		//subscribe to ATTACK events
		event_manager_.SubscribeEvent((int)MapEventManagerEventType::ATTACK,Functor(this,&Map::HandleAttackEvent));

		//subscribe to DIE events
		event_manager_.SubscribeEvent((int)MapEventManagerEventType::DIE, Functor(this, &Map::HandleDieEvent));
	}

	void Map::ShowHintText(const std::string &hint)
	{
		pending_hint_changes_mutex_.lock();
		pending_hint_changes_[false].push_back(hint);
		pending_hint_changes_mutex_.unlock();
	}

	void Map::DeleteHintText(const std::string &hint)
	{
		pending_hint_changes_mutex_.lock();
		pending_hint_changes_[true].push_back(hint);
		pending_hint_changes_mutex_.unlock();
	}

	void Map::EndSpeak()
	{
		dialogue_.SetText("");
	}

	void Map::HandlePendingHints()
	{
		pending_hint_changes_mutex_.lock();
		for(auto & label_text : pending_hint_changes_[true])
		{
			if (hint_texts_.find(label_text) != hint_texts_.end())
			{
				GLErrorVal();
				hint_texts_[label_text].CleanUp();
				hint_texts_.erase(label_text);
				GLErrorVal();
			}
		}
		for(auto & label_text : pending_hint_changes_[false])
		{
			//add these labels
			if(hint_texts_.find(label_text) == hint_texts_.end())
			{
				GLErrorVal();
				Label new_label(label_text, Point2F(400.f, 600.f), hint_style_);
				hint_texts_.insert(std::pair<std::string, Label>(label_text, new_label));
				GLErrorVal();
			}
		}
		pending_hint_changes_[true].clear();
		pending_hint_changes_[false].clear();
		pending_hint_changes_mutex_.unlock();
	}

	GLuint Map::GetShaderProgram() const
	{
		return shader_program_;
	}

	bool Map::FadeMap(float fade_amount, int in_out, Functor fade_callback)
	{
		if(!fade_callback.is_default_)
			fade_callback_ = fade_callback;
		if(fade_amount == 0 || (in_out == 0 && is_fading_ == 0))
			return false;
		float trans_before = transparency_;

		if(in_out == 0 && is_fading_ != 0)
		{
			FadeMap(fade_amount, is_fading_);
		}
		else if(in_out > 0)
		{
			transparency_ += fade_amount;
			is_fading_ = in_out;			//keep state, so next time won't need to provide in_out
		}
		else
		{
			transparency_ -= fade_amount;
			is_fading_ = in_out;			//keep state, so next time won't need to provide in_out
		}

		//if transparency has reached max/min, change is_fading to 0
		if(transparency_ >= 1.f || transparency_ <= 0.f)
		{
			is_fading_ = 0;
			transparency_ = std::round(transparency_);
			fade_callback_(nullptr);
			fade_callback_ = Functor();
		}

		return transparency_ != trans_before;
	}

	SDL_Rect TileSet::GetSourceRect(unsigned int tileset_gid)
	{
		SDL_Rect source_rect;

		//calculate sourcerect
		source_rect.x = (tileset_gid%column_count)*tile_width;
		source_rect.w = tile_width;
		source_rect.y = (tileset_gid /column_count)*tile_height;
		source_rect.h = tile_height;

		return source_rect;
	}

	std::vector<Map*> Map::maps_ = std::vector<Map*>();

	ActiveArea* Map::GetActiveAreaByName(const std::string &name)
	{
		//look through entities_, map this name to a unique id and call GetActiveAreaByID
		try
		{
			return GetActiveAreaByID(GetEntitiesByName(name).at(0).id);
		}
		catch(const std::exception & e)
		{
			return nullptr;
		}
	}

	std::vector<EntityInfo> Map::GetEntitiesByName(const std::string &name)
	{
		std::vector<EntityInfo> return_list;
		for(auto entity : entities_)
		{
			if(entity.name == name)
			{
				return_list.push_back(entity);
			}
		}
		return return_list;
	}

	Map* Map::GetMapByName(const std::string &name)
	{
		for(auto map : maps_)
		{
			if(map->file_path_.find(name) != std::string::npos)
			{
				return map;
			}
		}
		return nullptr;
	}

	void Map::EnterMap(const std::string &map_name, const std::string & target_active_area)
	{
		Map* target_map = nullptr;				//the map to enter
		if(map_name.empty())
			//enter self
			target_map = this;
		else
			target_map = GetMapByName(map_name);
		if(target_map == this)
			return;
		//start fading out this map with a callback to fade in the other map
		FadeMap(0.01, -1, Functor([=](void *){
			if(target_map)
			{
				this->is_active_ = false;
				//activate target
				target_map->is_active_ = true;
				//fade the map in
				target_map->FadeMap(0.01f, 1);

				//find the active area to set the player's position to
				auto area = target_map->GetActiveAreaByName(target_active_area);
				if(area)
				{
					auto center = area->GetArea().GetCenter();
					area->NoTrig();
					target_map->player_->SetPosition(center);
				}
			}
		}));

	}

	EntityInfo* Map::GetEntityByID(int id)
	{
		for(auto & entity : entities_)
		{
			if(entity.id == id)
			{
				return &entity;
			}
		}
		return nullptr;
	}

	void Map::WalkObjectTo(Object *object, const Point2F &point)
	{
		object->GoTo(point);
	}

	std::vector<EntityInfo> Map::GetEntityByNameOrID(const std::string &name_id)
	{
		try
		{
			//see if the string is an id
			EntityInfo* entity_info = GetEntityByID(std::stoi(name_id));
			if(entity_info)
			{
				return {*entity_info};
			}
		}
		catch(const std::exception & e)
		{
			return GetEntitiesByName(name_id);
		}
	}

	World * Map::GetWorld()
	{
		return &world_;
	}
}