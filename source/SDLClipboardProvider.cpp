#include "SDLClipboardProvider.h"

namespace CEGUI
{
	void SDLClipboardProvider::sendToClipboard(const String & mimeType, void * buffer, size_t size)
	{
		SDL_SetClipboardText((char*)(buffer));
	}

	void SDLClipboardProvider::retrieveFromClipboard(String & mimeType, void *& buffer, size_t & size)
	{
		mimeType = "text/plain";
		text = SDL_GetClipboardText();
		buffer = text;
		size = strlen(text);
	}

	void SDLClipboardProvider::Deallocate()
	{
		SDL_free(text);
	}
	

}