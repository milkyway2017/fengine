#include "GUI.h"

namespace Fengine
{
	CEGUI::Window * GUI::LoadWindow(const std::string & path)
	{
		try
		{
			if(loaded_windows_.count(path))
			{
				//try to return
				auto * NewWindow = loaded_windows_[path]->clone(true);
				NewWindow->setName(NewWindow->getName() + /*generate a unique name*/std::to_string(name_counter_));
				name_counter_++;			//increment
				return NewWindow;
			}
			else
			{
				//not loaded, load it
				CEGUI::Window* wnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile(path);

				loaded_windows_.insert(std::pair<std::string, CEGUI::Window*>
					(path, wnd));

				auto * NewWindow = wnd->clone(true);
				NewWindow->setName(NewWindow->getName() + /*generate a unique name*/std::to_string(name_counter_));
				name_counter_++;			//increment
				return NewWindow;
			}
		}
		catch (const CEGUI::Exception & e)
		{
			throw Exception({"CEGUI loading error",e.getMessage().c_str()}, true);
		}
	}
	GUI::GUI():
		name_counter_(0)
	{//init the CEGUI system
		opengl_renderer_ = &CEGUI::OpenGL3Renderer::bootstrapSystem();
		//set default paths for resources
		try
		{
			// initialise the required dirs for the DefaultResourceProvider
			CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>(CEGUI::System::getSingleton().getResourceProvider());
			rp->setResourceGroupDirectory("schemes", "Resources/GUI/schemes/");
			rp->setResourceGroupDirectory("imagesets", "Resources/GUI/imagesets/");
			rp->setResourceGroupDirectory("fonts", "Resources/Fonts");
			rp->setResourceGroupDirectory("layouts", "Resources/GUI/layouts/");
			rp->setResourceGroupDirectory("looknfeels", "Resources/GUI/looknfeel/");
			rp->setResourceGroupDirectory("lua_scripts", "Resources/GUI/lua_scripts/");
			// This is only really needed if you are using Xerces and need to
			// specify the schemas location
			rp->setResourceGroupDirectory("schemas", "Resources/GUI/xml_schemas/");

			// set the default resource groups to be used
			CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
			CEGUI::Font::setDefaultResourceGroup("fonts");
			CEGUI::Scheme::setDefaultResourceGroup("schemes");
			CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
			CEGUI::WindowManager::setDefaultResourceGroup("layouts");
			CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");
			// setup default group for validation schemas
			CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
			if (parser->isPropertyPresent("SchemaDefaultResourceGroup"))
				parser->setProperty("SchemaDefaultResourceGroup", "schemas");

			//Load schemes and fonts
			CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
			CEGUI::SchemeManager::getSingleton().createFromFile("VanillaSkin.scheme");
			CEGUI::SchemeManager::getSingleton().createFromFile("WindowsLook.scheme");

			//hide cursor
			SDL_ShowCursor(0);

			//set clipboard provider to SDLClipboardProvider
			CEGUI::System::getSingleton().getClipboard()->setNativeProvider(CEGUI::SDLClipboardProvider::GetInstance());
		}
		catch (CEGUI::Exception& e)
		{
			printf("CEGUI Error: %s\n", e.getMessage().c_str());
			throw("GUI init Error");
		}
	}
}