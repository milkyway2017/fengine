#include "Object.h"
#include "Map.h"

namespace Fengine
{
	Object::Object(ObjectType objType) :
		is_init_(false),
		item_selected_(0),
		is_following_path_(false),
		obj_type_(objType),
		highlight_color_(Graphics::GetInstance()->GetColor("Green"))
	{
		//set to transparent
		highlight_color_ = highlight_color_.ToTransparent(0.3f);

		//setup body
		body_.type_ = RECT_SHAPE;
		body_.is_movable_ = true;
		body_.is_solid_ = true;
		body_.Attach(&sprite_rect_.GetXY());
	}

	void Object::Init_Obj(Map *map, Rect4F collision_rect, GLuint shader_program, int id)
	{
		//assign basic stuff
		shader_program_ = shader_program;
		is_visible_ = true;
		this->map = map;
		id_ = id;

		body_.collision_rect_ = collision_rect;

		is_init_ = true;
		ConfigureZOrder();

		//subscribe the whole Object class to map's event_manager_'s debugmode event
		//we don't need to check whether we have already because SubscribeEvent will check
		map->GetEventManager()->SubscribeEvent(int(MapEventManagerEventType::DEBUGMODE), Functor(&ToggleDebugMode));
	}

	bool Object::is_debugging_ = false;

	Object::~Object()
	{
	}

	bool Object::PickUpItem(GameItem* item, unsigned int index)
	{
		//check if index is invalid or the slot is occupied
		if(index >= item_list_.size() || item_list_[index] != nullptr)
		{
			return false;
		}
		//if the item is already owned by another object, don't add
		if (!item->HasOwner())
		{
			//add it to the item_list_
			item_list_[index] = item;
			item_list_[index]->Own(this);

			//move the item to Object's collision box center
			item_list_[index]->MoveTo(GetCenter());

			//attach item to body
			body_.Attach(&item_list_[index]->GetLocation());

			//when picking up an item, fire an event
			map->GetEventManager()->FireEvent(Event(MAP_EVENTMANAGER, int(MapEventManagerEventType::ITEM_PICKEDUP), "Object", this));
			return true;
		}
		return false;
	}

	void Object::Die()
	{
		//drop all the items
		for (auto i : item_list_)
		{
			if(i)
				i->Drop();
		}

		//fire DIE event
		map->GetEventManager()->FireEvent(Event(MAP_EVENTMANAGER, (int)MapEventManagerEventType::DIE, "Object", this));
	}

	void Object::SendAttack(Object *other, float Damage)
	{
		//fire an attack event, map will subscribe to this event and do the rest for us
		map->GetEventManager()->FireEvent(Event(MAP_EVENTMANAGER,
			(int)MapEventManagerEventType::ATTACK,
			"Object",
			{ this,other },
			"(" + std::to_string(Damage) + ")"));
	}

	bool Object::DropItem(unsigned int i)
	{
		if(i < item_list_.size() && item_list_[i] != nullptr)
		{
			//drop
			item_list_[i]->Drop();

			//detach item from body
			body_.Detach(&item_list_[i]->GetLocation());

			//and reset slot back to nullptr
			item_list_[i] = nullptr;

			//when dropping an item, fire an event
			map->GetEventManager()->FireEvent(Event(MAP_EVENTMANAGER,
					int(MapEventManagerEventType::ITEM_DROPPED),
					"Object",
					this)
			);
			return true;
		}
		return false;
	}

	bool Object::DropItem()
	{
		return DropItem(item_selected_);
	}

	void Object::HandleEvent()
	{
		for (auto &e : EventQueue)
		{
			switch ((MapEventManagerEventType(e.command_type_)))
			{
				//Object does not handle any event right now
			default:
				//if not known to Object class, pass it to the derived class
				HandleSpecialEvent(e);
				break;
			}
		}
		EventQueue.clear();
	}


	void Object::GenPath(const Point2F & dest)
	{
		//get the list of tiles
		std::vector<Tile*> tile_path = map->FindPath(GetCenter(), dest);
		if (!tile_path.empty())
		{
			float diagonal_dist = (float)sqrt(pow(tile_path[0]->dest_rect_.w, 2)
				+ pow(tile_path[0]->dest_rect_.h, 2));				//the distance for a diagonal move
			float normal_dist = (float)tile_path[0]->dest_rect_.w;		//the distance for a normal move

			//start from the second tile
			for (unsigned int i = 1; i < tile_path.size(); i++)
			{
				//find out the relative position of this tile from last tile
				auto Delta = tile_path[i]->GetCenter() - tile_path[i - 1]->GetCenter();

				//determine the corresponding move

				if (Delta.x > 0 && Delta.y > 0)
					obj_path_.AddMove(diagonal_dist, SOUTH_EAST);
				else if (Delta.x == 0 && Delta.y > 0)
					obj_path_.AddMove(normal_dist, SOUTH);
				else if (Delta.x > 0 && Delta.y == 0)
					obj_path_.AddMove(normal_dist, EAST);
				else if (Delta.x < 0 && Delta.y < 0)
					obj_path_.AddMove(diagonal_dist, NORTH_WEST);
				else if (Delta.x < 0 && Delta.y == 0)
					obj_path_.AddMove(normal_dist, WEST);
				else if (Delta.x == 0 && Delta.y < 0)
					obj_path_.AddMove(normal_dist, NORTH);
				else if (Delta.x < 0 && Delta.y>0)
					obj_path_.AddMove(diagonal_dist, SOUTH_WEST);
				else if (Delta.x > 0 && Delta.y < 0)
					obj_path_.AddMove(diagonal_dist, NORTH_EAST);
			}
		}
	}

	void Object::BeginFollowing(Object* target)
	{
		if (target == nullptr)
			return;
		this->target_following_ = target;
		is_following_path_ = true;
		body_.is_recording_ = true;
		body_.dist_travelled_ = 0;
		GenPath(target->GetCenter());
		target_tile_ = map->GetInvolvedTile(target->GetCenter());
	}

	void Object::EndFollowing()
	{
		//clear all navigation related variables
		obj_path_.ResetPath();
		is_following_path_ = false;
		body_.is_recording_ = false;
		target_tile_ = nullptr;
	}

	void Object::ConfigureZOrder()
	{
		z_order_.Init(&body_.collision_rect_.y, std::bind(&Object::Draw, this), body_.collision_rect_.h);
	}

	void Object::StopWalking()
	{
		//if the object is stopping or already stopped , do nothing
		auto motion_dir = body_.motion_.GetAccelerationDirection();
		if (body_.motion_.IsStopping() || motion_dir.IsUndefined())
			return;
		SetMovementState(motion_dir.Simplify(), IDLE);
	}

	bool Object::IsMoving()
	{
		return body_.motion_.GetAcceleration();
	}

	void Object::SetMovementState(Angle direction, Status status)
	{
		body_animation_.PlayAction((int)status + direction.GetDegree());		//play current body animation(action id = status_ * angle)
		float acceleration_factor = (body_.motion_.GetSpeed()->SumSpeed() / 80.f);
		if (status != IDLE)
			body_.motion_.SetAcceleration(Point2F(std::cos(direction.GetRadian()) * acceleration_factor,
				std::sin(direction.GetRadian()) * acceleration_factor));
		else
			body_.motion_.StopWithAcceleration();
	}

	void Object::Update(int ElapsedTime, void * Data)
	{
		//handle incoming events, for types of events, please refer to CommandDef.h
		HandleEvent();

		//handle navigation stuff
		if (is_following_path_)
		{
			HandleNavigation();
		}

		//upate Animations
		body_animation_.Update(ElapsedTime);
		for(auto & i : animation_list)
		{
			i->Update(ElapsedTime);
		}

		//update all items
		for (auto &i : item_list_)
		{
			if(i != nullptr)
				i->Update(ElapsedTime, map);
		}
	}

	int Object::GetItemSelected()	const
	{
		return item_selected_;
	}

	std::vector<GameItem*>& Object::GetItemList()
	{
		return item_list_;
	}

	Z_order * Object::GetZOrder()
	{
		return &z_order_;
	}

	Body* Object::GetBody()
	{
		return &body_;
	}

	void Object::Draw()
	{

		if (is_visible_)			//draws only if Object is visible
		{
			//draw items
			for (auto & i : item_list_)
			{
				if(i != nullptr)
					i->Draw();
			}

			//if the game is in debugging mode, draw debug shape
			if (is_debugging_)
			{
				Graphics::GetInstance()->DisplayDebugShape(body_.collision_rect_,
					highlight_color_,
					shader_program_);
			}

			//draw body
			body_animation_.Draw(shader_program_);

			//draw other animations
			for(auto & i : animation_list)
			{
				i->Draw(shader_program_);
			}
		}
	}

	Speed& Object::GetSpeed()
	{
		return *body_.motion_.GetSpeed();
	}

	ObjectType Object::GetType() const
	{
		return obj_type_;
	}

	void Object::SubscribeEvent(MapEventManagerEventType eventType, Functor functor)
	{
		//subscribe
		map->GetEventManager()->SubscribeEvent(int(eventType), functor);
		//append to subscribeEvents
		subscribedEvents.insert(std::pair<int, Functor>(int(eventType), functor));
	}

	void Object::UnsubscribeEvent(MapEventManagerEventType eventType, Functor functor)
	{
		auto it = subscribedEvents.find(int(eventType));
		if (it != subscribedEvents.end())
		{	//if the event is subscribed, unsub
			map->GetEventManager()->UnsubscribeEvent(int(eventType), functor);
			//remove it from subscribedEvents
			subscribedEvents.erase(it);
		}
	}

	void Object::ToggleDebugMode(void *e)
	{
		if (std::stoi(((Event*)e)->param_string_list_[0]))
			is_debugging_ = true;
		else
			is_debugging_ = false;
	}
	void Object::setHighlightColor(const GLColor &highlightColor)
	{
		Object::highlight_color_ = highlightColor;
	}
	const GLColor &Object::getHighlightColor() const
	{
		return highlight_color_;
	}

	void Object::SetPosition(const Point2F &position)
	{
		MoveObjectCenterTo(position);
	}

	void Object::HandleNavigation()
	{
		//if the current step in obj_path_ increased (by checking if the current one is different from the previous one)
		static int previousStepInMap = obj_path_.GetStepInPath(body_.dist_travelled_);
		int currentStepInMap = obj_path_.GetStepInPath(body_.dist_travelled_);
		if (previousStepInMap != currentStepInMap)
		{
			//if the Target moved to a different tile, TODO: recalculating the path every time is inefficient, needs to be improved, (should just add the new extra bit of path to the Path object)
			if (target_following_ && target_tile_ != map->GetInvolvedTile(target_following_->GetCenter()))
			{
				EndFollowing();
				BeginFollowing(target_following_);
			}
			//update the previous to current
			previousStepInMap = currentStepInMap;

		}
		//if GetStepInPath returns -1, navigation is complete
		if (currentStepInMap == -1)
		{
			EndFollowing();
			StopWalking();
			body_.is_recording_ = false;
			std::printf("nav complete\n");
		}
		else
			SetMovementState(obj_path_.GetDirection(body_.dist_travelled_));
	}


/*Path Class*****************************************************/
		/****************************************************************/
	Path::Path()
	{
	}
	Path::~Path()
	{
	}
	void Path::AddMove(float Distance, Angle status)
	{
		if (PathInfo.begin() == PathInfo.end())
			//if no key in map, this is the first move
		{
			//start...
			PathInfo.insert(std::pair<float, Angle>(0.f, status));
			//stop after certain distance has passed
			PathInfo.insert(std::pair<float, Angle>(Distance, Angle()));
		}
		else
		{
			//change last element
			PathInfo.rbegin()->second = status;

			//set new stop...
			PathInfo.insert(std::pair<float, Angle>(PathInfo.rbegin()->first + Distance, Angle()));
		}
	}
	void Path::ResetPath()
	{
		PathInfo.clear();
	}

	int Path::GetStepInPath(float Distance)
	{

		//loop through the map backwards and find the first distance that is less than current distance
		unsigned int c = 0;
		for (auto i = PathInfo.rbegin(); i != PathInfo.rend() && c < PathInfo.size(); i++, c++)
		{
			if (i->first <= Distance)
			{
				if (c == 0)
					return -1;
				return PathInfo.size() - c;
			}
		}

		return -1;
	}

	Angle Path::GetDirection(float Distance)
	{

		//loop through the map backwards and find the first distance that is less than current time

		unsigned int c = 0;
		for (auto i = PathInfo.rbegin(); i != PathInfo.rend() && c < PathInfo.size(); i++, c++)
		{
			if (i->first <= Distance)
			{
				if (c == 0)
					return Angle();
				return i->second;
			}
		}

		return Angle();
	}

	bool Object::IsPointInSprite(const Point2F & point) const
	{
		return body_animation_.IsPointInShape(point);
	}

	void Object::GoTo(const Point2F &point)
	{
		this->target_following_ = nullptr;
		is_following_path_ = true;
		body_.is_recording_ = true;
		body_.dist_travelled_ = 0;
		GenPath(point);
		target_tile_ = map->GetInvolvedTile(point);
	}

	Point2F Object::GetCenter()
	{
		return body_.GetCenter();
	}
}