#pragma once
#include "Flashlight.h"
#include "Map.h"
#include "Graphics.h"
#include "Figure.h"
#include <glm/gtc/type_ptr.hpp>

namespace Fengine
{
	Flashlight::Flashlight(Map*map, GLuint ShaderProgram, int castRange, float castAngle, const std::string & droppedImagePath, const std::string & lightImagePath, Point2F Location, int id, Object* Owner) :
		GameItem(FLASHLIGHT, map, ShaderProgram, Owner, id, Location),
		map_points_(nullptr),
		map_lines_(nullptr)
	{
		//init light(turned on) and mask (dropped and turned off)
		light_sprite_.Init(lightImagePath, Rect4F(0, 0, 0, 0)/*we dont care about it for this case*/);
		icon_.Init(droppedImagePath, Rect4F(location_, Constants::ItemSpriteSize));

		RegisterShape(shape_);

		//if there is no owner, use the location passed in
		if (Owner == nullptr)
		{
			location_ = Location;
		}
		else		//if there is owner, just set to to owner
		{
			location_ = Owner->GetCenter();
		}
		Activate(false);			//set to inactivated by default
		Init(map);
		ConfigureZOrder();

		//init the cast square
		cast_side_len_ = float(castRange * 2);
		cast_square_ = Rect4F(location_.x - (cast_side_len_ / 2.0f), location_.y - (cast_side_len_ / 2.0f), cast_side_len_, cast_side_len_);
	}

	Flashlight::Flashlight(const std::string & type, Point2F location, GLuint shaderProgram, Map* map, int id, Object* owner) :
		GameItem(FLASHLIGHT, map, shaderProgram, owner, id, location),
		map_points_(nullptr),
		map_lines_(nullptr)
	{
		//init light(turned on) and mask (dropped and turned off)
		light_sprite_.Init("lightmask.png", Rect4F(0, 0, 0, 0)/*we dont care about it for this case*/);
		icon_.Init("flashlight4.png", Rect4F(location, Constants::ItemSpriteSize));

		RegisterShape(shape_);

		if (owner_ == nullptr)
		{
			this->location_ = location;
		}
		else
		{
			this->location_ = owner_->GetCenter();
		}
		Activate(false);			//set to inactivated by default
		Init(map);
		ConfigureZOrder();

		//init the cast square
		cast_side_len_ = def_map_[type].def_Range * 2;
		light_color_ = def_map_[type].def_Color;

		cast_square_ = Rect4F(location_.x - (cast_side_len_ / 2.0f), location_.y - (cast_side_len_ / 2.0f), cast_side_len_, cast_side_len_);
	}


	Flashlight::~Flashlight()
	{
	}

	void Flashlight::Update(Uint32 ElapsedTime, Map* MapInfo)
	{

		if (is_active_)
		{
			//change states only

			//does nothing, because things are not settled until the draw call
			//update shape in draw call

		}
	}
	void Flashlight::Init(Map * map)
	{
		//point the pointer to the vector<Point2F> provided by map
		this->map_points_ = &map->intersect_points_;
		this->map_lines_ = &map->lines_;

		//color
		light_color_ = Graphics::GetInstance()->GetColor("Blue");
	}

	void Flashlight::PackShape()
	{
		shape_.vertices.clear();
		std::sort(lines_.begin(), lines_.end());

		//two lines form one triangle
		for (unsigned int i = 0; i < lines_.size(); i++)
		{
			if ((unsigned int)i + 1 < lines_.size())
			{
				shape_.AddVertex({ lines_[i].P1,light_color_ });
				shape_.AddVertex({ lines_[i].P2,light_color_ });

				shape_.AddVertex({ lines_[i + 1].P2,light_color_ });
			}
			else
			{
				shape_.AddVertex({ lines_[i].P1,light_color_ });
				shape_.AddVertex({ lines_[i].P2,light_color_ });

				shape_.AddVertex({ lines_[0].P2,light_color_ });
			}
		}
		Graphics::GetInstance()->BindVAO(shape_.vao_id);
		Graphics::GetInstance()->BindVBO(shape_.vbo_id);
		ResizeShapeBuffer(shape_);
	}

	void Flashlight::GenPointsFromSys()
	{
		//find all points that are within the cast_square, only worry about these points
		near_by_direct_points = VerticesInRect(cast_square_, *map_points_);

		//find all lines in cast_square
		line_in_range_.clear();
		auto cast_rect = cast_square_;

		for (auto& map_line : *map_lines_)
		{
			if (PointInRect(&map_line.P1, &cast_rect) || PointInRect(&map_line.P2, &cast_rect))
			{
				line_in_range_.push_back(map_line);
				continue;
			}
			if (DoSegmentandRectIntersect(cast_rect, map_line))
			{
				line_in_range_.push_back(map_line);
				continue;
			}
		}

		//add cast_square as boundary lines
		auto boundary_lines = GenLines(GenShape(cast_square_,false));
		line_in_range_.insert(line_in_range_.end(), boundary_lines.begin(), boundary_lines.end());

		//add the intersection between cast_square and map_lines_ to near_by_direct_points
		std::vector<Point2F> intersections = FindABIntersections(line_in_range_, boundary_lines);
		near_by_direct_points.insert(near_by_direct_points.end(), intersections.begin(), intersections.end());

		intersections = FindVerticesandIntersections(boundary_lines);
		near_by_direct_points.insert(near_by_direct_points.end(), intersections.begin(), intersections.end());
	}

	void Flashlight::GenLinesFromPoints()
	{
		lines_.clear();
		for (auto& near_by_direct_point : near_by_direct_points)
		{
			lines_.emplace_back(location_, near_by_direct_point);
		}

		FindNeighboringLines();
	}
	void Flashlight::FindNeighboringLines()
	{
		std::vector<Line> neighbor_lines;
		auto angle_ranges = GenerateAngleRangeList(line_in_range_, location_);

		for (auto& line : lines_)
		{
			auto angle = FindAngle(line.P2 - line.P1);

			float dist1 = Dist(line.P1, line.P2);//before dist

			ExtendRayInSys(line, line_in_range_, angle_ranges);

			float dist2 = Dist(line.P1, line.P2);//after dist

			if (dist1 == dist2)
			{
				angle = angle + Angle(0.005f);
				{
					neighbor_lines.emplace_back(Line(location_, location_ + (Point2F(cos(angle.GetRadian()), sin(angle.GetRadian()))*dist1)));

					//extend it
					ExtendRayInSys(neighbor_lines[neighbor_lines.size() - 1], line_in_range_, angle_ranges);
				}

				angle = angle - Angle(0.01f);
				{
					neighbor_lines.emplace_back(Line(location_, location_ + (Point2F(cos(angle.GetRadian()), sin(angle.GetRadian()))*dist1)));

					//extend it
					ExtendRayInSys(neighbor_lines[neighbor_lines.size() - 1], line_in_range_, angle_ranges);
				}
			}
		}
		//add the neighbour lines to our Lines
		lines_.insert(lines_.end(), neighbor_lines.begin(), neighbor_lines.end());
	}
	void Flashlight::Draw()
	{
		//update flashlight location
		Move(GameItem::location_);
		if (is_active_)
		{
			//update shape

			//collecting all the points and lines, preparing for ray casting (
			GenPointsFromSys();


			GenLinesFromPoints();


			PackShape();

			//Shape.vertices.clear();
			//for (int i = 0; i < Lines.size(); i++)
			//{
			//	Shape.AddVertex({ Lines[i].P1,Color });
			//	Shape.AddVertex({ Lines[i].P2,Color });
			//}
			//Graphics::GetInstance()->BindVAO(Shape.vao_id);
			//Graphics::GetInstance()->BindVBO(Shape.vbo_id);
			//ResizeShapeBuffer(Shape);

			//Graphics::GetInstance()->DrawLines(shader_program_, Shape);
			//tell the shader about our topleft corner so that it can calculate the topleft texture UV

			///set uniforms <CameraMatrix> ,<FlashLightTopLeft> and <FlashlightRange>
			Graphics::GetInstance()->BindProgram(GetFlashlightShader());
			Point2F TopLeft = (location_ - Point2F(cast_side_len_ / 2, cast_side_len_ / 2));
			glUniform2fv(TopLeftUniLoc(), 1, &TopLeft.x);
			glUniformMatrix4fv(CameraMatrixLoc(), 1, GL_FALSE, glm::value_ptr(map_->camera_.camera_matrix_));
			glUniform1i(RangeLoc(), (int)cast_side_len_);
			//draw
			light_sprite_.Draw(GetFlashlightShader(), shape_);

		}
		else
		{
			icon_.Draw(shader_program_);
		}

	}

	void Flashlight::Move(Point2F deltaXY)
	{
		location_ += deltaXY;
		icon_.shape_.Move(deltaXY);
		location_.x += deltaXY.x;
		location_.y += deltaXY.y;

		cast_square_.GetXY() += deltaXY;
	}
	void Flashlight::MoveTo(Point2F location)
	{
		icon_.MoveSpriteCenterTo(location);
		location_ = location;

		//Update Cast Square
		cast_square_.SetCenter(location);
	}
	void Flashlight::Activate(bool OnOff)
	{
		GameItem::Activate(OnOff);
	}

	Flashlight::Flashlight(const Flashlight & other) :
		GameItem(other),
		cast_side_len_(other.cast_side_len_),
		casting_angle_(other.casting_angle_),
		light_color_(other.light_color_),
		light_sprite_(other.light_sprite_)
	{
		RegisterShape(shape_);
	}
	Flashlight* Flashlight::Clone()
	{
		Flashlight* temp = new Flashlight(*this);
		temp->Init(map_);
		return temp;
	}
	void Flashlight::ConfigureZOrder()
	{
		z_.Init(&location_.y, std::bind(&GameItem::Draw, this), 0);
	}

	void Flashlight::LoadDef(const std::string &defPath)
	{
		//since there can be more than one kind of Flashlight Def
		//we need to...
		//get the file name component of this path, and make it the name of this Def
		int begin = defPath.rfind('/') + 1;
		int end = defPath.rfind("Def");
		std::string defName = defPath.substr(begin, end - begin);

		//check if this def already exists, if exists return
		if (def_map_.find(defName) != def_map_.end())
			return;

		FlashlightDef defStruct;							//the structure that holds the information loaded from this ObjDef

		using json = nlohmann::json;
		std::ifstream defFile(defPath);

		//find the Def ObjectGroup
		json Def;
		defFile >> Def;
		Def = Def["layers"];

		bool defFound = false;
		for (auto i : Def)
		{//loop through and find the layer with the name "Def"
			try
			{
				using namespace std;
				string name = i["name"];
				int x = i["x"];
			}
			catch (const std::exception & e)
			{
				std::cout << e.what() << std::endl;
			}
			if (i["name"] == "Def")
			{
				Def = i;
				defFound = true;
				break;
			}
		}
		if (!defFound)
		{
			//throw
			throw Exception({ "Flashlight Def Loading", "failed to find def objectgroup" }, false);
		}

		//get all the properties
		try
		{
			json defProperties = Def["properties"];

			//get the angle
			defStruct.def_Angle = Angle((int)defProperties["Angle"]);
			//get the range
			defStruct.def_Range = defProperties["Range"];


			//get the color, this is a bit more complicated, as there are 4 components of a color and they are all in HEX e.g. "#ffaa55ff"
			std::string colorString = defProperties["Color"];
			colorString.erase(0, 1);			//erase the '#' in front

			//since the rest-"ffaa55ff" is in "AARRGGBB", we can get each component, but there is a chance this colour does not have the "AA" part
			int r, g, b, a = 255;
			if (colorString.length() == 8)		//with 'AA'
			{
				std::istringstream(colorString.substr(0, 2)) >> std::hex >> a;
				std::istringstream(colorString.substr(2, 2)) >> std::hex >> r;
				std::istringstream(colorString.substr(4, 2)) >> std::hex >> g;
				std::istringstream(colorString.substr(6, 2)) >> std::hex >> b;
			}
			else if (colorString.length() == 6)	//without 'AA'
			{
				std::istringstream(colorString.substr(0, 2)) >> std::hex >> r;
				std::istringstream(colorString.substr(2, 2)) >> std::hex >> g;
				std::istringstream(colorString.substr(4, 2)) >> std::hex >> b;
				defStruct.def_Color.a = 255.f;
			}
			else								//the color is invalid, throw
			{
				throw Exception({ "Flashlight Def Loading", "invalid flashlight colour: " + colorString }, false);
			}

			//since GLColor stores all values in range 0~1
			defStruct.def_Color.a = a / 255.f;
			defStruct.def_Color.r = r / 255.f;
			defStruct.def_Color.b = b / 255.f;
			defStruct.def_Color.g = g / 255.f;

			//done reading in
			//store the data read in

			def_map_.insert(std::pair<std::string, const FlashlightDef>(defName, defStruct));
		}
		catch (const std::exception & e)
		{
			throw Exception({ "Flashlight Def Loading", "error reading ObjDef file", e.what() }, false);
		}

	}

	std::map<std::string, const FlashlightDef> Flashlight::def_map_ = std::map<std::string, const FlashlightDef>();
}