#include "Logger.h"

namespace Fengine
{
	Logger::Logger()
	{
		log_.reserve(100000);							//reserve some space
	}

	void Logger::WriteToFile(const std::string & file_path)
	{
#ifdef WIN32
		//windows stype
		std::ofstream file(file_path);
#else
		//UNIX style
		std::ofstream file(file_path);
#endif
		file << log_;
		file.close();
	}

	void Logger::AddLine(const std::string & line)
	{
		log_.append(line);
		log_.append("\n");
	}
}