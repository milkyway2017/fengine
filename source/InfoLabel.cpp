#include "InfoLabel.h"

namespace Fengine
{
	InfoLabel::InfoLabel():
		default_font_(Graphics::GetInstance()->GetFont("consola.ttf", 20))
	{
	}


	void InfoLabel::Update()
	{
		int StrCount = 0;
		for (auto i = StringTable.begin(); i!= StringTable.end();)
		{
			auto& t = *i;
			Sprite& texture = std::get<3>(t);
			std::string & oldstring = std::get<5>(t);			//string drawn last time
			std::string & newstring = std::get<1>(t);
			if (std::get<4>(t))			//check if used this frame
			{
				if (oldstring != newstring)		//check if texture needs to be updated
				{
					TTF_Font* font = std::get<2>(t);


					if (texture.sprite_sheet_ != 0)
					{
						glDeleteTextures(1, &texture.sprite_sheet_);
					}
					SDL_Surface* s = TTF_RenderText_Blended(font, newstring.c_str(), { 255,255,255,255 });
					texture.Init(s, { 0,(float)20*StrCount,(float)s->w,(float)s->h}, false);
					SDL_FreeSurface(s);

					oldstring = newstring;

				}
				std::get<4>(t) = false;
					i++;
					StrCount++;
			}
			else						//if it's not used in this frame, remove it
			{
				if (texture.sprite_sheet_ != 0)			//free memory 
				{
					glDeleteTextures(1, &texture.sprite_sheet_);
				}
				UnregisterShape(texture.shape_);
				i = StringTable.erase(i);
			}
		}
	}


	void InfoLabel::DisplayString(const std::string & name, const std::string & text, TTF_Font * font)
	{
		if(!font)
		{
			font = default_font_;
		}
		//if the string of the same name exists already, just update the string (and font)
		for (auto& i : StringTable)
		{
			if (std::get<0>(i) == name)
			{
				std::get<1>(i) = text;				//update text
				std::get<2>(i) = font;				//update font
				std::get<4>(i) = true;				//set active

				return;
			}
		}
		//if does not exist
		Sprite sprite;
		sprite.Init();
		std::tuple<std::string, std::string, TTF_Font*, Sprite, bool, std::string> t{name,text,font,sprite,true,""};
		StringTable.push_back(t);
		return;
	}

	void InfoLabel::Draw()
	{
		//update textures
		Update();
		
		for (auto &i : StringTable)
		{
			auto& sprite = std::get<3>(i);
			sprite.Draw(Graphics::GetInstance()->GetShaderProgram("Normal Rendering"),{},Rect4F());
		}
	}

	InfoLabel::~InfoLabel()
	{
	}
}