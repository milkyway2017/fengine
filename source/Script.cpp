#include "Script.h"
#include "Map.h"

namespace Fengine
{
	Script::Script(Map * map, const std::string & script_path, const std::function<bool(void)> & should_run_script) :
		map_(map),
		should_run_script_(should_run_script)
	{
		//initialize functors
		InitFunctors();

		std::ifstream script_file(script_path);
		if (script_file.good())
		{
			//log some stuff just like every other compiler
			std::string line;
			int line_count = 0;
			while (std::getline(script_file, line))
			{
				line_count++;
				//remove leading and trailing spaces
				line = TrimString(line);
				//turn "\\n" into real line breakers like "\n"
				FindAndReplaceString(line, "\\n", "\n");
				if (line.empty())
				{
					//this line is insignificant
					//continue
					continue;
				}
				std::cout << "parsing line " + std::to_string(line_count) + ": \"" << line << "\"\n";
				//get the first token containing the command type
				std::string command_token = GetSubStrBeforeChar(line, ' ');
				command_token = ToUpperString(command_token);
				line = GetSubStrAfterChar(line, ' ');

				bool is_line_valid = false;
				ScriptCommand new_command;				//only added to steps_ if is_line_valid

				if (command_token == "TAKEOVERPLAYERCONTROL")
				{
					is_line_valid = ParseCommand(new_command, map->take_over_p_c_functor_, line, TAKE_OVER_PLAYER_CONTROL, { PARAMTYPE_BOOL }, line_count);
				}
				else if (command_token == "CHARACTERSPEAK")
				{
					is_line_valid = ParseCommand(new_command, map->character_speak_functor_, line, CHARACTER_SPEAK, { PARAMTYPE_STRING, PARAMTYPE_STRING }, line_count);
				}
				else if (command_token == "WAITFORKEYPRESS")
				{
					//no parameters required
					new_command = ScriptCommand(WAIT_FOR_KEY_PRESS, MapEventManagerEventType::KEY_PRESSED);
					is_line_valid = true;
				}
				else if (command_token == "WAITFOR")
				{
					std::vector<ScriptCommandParam> param_list;
					auto error_message = ScriptCommandParam::ParseParamFromText(line, &param_list, { PARAMTYPE_INT });
					if (!error_message.empty())
					{
						std::cout << "(line " << line_count << ") " << error_message << '\n';
						is_bad_ = true;
					}
					new_command = ScriptCommand(param_list[0]);
					is_line_valid = true;
				}
				else if (command_token == "ENDSPEAK")
				{
					new_command = ScriptCommand(END_SPEAK, map_->end_speak_);
					is_line_valid = true;
				}
				else if (command_token == "SHOWHINTTEXT")
				{
					is_line_valid = ParseCommand(new_command, map_->show_hint_text_, line, SHOW_HINT_TEXT, { PARAMTYPE_STRING }, line_count);
				}
				else if (command_token == "DELETEHINTTEXT")
				{
					is_line_valid = ParseCommand(new_command, map_->delete_hint_text_, line, DELETE_HINT_TEXT, { PARAMTYPE_STRING }, line_count);
				}
				else if (command_token == "WALKOBJECTTO")
				{
					is_line_valid = ParseCommand(new_command, map_->walk_object_to_, line, WALK_OBJECT_TO, { PARAMTYPE_STRING, PARAMTYPE_POINT }, line_count);
				}
				else if (command_token == "FADEFIGURE")
				{
					is_line_valid = ParseCommand(new_command, map_->fade_figure_, line, FADE_FIGURE, { PARAMTYPE_STRING, PARAMTYPE_FLOAT, PARAMTYPE_INT }, line_count);
				}
				else if (command_token == "ZOOMCAMERA")
				{
					is_line_valid = ParseCommand(new_command, map_->zoom_camera_, line, ZOOM_CAMERA, { PARAMTYPE_FLOAT, PARAMTYPE_INT }, line_count);
				}
				else if (command_token == "TRANSLATECAMERA")
				{
					is_line_valid = ParseCommand(new_command, map_->translate_camera_, line, TRANSLATE_CAMERA, { PARAMTYPE_POINT, PARAMTYPE_INT }, line_count);
				}
				else if(command_token == "SETCAMERATOPLAYER")
				{
					new_command = ScriptCommand(SET_CAMERA_TO_PLAYER, map_->set_camera_to_player_);
					is_line_valid = true;
				}
				else
				{
					std::cout << "(line " << line_count << ") " << "invalid command: " << command_token << '\n';
					is_bad_ = true;
					continue;
				}

				//add the parsed command
				if (is_line_valid)
				{
					steps_.push_back(new_command);
				}
			}
		}
		else
		{
			//file not found
			is_bad_ = true;
		}
	}

	void Script::Run()
	{
		if (!is_running_)
		{
			is_running_ = true;

			//case 1: Run() is called for the first time, nothing to unpause
			//case 2: Run() is called after a Pause() call, unpause stuff

			//case 1
			if (!is_paused_)
			{
				RunNext();
			}

			//case 2
			else
			{
				is_paused_ = false;
				if (steps_[current_step_].type != WAIT_FOR_KEY_PRESS)
				{
					current_timer_->Unpause();
				}
				else
				{
					map_->GetEventManager()->SubscribeEvent((int)steps_[current_step_].event_type, call_run_next_);
				}
			}
		}
	}

	void Script::Pause()
	{
		//if not already paused and is running (making sure that the script is at least running)
		if (!is_paused_ && is_running_)
		{
			is_paused_ = true;
			//is_running_ should be untouched, as when a script is paused, it still counts as a running script

			if (steps_[current_step_].type != WAIT_FOR_KEY_PRESS)
			{
				current_timer_->Pause();
			}
			else
			{
				map_->GetEventManager()->UnsubscribeEvent((int)steps_[current_step_].event_type, call_run_next_);
			}
		}
	}

	void Script::RunNext()
	{
		//check if is at the end of script
		if (current_step_ + 1 == steps_.size())
		{
			//decrease run_for_
			//run_for_--;
			is_running_ = false;
			return;
		}
		current_step_++; std::cout << current_step_ << '\n';
		//check what kind of task is being run
		if (steps_[current_step_].type == WAIT_FOR)
		{
			//to wait for some time, simply set a timer for RunNext so the script will continue after that time
			current_timer_ = Timer::AddTimer(
				(Uint32)(int)steps_[current_step_].param_list[0],		//get amount of time to wait
				call_run_next_,										//functor to RunNext(void * )
				nullptr);											//no param for RunNext
		}
		else if (steps_[current_step_].type == WAIT_FOR_KEY_PRESS)
		{
			//wait for event with a functor of RunNext
			map_->GetEventManager()->SubscribeEvent((int)steps_[current_step_].event_type,
				call_run_next_);
			ShowPressAnyKeyToContinue(true);
		}
		else
		{
			//call functor & pass in param list
			steps_[current_step_].task(&steps_[current_step_].param_list);
			call_run_next_(nullptr);
		}
	}

	bool Script::ShouldRunScript()
	{
		return should_run_script_();
	}

	void Script::ShowPressAnyKeyToContinue(bool on_off)
	{
		if (on_off)
		{
			map_->ShowHintText("press any key to continue...");
		}
		else
		{
			map_->DeleteHintText("press any key to continue...");
		}
	}

	Script::Script(const Script &other) :
		map_(other.map_),
		current_step_(other.current_step_),
		is_bad_(other.is_bad_),
		is_running_(other.is_running_),
		is_paused_(other.is_paused_),
		steps_(other.steps_),
		should_run_script_(other.should_run_script_),
		current_timer_(nullptr)
	{
		//initialize functors
		InitFunctors();

		run_for_a_.store(other.run_for_a_.load());
		if (other.is_running_)
			throw Exception("cannot copy a running script", true);
	}

	bool Script::ParseCommand(ScriptCommand& command, const Functor & f, const std::string & line, ScriptCommandType type, std::initializer_list<ScriptCommandParamType> target_types, unsigned int line_count) {
		std::vector<ScriptCommandParam> param_list;
		auto error_message = ScriptCommandParam::ParseParamFromText(line, &param_list, target_types);
		if (!error_message.empty())
		{
			std::cout << "(line " << line_count << ") " << error_message << '\n';
			is_bad_ = true;
			return false;
		}
		command = ScriptCommand(type, f, param_list);
		return true;
	}

	void Script::InitFunctors()
	{
		//generate call_run_next
		call_run_next_ = Functor([this](void* v)
		{
			//if the cause is WaitForKeyPress, remove the hint text "press any key to continue..."
			if (v)
			{
				if (MapEventManagerEventType((static_cast<Command*>(v)->command_type_)) == MapEventManagerEventType::KEY_PRESSED)
				{
					//unsubscribe event
					map_->GetEventManager()->UnsubscribeEvent((int)steps_[current_step_].event_type, call_run_next_);
					ShowPressAnyKeyToContinue(false);
				}
			}

			//reset timer
			current_timer_ = nullptr;

			this->RunNext();
		});
	}

	/*
	Script &Script::operator=(const Script &other)
	{
		return <#initializer#>;
	}*/

	ScriptCommandParam::ScriptCommandParam(const std::string & string) :
		type(PARAMTYPE_STRING),
		string_(string)
	{
	}

	ScriptCommandParam::ScriptCommandParam(int integer) :
		type(PARAMTYPE_INT),
		integer_(integer)
	{
	}

	ScriptCommandParam::ScriptCommandParam(float decimal) :
		type(PARAMTYPE_FLOAT),
		decimal_(decimal)
	{
	}

	ScriptCommandParam::ScriptCommandParam(bool boolean) :
		type(PARAMTYPE_BOOL),
		boolean_(boolean)
	{
	}

	ScriptCommandParam::ScriptCommandParam(const Rect4F & rect) :
		type(PARAMTYPE_RECT),
		rect_(rect)
	{
	}

	ScriptCommandParam::ScriptCommandParam(const Point2F & point) :
		type(PARAMTYPE_POINT),
		rect_(point, 0.f, 0.f)				//just don't use the w and h
	{
	}

	ScriptCommandParam::ScriptCommandParam(void * pointer) :
		type(PARAMTYPE_POINTER),
		pointer_(pointer)
	{
	}

	ScriptCommandParam::operator std::string()	const
	{
		if (type == PARAMTYPE_STRING)
			return string_;
		return "invalid type cast";
	}

	ScriptCommandParam::operator int()	const
	{
		if (type == PARAMTYPE_INT)
			return integer_;
		return INT_MAX;
	}

	ScriptCommandParam::operator float()	const
	{
		if (type == PARAMTYPE_FLOAT)
			return decimal_;
		return FLT_MAX;
	}

	ScriptCommandParam::operator bool()	const
	{
		return boolean_;
	}

	ScriptCommandParam::operator void*()	const
	{
		if (type == PARAMTYPE_POINTER)
			return pointer_;
		return nullptr;
	}

	ScriptCommandParam::operator Rect4F() const
	{
		if (type == PARAMTYPE_RECT)
			return rect_;
		return Rect4F();
	}

	ScriptCommandParam::operator Point2F() const
	{
		if (type == PARAMTYPE_RECT || type == PARAMTYPE_POINT)
			return rect_.GetXY();
		return Point2F();
	}

	ScriptCommandParam::ScriptCommandParam(const ScriptCommandParam & other) :
		ScriptCommandParam()
	{
		*this = other;
	}
	ScriptCommandParam &ScriptCommandParam::operator=(const ScriptCommandParam &other)
	{
		type = other.type;
		switch (type)
		{
		case PARAMTYPE_STRING:
			this->string_ = other.string_;
			break;
		case PARAMTYPE_INT:
			this->integer_ = other.integer_;
			break;
		case PARAMTYPE_FLOAT:
			this->decimal_ = other.decimal_;
			break;
		case PARAMTYPE_BOOL:
			this->boolean_ = other.boolean_;
			break;
		case PARAMTYPE_POINTER:
			this->pointer_ = other.pointer_;
		case PARAMTYPE_RECT:
		case PARAMTYPE_POINT:
			this->rect_ = other.rect_;
		}
		return *this;
	}
	bool ScriptCommandParam::operator==(const ScriptCommandParam & other)
	{
		if (type != other.type)
			return false;
		switch (type)
		{
		case PARAMTYPE_STRING:
			return this->string_ == other.string_;
		case PARAMTYPE_INT:
			return this->integer_ == other.integer_;
		case PARAMTYPE_FLOAT:
			return this->decimal_ == other.decimal_;
		case PARAMTYPE_BOOL:
			return this->boolean_ == other.boolean_;
		case PARAMTYPE_POINTER:
			return this->pointer_ == other.pointer_;
		case PARAMTYPE_RECT:
		case PARAMTYPE_POINT:
			return this->rect_ == other.rect_;
		}
	}
	std::string ScriptCommandParam::ParseParamFromText(std::string input, std::vector<ScriptCommandParam>* output, std::initializer_list<ScriptCommandParamType> types)
	{
		unsigned int count = 0;			//argument index starts from 1
		std::string errors; 			//error message to return
		for (auto & type : types)
		{
			count++;
			//remove leading space
			input = TrimString(input);
			if (input.empty())
			{
				errors += "<parameter " + std::to_string(count) + ": cannot be empty>";
				continue;
			}
			if (type == PARAMTYPE_STRING)
			{
				//parse out data enclosed by ""
				auto result = GetEnclosedSubStrings(input, "\"\"", 1);
				if (input.find('\"') != 0 || result.empty())
				{
					errors += "<parameter " + std::to_string(count) + ": expecting string>";
					continue;
				}
				output->emplace_back(result[0]);
				//remove the substring parsed from input
				auto quote_2_pos = input.find('\"', input.find('\"') + 1);
				input = input.substr(quote_2_pos + 1, input.size() - quote_2_pos);
				continue;
			}
			else if (type == PARAMTYPE_BOOL || type == PARAMTYPE_FLOAT || type == PARAMTYPE_INT)
			{
				//find first space
				std::string content;
				try
				{
					content = SplitStringByDelim(input, ' ')[0];
					//removed the part content occupies
					input = input.substr(content.size());
				}
				catch (...)
				{
					//account for last param
					content = input;
				}
				try
				{
					if (type == PARAMTYPE_BOOL)
					{
						bool b = false;
						content = ToLowerString(content);
						if (content == "true")
						{
							b = true;
						}
						else if (content == "false")
						{
							b = false;
						}
						else
						{
							b = std::stoi(content);
						}
						output->emplace_back(b);
					}
					else if (type == PARAMTYPE_INT)
					{
						output->emplace_back(std::stoi(content));
					}
					else if (type == PARAMTYPE_FLOAT)
					{
						output->emplace_back(std::stof(content));
					}
				}
				catch (...)
				{
					errors += "<parameter " + std::to_string(count) + ": expecting number/true/false>";
				}
			}
			else if (type == PARAMTYPE_POINT || type == PARAMTYPE_RECT)
			{
				//input: "(x,y)"
				try
				{
					auto coord_string = GetEnclosedSubStrings(input, "()", 1).at(0);	//get "x,y"
					input = input.substr(input.find_first_of(')') + 1);
					auto coords = SplitStringByDelim(coord_string, ',');				//get {"x", "y"}
					if ((type == PARAMTYPE_POINT && coords.size() != 2) || (type == PARAMTYPE_RECT && coords.size() != 4))
					{
						errors += "<parameter " + std::to_string(count) + ": incorrect number of coordinates given>";
					}
					else
					{
						if (type == PARAMTYPE_POINT)
						{
							output->emplace_back(Point2F(std::stof(coords[0]/*x*/), std::stof(coords[1]/*y*/)));
						}
						else
						{
							output->emplace_back(Rect4F(std::stof(coords[0]/*x*/), std::stof(coords[1]/*y*/),
								std::stof(coords[2]/*w*/), std::stof(coords[3]/*h*/)));
						}
					}
				}
				catch (const std::exception& e)
				{
					errors += "<parameter " + std::to_string(count) + ": cannot find '(' and ')' for coordinates>";
				}
			}
			else if (type == PARAMTYPE_POINTER)
			{
				errors += "<parameter " + std::to_string(count) + ": parsing pointer param from text is not supported>";
			}
		}
		return errors;
	}
}