#include "EventManager.h"
#include "Functor.h"
#include "Exception.h"

#include <algorithm>

namespace Fengine
{

	EventManager::EventManager(Logger* log) :
		ref_logger_(log)
	{
	}

	EventManager::~EventManager()
	{
	}


	void EventManager::FireEvent(const Event &e)
	{
		try
		{
			for (auto& i : subscriber_table_[e.command_type_])
			{
				i.operator()(const_cast<Command*>(&e));
			}
		}
		catch (...)
		{
			ref_logger_->log_ += "Event Manager Fire Event Exception\nEvent Detail:";
			//log->log_ += e.CommandInfo;
			ref_logger_->log_ += "\n";
			//throw Exception({"error firing event: ", e.CommandInfo},false);
		}
	}
	void EventManager::SubscribeEvent(int command_type, const  Functor&  f)
	{
		try
		{
			if(subscriber_table_.find(command_type) == subscriber_table_.end())
			{
				subscriber_table_.insert(std::pair<int, std::vector<Functor>>(command_type,{f}));
			}
			else
			{
				//check if the same functor already subscribed to the event
				if(std::find(subscriber_table_[command_type].begin(), subscriber_table_[command_type].end(), f) == subscriber_table_[command_type].end())
					subscriber_table_[command_type].push_back(f);
			}
		}
		catch (...)
		{
			ref_logger_->log_ += "Event Manager Subscribe Event Exception\n";
			throw Exception({"error subscribing to "+ std::to_string(command_type) + " event"},false);
		}
	}
	void EventManager::UnsubscribeEvent(int command_type, const  Functor&  f)
	{
		try
		{
			for (auto i = subscriber_table_[command_type].begin(); i != subscriber_table_[command_type].end();)
			{
				if (*i == f)
				{
					subscriber_table_[command_type].erase(i);
					break;
				}
				i++;
			}
		}
		catch (...)
		{
			ref_logger_->log_ += "Event Manager Unsubscribe Event Exception\n";
			throw Exception({"error unsubscribing from "+ std::to_string(command_type) + " event"},false);
		}
	}
}