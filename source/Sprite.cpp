#include "Sprite.h"


namespace Fengine
{
	Sprite::Sprite():
		sprite_sheet_(0),
		highlight_color_(Graphics::GetInstance()->GetColor("Teal Blue")),
		is_drawable_(false)
	{}


	void Sprite::Init(const std::string & path, Rect4F dest_rect)
	{
		ref_dest_rect_ = nullptr;
		this->sprite_sheet_ = Graphics::GetInstance()->GetTexture(path);
		sprite_height_ = (int)dest_rect.h;
		sprite_width_ = (int)dest_rect.w;
		wh_ = Graphics::GetInstance()->GetTextureWH(sprite_sheet_);
		shape_ = GenShape(Rect4F(dest_rect.x, dest_rect.y, dest_rect.w, dest_rect.h), true);
		is_drawable_ = true;
		SetSourceRect();
	}

	void Sprite::Init(SDL_Surface * surface, Rect4F dest_rect, bool gen_VAO)
	{
		ref_dest_rect_ = nullptr;
		if(sprite_sheet_)
			Graphics::GetInstance()->DeleteTexture(this->sprite_sheet_);			//delete current texture, if any
		Graphics::GetInstance()->SDLSurfaceToGLTexture(surface, sprite_sheet_);
		sprite_height_ = (int)dest_rect.h;
		sprite_width_ = (int)dest_rect.w;
		wh_ = Graphics::GetInstance()->GetTextureWH(sprite_sheet_);
		if (gen_VAO)
		{
			shape_ = GenShape(Rect4F(dest_rect.x, dest_rect.y, dest_rect.w, dest_rect.h), gen_VAO);
		}
		else
		{
			SubRectTexCoord(shape_, { 0,0,wh_.x,wh_.y }, wh_.x, wh_.y);
			SubRect(shape_, dest_rect);
		}
		is_drawable_ = true;
		SetSourceRect();
	}

	void Sprite::Init(const std::string & path, Rect4F * dest_rect)
	{
		ref_dest_rect_ = dest_rect;
		this->sprite_sheet_ = Graphics::GetInstance()->GetTexture(path);
		sprite_height_ = (int)dest_rect->h;
		sprite_width_ = (int)dest_rect->w;
		wh_ = Graphics::GetInstance()->GetTextureWH(sprite_sheet_);
		shape_ = GenShape(Rect4F(dest_rect->x, dest_rect->y, dest_rect->w, dest_rect->h), true);
		is_drawable_ = true;
		SetSourceRect();
	}

	void Sprite::Init()
	{
		ref_dest_rect_ = nullptr;
		RegisterShape(shape_);
		ResizeShapeBuffer(shape_, 6);
		is_drawable_ = false;
	}

	bool Sprite::IsPointInShape(const Point2F & p) const
	{
		SDL_Rect rect = (SDL_Rect)GenRectFromShape(shape_);
		SDL_Point point = (SDL_Point)p;
		return SDL_PointInRect(&point, &rect);
	}

	void Sprite::CleanUp()
	{
		Graphics::GetInstance()->DeleteTexture(sprite_sheet_);
		sprite_sheet_ = 0;
		UnregisterShape(shape_);
		is_drawable_ = false;
	}

	void Sprite::MoveSprite(Point2F delta)
	{
		shape_.Move(delta);
	}

	void Sprite::MoveSpriteTo(Point2F destination)
	{
		shape_.MoveTo(destination);
	}

	void Sprite::MoveSpriteCenterTo(Point2F destination)
	{
		Rect4F new_location = GenRectFromShape(shape_);
		new_location.SetCenter(destination);
		SubRect(shape_, new_location);
	}

	Sprite::~Sprite() {}

	void Sprite::Draw(GLuint shader_program, const SDL_Rect & src_rect, const Rect4F & dest_rect)
	{
		if(!(src_rect.w == 0 && src_rect.h == 0))
		{
			SetSourceRect(src_rect);
		}
		//if destination rect is specified, use it to update the shape
		if (!dest_rect.IsDefault())
		{
			SubRect(shape_, dest_rect);
		}
		//if destination rect is not specified, this sprite may be in reference mode
		else if(ref_dest_rect_)
		{
			SubRect(shape_, *ref_dest_rect_);
		}
		//update transparency of shape
		if (shape_.vertices[0].color.a != transparency_)
			shape_.SetTransparency(transparency_);
		Graphics::GetInstance()->BlitSurface(shader_program, sprite_sheet_, shape_);
	}

	void Sprite::Draw(GLuint shader_program, const GLShape & shape)
	{
		if (highlighted_)
		{
			Graphics::GetInstance()->DisplayDebugShape(GenRectFromShape(shape), highlight_color_, shader_program);
			highlighted_ = false;
		}
		shape_ = shape;
		//update transparency of shape
		if (shape_.vertices[0].color.a != transparency_)
			shape_.SetTransparency(transparency_);
		Graphics::GetInstance()->BlitSurface(shader_program, sprite_sheet_, shape_);
	}
	void Sprite::SetHighlightColor_Sprite(const GLColor &color)
	{
		this->highlight_color_ = color;
	}
	void Sprite::SetTransparency(float transparency)
	{
		transparency_ = transparency;
	}

	float * Sprite::GetTransparencyPtr()
	{
		return &transparency_;
	}

	void Sprite::SetSourceRect(const SDL_Rect &rect)
	{
		//if source rect is not specified, use the whole image
		if (rect.w == 0 && rect.h == 0)
		{
			SubRectTexCoord(shape_);
		}
			//otherwise use the specified area in the image
		else
		{
			SubRectTexCoord(shape_, rect, wh_.x, wh_.y);
		}
	}

	bool Sprite::IsDrawable() const
	{
		return is_drawable_;
	}

	void Sprite::SetDestRect(const Rect4F & rect)
	{
		sprite_width_ = rect.w;
		sprite_height_ = rect.h;
		SubRect(shape_, rect);
	}

	float Sprite::GetHeight() const
	{
		return sprite_height_;
	}

	float Sprite::GetWidth() const
	{
		return sprite_width_;
	}
}
