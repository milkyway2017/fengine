#include "Audio.h"

namespace Fengine
{
	Audio::Audio()
	{
		Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);
	}

	Audio::~Audio()
	{
		Mix_CloseAudio();
	}

	void Audio::PlayMusic(const std::string & path)
	{
		if (music_list_.count(path))
		{
			Mix_PlayMusic(music_list_.at(path), 1);
		}
		else
		{
			music_list_.insert(std::pair<std::string, Mix_Music*>(path, Mix_LoadMUS(path.c_str())));
			Mix_PlayMusic(music_list_.at(path), 1);
		}
	}

	void Audio::PlaySoundEffect(const std::string & path)
	{
		if (effect_list_.count(path))
		{
			Mix_PlayChannel(-1, effect_list_.at(path), 0);
		}
		else
		{
			effect_list_.insert(std::pair<std::string, Mix_Chunk*>(path, Mix_LoadWAV(path.c_str())));
			Mix_PlayChannel(-1, effect_list_.at(path), 0);
		}
	}
}
