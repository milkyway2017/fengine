#include "ActiveArea.h"
#include "Map.h"
#include "EventManager.h"

namespace Fengine
{
	ActiveArea::ActiveArea(Rect4F area, Map* map, int id, const std::string& name) :
		object_count_(0),
		area_(area),
		event_manager_(map->GetEventManager()),
		map_(map),
		id_(id),
		name_(name),
		no_trig_(false)
	{
	}

	ActiveArea::~ActiveArea()
	{
	}
	void ActiveArea::Update()
	{
		int previousCount = object_count_;				//make note of previous count
		object_count_ = map_->ObjectsInArea(area_);

		if (previousCount == 0 && object_count_ != 0)
		{
			if(no_trig_)
			{
				no_trig_ = !no_trig_;
			}
			else
			{
				FireEventAndTrigger(true);
				//cast spells
				for (auto spell : enter_spells_)
				{
					map_->EnterMap(spell.next_map, map_->GetEntityByID(id_)->name);
				}
			}
		}
		else if (previousCount != 0 && object_count_ == 0)
		{
			if(no_trig_)
			{
				no_trig_ = !no_trig_;
			}
			else
			{
				FireEventAndTrigger(false);
			}
		}
	}

	bool ActiveArea::IsEmpty()
	{
		return !object_count_;
	}
	const Rect4F &ActiveArea::GetArea() const
	{
		return area_;
	}
	void ActiveArea::SetArea(const Rect4F &area)
	{
		ActiveArea::area_ = area;
	}

	void ActiveArea::AddSpell(TeleportInfo spell)
	{
		enter_spells_.push_back(spell);
	}

	void ActiveArea::FireEventAndTrigger(bool filled)
	{
		event_manager_->FireEvent(Event(MAP_EVENTMANAGER,
										int(filled ? MapEventManagerEventType::ACTIVEAREA_FILLED
										: MapEventManagerEventType::ACTIVEAREA_EMPTIED),
										"ActiveArea" + std::to_string(id_),
										this));            //raise filled event
	}

	void ActiveArea::NoTrig()
	{
		no_trig_ = true;
	}
}