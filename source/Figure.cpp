#include "Figure.h"
#include "Map.h"
#include "Graphics.h"

namespace Fengine
{
	Figure::Figure(const Rect4F & rect, bool is_solid, bool is_movable, int id, Map* map, const std::string & image_path, const SDL_Rect & source_rect) :
		body_(rect, is_solid, is_movable),
		event_manager_(map->GetEventManager()),
		id_(id),
		map_(map)
	{
		ConfigureZOrder();
		if (!image_path.empty())
		{
			sprite_.Init(image_path, &body_.collision_rect_);
			sprite_.SetSourceRect(source_rect);
		}
	}

	Figure::Figure(const Figure & other):
		body_(other.body_),
		event_manager_(other.event_manager_),
		id_(other.id_),
		map_(other.map_)
	{
		ConfigureZOrder();

		if (other.sprite_.IsDrawable())
			sprite_ = other.sprite_;
	}

	bool Figure::Init(Map * map, GLShape shape, GeometryType type, int id, bool is_solid, bool is_movable)
	{
		event_manager_ = map->GetEventManager();
		id_ = id;

		body_.is_solid_ = is_solid;
		body_.is_movable_ = is_movable;
		body_.type_ = type;

		//check if the caller passed in an appropriate shape for the type
		switch (type)
		{
		case RECT_SHAPE:
			//use the shape to get rect
			body_.collision_rect_ = GenRectFromShape(shape);
			body_.type_ = RECT_SHAPE;
			break;
		case LINE_SHAPE:
			body_.collision_line_ = GenLines(shape, false, false)[0];
			body_.type_ = LINE_SHAPE;
			break;
		case POLYGON_SHAPE:
			body_.collision_polygon_ = shape;
			body_.type_ = POLYGON_SHAPE;
			break;
		default:
			break;
		}

		ConfigureZOrder();

		return false;
	}

	Figure::~Figure()
	{
	}

	void Figure::Move(Point2F delta)
	{
		body_.MoveCenter(delta);
	}

	//Move the figure's top-left corner to one position
	void Figure::MoveTo(Point2F destination)
	{
		body_.MoveCenter(body_.GetBound().GetXY() - destination);
	}

	//Move the figure's center to one position
	void Figure::MoveCenterTo(Point2F destination)
	{
		body_.MoveCenterTo(destination);
	}

	Body * Figure::GetBody()
	{
		return &body_;
	}

	Sprite * Figure::GetSprite()
	{
		return &sprite_;
	}

	void Figure::Draw_Fig_Debug(const GLColor & color, GLuint shader_program)
	{
		Graphics::GetInstance()->DisplayDebugShape(body_.GetBound(), color, shader_program);
	}

	void Figure::ConfigureZOrder()
	{
		switch (body_.type_)
		{
		case RECT_SHAPE:
			z_order_.Init(&body_.collision_rect_.y, std::bind(&Figure::Draw, this), body_.collision_rect_.h);
			break;
		default:
			//no initialization
			break;
		}

	}

	void Figure::Draw()
	{
		if (sprite_.IsDrawable())
			sprite_.Draw(map_->GetShaderProgram());
	}
}