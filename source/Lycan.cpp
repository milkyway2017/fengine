
#include "Lycan.h"
#include "Map.h"


namespace Fengine
{
	void Lycan::Init(Map * map, Point2F position, GLuint shader_program, int id)
	{
		//calculate SpriteRect and collision_rect_ using ObjDef properties & position given
		Object::sprite_rect_ = def_SpriteRect;
		Object::sprite_rect_.SetCenter(position);				//set the center to position
		Rect4F collision_rect_ = sprite_rect_ + def_SpriteToBody;

		//init animation, hardcode animation refresh rate
		InitAnimation(100);

		//set current facing direction to random
		SetMovementState(RANDOM_IDLE_DIRECTION, IDLE);

		//init base by calling Init_Bot()
		Bot::Init_Bot(map,
			collision_rect_,
			shader_program,
			id);


		damage_ = def_Damage;
		detection_ = def_Detection;
		this->speed_ = def_Speed;
		health_ = def_Health;
		stamina_ = def_Stamina;
		health_regen_ = def_HealthRegen;
		max_health_ = health_;

		GetSpeed().AddSpeedComponent("terminal_speed", def_Speed);

		/*
		//subscribe itempickedup event, chase the player when item picked up
		SubscribeEvent("itempickedup", Functor((Object*)this, &Object::AddEvent));
		SubscribeEvent("itemdropped", Functor((Object*)this, &Object::AddEvent));
		*/

		//npc stuff
		reference_point_ = GetCenter();
		SetNPCStatus(PATROLLING);
	}
	void Lycan::InitAnimation(int TimeToUpdate)
	{
		body_animation_.Init("LycanWalkWest.png", TimeToUpdate, &sprite_rect_);

		//initialize basic body animation
		body_animation_.AddAction(IDLE + SOUTH.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + SOUTH.GetDegree(), 53, 2, 53, 40, 6);

		body_animation_.AddAction(IDLE + WEST.GetDegree(), 0, 0, 64, 64, 1);
		body_animation_.AddAction(WALKING + WEST.GetDegree(), 0, 0, 64, 64, 6);

		body_animation_.AddAction(IDLE + EAST.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + WEST.GetDegree(), 53, 2, 53, 40, 6);

		body_animation_.AddAction(IDLE + NORTH.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + NORTH.GetDegree(), 53, 2, 53, 40, 6);

		body_animation_.AddAction(IDLE + SOUTH_WEST.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + SOUTH_WEST.GetDegree(), 53, 2, 53, 40, 6);

		body_animation_.AddAction(IDLE + SOUTH_EAST.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + SOUTH_EAST.GetDegree(), 53, 2, 53, 40, 6);

		body_animation_.AddAction(IDLE + NORTH_WEST.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + NORTH_WEST.GetDegree(), 53, 2, 53, 40, 6);

		body_animation_.AddAction(IDLE + NORTH_EAST.GetDegree(), 0, 0, 53, 40, 1);
		body_animation_.AddAction(WALKING + NORTH_EAST.GetDegree(), 53, 2, 53, 40, 6);

		//initialize other animations

		//Adding attack animation
		lycan_actions_.AddAction((int)LycanAnimations::ATTACK_ANIMATION, 0, 0, 300, 550, 4);
		lycan_actions_.Init("AttackAnimation.png", 140, &sprite_rect_);
		animation_list.push_back(&lycan_actions_);
	}
	void Lycan::Patrol()
	{
		float distFromRef = GetCenter().x - reference_point_.x;
		if (distFromRef <= -40.f)			//walk east if exceeds west bound
			SetMovementState(EAST);
		else if (distFromRef >= 40.f)
			SetMovementState(WEST);

		if (IsPlayerInVision())
		{
			//if the lycan sees the player, follow
			SetNPCStatus(FOLLOWING);
			//begin following
			BeginFollowing(map->GetPlayer());
		}
		else
		{
			//do nothing and keep patrolling if no player is detected
		}

	}
	void Lycan::Follow()
	{
		Graphics::GetInstance()->DisplayDebugShape(body_.collision_rect_,
			((GLColor)Graphics::GetInstance()->GetColor("Blue")).ToTransparent(0.5f),
			shader_program_);

		Object* player = map->GetPlayer();

		//player is DEAD, switch to patrol state
		if (player == nullptr)
		{
			SetNPCStatus(PATROLLING);
			return;
		}

		//if the lycan is able to make damage, end following and do attack
		if (CanMakeDamage(player))
		{
			EndFollowing();
			SetNPCStatus(ATTACKING);
			return;
		}
		//the lycan can no longer see the player when chasing, stop chasing and patrol
		if (!IsPlayerInVision())
		{
			//stop following

			EndFollowing();
			SetNPCStatus(PATROLLING);
			return;
		}
	}

	void Lycan::Attack()
	{
		//doesn't do anything, things are taken care of
	}

	void Lycan::HandleAnimationComplete(void * e)
	{
		//cast the e into param
		auto param = *(AnimationCallbackFunctorParam*)(e);
		//get the action code
		LycanAnimations action_code = (LycanAnimations)(param.action_code_);
		switch (action_code)
		{
		case LycanAnimations::ATTACK_ANIMATION:
		{
			//send attack at the beginning of 4th frame
			if(param.frame_of_callback_ == 3)
			{
				SendAttack(map->GetPlayer(), damage_);
			}
			//when the whole attack animation is done, continue chasing
			else if (param.frame_of_callback_ == -2)
			{
				SetNPCStatus(FOLLOWING);
				BeginFollowing(map->GetPlayer());
			}
		}
		}
	}

	void Lycan::Freeze()
	{
		//do nothing for now...
	}

	bool Lycan::CanMakeDamage(Object* other)
	{
		Point2F lycanPos = this->GetCenter();
		Point2F enemyPos = other->GetCenter();
		return Dist(lycanPos, enemyPos) <= 30;
	}

	void Lycan::ReceiveAttack(int Damage)
	{
		health_ -= Damage;
		if (health_ <= 0)
		{
			Die();
		}
	}

	void Lycan::Die()
	{
		//no need to reset any stats as this Lycan is getting deleted by the map anyway

		//call base Die()
		Object::Die();
	}

	bool Lycan::IsPlayerInVision()
	{
		//check if player is alive
		if (map->GetPlayer())
		{
			//for when the Lycan is not following
			if (this->npc_status_ != FOLLOWING)
			{
				//check if player is in front
				auto object_list = map->GetObjectInDirection(this, def_DetectionRadius /*cone radius*/, body_.motion_.GetDirection(), def_DetectionAngle);

				for (auto object : object_list)
				{
					if (Player::IsPlayer(object) && ((Player*)object)->GetHealth() != 0)
					{
						return true;
					}
				}
				return false;
			}
			//for when the Lycan is following
			return Dist(map->GetPlayer()->GetCenter(), GetCenter()) <= def_DetectionRadius;
		}
		return false;
	}

	void Lycan::SetNPCStatus(NPCStatus s)
	{
		//put logic for entering a new state here
		switch (s)
		{
		case PATROLLING:
			//set the reference point to current location for patrolling pattern
			reference_point_ = GetCenter();
			SetMovementState(PickRandom({ EAST, WEST }));
			//remove extra terminal_speed if there is 
			GetSpeed().RemoveSpeedComponent("sprint_speed");
			break;
		case FREEZING:
			StopWalking();
			break;
		case ATTACKING:
			//whenever status is switched to attacking, play attack animation
			lycan_actions_.PlayAction((int) LycanAnimations::ATTACK_ANIMATION,
									  {
										  AnimationCallback(3/*trigger on the 4th frame*/,
															Functor(this, &Lycan::HandleAnimationComplete)),
										  AnimationCallback(-2/*trigger on the end of PlayAction*/,
															Functor(this, &Lycan::HandleAnimationComplete))
									  }, 1);
			break;
		case FOLLOWING:
			//add extra terminal_speed when following, if there is not already one called "sprint_speed"
			GetSpeed().AddSpeedComponent("sprint_speed", speed_);
			break;
		}
		//change status
		npc_status_ = s;
	}
	void Lycan::HandleSpecialEvent(Event e)
	{
		HandleSpecialEvent_Bot(e);
	}

	Lycan::~Lycan()
	{
	}

	void Lycan::LoadDef(const std::string & DefPath)
	{
		if (!def_Loaded)
		{
			using json = nlohmann::json;

			//load the json file
			std::ifstream defFile(DefPath);
			json Def;
			defFile >> Def;
			Def = Def["layers"];

			bool defFound = false;
			for (auto i : Def)
			{//loop through and find the layer with the name "Def"
				if (i["name"] == "Def")
				{
					Def = i;
					defFound = true;
					break;
				}
			}
			if (!defFound)
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Lycan Def Loading", "failed to find def objectgroup" }, false);
			}

			json defProperties = Def["properties"];

			//check if the ObjType property is actually PLAYER
			if (defProperties["ObjType"] != "LYCAN")
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Lycan Def Loading", "incorrect ObjType" }, false);
			}

			//get SpriteRect("Sprite" in map editor) and collisionrect("Body" in map editor)
			Rect4F Body;
			for (auto i : Def["objects"])
			{
				if (i["name"] == "Body")
				{
					Body.x = i["x"];
					Body.y = i["y"];
					Body.h = i["height"];
					Body.w = i["width"];
				}
				else
				{
					def_SpriteRect.x = i["x"];
					def_SpriteRect.y = i["y"];
					def_SpriteRect.w = i["width"];
					def_SpriteRect.h = i["height"];
				}
			}
			//check if the body and sprite are actually loaded
			if (Body.IsDefault() || def_SpriteRect.IsDefault())
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Lycan Def Loading", "missing body and sprite" }, false);
			}
			//find out def_SpriteToBody
			def_SpriteToBody.x = Body.x - def_SpriteRect.x;
			def_SpriteToBody.y = Body.y - def_SpriteRect.y;
			def_SpriteToBody.w = Body.w - def_SpriteRect.w;
			def_SpriteToBody.h = Body.h - def_SpriteRect.h;

			//load properties
			try
			{
				def_Damage = defProperties["STAT_Damage"];
				def_Speed = (float)defProperties["STAT_Speed"]/1000.f;
				def_Detection = defProperties["STAT_Detection"];
				def_Health = defProperties["STAT_Health"];
				def_Stamina = defProperties["STAT_Stamina"];
				def_HealthRegen = (float)defProperties["STAT_HealthRegen"]/1000.f;
				def_DetectionAngle = Angle((int)defProperties["STAT_DetectionAngle"]);
				def_DetectionRadius = defProperties["STAT_DetectionRadius"];
			}
			catch (const std::exception &e)
			{
				//reset to default and throw
				def_Loaded = false;
				def_SpriteRect = Rect4F();
				def_SpriteToBody = Rect4F();
				throw Exception({ "Lycan Def Loading", "failed to load stats", e.what() }, false);
			}
			def_Loaded = true;
		}
	}
	Obj_ptr Lycan::Clone(Point2F Location)
	{
		Lycan* NewObj = new Lycan();
		NewObj->Init(map, Location, shader_program_, id_);
		return Obj_ptr(NewObj);
	}
	Lycan::Lycan() :
		Bot(LYCAN_OBJ)
	{
	}

	Rect4F Lycan::def_SpriteToBody = Rect4F();
	Rect4F Lycan::def_SpriteRect = Rect4F();
	bool Lycan::def_Loaded = false;
	float Lycan::def_Damage = 0;
	float Lycan::def_Detection = 0;
	float Lycan::def_Speed = 0;
	float Lycan::def_Health = 0;
	float Lycan::def_Stamina = 0;
	float Lycan::def_HealthRegen = 0;
	Angle Lycan::def_DetectionAngle = Angle();
	float Lycan::def_DetectionRadius = 0;

}
