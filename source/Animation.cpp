#include "Animation.h"

namespace Fengine
{

	Animation::Animation() :
		time_since_refresh_(0),
		current_frame_(0),
		current_action_(-1)
	{}

	void Animation::Init(const std::string & path, int refresh_time, Rect4F dest_rect)
	{
		Sprite::Init(path, dest_rect);
		refresh_time_ = refresh_time;
	}

	Animation::~Animation()
	{
	}

	void Animation::AddAction(int action_code, int start_x, int start_y, int sprite_width, int sprite_height, int frame_count)
	{
		std::vector<SDL_Rect> SRV;
		for (int i = 0; i < frame_count; i++)
		{
			SDL_Rect SR = { start_x + (i *sprite_width),start_y,sprite_width,sprite_height };		//find the source rect for this animation
			SRV.push_back(SR);
		}
		this->actions_.insert(std::pair<int, std::vector<SDL_Rect>>(action_code, SRV));				//insert the vector into the map
	}

	void Animation::PlayAction(int action_code)
	{
		if (current_action_ != action_code)
		{
			current_action_ = action_code;
			current_frame_ = 0;
		}
	}

	void Animation::PlayAction(int action_code, const std::initializer_list<AnimationCallback> &callbacks, int times_to_play)
	{
		current_action_ = action_code;
		callbacks_ = callbacks;
		times_remaining = times_to_play;
		current_frame_ = 0;
	}

	void Animation::Init(const std::string & path, int refresh_time, Rect4F * dest_rect)
	{
		Sprite::Init(path, dest_rect);
		refresh_time_ = refresh_time;
	}

	void Animation::Update(int time_passed)
	{
		if (current_action_ != -1)			//if animation is player
		{
			this->time_since_refresh_ += time_passed;
			//check if is time for refresh
			if (time_since_refresh_ > refresh_time_)
			{
				//refresh time, switch to next source rect if there is one
				time_since_refresh_ = 0;
				if ((unsigned int)current_frame_ < actions_[current_action_].size() - 1)
				{
					//increment current_frame_ for the next source rect
					current_frame_++;
					
					//check if any callback should be called
					for(auto &i : callbacks_)
					{
						if (std::get<0>(i) == current_frame_)
						{
							//call callback!
							AnimationCallbackFunctorParam param{ current_action_, current_frame_ };
							std::get<1>(i).operator()(&param);
							std::cout << "callback called" << std::endl;
						}
					}
				}
				//if this is the last frame, reset index to 0 to replay the animation
				else
				{
					current_frame_ = 0;
					if (!callbacks_.empty())
					{
						times_remaining -= 1;
						//check if any callback should be called
						for (auto &i : callbacks_)
						{
							if (std::get<0>(i) == -1)
							{
								//call callback!
								AnimationCallbackFunctorParam param{ current_action_, -1 };
								std::get<1>(i).operator()(&param);
								std::cout << "callback called" << std::endl;
							}
						}
					}
				}

				//if playing a callback action
				if (!callbacks_.empty())
				{
					//check if this is the end of the callback action
					if (times_remaining == 0 && current_frame_ == 0)
					{
						//call callback with frame_of_callback set to -2
						for (auto &i : callbacks_)
						{
							if (std::get<0>(i) == -2)
							{
								//call callback!
								AnimationCallbackFunctorParam param{ current_action_, -2 };
								std::get<1>(i).operator()(&param);
								std::cout << "callback called" << std::endl;
							}
						}

						//end the callback action, reset counters
						times_remaining = -1;
						callbacks_.clear();
						PlayAction(-1);
						current_frame_ = 0;
					}
				}
			}
		}
	}

	void Animation::Draw(GLuint shader_program)
	{
		if (current_action_ != -1)
		{
			Sprite::Draw(shader_program, actions_[current_action_][current_frame_]);

		}
	}

	int Animation::GetCurrentAction() const
	{
		return current_action_;
	}
}