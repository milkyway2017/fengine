
#include <Label.h>

#include "Label.h"

namespace Fengine
{

	Label::Label(const std::string &text, const Point2F &position, const LabelStyle &style):
		text_(text),
		position_(position),
		style_(style),
		is_valid_(false)
	{
		UpdateTexture();
	}

	LabelStyle Label::GetStyle() const
	{
		return style_;
	}

	void Label::SetStyle(const LabelStyle &style)
	{
		if(std::memcmp(&style,&style_,sizeof(LabelStyle)) != 0)
		{
			this->style_ = style;
			is_valid_ = false;
		}
	}

	void Label::SetText(const std::string &text)
	{
		if (text != this->text_)
		{
			this->text_ = text;
			is_valid_ = false;
		}
	}

	void Label::UpdateTexture()
	{
		//split the string by '\n'
		auto lines = SplitStringByDelim(this->text_, '\n');
		int line_height = TTF_FontHeight(style_.font) + TTF_FontLineSkip(style_.font);	//the amount to increment in y for new line
		int current_y_offset = 0;														//amount of offset to add to position_.y, increments by line_height

		//add sprites
		for(unsigned int i = 0; i < lines.size(); i++)
		{
			auto& line = lines[i];
			SDL_Surface* s = TTF_RenderText_Blended(style_.font, line.c_str(), (SDL_Color)style_.color);
			if(s)
			{
				//not enough sprites, add one!
				if(i >= sprites_.size())
				{
					sprites_.emplace_back();
					sprites_.back().Init(s, { position_.x, position_.y + (float) current_y_offset,(float)s->w,(float)s->h}, true);
				}
				else
				{
					sprites_[i].Init(s, {position_.x, position_.y + (float) current_y_offset, (float) s->w, (float) s->h}, false);
				}
			}
			SDL_FreeSurface(s);
			current_y_offset += line_height;
		}

		//remove unused sprites
		if(lines.size() < sprites_.size())
		{
			auto begin = sprites_.begin();
			std::advance(begin, lines.size());
			for(auto i = begin; i != sprites_.end();)
			{
				i->CleanUp();
				i = sprites_.erase(i);
			}
		}

		is_valid_ = true;
	}

	void Label::Draw(GLuint shader_program)
	{
		//if this is the first time rendering, record start time
		if(!this->start_time)
		{
			start_time = SDL_GetTicks();
		}
		//if the texture is not up to date, update it
		if(!is_valid_)
		{
			UpdateTexture();
		}
		if(style_.fade_in_time && style_.fade_out_time)
		{
			//configure current transparency
			float transparency = GetCurrentTransparency();
			std::for_each(sprites_.begin(), sprites_.end(), [=](Sprite& sprite)
			{
				sprite.SetTransparency(transparency);
			});
		}
		std::for_each(sprites_.begin(), sprites_.end(), [=](Sprite& sprite)
		{
			sprite.Draw(shader_program);
		});
	}

	float Label::GetCurrentTransparency()
	{
		int time_into_phase = (int) (SDL_GetTicks() - start_time) % (style_.fade_in_time + style_.fade_out_time);
		time_into_phase -= style_.fade_out_time;
		if(time_into_phase >= 0)
		{
			return float(time_into_phase)/(float)style_.fade_in_time;
		}
		return -float(time_into_phase)/(float)style_.fade_out_time;
	}
	void Label::CleanUp()
	{
		for(auto & sprite : sprites_)
		{
			sprite.CleanUp();
		}
	}
}