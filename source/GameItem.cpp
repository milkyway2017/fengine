
#include <GameItem.h>

#include "GameItem.h"
#include "Map.h"

namespace Fengine
{
	GameItem::GameItem(GameItemType Type, Map * map, GLuint ShaderProgram, Object * Owner, int id, Point2F Location) :
		Type(Type),
		map_(map),
		owner_(Owner),
		shader_program_(ShaderProgram),
		is_active_(0),
		event_manager_(map->GetEventManager()),
		location_(Location),
		id_(id)
	{
		ConfigureZOrder();
	}
	void GameItem::Own(Object * owner)
	{
		owner_ = owner;
	}
	void GameItem::Drop()
	{
		Activate(0);
		owner_ = nullptr;
	}
	bool GameItem::HasOwner()
	{
		return owner_ != nullptr;
	}
	GameItem::GameItem(const GameItem & other) :
		owner_(nullptr),			//do not copy owner
		shader_program_(other.shader_program_),
		map_(other.map_),
		Type(other.Type),
		is_active_(other.is_active_),
		location_(other.location_),
		id_(other.id_),
		icon_(other.icon_)
	{
		ConfigureZOrder();
	}
	Point2F& GameItem::GetLocation()
	{
		return location_;
	}

	Sprite *GameItem::GetIcon()
	{
		return &icon_;
	}
}