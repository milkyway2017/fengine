#include "TileLayer.h"
#include "EventManager.h"
#include "Map.h"

namespace Fengine
{
	void TileLayer::ToggleDebugMode(void * e)
	{
		if (stoi(((Event*)e)->param_string_list_[0]))
			is_debugging_ = true;
		else
			is_debugging_ = false;
	}
	TileLayer::TileLayer(Map* map) :
		is_ground_layer_(0),
		event_manager_(map->GetEventManager()),
		is_debugging_(false),
		map_(map),
		tile_selected_(nullptr),
		tile_id_counter_(0)
	{
	}

	void TileLayer::Init(GLuint shaderProgram, int columnCount, bool isGroundLayer, unsigned int tile_count)
	{
		TileList.reserve(tile_count);
		shader_program_ = shaderProgram;
		column_count_ = columnCount;
		is_ground_layer_ = isGroundLayer;

		//subscribe to debugmode events
		event_manager_->SubscribeEvent(int(MapEventManagerEventType::DEBUGMODE), Functor(this, &TileLayer::ToggleDebugMode));
	}

	void TileLayer::Update(Input * input)
	{
		//record the Tile selected only if the mouse clicked
		Tile* t = map_->GetInvolvedTile(map_->GetCursorLocInMap());
		if (t && input->GetLButtonState() == BUTTON_PRESSED)
		{
			tile_selected_ = t;
		}

		for (auto & i : TileList)
		{
			i.Update();
		}
	}

	void TileLayer::Draw()
	{
		if (is_debugging_)
		{
			if (tile_selected_)
			{
				tile_selected_->highlighted_ = 1;			//highlight it
				tile_selected_->hightlight_color_ = tile_selected_->hightlight_color_.ToTransparent(0.3f);
				std::string text = "Tile Number: " + std::to_string(tile_selected_->tile_id_) + "   " + "Tile Availability: " + (tile_selected_->no_collision_ ? "true" : "false");
				InfoLabel::GetInstance()->DisplayString("tileinfo", text);
			}
		}

		for (auto i = render_list_.begin(); i != render_list_.end(); i++)
		{
			Graphics::GetInstance()->BlitSurface(shader_program_, i->first, i->second);	//draw the tiles
		}
	}

	void TileLayer::AddTile(SDL_Rect source_rect, SDL_Rect dest_rect, GLuint texture, unsigned int tileset_gid, int z_order_offset)
	{
		TileList.emplace_back(texture, source_rect, dest_rect, map_ ,shader_program_, tile_id_counter_, tileset_gid, z_order_offset);

		tile_id_counter_++;

		if (texture == 0)		//if this texture doesnt have a texture return
		{
			return;
		}
		//if there is an offset, don't add to render_list
		if (z_order_offset == 0)
		{
			if (render_list_.find(texture) != render_list_.end())		//a tile of this texture already exists, just add the vertices
			{
				GLShape Rect = GenShape(Rect4F(dest_rect), false);
				for (int i = 0; i < 6; i++)
				{
					render_list_.at(texture).AddVertex(Rect.vertices[i].position);		//add the Vertices to our list
				}
				// add corresponding texture coordinate
				SDL_Point WH = Graphics::GetInstance()->GetTextureWH(texture);
				SubRectTexCoord(render_list_[texture], source_rect, WH.x, WH.y, render_list_[texture].vertices.size() - 6);
			}
			else
			{
				GLShape Rect = GenShape(Rect4F(dest_rect), true);
				render_list_.insert(std::pair<GLuint, GLShape>(texture, Rect));
				// add corresponding texture coordinate
				SDL_Point WH = Graphics::GetInstance()->GetTextureWH(texture);
				SubRectTexCoord(render_list_[texture], source_rect, WH.x, WH.y, render_list_[texture].vertices.size() - 6);
			}
		}
		else
		{
			//instead add to map's render sorter
			map_->GetRenderSorter()->AddZOrder(&TileList.back().z_order_);
		}
	}


	void TileLayer::Activate()
	{
		for (auto i = render_list_.begin(); i != render_list_.end(); i++)
		{
			Graphics::GetInstance()->BindVAO(i->second.vao_id);
			Graphics::GetInstance()->BindVBO(i->second.vbo_id);

			//expand the buffer
			ResizeShapeBuffer(i->second);
		}
	}
	Tile * TileLayer::GetAdjacentTile(Tile * t, Angle dir)
	{
		return GetAdjacentTile(t->tile_id_, dir);
	}

	Tile * TileLayer::GetAdjacentTile(int tile_id, Angle dir)
	{
		auto c = column_count_;
		int x = tile_id;
		int size = TileList.size() - 1;
		if (!IsWithin(x, 0, size))
		{
			return nullptr;
		}

		if(dir == NORTH)
		{
			if (x >= c && IsWithin(x, 0, size))
				return &TileList[x - c];
			//no tile available in dir
			return nullptr;
		}
		else if(dir == NORTH_EAST)
		{
			if (x >= c && (x + 1) % c != 0 && IsWithin(x - c + 1, 0, size))
				return &TileList[x - c + 1];
		}
		else if(dir == EAST)
		{
			if ((x + 1) % c != 0 && IsWithin(x + 1, 0, size))
				return &TileList[x + 1];
		}
		else if(dir == SOUTH_EAST)
		{
			if (size - x>c && (x + 1) % c != 0)
				return &TileList[x + c + 1];
		}
		else if(dir == SOUTH)
		{
			if (size - x>c)
				return &TileList[x + c];
		}
		else if(dir == SOUTH_WEST)
		{
			if (size - x>c && x%c)
				return &TileList[x + c - 1];
		}
		else if(dir == WEST)
		{
			if (x%c != 0)
				return &TileList[x - 1];
		}
		else if(dir == NORTH_WEST)
		{
			if (x >= c && x%c != 0)
			return &TileList[x - c - 1];
		}
		return nullptr;
	}

	Tile * TileLayer::GetSelectedTile()
	{
		return tile_selected_;
	}
	unsigned int TileLayer::GetNextAvailableTileID() const
	{
		return tile_id_counter_;
	}
	Tile * TileLayer::GetTile(unsigned int tile_id)
	{
		try
		{
			return &TileList.at(tile_id);
		}
		catch(...)
		{
			return nullptr;			//if the tile_id does not exist
		}
	}
}