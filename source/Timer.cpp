#include "Timer.h"

namespace Fengine
{
	std::vector<std::pair<Timer_ptr, void*>> Timer::timers_ = std::vector<std::pair<Timer_ptr, void*>>();

	Timer::Timer(Uint32 time, Functor f) :
		f_(f),
		time_(time),
		time_started_(SDL_GetTicks())
	{

	}


	Timer * Timer::AddTimer(Uint32 time, Functor f, void* param)
	{
		//add a new timer to the table
		timers_.push_back(std::pair<Timer_ptr, void*>(new Timer(time, f), param));

		//add timer to SDL with timer_id_ set to value returned by SDL_AddTimer
		timers_.back().first->timer_id_ = SDL_AddTimer(time, &SDLTimerCallback, &timers_.back().first->timer_id_);

		return timers_.back().first.get();
	}

	void Timer::RemoveTimer(Timer* timer)
	{
		auto timer_id = timer->timer_id_;
		if (timer_id != 0)
		{
			//remove from SDL
			SDL_RemoveTimer(timer_id);
		}
		//otherwise timer is paused, which means it does not exist to SDL

		//erase from the timer table
		for (auto i = timers_.begin(); i != timers_.end();)
		{
			auto a = i->first->timer_id_;
			if (a == timer_id)
			{
				//found

				//delete
				timers_.erase(i);
				break;
			}
			i++;
		}
	}

	Uint32 Timer::SDLTimerCallback(Uint32 t, void * p)
	{
		int timer_id = *(int*)(p);
		for (auto i = timers_.begin(); i != timers_.end();)
		{
			if (i->first->timer_id_ == timer_id)
			{
				//found

				//delete first, then call
				auto function = i->first->f_;
				auto param = i->second;
				timers_.erase(i);

				//call functor
				function(param);
				break;
			}
			i++;
		}
		return 0;
	}

	void Timer::PauseAllTimers()
	{
		for(auto & i : timers_)
		{
			i.first->Pause();
		}
	}

	bool Timer::Pause()
	{
		//if not already paused
		if(timer_id_ != 0)
		{
			SDL_RemoveTimer(timer_id_);

			//calculate time left
			auto time_left = time_ - (SDL_GetTicks() - time_started_);
			time_ = time_left;

			timer_id_ = 0;
			return true;
		}
		return false;
	}

	bool Timer::Unpause()
	{
		//if not already running
		if(timer_id_ == 0)
		{
			time_started_ = SDL_GetTicks();
			timer_id_ = SDL_AddTimer(time_, &SDLTimerCallback, &timer_id_);
			return true;
		}
		return false;
	}

	int Timer::GetTimeLeft() const
	{
		if (timer_id_ != 0)
		{
			return time_ - (SDL_GetTicks() - time_started_);
		}
		return time_;
	}

	Timer::~Timer()
	{
	}
}