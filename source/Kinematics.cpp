#include "Kinematics.h"
#include "InfoLabel.h"

namespace Fengine
{
	float Speed::SumSpeed()
	{
		if (is_updated)
			return speed_sum;
		float result = 0;
		for (auto i : component_map)
		{
			result += i.second;
		}
		speed_sum = result;
		is_updated = true;
		return result;
	}

	void Speed::AddSpeedComponent(std::string key, float val)
	{
		if (component_map.find(key) == component_map.end())
		{
			component_map.insert(std::pair<std::string, float>(key, val));
		}
		else
		{
			component_map[key] = val;
		}
		//invalidate the speed
		is_updated = false;
	}

	void Speed::RemoveSpeedComponent(std::string key)
	{
		if (component_map.find(key) != component_map.end())
		{
			component_map.erase(key);
		}
		//invalidate the speed
		is_updated = false;
	}

	void Motion::SetAcceleration(const Point2F & acceleration)
	{
		if (this->acceleration != acceleration)
		{
			this->acceleration = acceleration;
			this->is_stopping = false;
			is_dirty = true;
		}
	}

	bool Motion::IsStopping() const
	{
		return is_stopping;
	}

	void Motion::StopWithAcceleration()
	{
		//only stop if the motion is moving
		if (acceleration)
		{
			is_stopping = true;
			//reverse acceleration
			acceleration = -acceleration;
			stopping_velocity = current_velocity;
			terminal_velocity.x = 0;
			terminal_velocity.y = 0;
		}
	}


	Point2F Motion::GetDisplacement(Uint32 time)
	{
		//if direction/acceleration/speed is changed (dirty), update terminal velocity and accleration
		if (is_dirty || !terminal_speed.is_updated)
		{
			//calculate new terminal velocity
			float spd = terminal_speed.SumSpeed();
			if (acceleration)
			{
				Angle direction((Radian)std::atan2(acceleration.y, acceleration.x));
				terminal_velocity.x = std::cos(direction.GetRadian()) * spd;
				terminal_velocity.y = std::sin(direction.GetRadian()) * spd;
			}
			is_dirty = false;
		}

		//update velocity if it has not reached terminal vel.
		current_velocity += acceleration * (float)time;
		if (current_velocity > terminal_velocity)
		{
			//if the cause is the motion stopping and the current velocity has opposite direction with the 
			//stopping velocity, zero the current velocity and stop stopping
			if (is_stopping &&
				(stopping_velocity + current_velocity).PsuedoNorm() <= stopping_velocity.PsuedoNorm())
			{
				is_stopping = false;
				current_velocity.x = 0;
				current_velocity.y = 0;
				acceleration.x = 0;
				acceleration.y = 0;
				stopping_velocity.x = 0;
				stopping_velocity.y = 0;
			}
			else
			{
				//normal case: scale down the velocity vector, becaues it has exceeded the terminal vel.
				current_velocity = current_velocity * (terminal_velocity.Norm() / current_velocity.Norm());
			}
		}

		//get displacement based on velocity
		return current_velocity * (float)time;
	}

	Angle Motion::GetDirection() const
	{
		return Angle((Radian)std::atan2(current_velocity.y, current_velocity.x));
	}

	Angle Motion::GetAccelerationDirection() const
	{
		if (!acceleration)
			return Angle();			//angle is undefined if there is no acceleration
		return Angle((Radian)std::atan2(acceleration.y, acceleration.x));
	}

	Point2F Motion::GetAcceleration() const
	{
		return acceleration;
	}

	Speed* Motion::GetSpeed()
	{
		return &terminal_speed;
	}

	Body::Body(Rect4F rect, bool is_solid, bool is_movable) :
		collision_rect_(rect),
		is_solid_(is_solid),
		is_movable_(is_movable),
		type_(RECT_SHAPE)
	{
	}

	Body::Body(Circle circle, bool is_solid, bool is_movable) :
		collision_circ_(circle),
		is_solid_(is_solid),
		is_movable_(is_movable),
		type_(CIRCLE_SHAPE)
	{
	}

	Body::Body(Line line, bool is_solid, bool is_movable) :
		collision_line_(line),
		is_solid_(is_solid),
		is_movable_(is_movable),
		type_(LINE_SHAPE)
	{
	}

	Body::Body(GLShape polygon, bool is_solid, bool is_movable) :
		collision_polygon_(polygon),
		is_solid_(is_solid),
		is_movable_(is_movable),
		type_(POLYGON_SHAPE)
	{
	}

	Body::Body(const Body & other)
	{
		std::memcpy(this, &other, sizeof(Body));
		if (type_ == POLYGON_SHAPE)
			collision_polygon_ = other.collision_polygon_;
		if (!other.points_attached_.empty())
			this->points_attached_ = other.points_attached_;
	}

	Body & Body::operator=(const Body & other)
	{
		std::memcpy(this, &other, sizeof(Body));
		if (type_ == POLYGON_SHAPE)
			collision_polygon_ = other.collision_polygon_;
		if (!other.points_attached_.empty())
			this->points_attached_ = other.points_attached_;
		return *this;
	}

	void Body::Attach(Point2F * point)
	{
		if (std::find(points_attached_.begin(), points_attached_.end(), point) == points_attached_.end())
			points_attached_.push_back(point);
	}

	void Body::Detach(Point2F * point)
	{
		auto i = std::find(points_attached_.begin(), points_attached_.end(), point);
		if (i != points_attached_.end())
			points_attached_.erase(i);
	}

	Point2F Body::GetCenter() const
	{
		switch (type_)
		{
		case RECT_SHAPE:
			return collision_rect_.GetCenter();
		case CIRCLE_SHAPE:
			return collision_circ_.xy;
		case LINE_SHAPE:
			return (collision_line_.P1 + collision_line_.P2) * 0.5f;
		case POLYGON_SHAPE:
			throw Exception("not implemented", false);
		}
	}

	Rect4F Body::GetBound() const
	{
		switch (type_)
		{
		case RECT_SHAPE:
			return collision_rect_;
		case CIRCLE_SHAPE:
			return Rect4F(collision_circ_.xy.x - collision_circ_.radius,
				collision_circ_.xy.y - collision_circ_.radius,
				collision_circ_.radius * 2,
				collision_circ_.radius * 2);
		case LINE_SHAPE:
			return Rect4F(std::min(collision_line_.P1.x, collision_line_.P2.x),
				std::min(collision_line_.P1.y, collision_line_.P2.y),
				std::abs(collision_line_.P1.x - collision_line_.P2.x),
				std::abs(collision_line_.P1.y - collision_line_.P2.y));
		case POLYGON_SHAPE:
		{
			float min_x = collision_polygon_.vertices[0].position.x, 
				max_x = collision_polygon_.vertices[0].position.x;
			float min_y = collision_polygon_.vertices[0].position.y,
				max_y = collision_polygon_.vertices[0].position.y;
			for(auto vertex : collision_polygon_.vertices)
			{
				if (vertex.position.x < min_x)
					min_x = vertex.position.x;
				if (vertex.position.x > max_x)
					max_x = vertex.position.x;
				if (vertex.position.y < min_y)
					min_y = vertex.position.y;
				if (vertex.position.y > max_y)
					max_y = vertex.position.y;
			}
			return Rect4F(min_x, min_y, max_x - min_y, max_y - min_y);
		}
		default:
			throw Exception("Not implemented", true);
		}
	}

	bool Body::Update(unsigned int time_passed)
	{
		//update body position
		auto displacement = motion_.GetDisplacement(time_passed);
		if (displacement)
		{
			MoveCenter(displacement);
			if (is_recording_)
				dist_travelled_ += displacement.Norm();
			return true;
		}
		return false;
	}

	void Body::MoveCenterTo(const Point2F & point)
	{
		Point2F delta(0,0);
		switch (type_)
		{
		case RECT_SHAPE:
			delta = point - collision_rect_.GetCenter();
			break;
		case CIRCLE_SHAPE:
			delta = point - collision_circ_.xy;
			break;
		case LINE_SHAPE:
			delta = point - (collision_line_.P1 + collision_line_.P2) * 0.5f;
			break;
		case POLYGON_SHAPE:
			collision_polygon_.MoveTo(point);
			break;
		default:
			throw Exception("not implemented", false);
		}
		MoveCenter(delta);
	}

	void Body::MoveCenter(const Point2F & delta)
	{
		switch (type_)
		{
		case RECT_SHAPE:
			collision_rect_.GetXY() += delta;
			break;
		case CIRCLE_SHAPE:
			collision_circ_.xy += delta;
			break;
		case LINE_SHAPE:
			collision_line_.P1 += delta;
			collision_line_.P2 += delta;
			break;
		case POLYGON_SHAPE:
			collision_polygon_.Move(delta);
			break;
		default:
			throw Exception("not implemented", false);
		}
		//move all the attached points
		for (auto p : points_attached_)
			*p += delta;
	}

	void Body::ResolveCollision(Body & other)
	{
		if (is_movable_ == other.is_movable_ || !is_solid_ || !other.is_solid_)
		{
			return;
		}

		//only handle moving if this is movable and other is not
		if (!is_movable_)
		{
			other.ResolveCollision(*this);
			return;
		}

		if (this->type_ == RECT_SHAPE)
		{
			if (other.type_ == RECT_SHAPE)
				this->MoveCenter(Collision::Rect_Rect(other.collision_rect_, this->collision_rect_));
			else if (other.type_ == LINE_SHAPE)
				this->MoveCenter(Collision::Line_Rect(other.collision_line_, this->collision_rect_));
			else if (other.type_ == CIRCLE_SHAPE)
				this->MoveCenter(Collision::Circle_Rect(other.collision_circ_, this->collision_rect_));
			//else if (other.type_ == POLYGON_SHAPE)
			//	this->MoveCenter(Collision::Polygon_Polygon(other.collision_polygon_, this->GetShape()));
		}
		else if (this->type_ == LINE_SHAPE)
		{
			if (other.type_ == RECT_SHAPE)
				this->MoveCenter(-Collision::Line_Rect(this->collision_line_, other.collision_rect_));
			else if (other.type_ == LINE_SHAPE)
				this->MoveCenter(Collision::Line_Line(other.collision_line_, this->collision_line_));
			else if (other.type_ == CIRCLE_SHAPE)
				this->MoveCenter(-Collision::Line_Circle(this->collision_line_, other.collision_circ_));
		}
		else if (this->type_ == CIRCLE_SHAPE)
		{
			if (other.type_ == RECT_SHAPE)
				this->MoveCenter(-Collision::Circle_Rect(this->collision_circ_, other.collision_rect_));
			else if (other.type_ == LINE_SHAPE)
				this->MoveCenter(Collision::Line_Circle(other.collision_line_, this->collision_circ_));
			else if (other.type_ == CIRCLE_SHAPE)
				this->MoveCenter(Collision::Circle_Circle(other.collision_circ_, this->collision_circ_));
		}
		else if(this->type_ == POLYGON_SHAPE)
		{
			//if (other.type_ == CIRCLE_SHAPE)
				throw Exception("polygon circle collision not supported", false);
			//this->MoveCenter(Collision::Polygon_Polygon(other.GetShape(), this->collision_polygon_));
		}
	}

	GLShape Body::GetShape()	const
	{
		switch (type_)
		{
		case RECT_SHAPE:
			return GenShape(collision_rect_, false);
		case LINE_SHAPE:
			return GenShape(collision_line_, false);
		case POLYGON_SHAPE:
			return collision_polygon_;
		default:
			throw Exception("shape not supported", false);
		}
	}

	void Body::DrawDebugShape() const
	{
		switch(type_)
		{
		case POLYGON_SHAPE:
			Graphics::GetInstance()->DisplayDebugShape(collision_polygon_,
				Graphics::GetInstance()->GetColor("Gray").ToTransparent(0.3f),
				Graphics::GetInstance()->GetShaderProgram("Game Rendering"));
			break;
		case RECT_SHAPE:
			Graphics::GetInstance()->DisplayDebugShape(collision_rect_,
				Graphics::GetInstance()->GetColor("Gray").ToTransparent(0.3f),
				Graphics::GetInstance()->GetShaderProgram("Game Rendering"));
			break;
		default:
			break;
		}
	}

	Quadrant::Quadrant(int level, const Rect4F & bound) :
		level_(level),
		bound_(bound)
	{
	}

	bool Quadrant::Insert(Body * body)
	{
		bool is_added = false;

		//if the body does not belong, return immediately
		if (!RectCollide(bound_, body->GetBound()))
		{
			return false;
		}

		//if this is splitted, add to sub-quadrants
		if (!quadrants_.empty())
		{
			for (auto & quadrant : quadrants_)
				quadrant.Insert(body);
			is_added = true;
		}
		//otherwise, add to this quadrant
		else
		{
			bodies_.push_back(body);
			is_added = true;
		}

		//check if the number of bodies exceeds the max
		if (bodies_.size() > max_body_count_)
		{
			Split();

			//add all bodies to sub-quadrants
			for (auto b : bodies_)
			{
				for (auto & quadrant : quadrants_)
				{
					quadrant.Insert(b);
				}
			}

			bodies_.clear();
		}

		return is_added;
	}

	bool Quadrant::Remove(Body* body)
	{
		bool removed = false;
		if (quadrants_.empty())
		{
			auto i = std::find(bodies_.begin(), bodies_.end(), body);
			//if found remove
			if (i != bodies_.end())
			{
				bodies_.erase(i);
				removed = true;
			}
		}
		else
		{
			for (auto & quadrant : quadrants_)
			{
				if (quadrant.Remove(body))
					removed = true;
			}
		}
		return removed;
	}

	std::vector<Body*> Quadrant::Retrieve(const Rect4F& rect)
	{
		//if the rect is not in this quadrant at all, return empty
		if (!RectCollide(rect, bound_))
		{
			return {};
		}
		//if the quadrant is not split, get all bodies in this quadrant
		if (quadrants_.empty())
		{
			return bodies_;
		}
		//if the quadrant is split, just retreive from sub-quadrants
		std::vector<Body*> return_bodies;
		for (auto & quadrant : quadrants_)
		{
			auto bodies = quadrant.Retrieve(rect);
			return_bodies.insert(return_bodies.end(), bodies.begin(), bodies.end());
		}
		return return_bodies;

	}

	unsigned int Quadrant::max_body_count_ = 20;

	void Quadrant::Split()
	{
		float width = bound_.w / 2, height = bound_.h / 2;
		//top left
		quadrants_.push_back(Quadrant(level_ + 1, Rect4F(bound_.GetXY(), width, height)));
		//top right
		quadrants_.push_back(Quadrant(level_ + 1, Rect4F(bound_.x + width, bound_.y, width, height)));
		//bottom left
		quadrants_.push_back(Quadrant(level_ + 1, Rect4F(bound_.x, bound_.y + height, width, height)));
		//bottom right
		quadrants_.push_back(Quadrant(level_ + 1, Rect4F(bound_.x + width, bound_.y + height, width, height)));
	}

	World::World(const Rect4F & bound)
	{
		quadtree_ = std::make_unique<Quadrant>(0, bound);
	}

	void World::Update(unsigned int time_passed)
	{
		std::vector<Body*> bodies_moved;		//stores the bodies that moved

		//update all bodies
		for (auto body : bodies_)
		{
			auto moved = body->Update(time_passed);

			//if the body moved, add this to bodies_moved and re-insert this body into the tree
			if (moved)
			{
				bodies_moved.push_back(body);
				quadtree_->Remove(body);
			}
		}

		//do collision detection for all bodies that moved
		for (auto body : bodies_moved)
		{
			//retrive a list of bodies this body can collide with
			auto colliders = quadtree_->Retrieve(body->GetBound());
			//remove duplicates in colliders
			colliders.erase(std::unique(colliders.begin(), colliders.end()), colliders.end());

			//resolve all collisions
			for (auto i : colliders)
			{
				body->ResolveCollision(*i);
				//i->DrawDebugShape();
			}
			quadtree_->Insert(body);
		}
	}

	void World::AddBody(Body * body)
	{
		if (quadtree_->Insert(body))
		{
			bodies_.push_back(body);
		}
	}

	void World::RemoveBody(Body * body)
	{
		const auto iter = std::find(bodies_.begin(), bodies_.end(), body);
		if (iter != bodies_.end())
			bodies_.erase(iter);
		quadtree_->Remove(body);
	}

	Point2F Collision::Rect_Rect(const Rect4F& A, const Rect4F& B)
	{
		switch (RectCollide(B, A))
		{
		case 1:
			return Point2F(0, (A.y + A.h) - B.y);
		case 2:
			return Point2F(0, A.y - (B.y + B.h));
		case 3:
			return Point2F((A.x + A.w) - B.x, 0);
		case 4:
			return Point2F(A.x - (B.x + B.w), 0);
		case 6:
			if ((A.y + A.h) - B.y > (A.x + A.w) - B.x)		//if collision in y is more than in x, modify x
				return Point2F((A.x + A.w) - B.x, 0);
			return Point2F(0, ((A.y + A.h) - B.y));
		case 7:
			if ((B.y + B.h) - A.y > (A.x + A.w) - B.x)		//if collision in y is more than in x, modify x
				return Point2F((A.x + A.w) - B.x, 0);
			return Point2F(0, A.y - (B.y + B.h));
		case 8:
			if ((A.h + A.y) - B.y > (B.x + B.w) - A.x)		//if collision in y is more than in x, modify x
				return Point2F(A.x - (B.x + B.w), 0);
			return Point2F(0, (A.y + A.h) - B.y);
		case 9:
			if ((B.y + B.h) - A.y > B.w + B.x - A.x)			//if collision in y is more than in x, modify x
				return Point2F(A.x - (B.x + B.w), 0);
			return Point2F(0, A.y - (B.y + B.h));
		default:
			return Point2F(0, 0);
		}
	}

	Point2F Collision::Circle_Rect(const Circle & A, const Rect4F & B)
	{
		auto lines = GenLines(GenShape(B, false));
		for (auto & i : lines)
		{
			Point2F response = -Line_Circle(i, A);
			if (response)
				return response;
		}
		return Point2F(0, 0);
	}

	Point2F Collision::Line_Rect(const Line & A, const Rect4F & B)
	{
		auto lines = GenLines(GenShape(B, false));
		for (auto & i : lines)
		{
			Point2F response = Line_Line(A, i);
			if (response)
				return response;
		}
		return Point2F(0, 0);
	}

	Point2F Collision::Line_Line(const Line & A, const Line & B)
	{
		//check if the lines intersect
		auto point = DoSegmentsIntersect(A, B);
		if (!point)
			return Point2F(0, 0);

		//split B into 2 vectors from the intersection and find the shorter one 
		Point2F x = A.P2 - A.P1;
		Point2F v = { Dist(B.P1, *point) < Dist(B.P2, *point) ? Point2F(B.P1 - *point) : Point2F(B.P2 - *point) };

		//apply formula
		return ((x * v) / (x * x) * x) - v;
	}
	Point2F Collision::Line_Circle(const Line & A, const Circle & B)
	{
		//find vector for A and hypotenuse
		Point2F v = A.P2 - A.P1;
		Point2F x = B.xy - A.P1;
		Point2F point_of_contact;	//the point of contact after the collision

		//project x onto v, return (0,0) if (cv-x) is greater than or equal to radius of B
		float c = (v*x) / (v*v);
		if ((c*v - x).Norm() >= B.radius)
			//no collision
			return Point2F(0, 0);
		//if c is in [0,1], set point of contact on the line
		if(IsWithin(c, 0.f, 1.f))
		{
			point_of_contact = A.P1 + (c * v);
		}
		else
		{
			//otherwise set the point of contact as one of the line vertices
			if ((A.P1 - B.xy).Norm() < B.radius)
				point_of_contact = A.P1;
			else if ((A.P2 - B.xy).Norm() < B.radius)
				point_of_contact = A.P2;
			else
				return Point2F(0, 0);
		}
		//collision happens
		return (point_of_contact - B.xy) - (point_of_contact - B.xy).Normalize() * B.radius;
	}
	Point2F Collision::Circle_Circle(const Circle & A, const Circle & B)
	{
		//check if no collision happens at all
		Point2F A_B = B.xy - A.xy;
		float dist = A_B.Norm();
		if (dist >= (A.radius + B.radius))
			return Point2F(0, 0);

		//collision happens
		return (A_B / dist) * (A.radius + B.radius - dist);
	}
	Point2F Collision::Polygon_Polygon(const GLShape & A, const GLShape & B)
	{
		//generate lines for A and B
		std::vector<Line> A_lines = GenLines(A, false, true);
		std::vector<Line> B_lines = GenLines(B, false, true);

		float min_overlap = 1000.f;
		Vector2F min_translation(0,0);			//minimum translation to apply to B to resolve collision
		Vector2F edge;

		for(unsigned int i = 0; i < A_lines.size() + B_lines.size(); i++)
		{
			if (i < A_lines.size())
				edge = B_lines[i].ToVector().Normalize();
			else
				edge = B_lines[i - A_lines.size()].ToVector().Normalize();

			//get the edge's perpendicular axis
			Vector2F axis = Vector2F(-edge.y, edge.x);

			//project A B onto the axis
			float min_A, max_A, min_B, max_B;
			ProjectPolygonOntoAxis(axis, A, &min_A, &max_A);
			ProjectPolygonOntoAxis(axis, B, &min_B, &max_B);

			//if the ranges do not overlap, there is no collision between A and B
			float distance = GetIntervalDistance(min_A, max_A, min_B, max_B);
			if (distance > 0)
				break;

			if(distance < min_overlap)
			{
				min_overlap = distance;
				min_translation = axis;
			}
		}
		return min_translation * min_overlap;
	}
	void Collision::ProjectPolygonOntoAxis(const Vector2F & axis, const GLShape & polygon, float * min_pos, float * max_pos)
	{
		*min_pos = axis * polygon.vertices[0].position;
		*max_pos = *min_pos;
		for(auto & vertex : polygon.vertices)
		{
			const float pos = axis * vertex.position;
			if (pos < *min_pos)
				*min_pos = pos;
			else if (pos > *max_pos)
				*max_pos = pos;
		}
	}
	float Collision::GetIntervalDistance(float min_A, float max_A, float min_B, float max_B)
	{
		if (min_A < min_B)
			return min_B - max_A;
		return min_A - max_B;
	}
}
