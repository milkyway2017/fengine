#include "Tile.h"
#include "Map.h"

namespace Fengine
{
	Tile::Tile() :
		texture_(0),
		highlighted_(0),
		hightlight_color_(Graphics::GetInstance()->GetColor("Teal Blue"))
	{
	}

	Tile::Tile(GLuint Texture, SDL_Rect sourceRect, SDL_Rect DestRect, Map* map, GLuint shader_program, unsigned int TileID, unsigned int tileset_gid, int z_order_offset) :
		texture_(Texture),
		dest_rect_(DestRect),
		tile_id_(TileID),
		source_rect_(sourceRect),
		highlighted_(0),
		hightlight_color_(Graphics::GetInstance()->GetColor("Teal Blue")),
		no_collision_(true),
		map_(map),
		shader_program_(shader_program),
		tileset_gid_(tileset_gid)
	{
		if (Texture)				// if this tile is visible
		{
			shape_ = GenShape(Rect4F(DestRect), true);
			SDL_Point texture_wh = Graphics::GetInstance()->GetTextureWH(Texture);
			SubRectTexCoord(shape_, source_rect_, texture_wh.x, texture_wh.y);
			z_order_.Init(dest_rect_.y, std::bind(&Tile::Draw, this), (float)z_order_offset);
		}
	}


	Tile::~Tile()
	{
	}
	void Tile::Draw()
	{
		//check if the tile is in map's field of view
		auto camera_view = map_->GetCamera()->GetFieldOfView();
		if (!RectCollide(camera_view, Rect4F(dest_rect_)))
		{
			return;
		}
		//Draw tile
		if (texture_)
		{
			Graphics::GetInstance()->BlitSurface(shader_program_, texture_, shape_);
		}

		//if highlighted, draw shade over tile as well
		if (highlighted_)
		{
			Graphics::GetInstance()->DisplayDebugShape(Rect4F(this->dest_rect_), hightlight_color_, shader_program_);
			highlighted_ = false;
		}
	}
	Point2F Tile::GetCenter()
	{
		return Point2F((float)dest_rect_.x+(dest_rect_.w/2),(float)dest_rect_.y+(dest_rect_.h)/2);
	}

}
