#include "Primitive.h"
#include <utility>
#include <algorithm>
#include <vector>
#include <cstring>
#include <string>

namespace Fengine
{
	Point2F::Point2F() :
		x(std::numeric_limits<float>::quiet_NaN()),
		y(std::numeric_limits<float>::quiet_NaN())
	{
	}

	Point2F::Point2F(float x, float y) :
		x(x),
		y(y)
	{
	}

	Point2F::Point2F(const SDL_Point & other) :
		x((float)other.x),
		y((float)other.y)
	{
	}

	Point2F Point2F::operator-() const
	{
		return Point2F(-x, -y);
	}

	Point2F Point2F::operator+() const
	{
		return *this;
	}

	Point2F Point2F::operator*(const float & scale) const
	{
		return Point2F(x * scale, y * scale);
	}

	Point2F Point2F::operator/(const float & scale) const
	{
		return Point2F(x / scale, y / scale);
	}

	Point2F Point2F::operator+(const Point2F & other) const
	{
		return Point2F(x + other.x, y + other.y);
	}

	Point2F Point2F::operator-(const Point2F & other) const
	{
		return Point2F(x - other.x, y - other.y);
	}

	float Point2F::operator*(const Point2F & other) const
	{
		return (x*other.x) + (y*other.y);
	}

	float Point2F::Norm() const
	{
		return std::sqrt(x*x + y*y);
	}

	Point2F Point2F::Normalize() const
	{
		const auto norm = Norm();
		return Point2F(x/norm,y/norm);
	}

	float Point2F::PsuedoNorm() const
	{
		return x*x + y*y;
	}

	float Point2F::Angle(const Point2F & other) const
	{
		return std::acos((*this*other)/(Norm() * other.Norm()));
	}

	void Point2F::operator+=(const Point2F & other)
	{
		this->x += other.x;
		this->y += other.y;
	}

	void Point2F::operator-=(const Point2F & other)
	{
		this->x -= other.x;
		this->y -= other.y;
	}

	bool Point2F::operator==(const Point2F & other)	const
	{
		return (x == other.x && y == other.y);
	}

	bool Point2F::operator!=(const Point2F & other)	const
	{
		return !(*this == other);
	}

	bool Point2F::operator<(const Point2F & other) const
	{
		return PsuedoNorm() < other.PsuedoNorm();
	}

	bool Point2F::operator>(const Point2F & other) const
	{
		return PsuedoNorm() > other.PsuedoNorm();
	}

	Point2F::operator SDL_Point()	const
	{
		return { (int)x,(int)y };
	}

	float Point2F::Dist(const Point2F & other) const
	{
		return (*this - other).Norm();
	}

	bool Point2F::IsDefault()	const
	{
		return (std::isnan(x) && std::isnan(y));
	}

	std::string Point2F::ToString()
	{
		return "x: " + std::to_string(x) + "\ty: " + std::to_string(y);
	}
	
	Point2F::operator bool() const
	{
		return (x != 0.f || y != 0.f);
	}

	Rect4F::Rect4F(Point2F xy, Point2F wh) :
		x(xy.x),
		y(xy.y),
		w(wh.x),
		h(wh.y)
	{
	}

	Rect4F::Rect4F(float x, float y, float w, float h) :
		x(x),
		y(y),
		w(w),
		h(h)
	{
	}

	Rect4F::Rect4F(Point2F xy, float w, float h):
		x(xy.x),
		y(xy.y),
		w(w),
		h(h)
	{
	}

	Rect4F::Rect4F() :
		x(std::numeric_limits<float>::quiet_NaN()),
		y(std::numeric_limits<float>::quiet_NaN()),
		w(std::numeric_limits<float>::quiet_NaN()),
		h(std::numeric_limits<float>::quiet_NaN())
	{
	}

	Rect4F::Rect4F(const SDL_Rect & other) :
		x((float)other.x),
		y((float)other.y),
		w((float)other.w),
		h((float)other.h)
	{
	}

	void Rect4F::SetCenter(const Point2F& pos)
	{
		GetXY() = pos;
		x -= w / 2;
		y -= h / 2;
	}

	Point2F& Rect4F::GetXY() const
	{
		return *(Point2F*)this;
	}

	Point2F Rect4F::GetCenter() const
	{
		return Point2F(x + (w / 2.f), y + (h / 2.f));
	}

	Rect4F::operator SDL_Rect() const
	{
		return { (int)x, (int)y, (int)w, (int)h };
	}

	bool Rect4F::IsDefault()	const
	{
		return (std::isnan(x) && std::isnan(y) && std::isnan(w) && std::isnan(h));
	}

	bool Rect4F::operator==(const Rect4F & other) const
	{
		return !std::memcmp(this,&other,sizeof(Rect4F));
	}

	Rect4F Rect4F::operator+(const Rect4F & other) const
	{
		return Rect4F(x + other.x, y + other.y, w + other.w, h + other.h);
	}

	Rect4F Rect4F::operator-(const Rect4F & other) const
	{
		return Rect4F(x - other.x, y - other.y, w - other.w, h - other.h);
	}

	Angle::Angle(Radian radians) :
		radian(radians),
		is_radian(true),
		is_undefined(false)
	{
	}

	Angle::Angle(Degree degrees) :
		degree(degrees),
		is_radian(false),
		is_undefined(false)
	{
	}

	Radian Angle::GetRadian()	const
	{
		if (is_radian)
			return radian;
		return (float)(degree*pi / 180.f);
	}

	Degree Angle::GetDegree()	const
	{
		if (is_radian)
			return (int)(radian*180.f / pi);
		return degree;
	}

	Angle& Angle::ToRadian()
	{
		if (is_radian)
			return *this;
		radian = GetRadian();
		return *this;
	}

	Angle& Angle::ToDegree()
	{
		if (!is_radian)
			return *this;
		degree = GetDegree();
		return *this;
	}

	Angle Angle::operator+(const Angle& other)	const
	{
		return Angle(this->GetRadian() + other.GetRadian());
	}

	Angle Angle::operator-(const Angle& other) const
	{
		return Angle(this->GetRadian() - other.GetRadian());
	}

	Angle Angle::operator*(const float& scale)	const
	{
		return Angle(this->GetRadian() / scale);
	}

	Angle& Angle::Simplify()
	{
		//if is radian convert it back to deg
		if (is_radian)
		{
			//make it positive
			if (radian < 0.f)
				radian += std::ceil(-radian / (2 * pi)) * 2 * pi;
			//mod 2pi
			radian = (float)fmod(radian, 2 * pi);
		}
		else
		{
			//make it positive
			if (degree < 0)
				degree += (int)std::ceil(-degree / (360)) * 360;
			//mod 360
			degree %= 360;
		}

		return *this;
	}

	Angle& Angle::FlipX()
	{
		if (is_radian)
		{
			radian = (float)fmod(2 * pi - radian, pi);
		}

		degree = (360 - degree) % 360;

		return *this;
	}

	void Angle::SetUndefined()
	{
		is_undefined = true;
	}

	bool Angle::IsUndefined() const
	{
		return is_undefined;
	}

	bool Angle::operator>(const Angle& other)	const
	{
		return this->GetRadian() > other.GetRadian();
	}

	bool Angle::operator<(const Angle& other)	const
	{
		return this->GetRadian() < other.GetRadian();
	}

	bool Angle::operator==(const Angle& other)	const
	{
		return this->GetRadian() == other.GetRadian();
	}

	bool Angle::operator>=(const Angle& other)	const
	{
		return (*this>other) || (*this == other);
	}

	bool Angle::operator<=(const Angle & other)	const
	{
		return (*this<other) || (*this == other);
	}

	bool Angle::operator!=(const Angle & other) const
	{
		return !(*this==other);
	}

	GLColor::GLColor(const SDL_Color& sdlcolor) :
		r(((float)sdlcolor.r) / 255.f),
		g(((float)sdlcolor.g) / 255.f),
		b(((float)sdlcolor.b) / 255.f),
		a(((float)sdlcolor.a) / 255.f)
	{
	}

	GLColor::GLColor(float r, float g, float b, float a) :
		r(r),
		g(g),
		b(b),
		a(a)
	{
	}

	GLColor::GLColor() :
		r(0.f),
		g(0.f),
		b(0.f),
		a(1.f)
	{
	}

	GLColor::operator SDL_Color() const
	{
		SDL_Color returnVal;
		returnVal.r = Uint8(r * 255);
		returnVal.g = Uint8(g * 255);
		returnVal.b = Uint8(b * 255);
		returnVal.a = Uint8(a * 255);
		return returnVal;
	}

	GLColor GLColor::ToTransparent(float t)	const
	{
		return GLColor(r, g, b, t);
	}
	
	GLVertex::GLVertex(Point2F location, GLColor color, Point2F tex_coord) :
		position(location),
		color(color),
		texture_coordinate(tex_coord)
	{
	}

	bool GLVertex::operator==(const GLVertex& other)	const
	{
		return !(*this != other);
	}

	bool GLVertex::operator !=(const GLVertex& other)	const
	{
		return std::memcmp(this, &other, sizeof(GLVertex));
	}

	void GLShape::Move(const Point2F& delta)
	{
		for (unsigned int i = 0; i < vertices.size(); i++)
		{
			vertices[i].position.x += delta.x;
			vertices[i].position.y += delta.y;
		}
	}

	void GLShape::MoveTo(const Point2F& destination)
	{
		if (vertices.size() > 0)
			Move(destination - vertices[0].position);
	}

	void GLShape::AddVertex(GLVertex vertex)
	{
		vertices.push_back(vertex);
	}

	void GLShape::SetTransparency(float transparency)
	{
		std::for_each(vertices.begin(), vertices.end(), [=](GLVertex & v) {
			v.color.a = transparency;
		});
	}

	bool GLShape::operator==(const GLShape & other)	const
	{
		if (this->vertices.size() != other.vertices.size())
		{
			return false;
		}
		for (unsigned int i = 0; i < vertices.size(); i++)
		{
			if (vertices[i] != other.vertices[i])
			{
				return false;
			}
		}
		return true;
	}

	bool GLShape::operator!=(const GLShape & other)	const
	{
		return !(*this == other);
	}

	Line::Line(Point2F p1, Point2F p2) :
		P1(p1),
		P2(p2)
	{}

	bool Line::IsDefault() const
	{
		return P1.IsDefault() && P2.IsDefault();
	}

	const Point2F* Line::operator^(const Line& other)	const
	{
		return((P1 == other.P1 || P1 == other.P2) ? &P1 : (P2 == other.P1 || P2 == other.P2) ? &P2 : nullptr);
	}

	Vector2F Line::ToVector() const
	{
		return P2-P1;
	}

	Circle::Circle(Point2F center, float radius):
		xy(center),
		radius(radius)
	{
	}

	bool Circle::IsDefault() const
	{
		return xy.IsDefault() && radius == 0;
	}
	Point2F operator*(float scale, const Point2F & vector)
	{
		return vector * scale;
	}
}
