#include "Bot.h"
#include "Map.h"

namespace Fengine
{
	void Bot::Init_Bot(Map *map, Rect4F collision_rect, GLuint shader_program, int id)
	{
		//just init base by calling Init_Obj()
		Init_Obj(map, collision_rect, shader_program, id);
	}
	void Bot::Update(int time_passed, void * data)
	{
		switch (npc_status_)
		{
		case PATROLLING:
			Patrol();
			break;
		case FOLLOWING:
			Follow();
			break;
		case FREEZING:
			Freeze();
			break;
		case ATTACKING:
			Attack();
			break;
		}
		//update stats
		UpdateStats(time_passed);
		//Update the object
		Object::Update(time_passed, nullptr);
	}
	void Bot::UpdateStats(int time_passed)
	{
		//if the health is not max, regen
		if(health_ < max_health_)
			health_ += time_passed * (health_regen_);

		//if the Health is greater than max, set it back to max
		if (health_ > max_health_)
			health_ = max_health_;
	}
	Bot::Bot(ObjectType t) :
		npc_status_(PATROLLING),
		Object(t)
	{
	}

	Bot::~Bot()
	{
	}
	void Bot::HandleSpecialEvent_Bot(Event e)
	{
		//TODO: re-implement the Bot's event handling logic, make base virtual and override
		switch (MapEventManagerEventType(e.command_type_))
		{
		case MapEventManagerEventType::FOLLOW:
			this->BeginFollowing((Object*)e.target_);
			SetNPCStatus(FOLLOWING);
			e.Resolve();
			break;
		case MapEventManagerEventType::FREEZE:	//stop any activity and become frozen
			SetNPCStatus(FREEZING);
			e.Resolve();
			break;
		case MapEventManagerEventType::PATROL:
			SetNPCStatus(PATROLLING);
			e.Resolve();
			break;
		default:
			e.CannotResolve();
			break;
		}
	}
}