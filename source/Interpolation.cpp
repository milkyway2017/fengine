#include "Interpolation.h"
#include "Primitive.h"
#include <SDL.h>

namespace Fengine
{
	bool Interpolation::Update(Uint32 time_passed)
	{
		total_time_passed_ += time_passed;
		return false;
	}
	Interpolation::Interpolation(float * value, const float & initial_value, const float & final_value, const Uint32 & duration, const Functor & callback):
		value_(value),
		initial_value_(initial_value),
		final_value_(final_value),
		callback_(callback),
		duration_(duration),
		time_began_(SDL_GetTicks()),
		total_time_passed_(0)
	{
	}
	void Interpolation::AddInterpolationToPool(Interpolation *interpolation)
	{
		interpolation_pool.push_back(interpolation);
	}

	void Interpolation::UpdatePool(Uint32 time_passed)
	{
		//loop backwards and delete any complete interp. as needed
		for (int i = interpolation_pool.size() - 1; i >= 0; i--)
		{
			if (interpolation_pool[i]->Update(time_passed))
			{
				delete interpolation_pool[i];
				interpolation_pool.erase(interpolation_pool.begin() + i);
			}
		}
	}

	LinearInterpolation::LinearInterpolation(float * value, const float & initial_value, const float & final_value, const Uint32 & duration, const Functor & callback):
		Interpolation(value, initial_value, final_value, duration, callback)
	{
	}

	bool LinearInterpolation::Update(Uint32 time_passed)
	{
		Interpolation::Update(time_passed);
		*value_ = initial_value_ + (final_value_ - initial_value_) / duration_ * total_time_passed_;

		if (total_time_passed_ > duration_)
		{
			*value_ = final_value_;
			callback_(nullptr);
			return true;
		}
		return false;
	}

	SineInterpolation::SineInterpolation(float * value, const float & initial_value, const float & final_value, const Uint32 & duration, const Functor & callback) :
		Interpolation(value, initial_value, final_value, duration, callback)
	{
	}

	bool SineInterpolation::Update(Uint32 time_passed)
	{
		Interpolation::Update(time_passed);
		*value_ = (final_value_ - initial_value_) * std::sin(pi* total_time_passed_/(2*duration_)) + initial_value_;

		if (total_time_passed_ > duration_)
		{
			*value_ = final_value_;
			callback_(nullptr);
			return true;
		}
		return false;
	}

	std::vector<Interpolation*> Interpolation::interpolation_pool = {};
}
