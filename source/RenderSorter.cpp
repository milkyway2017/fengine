#include "RenderSorter.h"

namespace Fengine
{

	RenderSorter::RenderSorter()
	{
	}


	RenderSorter::~RenderSorter()
	{
	}

	void RenderSorter::SortAndDraw()
	{
		//sort
		std::sort(z_order_list_.begin(), z_order_list_.end(),CompareZOrder);

		//draw
		for (auto z : z_order_list_)
		{
			z->Draw();
		}
	}
	void RenderSorter::RemoveZOrder(Z_order * z)
	{
		auto it = std::find(z_order_list_.begin(), z_order_list_.end(), z);
		if(it != z_order_list_.end())
			z_order_list_.erase(it);
	}
	void RenderSorter::AddZOrder(Z_order * z)
	{
		bool is_ref_used = z->is_ref_used;
		z_order_list_.push_back(z);
	}
}
