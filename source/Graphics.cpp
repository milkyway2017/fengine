#include "Graphics.h"
#include "InfoLabel.h"

namespace Fengine
{
	Graphics::Graphics()
	{
	}

	void Graphics::Init(const std::string& title)
	{
		SDL_Init(SDL_INIT_EVERYTHING);
		if (!IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG))
		{
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", SDL_GetError(), window_);
		}
		if (TTF_Init() < 0)
		{
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", SDL_GetError(), window_);
		}

		window_ = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 720, SDL_WINDOW_OPENGL);

		gl_context_ = SDL_GL_CreateContext(window_);	//create opengl context for opengl rendering
		if (gl_context_ == nullptr)
		{
			std::cout<< SDL_GetError() << std::endl;
		}

		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);		//enable double buffer
		SetGLVersion(4, 3);			//set the version of opengl

		std::cout << glGetString(GL_VERSION)<<std::endl;
		//init opengl
		GLenum error = glewInit();
		if (error != GLEW_OK)
		{
			throw("GLEW init error");
		}
		//set clear color
		glClearColor(0, 0, 0, 1);
		//enable alpha blending?
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void Graphics::GenShaderProgram(std::string vertpath/*vertex shader path*/, std::string fragpath/*fragment shader path*/, GLuint& ProgramID_Holder, const std::string & name)
	{

		//see if the shader program is already added
		if (shader_program_map_.find(name) != shader_program_map_.end())
			//found
		{
			ProgramID_Holder = shader_program_map_[name];
			return;
		}

		std::vector<GLuint> shader_ids_;		//stores all the shaders(shaders that are currently in the queue)

		ProgramID_Holder = glCreateProgram();

		GLuint vertexshader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);

		shader_ids_.push_back(vertexshader);
		shader_ids_.push_back(fragmentshader);

		Graphics::CompileShader(vertpath, vertexshader);
		Graphics::CompileShader(fragpath, fragmentshader);

		AttachShader(vertexshader, ProgramID_Holder);
		AttachShader(fragmentshader, ProgramID_Holder);

		LinkandCleanShaders(ProgramID_Holder,&shader_ids_);

		//add to shader_program_map_
		shader_program_map_[name] = ProgramID_Holder;
	}

	Graphics::~Graphics()
	{
		texture_map_.clear();
		Close();
	}

	void Graphics::Close()
	{
		SDL_GL_DeleteContext(gl_context_);	//delete opengl context
		SDL_DestroyWindow(this->window_);	//destroy window
		IMG_Quit();							//shut down IMG
		TTF_Quit();							//shut down TTF
		SDL_Quit();							//shut down sdl
	}

	GLuint Graphics::GetTexture(const std::string & path)
	{
		GLErrorVal();
		//figure out the full path of path
		std::string full_path = path;
		if(!FileExist(path))
		{
			//the path given may be a filename under font_directory_
			if(image_directory_.empty() || !FileExist(image_directory_ + path))
			{
				//still not found? no such file!
				return 0;
			}
			full_path = image_directory_ + path;
		}

		if (texture_map_.count(full_path) > 0)		//if this texture is already loaded
		{
			return texture_map_.at(full_path);
		}
		//first load the file into SDL_Surface, then convert it!
		GLuint holder = 0;
		SDL_Surface* surface = IMG_Load(full_path.data());
		SDLSurfaceToGLTexture(surface, holder);
		SDL_FreeSurface(surface);
		if (holder <= 0)
		{
			std::cout<< IMG_GetError() << std::endl;
		}
		texture_map_.insert(std::pair<std::string, GLuint>(full_path, holder));
		GLErrorVal();
		return texture_map_.at(full_path);
	}

	void Graphics::DeleteTexture(GLuint texture)
	{
		GLErrorVal();
		for (auto i = texture_map_.begin(); i != texture_map_.end();)
		{
			if ((*i).second == texture)
			{
				glDeleteTextures(1, &(*i).second);
				texture_map_.erase(i);
				return;
			}
			i++;
		}
		//texture is not loaded from image, just delete
		glDeleteTextures(1, &texture);
		GLErrorVal();
		return;
	}

	TTF_Font* Graphics::GetFont(const std::string& path, unsigned int font_size)
	{
		//figure out the full path of path
		std::string full_path = path;
		if(!FileExist(path))
		{
			//the path given may be a filename under font_directory_
			if(font_directory_.empty() || !FileExist(font_directory_ + path))
			{
				//still not found? no such file!
				return nullptr;
			}
			full_path = font_directory_ + path;
		}
		if (!font_map_.count(full_path) || !font_map_[full_path].count(font_size))
			font_map_[full_path][font_size] = TTF_OpenFont(full_path.c_str(), font_size);
		return font_map_[full_path][font_size];
	}

	void Graphics::BlitSurface(const GLuint program, const GLuint Texture, const GLShape& Shape)
	{
		GLErrorVal();
		if (Shape.vertices.size() != 0)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			BindVBO(Shape.vbo_id);
			GLErrorVal();
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLVertex)*Shape.vertices.size(), &Shape.vertices[0]);
			GLErrorVal();
			//bind VAO
			BindVAO(Shape.vao_id);
			GLErrorVal();
			//draw
			GLint location = glGetUniformLocation(program, "Texture");
			GLErrorVal();
			BindProgram(program);
			glUniform1i(location, 0);
			BindTexture(Texture);
			glDrawArrays(GL_TRIANGLES, 0, Shape.vertices.size());
			GLErrorVal();

			UnbindAll();
		}
		GLErrorVal();
	}

	void Graphics::DrawLines(const GLuint shader_program, const GLShape & shape)
	{
		GLErrorVal();
		if (shape.vertices.size() != 0)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			BindTexture(0);
			BindVBO(shape.vbo_id);
			BindProgram(shader_program);
			GLint location = glGetUniformLocation(shader_program, "Texture");
			glUniform1i(location, 0);
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLVertex)*shape.vertices.size(), &shape.vertices[0]);

			BindVAO(shape.vao_id);
			GLErrorVal();
			glDrawArrays(GL_LINES, 0, shape.vertices.size());
			GLErrorVal();
			GLErrorVal();

			UnbindAll();
		}
		GLErrorVal();
	}

	void Graphics::SetGLVersion(int major, int minor)
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor);
	}

	void Graphics::BeginDrawing()
	{
		glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		//clear the buffer
	}

	void Graphics::EndDrawing()
	{
		DrawDebugShapes();
		InfoLabel::GetInstance()->Draw();
		SDL_GL_SwapWindow(window_);
	}

	bool Graphics::LoadColor(const std::string & path)
	{
		if(!FileExist(path))
			return true;

		//load
		try
		{
			using json = nlohmann::json;
			std::ifstream file(path);
			json root;
			file >> root;

			for(auto color: root["colors"])
			{
				SDL_Color new_color = {0, 0, 0, 255};
				std::string color_name = ToUpperString(color["name"]);
				std::string color_string = color["rgb"];
				color_string = GetEnclosedSubStrings(color_string,"()")[0];
				auto rgb_values = SplitStringByDelim(color_string, ',');

				new_color.r = (Uint8)(std::stoi(TrimString(rgb_values[0])));
				new_color.g = (Uint8)(std::stoi(TrimString(rgb_values[1])));
				new_color.b = (Uint8)(std::stoi(TrimString(rgb_values[2])));

				color_map_.insert(std::pair<std::string, SDL_Color> (color_name, new_color));
			}
		}
		catch (const std::exception & e)
		{
			return true;
		}

		return false;
	}

	void Graphics::SetWindowSize(const SDL_Point & p)
	{
		SDL_SetWindowSize(this->window_, p.x, p.y);
	}

	void Graphics::DisplayDebugShape(const Rect4F & s, const GLColor& color, GLuint ShaderProgram)
	{
		for (auto &i : debug_shapes_)
		{		//find an available shape, use it
			if (!i.in_use_)
			{
				i.in_use_ = true;
				i.times_unused_ = 0;
				i.vertices.clear();
				SubRect(i, s);
				SubColor(i, color);
				SubRectTexCoord(i, 0);
				BindVAO(i.vao_id);
				BindVBO(i.vbo_id);
				ResizeShapeBuffer(i);
				i.shader_program = ShaderProgram;
				return;
			}
		}
		//create a new AutoDrawShape if not shape is available
		AutoDrawShape shape;
		RegisterShape(shape);
		SubRect(shape, s);
		SubColor(shape, color);
		shape.shader_program = ShaderProgram;
		SubRectTexCoord(shape, 0);
		ResizeShapeBuffer(shape);
		shape.in_use_ = true;
		debug_shapes_.push_back(shape);
	}

	void Graphics::DisplayDebugShape(const GLShape & s, const GLColor & color, GLuint shader_program)
	{
		for (auto &i : debug_shapes_)
		{		//find an available shape, use it
			if (!i.in_use_)
			{
				i.in_use_ = true;
				i.times_unused_ = 0;
				i.vertices = s.vertices;
				SubColor(i, color);
				BindVAO(i.vao_id);
				BindVBO(i.vbo_id);
				ResizeShapeBuffer(i);
				i.shader_program = shader_program;
				return;
			}
		}
		//create a new AutoDrawShape if not shape is available
		AutoDrawShape shape;
		RegisterShape(shape); 
		shape.vertices = s.vertices;
		SubColor(shape, color);
		shape.shader_program = shader_program;
		ResizeShapeBuffer(shape);
		shape.in_use_ = true;
		debug_shapes_.push_back(shape);
	}

	void Graphics::SDLSurfaceToGLTexture(SDL_Surface *surface, GLuint &TextureID_Holder)
	{
		GLErrorVal();
		glGenTextures(1, &TextureID_Holder);
		glBindTexture(GL_TEXTURE_2D, TextureID_Holder);

		int Mode= GL_BGRA;
		int InternalFormat/*number of color components*/ = 4;
		if (surface->format->BytesPerPixel == 3)
		{
			if (surface->format->Rmask == 0x000000ff)
				Mode = GL_RGB;
			else
				Mode = GL_BGR;

			InternalFormat = 3;
		}
		else if (surface->format->BytesPerPixel == 4) {
			if (surface->format->Rmask == 0x000000ff)
				Mode = GL_RGBA;
			else
				Mode = GL_BGRA;
		}

		GLErrorVal();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		GLErrorVal();
		glTexImage2D(GL_TEXTURE_2D, 0, InternalFormat, surface->w, surface->h, 0, Mode, GL_UNSIGNED_BYTE, surface->pixels);
		GLErrorVal();
		glGenerateMipmap(GL_TEXTURE_2D);
		GLErrorVal();
	}

	void Graphics::CompileShader(std::string path, GLuint ShaderID)
	{
		std::ifstream file;
		std::string line;
		std::string fileContent;
		file.open(path);
		while (std::getline(file, line))
		{
			fileContent += line + "\n";
		}
		file.close();
		fileContent += "\0";

		//convert our filecontent in to raw C++ data structure
		const char* fileC = fileContent.c_str();
		glShaderSource(ShaderID, 1, &fileC, 0);
		glCompileShader(ShaderID);

		//error checking
		GLint status;
		glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &status);
		if (status != GL_TRUE)
		{
			std::cout << "Shader Error" << std::endl;

			char buffer[512];
			glGetShaderInfoLog(ShaderID, 512, NULL, buffer);
			std::printf("%s", buffer);
		}
	}

	void Graphics::AttachShader(GLuint Shader, GLuint ProgramID)
	{
		glAttachShader(ProgramID, Shader);
	}

	void Graphics::DrawDebugShapes()
	{
		//draw and clean shapes
		for (auto i = debug_shapes_.begin(); i != debug_shapes_.end();)
		{
			if (i->Draw())
			{
				i = debug_shapes_.erase(i);
				continue;
			}
			i++;
		}
	}

	void Graphics::LinkandCleanShaders(const GLuint &ProgramID, std::vector<GLuint>* shader_ids)
	{
		glLinkProgram(ProgramID);

		// Check the program
		int InfoLogLength = 0;

		glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0)
		{
			std::vector<char> error_message(InfoLogLength + 1);
			glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &error_message[0]);
			printf("%s\n", &error_message[0]);
		}
		for (auto i = shader_ids->begin(); i != shader_ids->end();)
		{
			glDetachShader(ProgramID, *i);
			glDeleteShader(*i);
			i = shader_ids->erase(i);
		}

	}

	void Graphics::UnbindAll()
	{
		BindTexture(0);
		BindVAO(0);
		BindVBO(0);
		BindProgram(0);
		GLErrorVal();
	}

	GLuint Graphics::GetShaderProgram(const std::string & name)
	{
		try
		{
			return shader_program_map_[name];
		}
		catch (...)
		{
			throw("shader_program_ not found");
		}
	}

	void Graphics::BindProgram(GLuint program)
	{
		glUseProgram(program);
	}

	void Graphics::BindVBO(GLuint vbo_id)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vbo_id);
	}

	void Graphics::BindVAO(GLuint vao_id)
	{
		glBindVertexArray(vao_id);
	}

	void Graphics::BindTexture(GLuint texture)
	{
		glActiveTexture(GL_TEXTURE0);		//bind to 0th slot
		glBindTexture(GL_TEXTURE_2D, texture);
	}

	SDL_Point Graphics::GetWindowSize()
	{
		int x, y; SDL_GetWindowSize(window_, &x, &y); return {x, y};
	}

	SDL_Point Graphics::GetTextureWH(const GLuint texture_id)
	{
		BindTexture(texture_id);
		int wh[2];
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &wh[0]);
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &wh[1]);
		return {wh[0], wh[1]};
	}

	GLColor Graphics::GetColor(std::string color_name)
	{
		color_name = ToUpperString(color_name);
		if (color_map_.find(color_name) == color_map_.end())
		{
			return {0, 0, 0, 0};
		}
		else
		{
			return color_map_.at(color_name);
		}
	}

	void Graphics::SetPaths(const std::string &image_directory, const std::string &font_directory)
	{
		if(image_directory.back() != '/')
		{
			//append slash if there is none
			this->image_directory_ = image_directory + '/';
		}
		else
		{
			this->image_directory_ = image_directory;
		}
		if(font_directory.back() != '/')
		{
			//append slash if there is none
			this->font_directory_ = font_directory + '/';
		}
		else
		{
			this->font_directory_ = font_directory;
		}
	}

	bool AutoDrawShape::Draw()
	{
		if (in_use_)
		{
			Graphics::GetInstance()->BlitSurface(shader_program, 0, *this);
			in_use_ = false;
		}
		else
			times_unused_++;

		return times_unused_ > limit_;
	}
}
