#pragma once
#include "Input.h"

#include "InfoLabel.h"

namespace Fengine
{
	void Input::BeginNewFrame()
	{
		pressed_keys_.clear();
		released_keys_.clear();
		wheel_scroll_ = 0;
		is_key_released_ = 0;
		is_key_pressed_ = 0;

		switch (l_button_)
		{
		case BUTTON_PRESSED:
			l_button_ = BUTTON_DOWN;
			break;
		case BUTTON_RELEASED:
			l_button_ = BUTTON_UP;
			break;
		}

		switch (r_button_)
		{
		case BUTTON_PRESSED:
			r_button_ = BUTTON_DOWN;
			break;
		case BUTTON_RELEASED:
			r_button_ = BUTTON_UP;
			break;
		}
	}

	Input::Input() :wheel_scroll_(0),
		l_button_(MouseButtonState(0)),
		r_button_(MouseButtonState(0)),
		is_key_pressed_(0),
		is_key_released_(0)
	{
		Init();
	}

	void Input::KeyDownEvent(SDL_Scancode Key)
	{
		//is_key_pressed_ = true; don't do this way, because computers spam key presses if they key is pressed for a long time
		if (pressed_keys_.count(Key) == 0)
		{
			pressed_keys_.insert(std::pair<SDL_Scancode, bool>(Key, true));
		}
		else
		{
			pressed_keys_[Key] = true;
		}

		if (held_keys_.count(Key) == 0)
		{
			held_keys_.insert(std::pair<SDL_Scancode, bool>(Key, true));
			//this means the key is freshly pressed
			is_key_pressed_ = true;

			//push this into latest keys
			latest_key_press_.insert(latest_key_press_.begin(), Key);
			if (latest_key_press_.size() > 20)
			{
				latest_key_press_.resize(10);
			}
		}
		else if(!held_keys_[Key])
		{
			//this also means the key is freshly pressed
			is_key_pressed_ = true;
			held_keys_[Key] = true;

			//push this into latest keys
			latest_key_press_.insert(latest_key_press_.begin(), Key);
			if (latest_key_press_.size() > 20)
			{
				latest_key_press_.resize(10);
			}
		}
	}

	void Input::KeyUpEvent(SDL_Scancode Key)
	{
		is_key_released_ = false;
		if (released_keys_.count(Key) == 0)
		{
			released_keys_.insert(std::pair<SDL_Scancode, bool>(Key, true));
		}
		else
		{
			released_keys_[Key] = true;
		}
		
		held_keys_[Key] = false;
	}

	int Input::GetWheelEvent()
	{
		return wheel_scroll_;
	}

	bool Input::IsKeyPressed(SDL_Scancode Key)
	{
		if(pressed_keys_.count(Key) == 0)
			return 0;
		return pressed_keys_[Key];
	}

	bool Input::IsKeyReleased(SDL_Scancode Key)
	{
		if(released_keys_.count(Key) == 0)
			return 0;
		return released_keys_[Key];
	}

	bool Input::IsKeyHeld(SDL_Scancode Key)
	{
		if(held_keys_.count(Key) == 0)
			return 0;
		return held_keys_[Key];
	}

	MouseButtonState Input::GetLButtonState()	const
	{
		return l_button_;
	}

	MouseButtonState Input::GetRButtonState()	const
	{
		return r_button_;
	}

	std::vector<SDL_Scancode> Input::GetPlayerMovementKeySet()
	{
		std::vector<SDL_Scancode> ReturnList;
		if (!latest_key_press_.empty())
		{
			for (auto i = latest_key_press_.begin(); i != latest_key_press_.end(); i++)
			{
				if (*i == SDL_SCANCODE_A || *i == SDL_SCANCODE_D || *i == SDL_SCANCODE_S || *i == SDL_SCANCODE_W)
				{
					if (IsKeyHeld(*i)&& (std::find(ReturnList.begin(),ReturnList.end(),*i)==ReturnList.end()))
					{
						ReturnList.push_back(*i);
					}
				}
			}
		}
		return ReturnList;
	}

	void Input::WheelEvent(int Magnitude)
	{
		wheel_scroll_ = Magnitude;
	}

	void Input::CursorUpdate(SDL_Event& CursorEvent)
	{
		//do not use the event to get cursor location
		//CursorLoc.x = CursorEvent.motion.x;
		//CursorLoc.y = CursorEvent.motion.y;
		CursorPositionUpdate();


		//handle button down event
		if (CursorEvent.button.button == SDL_BUTTON_LEFT && CursorEvent.type == SDL_MOUSEBUTTONDOWN)
		{
			l_button_ = BUTTON_PRESSED;
		}
		else if (CursorEvent.button.button == SDL_BUTTON_RIGHT && CursorEvent.type == SDL_MOUSEBUTTONDOWN)
		{
			r_button_ = BUTTON_PRESSED;
		}
		//handle button up event
		else if (CursorEvent.button.button == SDL_BUTTON_LEFT && CursorEvent.type == SDL_MOUSEBUTTONUP)
		{
			l_button_ = BUTTON_RELEASED;
		}
		else if (CursorEvent.button.button == SDL_BUTTON_RIGHT&& CursorEvent.type == SDL_MOUSEBUTTONUP)
		{
			r_button_ = BUTTON_RELEASED;
		}
	}
	void Input::CursorPositionUpdate()
	{
		SDL_GetMouseState(&cursor_loc_.x, &cursor_loc_.y);
	}
	void Input::Init()
	{
		SDL_CEGUI_keymap_[SDLK_1] = CEGUI::Key::One;
		SDL_CEGUI_keymap_[SDLK_2] = CEGUI::Key::Two;
		SDL_CEGUI_keymap_[SDLK_3] = CEGUI::Key::Three;
		SDL_CEGUI_keymap_[SDLK_4] = CEGUI::Key::Four;
		SDL_CEGUI_keymap_[SDLK_5] = CEGUI::Key::Five;
		SDL_CEGUI_keymap_[SDLK_6] = CEGUI::Key::Six;
		SDL_CEGUI_keymap_[SDLK_7] = CEGUI::Key::Seven;
		SDL_CEGUI_keymap_[SDLK_8] = CEGUI::Key::Eight;
		SDL_CEGUI_keymap_[SDLK_9] = CEGUI::Key::Nine;
		SDL_CEGUI_keymap_[SDLK_0] = CEGUI::Key::Zero;

		SDL_CEGUI_keymap_[SDLK_q] = CEGUI::Key::Q;
		SDL_CEGUI_keymap_[SDLK_w] = CEGUI::Key::W;
		SDL_CEGUI_keymap_[SDLK_e] = CEGUI::Key::E;
		SDL_CEGUI_keymap_[SDLK_r] = CEGUI::Key::R;
		SDL_CEGUI_keymap_[SDLK_t] = CEGUI::Key::T;
		SDL_CEGUI_keymap_[SDLK_y] = CEGUI::Key::Y;
		SDL_CEGUI_keymap_[SDLK_u] = CEGUI::Key::U;
		SDL_CEGUI_keymap_[SDLK_i] = CEGUI::Key::I;
		SDL_CEGUI_keymap_[SDLK_o] = CEGUI::Key::O;
		SDL_CEGUI_keymap_[SDLK_p] = CEGUI::Key::P;
		SDL_CEGUI_keymap_[SDLK_a] = CEGUI::Key::A;
		SDL_CEGUI_keymap_[SDLK_s] = CEGUI::Key::S;
		SDL_CEGUI_keymap_[SDLK_d] = CEGUI::Key::D;
		SDL_CEGUI_keymap_[SDLK_f] = CEGUI::Key::F;
		SDL_CEGUI_keymap_[SDLK_g] = CEGUI::Key::G;
		SDL_CEGUI_keymap_[SDLK_h] = CEGUI::Key::H;
		SDL_CEGUI_keymap_[SDLK_j] = CEGUI::Key::J;
		SDL_CEGUI_keymap_[SDLK_k] = CEGUI::Key::K;
		SDL_CEGUI_keymap_[SDLK_l] = CEGUI::Key::L;
		SDL_CEGUI_keymap_[SDLK_z] = CEGUI::Key::Z;
		SDL_CEGUI_keymap_[SDLK_x] = CEGUI::Key::X;
		SDL_CEGUI_keymap_[SDLK_c] = CEGUI::Key::C;
		SDL_CEGUI_keymap_[SDLK_v] = CEGUI::Key::V;
		SDL_CEGUI_keymap_[SDLK_b] = CEGUI::Key::B;
		SDL_CEGUI_keymap_[SDLK_n] = CEGUI::Key::N;
		SDL_CEGUI_keymap_[SDLK_m] = CEGUI::Key::M;

		SDL_CEGUI_keymap_[SDLK_COMMA] = CEGUI::Key::Comma;
		SDL_CEGUI_keymap_[SDLK_PERIOD] = CEGUI::Key::Period;
		SDL_CEGUI_keymap_[SDLK_SLASH] = CEGUI::Key::Slash;
		SDL_CEGUI_keymap_[SDLK_BACKSLASH] = CEGUI::Key::Backslash;
		SDL_CEGUI_keymap_[SDLK_MINUS] = CEGUI::Key::Minus;
		SDL_CEGUI_keymap_[SDLK_EQUALS] = CEGUI::Key::Equals;
		SDL_CEGUI_keymap_[SDLK_SEMICOLON] = CEGUI::Key::Semicolon;
		SDL_CEGUI_keymap_[SDLK_LEFTBRACKET] = CEGUI::Key::LeftBracket;
		SDL_CEGUI_keymap_[SDLK_RIGHTBRACKET] = CEGUI::Key::RightBracket;
		SDL_CEGUI_keymap_[SDLK_QUOTE] = CEGUI::Key::Apostrophe;
		SDL_CEGUI_keymap_[SDLK_BACKQUOTE] = CEGUI::Key::Grave;

		SDL_CEGUI_keymap_[SDLK_RETURN] = CEGUI::Key::Return;
		SDL_CEGUI_keymap_[SDLK_SPACE] = CEGUI::Key::Space;
		SDL_CEGUI_keymap_[SDLK_BACKSPACE] = CEGUI::Key::Backspace;
		SDL_CEGUI_keymap_[SDLK_TAB] = CEGUI::Key::Tab;

		SDL_CEGUI_keymap_[SDLK_ESCAPE] = CEGUI::Key::Escape;
		SDL_CEGUI_keymap_[SDLK_PAUSE] = CEGUI::Key::Pause;
		SDL_CEGUI_keymap_[SDLK_SYSREQ] = CEGUI::Key::SysRq;
		SDL_CEGUI_keymap_[SDLK_POWER] = CEGUI::Key::Power;

		SDL_CEGUI_keymap_[SDLK_NUMLOCKCLEAR] = CEGUI::Key::NumLock;
		SDL_CEGUI_keymap_[SDLK_SCROLLLOCK] = CEGUI::Key::ScrollLock;

		SDL_CEGUI_keymap_[SDLK_F1] = CEGUI::Key::F1;
		SDL_CEGUI_keymap_[SDLK_F2] = CEGUI::Key::F2;
		SDL_CEGUI_keymap_[SDLK_F3] = CEGUI::Key::F3;
		SDL_CEGUI_keymap_[SDLK_F4] = CEGUI::Key::F4;
		SDL_CEGUI_keymap_[SDLK_F5] = CEGUI::Key::F5;
		SDL_CEGUI_keymap_[SDLK_F6] = CEGUI::Key::F6;
		SDL_CEGUI_keymap_[SDLK_F7] = CEGUI::Key::F7;
		SDL_CEGUI_keymap_[SDLK_F8] = CEGUI::Key::F8;
		SDL_CEGUI_keymap_[SDLK_F9] = CEGUI::Key::F9;
		SDL_CEGUI_keymap_[SDLK_F10] = CEGUI::Key::F10;
		SDL_CEGUI_keymap_[SDLK_F11] = CEGUI::Key::F11;
		SDL_CEGUI_keymap_[SDLK_F12] = CEGUI::Key::F12;
		SDL_CEGUI_keymap_[SDLK_F13] = CEGUI::Key::F13;
		SDL_CEGUI_keymap_[SDLK_F14] = CEGUI::Key::F14;
		SDL_CEGUI_keymap_[SDLK_F15] = CEGUI::Key::F15;

		SDL_CEGUI_keymap_[SDLK_LCTRL] = CEGUI::Key::LeftControl;
		SDL_CEGUI_keymap_[SDLK_LALT] = CEGUI::Key::LeftAlt;
		SDL_CEGUI_keymap_[SDLK_LSHIFT] = CEGUI::Key::LeftShift;
		SDL_CEGUI_keymap_[SDLK_LGUI] = CEGUI::Key::LeftWindows;
		SDL_CEGUI_keymap_[SDLK_RCTRL] = CEGUI::Key::RightControl;
		SDL_CEGUI_keymap_[SDLK_RALT] = CEGUI::Key::RightAlt;
		SDL_CEGUI_keymap_[SDLK_RSHIFT] = CEGUI::Key::RightShift;
		SDL_CEGUI_keymap_[SDLK_RGUI] = CEGUI::Key::RightWindows;
		SDL_CEGUI_keymap_[SDLK_MENU] = CEGUI::Key::AppMenu;

		SDL_CEGUI_keymap_[SDLK_KP_0] = CEGUI::Key::Numpad0;
		SDL_CEGUI_keymap_[SDLK_KP_1] = CEGUI::Key::Numpad1;
		SDL_CEGUI_keymap_[SDLK_KP_2] = CEGUI::Key::Numpad2;
		SDL_CEGUI_keymap_[SDLK_KP_3] = CEGUI::Key::Numpad3;
		SDL_CEGUI_keymap_[SDLK_KP_4] = CEGUI::Key::Numpad4;
		SDL_CEGUI_keymap_[SDLK_KP_5] = CEGUI::Key::Numpad5;
		SDL_CEGUI_keymap_[SDLK_KP_6] = CEGUI::Key::Numpad6;
		SDL_CEGUI_keymap_[SDLK_KP_7] = CEGUI::Key::Numpad7;
		SDL_CEGUI_keymap_[SDLK_KP_8] = CEGUI::Key::Numpad8;
		SDL_CEGUI_keymap_[SDLK_KP_9] = CEGUI::Key::Numpad9;
		SDL_CEGUI_keymap_[SDLK_KP_PERIOD] = CEGUI::Key::Decimal;
		SDL_CEGUI_keymap_[SDLK_KP_PLUS] = CEGUI::Key::Add;
		SDL_CEGUI_keymap_[SDLK_KP_MINUS] = CEGUI::Key::Subtract;
		SDL_CEGUI_keymap_[SDLK_KP_MULTIPLY] = CEGUI::Key::Multiply;
		SDL_CEGUI_keymap_[SDLK_KP_DIVIDE] = CEGUI::Key::Divide;
		SDL_CEGUI_keymap_[SDLK_KP_ENTER] = CEGUI::Key::NumpadEnter;

		SDL_CEGUI_keymap_[SDLK_UP] = CEGUI::Key::ArrowUp;
		SDL_CEGUI_keymap_[SDLK_LEFT] = CEGUI::Key::ArrowLeft;
		SDL_CEGUI_keymap_[SDLK_RIGHT] = CEGUI::Key::ArrowRight;
		SDL_CEGUI_keymap_[SDLK_DOWN] = CEGUI::Key::ArrowDown;

		SDL_CEGUI_keymap_[SDLK_HOME] = CEGUI::Key::Home;
		SDL_CEGUI_keymap_[SDLK_END] = CEGUI::Key::End;
		SDL_CEGUI_keymap_[SDLK_PAGEUP] = CEGUI::Key::PageUp;
		SDL_CEGUI_keymap_[SDLK_PAGEDOWN] = CEGUI::Key::PageDown;
		SDL_CEGUI_keymap_[SDLK_INSERT] = CEGUI::Key::Insert;
		SDL_CEGUI_keymap_[SDLK_DELETE] = CEGUI::Key::Delete;
	}
}