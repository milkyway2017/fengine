#pragma once
#include <string>
#include <unordered_map>
#include <SDL_mixer.h>

namespace Fengine
{
	/**
	 * A singleton class that handles all Audio loading & playing
	 * @note the Audio class can be retrieved by calling Audio::GetAudioInstance
	 */
	class Audio
	{
	private:
		Audio();

		/**
		 * stores the list of effects used along with their file paths
		 */
		std::unordered_map<std::string, Mix_Chunk*> effect_list_;
		/**
		 * stores the list of musics used along with their file paths
		 */
		std::unordered_map<std::string, Mix_Music*> music_list_;
	public:
		~Audio();

		/**
		 * gets the singleton Audio class
		 * @return the pointer to the singleton Audio class
		 */
		static Audio* GetAudioInstance()
		{
			static Audio* audio;
			if (audio == NULL)
			{
				audio = new Audio();
			}
			return audio;
		}

		/**
		 * play music, supports file format: WAVE, MOD, MIDI, OGG, MP3, FLAC
		 * @param path the path of the music file
		 * @note a music differs from sound effect, a music is a sound that lasts longer and is not played multiple times in a short period of time
		 */
		void PlayMusic(const std::string & path);

		/**
		 * player sound effect, supports file format: WAVE, AIFF, RIFF, OGG, VOC
		 * @note a sound effect differs from a music, a sound effect is a sound that is played multiple times in a short period of time
		 * @param path
		 */
		void PlaySoundEffect(const std::string & path);
	};

}