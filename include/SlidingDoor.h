#pragma once
#include "Figure.h"
#include "Sprite.h"
#include "InfoLabel.h"
#include <memory>

namespace Fengine {
	class Map;

	struct SlidingDoorDef
	{
		bool isDouble;					//if the door is double or not
		bool isNegative;				//(single door only) is the door '+' or '-', this does not apply to double door because double door has both
		bool isHorizontal;				//if the door is horizontal
		Rect4F spriteRect;				//the left/bottom door's sprite rectangle at an arbitrary location
		Rect4F spriteToBody1;			//(first door)the Rect to add to the spriteRect to get the bodyRect
		Rect4F spriteToBody2;			//(second door)the Rect to add to the spriteRect to get the bodyRect

		/*
		 * (double door only): the delta to add to spriteRect in order to go from the
		 * first door (this) to the
		 * second door (SlidingDoor other)
		 */
		Point2F delta;
		/**
		 * if the door is a double door
		 * the first door is always the '-' door (the door that opens left or up)
		 */
		Rect4F spriteToActiveArea;		//the Rect to add to the spriteRect to get the ActiveArea Rect
		int duration_ms;				//duration of opening in millisecond
		int distance;					//the distance the doors open

		Rect4F source_rect_1;			//store the source texture coordinates
		Rect4F source_rect_2;			
		std::string texture_path;		//the path where the texture of this door can be found
	};
	/**
	 *  SlidingDoor, can be either single or double depending on the ObjDef used to create
	 */
	class SlidingDoor :
		virtual public Figure
	{
	protected:
		inline virtual void ConfigureZOrder()
		{
			z_order_.Init(&body_.collision_rect_.y, std::bind(&SlidingDoor::Draw, this), body_.collision_rect_.h);
		}
	private:
		std::unique_ptr<SlidingDoor> other;						//since this is a double door, this is the 'other' part
		Point2F ClosedLoc, OpenedLoc;			//closedloc and openedloc for sprite
		Rect4F current_sprite_pos_;					
		Point2F ClosedLoc_Fig, OpenedLoc_Fig;	//closedloc and openedloc for collision box
		SDL_Rect SrcRect;			//the texture rect of door
		int Duration;			//how long does it take for the door to go from one end to another
		Sprite sprite;
		int ActivateAreaID;		// if any object presents in this area, this door will be opened or opening
		DoorStatus status;

		Map* map;					//map that holds this door
		GLuint ShaderProgram;

		/**
		 * return true if complete, false if not moving or just started
		 */
		inline bool IsComplete()	const
		{
			switch(status)
			{
			case CLOSED:
			case OPENED:
				return true;
			case CLOSING:
				return (body_.collision_rect_.GetXY() - OpenedLoc_Fig).Norm() >= 
				(ClosedLoc_Fig - OpenedLoc_Fig).Norm();
			case OPENING:
				return (body_.collision_rect_.GetXY() - ClosedLoc_Fig).Norm() >=
				(ClosedLoc_Fig - OpenedLoc_Fig).Norm();
			}
		}

		/**
		 * LoadDef helper
		 */
		static void GetTextureCoord(const nlohmann::json & source, Rect4F * source_rect);
	public:

		/**
		 * get the body of the second door
		 */
		Body* GetOtherBody();

		/**
		 * check if the door is double door
		 * @return if the door is a double door
		 */
		bool isDouble() const
		{
			return (bool)other.get();
		}

		/**
		 * removes any subscription at the event manager
		 */
		void CleanUp();

		/**
		 *  loads the ObjDef for sliding door
		 * @param defPath the path to the ObjDef file
		 */
		static void LoadDef(const std::string & defPath);

		static std::map<std::string, const SlidingDoorDef> defMap;					//stores the defaults for different types of Doors

		/**
		 *  create a sliding door just
		 * @param type the type in the map (one that has a corresponding loaded Def.json file)
		 * @param location if type is horizontal, this is the bottom left location the door reaches when opened
		 *					if type is vertical, this is the bottom left corner the door reaches when opened
	 	 * @param map the pointer to the map object
	 	 * @param shaderProgram the shader program of the map
		 * @param id unique object id issued by Tiled Map Editor
		 */
		SlidingDoor(const std::string & type, Point2F location, GLuint shaderProgram, Map* map, int id = 0);

		/**
		 *  create a single custom sliding door
		 * @warning this constructor is deprecated, should not be used unless for testing purpose
		 * @param collisionRect the collision box of the door
		 * @param ClosedLoc the top left corner of the closed sprite rect
		 * @param OpenedLoc the top left corner of the opened sprite rect
		 * @param WH width and height of the sprite rect at all times
		 * @param imagepath the path to the image file
		 * @param SrcRect the source rect to take the texture from
		 * @param Duration the duration for the door to open in millisecond
		 * @param map the pointer to the map
		 * @param mng the EventManager to the map
		 * @param ActivateArea the ID of the ActiveArea this door is associated with
		 * @param ShaderProgram the shader program used to render
		 * @param id unique object id issued by Tiled Map Editor
		 */
		SlidingDoor(const Rect4F &collisionRect, const Point2F& ClosedLoc, const Point2F & OpenedLoc, const Point2F & WH, const std::string& imagepath, const SDL_Rect& SrcRect, int Duration, Map* map, int ActivateArea, GLuint ShaderProgram, int id = 0);
		~SlidingDoor();

		//handles <activeareafilled> and <activeareaemptied> event raised by ActiveAreas
		void HandleAreaEvent(void * e);

		virtual void Update(int TimeElapsed);

		virtual void Draw();

		/**
		 *  toggles debug mode on/off with a command
		 * @param e the void pointer of the event
		 */
		static void ToggleDebugMode(void * e);

		/**
		 *   this static function subscribe all SlidingDoors to debug mode events, only called once
		 *  @param the event manger to subscribe with
		 */
		static void	SubcribeToDebugMode(EventManager * eventManager);
		static bool SubsribedToDebugMode;				//has the SlidingDoor been subscribe to debugmode event
		static bool isDebugging;						//is the game in debug mode
	};
}

