#pragma once
#include <vector>
#include <map>
#include <iterator>
#include <unordered_map>
#include <string>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <numeric>
#include <algorithm>
#include <tuple>
#include <random>
#include <locale>
#include <boost/bimap.hpp>
#include <boost/assign.hpp>
#include <nlohmann/json.hpp>

#include "Primitive.h"

#ifdef WIN32

#else
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif


/*conventional way of beginnning a CommandProc()*/
#define COMMANDPROCBEGIN 	std::vector<Command> ReturnList = std::vector<Command>();\
 for (auto& i : CommandQueue)\
	{
/*conventional way of ending a CommandProc()*/
#define COMMANDPROCEND 	\
	}\
	CommandQueue.clear();\
	return ReturnList;


#define RANDOM_IDLE_DIRECTION PickRandom({EAST, NORTH, WEST, SOUTH, NORTH_EAST, NORTH_WEST, SOUTH_WEST, SOUTH_WEST})


namespace Fengine
{
	//get the gl error and assert
	static void GLErrorVal()
	{
		GLuint error = glGetError();
		if (error)
			throw("OpenGL error");
	}

	//picks a random element in a given list
	template <class T>
	T PickRandom(std::initializer_list<T> container) {
		static std::default_random_engine re;
		std::uniform_int_distribution<size_t> range{ 0, container.size() - 1 };
		auto random_iterator = container.begin();
		std::advance(random_iterator, range(re));
		return *random_iterator;
	};

	/**
	 * typical angles used by object and tile system
	 */

	static Angle NORTH = Angle(360 - 90);
	static Angle NORTH_EAST = Angle(360 - 45);
	static Angle EAST = Angle(0);
	static Angle SOUTH_EAST = Angle(45);
	static Angle SOUTH = Angle(90);
	static Angle SOUTH_WEST = Angle(135);
	static Angle WEST = Angle(180);
	static Angle NORTH_WEST = Angle(225);
	static Angle UNDEFINED = Angle(std::numeric_limits<float>::infinity());

	/**
	 * find the angle between a vector and positive x
	 * @param v the vector
	 * @param inner true if only want returned angle to be 0-pi
	 * @return the angle
	 */
	extern Angle FindAngle(Point2F v, bool inner = false);

	enum NPCStatus
	{
		PATROLLING,
		FOLLOWING,
		FREEZING,
        ATTACKING
	};

	enum DoorStatus
	{
		CLOSED,
		CLOSING,
		OPENED,
		OPENING
	};

	enum WindowFrameType
	{
		STARTMENU_FRAME,
		GAMEPLAY_FRAME,
		LOADINGGAME_FRAME,
		SETTINGS_FRAME
	};

	enum VAO_ATTRIB_INDEX
	{
		VAO_POSITION = 0,
		VAO_COLOR = 1,
		VAO_TEXTURE_COORD = 2
	};

	/**
	 * extra rectangle utilities
	 */

	extern float GetTop(const Rect4F & r);
	extern float GetLeft(const Rect4F & r);
	extern float GetBottom(const Rect4F & r);
	extern float GetRight(const Rect4F & r);


	/**
 	 *detects the kind of collision between two rectangles
	 *@return 
	 *	1 if obj2 is colliding at obj1's top\n
	 *	2 if obj2 is colliding at obj1's bot\n
	 *	3 if obj2 is colliding at obj1's left\n
	 *	4 if obj2 is colliding at obj1's right\n
	 *	5 if either one is inside another\n
	 *	6 if obj2 is colliding at obj1's topleft\n
	 *	7 if obj2 is colliding at obj1's bottomleft\n
	 *	8 if obj2 is colliding at obj1's topright\n
	 *	9 if obj2 is colliding at obj1's bottomright\n
	 *	0 if not colliding
	 */
	extern int RectCollide(const Rect4F& obj1, const Rect4F& obj2);

	/**
	 * helper function, checks if the project of two rects on x axis collide
	 * @return 
	 *	1 if obj1|obj2\n
	 *	2 if obj2|obj1\n
	 *	3 if oj1|obj2|obj1 or obj2|obj1|obj2\n
	 *	0 no collision\n
	 */
	extern int CollideOnX(const Rect4F& obj1, const  Rect4F& obj2);

	/**
	* helper function, checks if the project of two rects on y axis collide
	* @return
	*	1 if obj1|obj2\n
	*	2 if obj2|obj1\n
	*	3 if oj1|obj2|obj1 or obj2|obj1|obj2\n
	*	0 no collision\n
	*/
	extern int CollideOnY(const Rect4F& obj1, const Rect4F& obj2);

	/**
	 * attempt to merge two rect, will succeed if they share 1 side
	 * @param r1 the first rect
	 * @param r2 the second rect
	 * @return the merged rect if success, Rect4F() if failed
	 */
	extern Rect4F MergeRect(const Rect4F& r1, const Rect4F& r2);

	/**
	 * get all the lines of a GLShape
	 * @param shape the shape
	 * @param is_rect if the shape is rectangle, because a rectangle is stored as 6 vertices in OpenGL, extra steps 
	 * @param shape_enclosed true if the shape is enclosed
	 * are required to convert
	 */
	extern std::vector<Line> GenLines(const GLShape& shape, bool is_rect = true, bool shape_enclosed = true);

	enum ObjectType
	{
		PLAYER_OBJ,
		LYCAN_OBJ,
		DEFAULT_OBJ
	};

	/**
	 * compares the angles of two lines
	 */
	extern bool operator<(const Line& t, const Line& other);

	extern bool PointInRect(const Point2F *p, const Rect4F *r);

	/**
	 * generate a GLShape with rectangular vertices
	 * @param rect the Rect4F to generate from
	 * @param new_vao true if the generated shape is for rendering purpose
	 * @note vertices breakdown:
	 *  vertices[0 to 2] => top-left triangle
	 *  vertices[3 to 5] => bottom-right triangle
	 */
	extern GLShape GenShape(const Rect4F &rect, bool new_vao);

	/**
	 * generate a GLShape with line vertices
	 * @param line the Line to generate shape from
	 * @param new_vao true if the generated shape is for rendering purpose
	 * @note vertices breakdown:
	 *	vertices[0] => line.p1
	 *	vertices[1] => line.p2
	 */
	extern GLShape GenShape(const Line &line, bool new_vao);

	/*sub rect data to <Target>
	*/
	extern void SubRect(GLShape & Target, const Rect4F & Rect);

	//sub a GLColor to GLShape
	extern void SubColor(GLShape & Target, const GLColor& color);

	/**
	 *  sub texture coordinate data into a rectangular GLShape
	 * @param Target the shape to sub
	 * @param TextureRect texture coordinate measured by pixels
	 * @param width the width of the texture
	 * @param height the height of the texture
	 * @param offset the offset to start subbing (default 0)
	 */
	extern void SubRectTexCoord(GLShape & Target, const SDL_Rect& TextureRect, int width, int height, int offset = 0);
	/**
	 *  sub full-texture coordinate data into a rectangular GLShape
	 * @param Target the shape to sub
	 * @param offset the offset to start subbing (default 0)
	 */
	extern void SubRectTexCoord(GLShape & Target, int offset = 0);

	/**
	 *  generate a Rect4F from a rectangle GLShape
	 * @param Target the target GLShape to generate from
	 * @return the Rect4F generated
	 */
	extern Rect4F GenRectFromShape(const GLShape &Target);


	//give the shape a new VAO and VBO ID
	extern void RegisterShape(GLShape & Target);

	//invalidate the VAO and VBO
	extern void UnregisterShape(GLShape & Target);

	//resize the buffers based on the vertices count of target shape
	//VAO and VBO should already be bound before this call
	//pass in vertex_anticipated if the target is empty for now
	extern void ResizeShapeBuffer(GLShape & target, unsigned int vertex_anticipated = 0);

	//check if two LINES intersect
	extern Point2F* DoLinesIntersect(Line l1, Line l2);

	//check if two LINE_SHAPE SEGMENTS intersect, if two segemnts do not intersect, return nullptr
	//warning: if the overlapping segments are on the same line, the returned Point2F* will be arbitrary
	extern Point2F* DoSegmentsIntersect(const Line & l1, const Line & l2);

	//check if a segment<l1> and Ray<l2> intersect, if not, return nullptr
	extern Point2F* DoSegmentandRayIntersect(Line l1, Line ray);

	extern bool IsVertical(Line l);

	extern bool IsHorizontal(Line l);

	//check if a point on a line<p> is also within a segment of the line <l>
	extern bool OnSegment(Point2F p, Line l);

	//check if a point on a line<p> is also within a ray of the line <l.P1->l.P2>
	extern bool OnRay(Point2F p, Line l);

	//find and store all points and intersections and return them
	extern std::vector<Point2F> FindVerticesandIntersections(std::vector<Line> Segments);

	//find and store all the intersection between vector<Line>A and vector<Line>B
	extern std::vector<Point2F> FindABIntersections(const std::vector<Line>& A, const std::vector<Line>& B);

	//find and return all points that are in rect<rect>
	extern std::vector<Point2F> VerticesInRect(Rect4F rect, std::vector<Point2F>& Points);

	//generate the angle range list around a point of line segments
	extern std::vector<std::pair<Angle, Angle>> GenerateAngleRangeList(std::vector<Line> segments, Point2F p);

	//distance between 2 points
	extern float Dist(Point2F p1, Point2F p2);

	//swap two values
	template <class T>
	void Swap(T * a, T * b)
	{
		T temp = *a;
		*a = *b;
		*b = temp;
	}

	/**
	 * check if a is within bounds
	 */
	template <class T>
	bool IsWithin(T a, T bound1, T bound2)
	{
		return ((a >= bound1) && (a <= bound2))||((a >= bound2) && (a <= bound1));
	}

	//returns true of range a overlaps with range b
	template <class T>
	bool DoRangesOverlap(T amin, T amax, T bmin, T bmax)
	{
		return (IsWithin(bmin, amin, amax)) || (IsWithin(bmax, amin, amax)) || (IsWithin(amin, bmin, bmax)) || (IsWithin(amax, bmin, bmax));
	}

	//compares two points regarding to the angle to origin
	extern bool ComparePoints(Point2F a, Point2F b);

	//extend the ray and set the P2 to the extended point,
	//returns false if the extended ray is shorter, true if the extended ray is longer
	//<AngleRangeList>: the list of the system generated by GenerateAngleRangeList()
	extern void ExtendRayInSys(Line & l, std::vector<Line>& sys, std::vector<std::pair<Angle, Angle>> AngleRangeList);

	//return true if they do, false if dont
	bool DoSegmentandRectIntersect(const Rect4F& Shape, const Line& s);

	/**
	 * retrieve the string that's inside delimpair
	 * @param s the string to parse
	 * @param delimpair a pair of delimiter that separates the string, for example "()", "{}", "[]"
	 * @param count the max number of output wanted, default of 0 means as many as there are
	 * @return a list of elements without the delim
	 */
	extern std::vector<std::string> GetEnclosedSubStrings(const std::string & s, const std::string & delimpair, unsigned int count = 0);

	/**
	 * get string in uppercase
	 */
	extern std::string ToUpperString(const std::string & str);


	/**
	 * get string in lowercase
	 */
	extern std::string ToLowerString(const std::string & str);


	/**
	 * get the substring that precedes a character (which does not include c)
	 */
	extern std::string GetSubStrBeforeChar(const std::string & s, char c);

	/**
	 * get the substring that follows a character (which does not include c)
	 */
	extern std::string GetSubStrAfterChar(const std::string& str, char c);

	/**
	 * remove leading or trailing space characters
	 */
	extern std::string TrimString(const std::string &str);

	/**
	 * helper function of SplitStringByDelim
	 */
	template<typename Out>
	extern void split(const std::string & s, char delim, Out result);

	/**
	 * split a string by delim
	 * @param s string to split
	 * @param delim delimiter
	 * @return a vector of string split
	 */
	extern std::vector<std::string> SplitStringByDelim(const std::string &s, char delim);

	/**
	 * find and replace function for string
	 * @param string the string to do the operation on
	 * @param find the text to find in the string
	 * @param replace the text to replace found strings
	 * @return 1 if string found and replaced, 0 otherwise
	 */
	extern bool FindAndReplaceString(std::string & string, const std::string & find, const std::string & replace);

	/**
	 * check if a file exists
	 * @param file_path file path
	 * @return true if file exists
	 */
	extern bool FileExist(const std::string& file_path);

	/**
	 * read json from a file
	 * @param json_object the json object to read the file into
	 * @param file_path the file to read
	 * @return error string, empty when no error
	 */
	extern std::string ReadJson(nlohmann::json & json_object, const std::string file_path);

	namespace Constants
	{
		const static int MAX_FRAME_TIME = 20;
		const static std::map<std::string, SDL_Point> Resolutions = { std::pair<std::string,SDL_Point>("ancient",{640,480}),
			std::pair<std::string,SDL_Point>("720P",{1280,720})
		};

		//the size of item image on map
		const Point2F ItemSpriteSize(30.f, 30.f);
	}



}