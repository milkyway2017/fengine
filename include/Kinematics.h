#pragma once
#include <unordered_map>

#include "Primitive.h"
#include "Utility.h"
#include "Exception.h"

namespace Fengine
{
	//forward decl
	class Quadrant;

	/**
	 * represents speed - one dimension velocity
	 */
	struct Speed
	{
		/**
		 * stores the speed in px/ms
		 */
		std::unordered_map<std::string, float> component_map;

		float speed_sum = 0;					//!< the updated speed in px/ms
		bool is_updated = false;				//!< true if the speed_sum is updated

		/**
		 * get the speed in pixel/ms
		 * @return the SpeedSum
		 */
		float SumSpeed();

		/**
		 * add a speed component in pixels per milisecond
		 * @param key the name of the speed
		 * @param val the speed
		 * @note if a key already exists, the val will be simply be updated
		 */
		void AddSpeedComponent(std::string key, float val);

		/**
		 *  remove a speed component
		 * @param key the name of the speed to remove
		 */
		void RemoveSpeedComponent(std::string key);

	};

	/**
	 * represents velocity (2d) with acceleration, used to track an object's motion
	 * supports uniform accleration, and uniform motion 
	 */
	struct Motion
	{
	private:
		bool is_stopping = false;		//!< if true, accelerate at acceleration until reaching terminal_velocity
										//!< if false, accelerate at -acceleration until reaching (0,0) velocity
		bool is_dirty = true;			//!< if terminal_velocity and current_accleration need to be re-calculated

		Speed terminal_speed;			//!< speed
		Point2F current_velocity = Point2F(0,0);
		Point2F terminal_velocity = Point2F(0, 0);		//!< stores the terminal velocity 
		Point2F stopping_velocity = Point2F(0, 0);		//!< records the terminal velocity when the motion starts to stop 
		Point2F acceleration = Point2F(0, 0);
	public:

		void SetAcceleration(const Point2F & acceleration);

		bool IsStopping()	const;

		/**
		 * stop motion by reversing the direction of acceleration until the velocity is around 0
		 * @note the effect of this function will be discarded if, during the process,
		 * the direction, acceleration, or speed is changed
		 */
		void StopWithAcceleration();

		/**
		 * get the displacement over some amount of time at terminal velocity with accleration in mind
		 * @param time amount of time passed
		 * @return the displacement
		 * @note direction is used to calculate accleration
		 */
		Point2F GetDisplacement(Uint32 time);

		/**
		 * get the direction of this motion
		 */
		Angle GetDirection()	const;

		/**
		 * get the direction of the acceleration
		 */
		Angle GetAccelerationDirection()	const;

		/**
		 * get the current acceleration of the motion object, can be used to determine if it is moving
		 */
		Point2F GetAcceleration()	const;

		/**
		 * @return the pointer to the Speed object owned by this Motion
		 */
		Speed* GetSpeed();
	};

	/**
	 * a kinematic body is one that does not take forces into account
	 */
	class Body
	{
	public:
		Motion motion_;
		bool is_solid_ = false;						//!< if a body is solid, it responds to collision
		bool is_movable_ = false;					//!< if the body can be moved by collision response, for example,
													//!< walls will have this set to false

		GeometryType type_ = RECT_SHAPE;			//!< stores the type stored in anon. union, point and polyline are not
													//!< yet supported

		bool is_recording_ = false;					//!< if the body is recording the distance it has travelled
		float dist_travelled_ = 0;					//!< distance travelled recorded

		union
		{
			Rect4F collision_rect_ = Rect4F();
			Circle collision_circ_;
			Line collision_line_;
		};
		GLShape collision_polygon_;

		std::vector<Point2F*> points_attached_;

		/***************************************************
		 *************  constructors ***********************
		 ***************************************************/

		Body(){};
		Body(Rect4F rect, bool is_solid, bool is_movable);
		Body(Circle circle, bool is_solid, bool is_movable);
		Body(Line line, bool is_solid, bool is_movable);
		Body(GLShape polygon, bool is_solid, bool is_movable);
		Body(const Body& other);
		Body& operator=(const Body& other);
		~Body(){};

		/**
		 * attach a point to this body, such as a sprite or an item
		 * the point will move with this body
		 * @param point the pointer to the point to attach
		 */
		void Attach(Point2F * point);

		/**
		 * detach a point
		 * @param point the point to detach
		 */
		void Detach(Point2F * point);

		/**
		 * get the center of the collision shape
		 */
		Point2F GetCenter()	const;

		/**
		 * get the bound of this body
		 */
		Rect4F GetBound()	const;

		/**
		 * update this body
		 * @return true if the body moved, false otherwise
		 */
		bool Update(unsigned int time_passed);

		/**
		 * move the center of this body to a position
		 */
		void MoveCenterTo(const Point2F & point);

		/**
		 * move the center of this body by some displacement
		 */
		void MoveCenter(const Point2F & delta);

		/**
		 * resolve collision between two bodies
		 * @param other the body to resolve collision with
		 */
		void ResolveCollision(Body& other);

		/**
		 * get the glshape that represents this body graphically
		 */
		GLShape GetShape()	const;

		/**
		 * draw the collision body
		 */
		void DrawDebugShape()	const;
	};

	/**
	 * represents a kinematic world, where forces are neglected
	 */
	class World
	{
		std::vector<Body*>	bodies_;			//!< all the bodies in this world
												//!< @note the world does not the ownership of the bodies 

		std::unique_ptr<Quadrant> quadtree_;	//!< stores elements of bodies_ in a quadtree structure
	public:
		/**
		 * default constructor
		 */
		World() = default;

		/**
		 * constructor
		 * @param bound the edge of this world, used to initialize quadtree
		 */
		World(const Rect4F & bound);

		/**
		 * update the world, handle movement of bodies & collision
		 */
		void Update(unsigned int time_passed);

		/**
		 * @param body body to add
		 * @note although the world does not own this body, the world is responsible for calling Update() and handling
		 * collision between bodies
		 */
		void AddBody(Body* body);

		/**
		 * @param body body to remove, the world does not own this body,
		 *  therefore the caller is responsible for the destruction of the body
		 *  when neccessary
		 */
		void RemoveBody(Body * body);
	};

	namespace Collision
	{
		/*********************************************************************
		 * functions that detect collision and return the collision response *
		 * always return how much B should be moved to resolve collision     *
		 *********************************************************************/
		Point2F Rect_Rect(const Rect4F & A, const Rect4F & B);
		Point2F Circle_Rect(const Circle & A, const Rect4F & B);
		Point2F Line_Rect(const Line & A, const Rect4F & B);
		Point2F Line_Line(const Line & A, const Line & B);
		Point2F Line_Circle(const Line & A, const Circle & B);
		Point2F Circle_Circle(const Circle & A, const Circle & B);
		//an implementation of the separated axis theorem
		Point2F Polygon_Polygon(const GLShape & A, const GLShape & B);

		/********************
		 * helper functions *
		 ********************/
		void ProjectPolygonOntoAxis(const Vector2F & axis, const GLShape & polygon, float* min_pos, float* max_pos);
		float GetIntervalDistance(float min_A, float max_A, float min_B, float max_B);
	}

	/**
	 * a modified implementation of the quadtree algo.
	 * splits a plane into 4 quadrants, recursively. A quadrant contains bodies and potentially 4 sub quadrants
	 */
	class Quadrant
	{
		static unsigned int max_body_count_;				//!< max number of bodies a quadrant can hold
		unsigned int level_;					//!< the level of this quadrant in the tree
		std::vector<Quadrant> quadrants_;		//!< array of sub quadrants, only allocated when Split is called
		std::vector<Body*> bodies_;				//!< contains the bodies in this quadrant, the quadrant does not 
												//!< have the ownership of the bodies 
		Rect4F bound_;							//!< bound of this quadrant


		/*************************************
		 ** private helpers ******************
		 *************************************/

		/**
		 * split the quadrant into 4 smaller ones, and
		 * move all bodies in the current quadrant into the newly created ones
		 */
		void Split();
	public:
		Quadrant(int level, const Rect4F & bound);

		/**
		 * insert the a body into this quadrant
		 * @param body the body to add 
		 * @return true if the body is successfully added to this quadrant or any sub-quadrants
		 * @note the quadrant does not own this body
		 */
		bool Insert(Body* body);

		/**
		 * remove the body from this tree, useful for bodies that move (just remove and re-insert to update the quadtree). 
		 * @param body the body to remove
		 * @return true if the body is successfully removed from this quadrant or any sub-quadrants
		 */
		bool Remove(Body* body);

		/**
		 * retrieve all possible bodies this rect can collide with
		 * @param rect the bound to check
		 * @return a list a bodies the rect can collide with
		 */
		std::vector<Body*> Retrieve(const Rect4F & rect);
	};
}