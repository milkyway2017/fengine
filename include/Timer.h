#pragma once
#include <SDL.h>
#include "Functor.h"
#include <map>
#include <vector>
#include <tuple>

namespace Fengine
{
	//fwd declaration
	class Timer;
	typedef std::unique_ptr<Timer> Timer_ptr;

	/**
	 * class that allows scheduled events (with the help of functor). The constructor of Timer is private because all Timer should be set using
	 * Timer::AddTimer.
	 */
	class Timer
	{
	private:
		Timer(Uint32 time, Functor f);
		static std::vector<std::pair<Timer_ptr, void*>> timers_; 			//a table that stores all Timers and param

		//t is time for callback
		//p is a pointer to
		static Uint32 SDLTimerCallback(Uint32 t, void* p);
	public:
		Functor f_;					//the functor this Timer calls when time up
		int timer_id_;			//ID of timer
		int time_started_;		//when the timer is started/unpaused
		int time_;				//time set for timer when starting/unpausing, useful when calculating how much timer is left when pausing

		/**
		 * add a timer
		 * @param time time in ms
		 * @param f functor to call when time is up
		 * @param param the param that is passed to f when the time is up
		 * @return a pointer to Timer object, which can be used to remove timer with Timer::RemoveTimer
		 * @note timer is automatically deleted after calling the functor
		 */
		static Timer* AddTimer(Uint32 time, Functor f, void* param);

		/**
		 * remove a timer
		 * @param timer_id the pointer to timer
		 */
		static void RemoveTimer(Timer* timer);

		/**
		 * pause all timers
		 */
		static void PauseAllTimers();

		/**
		 * pause this timer
		 * @return false if already paused
		 */
		bool Pause();

		/**
		 * unpause this timer
		 * @return false if already running
		 */
		bool Unpause();

		/**
		 * @return the ammount of time left before timer runs out, returns time_ if timer is paused
		 */
		int GetTimeLeft()	const;

		~Timer();
	};
}
