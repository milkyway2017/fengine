#pragma once
#include "Tile.h"
#include "Object.h"
#include "Kinematics.h"
#include "Input.h"
#include "Camera.h"
#include "TileLayer.h"
#include "Player.h"
#include "Flashlight.h"
#include "Logger.h"
#include "Lycan.h"
#include "SlidingDoor.h"
#include "ActiveArea.h"
#include "RenderSorter.h"
#include "EventManager.h"
#include "Graphics.h"
#include "Script.h"
#include "Label.h"

#include <memory>
#include <MicroPather/micropather.h>
#include <mutex>

namespace Fengine
{
	/**
	 * a structure that describes the properties of a tile in a tile set
	 */
	struct TileProperties
	{
		std::vector<Figure> collision_bodies = std::vector<Figure>();			//!< stores the collision shapes in this tile, coordinates are relative to this tile
														//!< collision bodies in this vector have 0 as id
		float z_order_offset = 0;							//!< z order offset of this tile, useful for wall tiles
	};

	/**
	 * Map's internal structure used for map loading, this corresponds to a tile set in Tiled Map Editor
	 */
	struct TileSet
	{
		GLuint texture;				//!< texture used by this tile set
		std::string image_path = "";
		unsigned int first_gid = 0;
		unsigned int tile_width = 0;
		unsigned int tile_height = 0;
		unsigned int row_count = 0;
		unsigned int column_count = 0;
		unsigned int tile_count = 0;
		std::map<unsigned int, TileProperties> tiles_properties;		//!< contains all tile properties in this tile set
																		//!< this stores <gid, tileproperties>
		TileSet()
		{
		}
		~TileSet()
		{}
		void CleanUp()
		{
			Graphics::GetInstance()->DeleteTexture(texture);
		}

		/**
		 * finds the source rect for a tile given the tileset gid
		 * @param tileset_gid the tileset gid
		 * @return the source rect for that tile
		 */
		SDL_Rect GetSourceRect(unsigned int tileset_gid);
	};

	/**
	 * every object in Tiled translates into an EntityInfo, which remains constant throughout the Map's life.
	 * this structure can help map query information.
	 */
	struct EntityInfo
	{
		std::string name;		//not unique
		int id;					//unique across all objects
		std::string type;		//type of this entity
		void* object;			//pointer to Fengine object that represents this entity
	};

	/**
	 * represents a map
	 */
	class Map : public micropather::Graph
	{
	public:
		Map();

		//copy constructor
		Map(const Map& Copy) = delete;

		//copy assignment
		Map& operator=(const Map& other) = delete;

		//move constructor
		Map(Map&& Other) = delete;

		//move assignment
		Map& operator=(Map&& other) = delete;


		Map(const std::string & path, Logger* systemLog, bool is_active);
		~Map();

		void CleanUp()	//clean up the textures used 
		{
			for (auto& i : tilesets_)
			{
				i.CleanUp();
			}
		}

		friend void Flashlight::Update(Uint32 ElapsedTime, Map* MapInfo);
		friend void Flashlight::Init(Map * info);
		friend void Flashlight::Draw();

		void AddCommand(Command c)
		{
			CommandQueue.push_back(c);
		}

		//draws the map
		void Draw();

		std::vector<Command> Update(Input & input, int time_passed);
		void SetCameraToPlayer();


		bool is_active_;			//!< a map should only be updated/drawn when it's active, true by default

		/************************Map Utilities**********************************/
		//Get list of objects at a location (sprite rect is used to determine)
		std::vector<Object*> GetObjectAtLoc(Point2F location);

		//finds the ground layer
		TileLayer* GetGroundLayer();

		//get the tile the Point resides in
		Tile* GetInvolvedTile(Point2F p);

		//get all tiles that contain the line
		std::vector<Tile*> GetInvolvedTiles(Line l);

		//return a list of Items in range, returns maximum of itemcount of items
		std::vector<GameItem*> GetItemInRange(Point2F Location, float range, int itemcount);

		//return a list of Objects in range, returns maximum of objectcount
		std::vector<Object*> GetObjectInRange(Point2F Location, float range, int objectcount, Object* exclude = nullptr);

		/**
		 * get the objects in a direction
		 * @param caller the caller
		 * @param DistRange the distance range to check
		 * @param direction the direction to check
		 * @param AngleRange the angle range in the direction to check
		 * @return a list of objects in the specified area
		 */
		std::vector<Object*> GetObjectInDirection(Object* caller, float DistRange, Angle direction, Angle AngleRange);

		Point2F GetCursorLocInMap();

		//return number of object in certain area of the map
		int ObjectsInArea(const Rect4F & area);

		//find path from one position to another Position, given a vector that stores the path
		void FindPath(const Point2F & StartPos, const Point2F & EndPos, std::vector<Tile*>* path);
		std::vector<Tile*> FindPath(const Point2F & StartPos, const Point2F & EndPos);

		/**
		 * find the adjacent tile in direction
		 * @param t tile
		 * @param Direction direction of adjacent tile (must be one of the predefined Angles (see what follows the definition of struct Angle))
		 * @return
		 */
		Tile* GetAdjacentTile(Tile* t, Angle Direction);

		//gets the display size of a tile
		inline SDL_Point GetTileSize()
		{
			return { tile_width_, tile_height_ };
		}

		//gets the map size in tiles
		inline SDL_Point GetMapSize()
		{
			return { map_width_, map_height_ };
		}

		//check if a point is inside the defined map area
		bool IsInsideMap(Point2F p);

		///  get the event manager
		/// @return the event manager of the map
		inline EventManager* GetEventManager() { return &event_manager_; }

		/**
		 *  add an ActiveArea to the map
		 * @param activeArea the heap allocated active area to add
		 * @code
		 * //example of adding an active area
		 * Area_ptr area = std::make_shared<ActiveArea>(Rect4F(0,0,100,100),map);
		 * map->AddActiveArea(area);
		 * @endcode
		 */
		void AddActiveArea(Area_ptr activeArea);

		/**
		 * enter another loaded map
		 * @param map_name file name of the map, this function searches if any substring in the absolute paths of maps match
		 * @note pass in empty map_name (default) to enter this map
		 * @param target_active_area the name of the active area the player is going to land in
		 */
		void EnterMap(const std::string & map_name = "", const std::string & target_active_area = "");

		/**
		 *  get an ActiveArea in map given its ID
		 * @param area_id the ID of the ActiveArea
		 * @return the pointer to the area
		 */
		ActiveArea* GetActiveAreaByID(int area_id);

		/**
		 * get an ActiveArea in map given its name assigned in Tiled
		 * @param name name in Tiled
		 * @return the pointer to the area
		 */
		ActiveArea* GetActiveAreaByName(const std::string & name);

		/**
		 * get the player
		 * @return the pointer to player if the player is alive, otherwise returns nullptr
		 */
		Object* GetPlayer();

		/**
		 *  get the object at a location in map
		 * @param location the location in map
		 * @return the pointer to the Object* at that location, returns nullptr if no object is at location
		 */
		Object* GetObjectByLocation(const Point2F & location);

		/**
		 * @param id unique object id issued by Tiled Map Editor
		 */
		Object* GetObjectByID(int id);

		/**
		 * @param id unique object id issued by Tiled Map Editor
		 */
		GameItem* GetItemByID(int id);

		/**
		 * @param id unique object id issued by Tiled Map Editor
		 */
		Figure* GetFigureByID(int id);

		/**
		 * get the camera of the map
		 */
		Camera* GetCamera();

		/**
		 * get the render sorter of the map
		 */
		RenderSorter* GetRenderSorter();

		/**
		 * get the shader program of the map
		 */
		GLuint GetShaderProgram() const;

		/**
		 * go through entities_ and return all EntityInfo with the given name
		 * @param name name given in Tiled
		 * @return entities that are given the name
		 */
		std::vector<EntityInfo> GetEntitiesByName(const std::string & name);

		/**
		 * go through entities_ and return the EntitiyInfo with the given id, which
		 * is issued by Tiled
		 * @param id unique id generated by tiled
		 * @return the entity info pointer (DO NOT STORE THIS, it may become invalidated)
		 */
		EntityInfo* GetEntityByID(int id);

		/**
		 * encouraged for situations where the string referring to an entity may contain either the name or id
		 * @param name_id the string that identifies the entity
		 * @return a vector of EntityInfos that satisfy
		 */
		std::vector<EntityInfo> GetEntityByNameOrID(const std::string & name_id);

		/**
		 * @return the world of the physics engine
		 */
		World* GetWorld();
		/***********************************************************************/

		/***************************script commands functions, all return immediately*******************/
		/**
		 * take over player control
		 * @param on_off true to take over, false to free
		 */
		void TakeOverPlayerControl(bool on_off);
		/**
		 * functor of TakeOverPlayerControl, called in Script::RunNext
		 * @param v pointer to a vector<ScriptCommandParam>: {bool: true to take over control}
		 */
		Functor take_over_p_c_functor_ = Functor([this](void* v)
		{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);
			this->TakeOverPlayerControl(param_list->at(0));
		});

		/**
		 * make an object speak
		 * @param object_speaking the object that's speaking
		 * @param speech the speech
		 */
		void CharacterSpeak(Object* object_speaking, const std::string& speech);
		/**
		* functor of CharacterSpeak, called in Script::RunNext
		* @param v pointer to a vector<ScriptCommandParam>: {string: object name/id, string: speech}
		*/
		Functor character_speak_functor_ = Functor([this](void* v)
		{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);
			this->CharacterSpeak((Object*)(void*)param_list->at(0), param_list->at(1));
		});

		/**
		 * show red hint text at the bottom of the screen
		 * @param hint the hint text to display
		 */
		void ShowHintText(const std::string & hint);
		/*
		 * functor of ShowHintText
		 * @param v pointer to a vector<ScriptCommandParam>: {<string: hint text to show>}
		 */
		Functor show_hint_text_ = Functor([this](void* v)
	  	{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);
			this->ShowHintText((std::string)param_list->at(0));
	  	});

		/**
		 * delete a hint text
		 * @param hint the hint text to delete
		 */
		void DeleteHintText(const std::string & hint);
		/**
		 * functor of DeleteHintText
		 * @param v pointer to a vector<ScriptCommandParam>: {<string: hint text to delete>}
		 */
		Functor delete_hint_text_ = Functor([this](void* v)
		{
	  		auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);
			this->DeleteHintText((std::string)param_list->at(0));
		});

		/**
		 * clear any dialogue text on screen
		 */
		void EndSpeak();
		/**
		 * functor of EndSpeak
		 */
		Functor end_speak_ = Functor([this](void* v)
	 	{
			this->EndSpeak();
		});

		/**
		 * walk an object to a position
		 * @param object the object walking
		 * @param point the destination
		 */
		void WalkObjectTo(Object* object, const Point2F & point);
		/**
		 * functor of WalkObjectTo
		 * @param v pointer to a vector<ScriptCommandParam>: {string: name/id of the walking object, point: the destination}
		 */
		Functor walk_object_to_ = Functor([this](void* v)
		{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);
			auto entities = this->GetEntityByNameOrID((std::string)param_list->at(0));
			if(entities.empty())
			{
				//log error
				this->logger_.AddLine((std::string)param_list->at(0) + " is not an entity!");
				return;
			}
			this->WalkObjectTo((Object*)(entities.at(0).object), (Point2F)param_list->at(1));

		});

		/*
		 * control an figure's transparency
		 * @param figure the name of the figure specified in Tiled
		 * @param the target amount of opaqueness 
		 * @param the amount of time the fading takes
		 */
		void FadeFigure(Figure* figure, float target_amount, int time);
		Functor fade_figure_ = Functor([this] (void* v)
		{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);
			auto entities = this->GetEntityByNameOrID((std::string)param_list->at(0));

			if (entities.empty())
			{
				//log error
				this->logger_.AddLine((std::string)param_list->at(0) + " is not an entity!");
				return;
			}
			this->FadeFigure((Figure*)entities.at(0).object, (float)param_list->at(1), param_list->at(2));
		});

		/**
		 * zoom the camera from current scale to a new scale
		 * @param scale the new scale
		 * @param time the time taken to finish zooming
		 */
		void ZoomCamera(float scale, int time);
		Functor zoom_camera_ = Functor([this](void* v)
		{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);

			this->ZoomCamera((float)param_list->at(0), (int)param_list->at(1));
		});

		/**
		 * translate the camera from current position to a new position 
		 * @param position the new position 
		 * @param time the time taken to finish translating
		 */
		void TranslateCamera(const Point2F & position, int time);
		Functor translate_camera_ = Functor([this](void* v)
		{
			auto param_list = static_cast<std::vector<ScriptCommandParam>*>(v);

			this->TranslateCamera((Point2F)param_list->at(0), (int)param_list->at(1));
		});

		/**
		 * functor that uses SetCameraToPlayer 
		 */
		Functor set_camera_to_player_ = Functor([this](void*)
		{
			this->camera_.MoveTo(this->player_->GetCenter(),1000,Functor([=](void*)
			{
				this->SetCameraToPlayer();
			}));
		});
		/***********************************************************************/

		/*************************micropather::Graph functions********************/

		float LeastCostEstimate(void* stateStart, void* stateEnd) final;
		void AdjacentCost(void* state, std::vector< micropather::StateCost > *adjacent) final;
		void  PrintStateInfo(void* state) final {}

		/***********************************************************************/
	private:

		static std::vector<Map*> maps_;					//contains all the maps in game, used for inter-map communication

		std::string file_path_;							//the json file from which this map is loaded

		std::vector<TileSet> tilesets_;					//list of tileset used in current map ***all the textures are stored here***

		std::vector<TileLayer> layers_;					//list of all the tiles in current map
		Rect4F map_rect_;	SDL_Rect map_rect_int_;		//the defined rect of the map
		int map_width_, map_height_;					//the w and h of map (number of tiles)
		int tile_width_, tile_height_;					//the w and h of each tile in this map

		std::vector<Point2F> intersect_points_;			//list of end points and intersection points of lines
		std::vector<Line> lines_;						//list of collision/boundary lines in map
		GLuint shader_program_;							//shader program

		Logger logger_;									//internal logger of this map
		Logger* logger_system_;							//shared logger of the entire program

		RenderSorter render_sorter_;					//sorts the object's z order when drawing
		Camera camera_;									//the camera

		EventManager event_manager_;					//in game event system
		Input* input_;									//a pointer to the input object of the game
		std::vector<Command> CommandQueue;				//stores all commands
		micropather::MicroPather path_solver_;

		std::vector<Command> CommandProc();				//process all commands in CommandQueue

		Player* player_;									//the player pointer, this is plain pointer because the Obj_ptr holding the player is stored in objects_
		bool player_has_control_;						//true if player has on the Player object

		World world_;									//physics engine
		std::vector<Obj_ptr> objects_;					//contains all the objects
		std::vector<Object*> dead_objects_;				//!< the object that died in current update, this will be removed at the end of the update
		std::vector<Figure_ptr> figures_;				//contains all the figures
		std::vector<Item_ptr> items_;					//contains all the items of the map which are dropped
		std::vector<Area_ptr> areas_;					//contains all active areas in the map
		std::vector<Script> scripts_;					//contains all scripts
		std::vector<EntityInfo> entities_;				//contains all objects loaded from the map's json

		Label dialogue_;								//displays character dialogue
		LabelStyle dialogue_style_;						//dialogue's font style

		LabelStyle hint_style_;							//hint text's style
		std::map<std::string, Label> hint_texts_;		//all the hint text labels with their texts

		std::map<bool, std::vector<std::string>> pending_hint_changes_;			//queue of labels to be added(0)/delete(1), this is necessary because the script's thread cannot call OpenGl
		std::mutex pending_hint_changes_mutex_;			//protects pending_hint_changes_

		int is_fading_;									//0 for not fading, 1 for fading in, -1 for fading out
		float transparency_;							//0 for transparent, 1 for opaque
		Functor fade_callback_;							//called when transparency_ reaches 0 or 1
		/*************************map private utilities********************/

		/**
		 * Generate end points specifically for ray casting
		 */
		void GenIntersectionPoints();

		/**
		 * Helper function, loads an ObjDef with unknown target type (one of Lycan, Player, Slidingdoor, etc), this function will take care of it
		 * @param DefPath the path to the ObjDef.json
		 * @param systemLog the logger of the system, used to display error message instead of throw exception
		 */
		static void LoadObjDef(const std::string & DefPath, Logger* systemLog);

		//get the specified column of tiles from begin to end
		//https://imgur.com/a/ZAZDH
		std::vector<Tile*> GetTileColumn(int columnNum, int begin, int end);

		/**
		 * helper function, given the raw tile gid read from map file, find the tile set this gid uses
		 * @return the index of the tile set in TSList, returns -1 if error occurs
		 */
		unsigned int FindTileSet(unsigned int gid);

		/**
		 * add a gameitem to the map
		 * @param item the pointer of the new item
		 */
		void AddGameItem(GameItem* item);

		//remove the game item at index <i> in item_list_
		void RemoveGameItem(int i);
		//remove the game item
		void RemoveGameItem(GameItem* item);

		/**
		 * spawn an Object
		 * @param type the ObjType of the object to spawn
		 * @param location the location to spawn
		 */
		void AddObject(ObjectType type, Point2F location, int id = 0);

		//remove the object at index <i> in objects_
		void RemoveObject(int index);
		//remove the object
		void RemoveObject(Object* obj);

		/**
		 * add a Figure to the map
		 * @param figure a pointer to a HEAP allocated Figure
		 * @param drawable if the figure is visible/drawable 
		 */
		void AddFigure(Figure* figure, bool drawable = false);

		/**
		 * remove a Figure from map
		 * @param the index of the figure in figures_
		 */
		void RemoveFigure(unsigned int index);

		/**
		 *  called by the GameEventManager when an ATTACK event is fired,
		 * this functions logs the attack and inform the attacked object about it
		 * @warning this function is not intended for direct use, it should serve as a functor callback
		 */
		void HandleAttackEvent(void* e);

		/**
		 *  called by the GameEventManager when an DIE event is fired,
		 * this functions logs the death and removes the died Object
		 * @warning this function is not intended for direct use, it should serve as a functor callback
		 */
		void HandleDieEvent(void* e);

		/**
		 * the function which loads the json map file and parse it
		 * @param path the path to the json map file
		 */
		void LoadMap(const std::string & path);

		/**
		 * where the map initializes the fonts styles it uses and labels
		 */
		void InitLabels();

		/**
		 * subscribe events that are essential to the map's operation
		 */
		void SubscribeEvents();

		/**
		 * add/delete labels based on pending_hint_changes_
		 */
		void HandlePendingHints();

		/**
		 * fades in / out, depending on the value of is_fading_ and transparency_
		 * @param fade_amount the change in transparency
		 * @param in_out 1 for fading in, -1 for fading out, 0 (default) would fade depending on is_fading_
		 * @param fade_callback optional callback when transparency reaches 0 or 1, this function does not allow parameter
		 * @return true if transparency changed, false otherwise
		 */
		bool FadeMap(float fade_amount, int in_out = 0, Functor fade_callback = Functor());

		/**
		 * get a map from maps_ by its filename
		 * @param name map's filename
		 * @return pointer to the map
		 */
		static Map* GetMapByName(const std::string & name);
		/******************************************************************/
	};

}