#pragma once
#include "Object.h"

namespace Fengine
{

	/**
	 * A very abstract class that serves as a Base class to all AI bots:
	 * a Bot is a finite-state machine, which means it has a few states and what it does is dependent on the state,
	 * if a class derives from Bot, it will not be able to override Update(), the derived class can only:
	 * 		- override Bot::Patrol(), Bot::Follow(), Bot::Attack(), Bot::Freeze() to customize what the Bot does when in those phases
	 * 		- override Bot::SetNPCStatus(), which allows the derived Bot to control what happens when changing from one state to another
	 */
	class Bot:
		public Object
	{
	private:	
	public:
		/**
		 * initializes the Bot and the underlying Object class
		 * @note see Object::Init_Obj for list of things to do before calling this function
		 * @param map the pointer to the map
		 * @param collision_rect the collision rect
		 * @param shader_program the map's shader program
		 */
		void Init_Bot(Map *map, Rect4F collision_rect, GLuint shader_program, int id = 0);

		/**
		 * updates the bot: one of Bot::Patrol(), Bot::Follow(), Bot::Attack(), Bot::Freeze() will be called depending of the state of the Bot
		 * @param time_passed the amount of time passed since last update
		 * @param data for Bot, this is always nullptr as the Bot does not need any extra data to operate
		 */
		virtual void Update(int time_passed, void * data);

		/**
		 * constructor of Bot, this should be called in the derived class's constructor to allocate memory
		 * @note do not call this to initialize a bot, please call Bot::Init_Bot to do so
		 * @param t the type of the bot
		 */
		Bot(ObjectType t);
		virtual ~Bot();

		/**
		 * clone this bot at a location in map, this function should be provided by the derived class, Lycan::Clone is a good example of Clone's implementation
		 * @param location the location in map the put the cloned Bot
		 * @return an unique_ptr to the cloned Bot
		 */
		virtual Obj_ptr Clone(Point2F location) = 0;

	protected:
		/*********Used By Bot class, implemented by derived***************************/

		/**
		 * bot-specific logic for when it's PATROLLING, see Lycan::Patrol for a detailed example
		 * @note must be implemented in derived classes
		 */
		virtual void Patrol() = 0;

		/**
		 * bot-specific logic for when it's FOLLOWING, see Lycan::Follow for a detailed example
		 * @note must be implemented in derived classes
		 */
		virtual void Follow() = 0;

		/**
		 * bot-specific logic for when it's ATTACKING, see Lycan::Attack for a detailed example
		 * @note must be implemented in derived classes
		 */
		virtual void Attack() = 0;
		/**
		 * bot-specific logic for when it's FREEZING, see Lycan::Freeze for a detailed example
		 * @note must be implemented in derived classes
		 */
		virtual void Freeze() = 0;

		/**
		 * checks to see if the player is in vision
		 * @note this must be implemented in derived classes
		 * @return true if is in vision, or false if is not in vision
		 */
		virtual bool IsPlayerInVision() = 0;
		/********************************************************************************/

		/******************************NPC Stats******************************************/
		float health_;				//!< current health of the Bot
		float health_regen_;		//!< health regen/ms
		float max_health_;			//!< max health
		float damage_;				//!< attack damage
		float detection_;			//!< the ability to detect
								//!< @note this is not detection RANGE! this and the detection_range_ together determines if a Bot detects something else
								//!< @todo add detection range
		float speed_;			//!< the base terminal_speed, a constant value that does not change throughout the game in pixel/ms
								//!< however any derived Bot can change it's actual terminal_speed whenever is required by calling Speed::AddSpeedComponent on Object::GetSpeed
		float stamina_;			//!< stamina is an ability to do something of the Bot's choice, for Lycan, stamina is consumed when it's sprinting
		/*********************************************************************************/

	    /*******************************Bot Update Stats**********************************/
	    /**
	     * updates the Bot stats
	     */
		void UpdateStats(int time_passed) final;

		/***************Bot Event Handling*************************************************************/

		//TODO: change the way Bot's Event handling works
		/**
		 * this is a common functionality all Bots can use, Bots choose to call this function in their HandleSpecialEvent to handle basic Bot Events
		 */
		void HandleSpecialEvent_Bot(Event e);
		/**
		 * this must be implemented in derived classes, it's required by Object class for event handling
		 * 		Logic: 
		 *		- Bot::HandleSpecialEvent is called by Object class
		 *		- Since Bot does not hold an implementation of HandleSpecialEvent(), the derived NPC class's implementation will be called
		 *		- The derived NPC class's implementation will process the event if it wants to, it can pass the event to Bot::HandleSpecialEvent_Bot
		 *		- HandeSpecialEvent_Bot() processes the commands known to it except that when the Event is unknown to it, it calls Event::CannotResolve
		 */
		virtual void HandleSpecialEvent(Event e) = 0;
		/**********************************************************************************************/

		/**
		 * sets the NPC to a specific status instead of directly changing
		 * allows for special logic, i.e. load a weapon when start following
		 */
		virtual void SetNPCStatus(NPCStatus s) = 0;

		NPCStatus npc_status_;
	};
}

