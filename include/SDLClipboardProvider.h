#include <SDL.h>
#include <CEGUI/CEGUI.h>
#include <iostream>

namespace CEGUI
{
	class SDLClipboardProvider : public NativeClipboardProvider
	{
	private:
		SDLClipboardProvider() {};
		char* text;
	public:
		//get the singleton class
		static SDLClipboardProvider* GetInstance()
		{
			static SDLClipboardProvider* inst = new SDLClipboardProvider();			//create static singleton
			return inst;		//return singleton
		}
		~SDLClipboardProvider() {};
		virtual void sendToClipboard(const String& mimeType, void* buffer, size_t size) ;
		virtual void retrieveFromClipboard(String& mimeType, void*& buffer, size_t& size) ;

		void Deallocate();
	};
}