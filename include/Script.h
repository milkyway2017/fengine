#pragma once
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <mutex>
#include <atomic>

#include "Exception.h"
#include "Functor.h"
#include "Command.h"
#include "Timer.h"
#include "Primitive.h"


namespace Fengine
{
	//forward decl
	class Map;

	/**
	* types of commands available in the script
	* @note all \@param in this enum are stored in the param_list of ScriptCommand as strings
	*/
	enum ScriptCommandType
	{
		/**
		 * task, takes over the player keyboard input control of the map
		 * @param param_list[0] "0" to release player control, "1" to take over
		 */
		TAKE_OVER_PLAYER_CONTROL,
		/**
		 * task, displays the speech of a character
		 * @param param_list[0] name or id of the character in map
		 * @param param_list[1] speech
		 */
		CHARACTER_SPEAK,
		/**
		 * wait for a key press event, script will continue once there is a KEY_PRESSED event in EventManager
		 */
		WAIT_FOR_KEY_PRESS,
		/**
		 * wait for some time, script will continue when time is up
		 * @param param_list[0] the amount of time to wait in milliseconds
		 */
		WAIT_FOR,
		/**
		 * clear any dialogue text on screen
		 */
		END_SPEAK,
		/**
		 * show hint text on screen
		 * @param param_list[0] the hint text to show
		 */
		SHOW_HINT_TEXT,
		/**
		 * delete a shown hint text on screen
		 * @param param_list[0] the hint text to delete, must be shown first
		 */
		DELETE_HINT_TEXT,
		/**
		 * walk an object to a location
		 * @param param_list[0] the name/id of the object that's walking
		 * @param param_list[1] the destination point (Point2F)
		 */
		WALK_OBJECT_TO,
		/*
		 * control an figure's transparency
		 * @param param_list[0] figure the name of the figure specified in Tiled
		 * @param param_list[1] the target amount of opaqueness
		 * @param param_list[2] the amount of time the fading takes
		 */
		FADE_FIGURE,
		/**
		 * zoom the camera from current scale to a new scale
		 * @param param_list[0] scale the new scale
		 * @param param_list[1] time the time taken to finish zooming
		 */
		ZOOM_CAMERA,
		/**
		 * translate the camera from current position to a new position
		 * @param param_list[0] position the new position
		 * @param param_list[1] time the time taken to finish translating
		 */
		TRANSLATE_CAMERA,
		SET_CAMERA_TO_PLAYER,
		//default
		COMMAND_UNDEFINED
	};

	enum ScriptCommandParamType
	{
		PARAMTYPE_STRING,
		PARAMTYPE_INT,
		PARAMTYPE_FLOAT,
		PARAMTYPE_BOOL,
		PARAMTYPE_POINTER,
		PARAMTYPE_RECT,
		PARAMTYPE_POINT
	};

	/**
	* represents a parameter in script commands
	*/
	struct ScriptCommandParam
	{
		/**
		 * useless default constructor
		 */
		 ScriptCommandParam():
		 	type(PARAMTYPE_STRING),
		 	string_("invalid param, created by default constructor")
		 {}

		/**
		* create a string command parameter
		*/
		ScriptCommandParam(const std::string & string);

		/**
		* create an integer command parameter
		*/
		ScriptCommandParam(int integer);

		/*
		* create a decimal command parameter
		*/
		ScriptCommandParam(float decimal);

		/**
		 * create a boolean command parameter
		 */
		ScriptCommandParam(bool boolean);

		/**
		 * create a rect command param
		 */
		ScriptCommandParam(const Rect4F & rect);

		/**
		 * create a point parameter
		 */
		ScriptCommandParam(const Point2F & point);

		/**
		 * create a pointer command parameter, this pointer can point to any entity in map 
		 */
		ScriptCommandParam(void* pointer);

		/**
		* casting operators, with these,
		* you can just treat a ScriptCommandParam as if it's type is any of the supported types
		*/

		/**
		* casting operator to string, will give string "invalid type cast" if the stored type is not string
		*/
		operator std::string()	const;

		/**
		* casting operator to integer, will give INT_MAX if the stored type is not int
		*/
		operator int()	const;

		/**
		* casting operator to float, will give FLT_MAX if the stored type is not float
		*/
		operator float()	const;

		/**
		* casting operator to bool
		*/
		operator bool()	const;

		/**
		 * casting operator to void*, will give nullptr if the stored type is not pointer
		 */
		operator void*() const;

		/**
		 * casting operator to Rect4F, will return default Rect if the stored type is not rect
		 */
		operator Rect4F() const;

		/**
		 * casting operator to Point2F, will return default Point if the stored type is not point
		 */
		operator Point2F() const;

		union
		{
			std::string string_;
			int integer_;
			float decimal_;
			bool boolean_;
			Rect4F rect_;
			void* pointer_;
		};

		~ScriptCommandParam(){}

		ScriptCommandParam(const ScriptCommandParam& other);

		ScriptCommandParam& operator =(const ScriptCommandParam& other);

		bool operator==(const ScriptCommandParam& other);

		/**
		 * given a string of parameters, this function attempts to parse the string
		 * @param input the string to parse, this string should not contain any leading/trailing space
		 * @param output the vector to receive the output of this function
		 * @param types list of resulting types
		 * @return error message, if successful, this string will be empty
		 */
		static std::string ParseParamFromText(std::string input, std::vector<ScriptCommandParam>* output, std::initializer_list<ScriptCommandParamType> types);
	private:
		ScriptCommandParamType type;
	};

	/**
	* represents a command in script
	*/
	struct ScriptCommand
	{
		ScriptCommandType type;		//!< indicates the type of this ScriptCommand
		bool is_task;				//!< true if union stores a task
		union
		{
			Functor task;									//!< functor that contains functions in section "script commands functions" in Map.cpp
			MapEventManagerEventType event_type;			//!< event type this ScriptCommand waits for
		};

		/**
		* create a ScriptCommand with a task
		* @param type the type of this script command
		* @param task the task, a functor that calls one of the map's script commands functions (e.g. void TakeOverPlayerControl(void* ))
		* @param params the parameter to pass
		*/
		ScriptCommand(ScriptCommandType type, const Functor& task, const std::vector<ScriptCommandParam> & params = {}) :
			type(type),
			task(task),
			is_task(true),
			param_list(params)
		{
		}

		/**
		* create a ScriptCommand with an event to wait for an event
		* @param type the type of this script command
		* @param event_ty the event type, details can be found at CommandDef.h
		*/
		ScriptCommand(ScriptCommandType type, MapEventManagerEventType event_ty) :
			type(type),
			is_task(false),
			event_type(event_ty)
		{
		}

		/**
		 * default constructor, results in useless command
		 */
		ScriptCommand():
			type(COMMAND_UNDEFINED),
			task(Functor())
		{
		}

		/**
		 * special constructor for WaitFor command
		 * @param time the time param (type: int)
		 */
		ScriptCommand(ScriptCommandParam time):
			type(WAIT_FOR)
		{
			param_list.push_back(time);
		}

		/**
		 * copy constructor
		 */
		ScriptCommand(const ScriptCommand& other):
			ScriptCommand()
		{
			*this = other;
		}

		ScriptCommand& operator=(const ScriptCommand& other)
		{
			this->type = other.type;
			this->is_task = other.is_task;
			this->param_list = other.param_list;
			if(other.is_task)
			{
				this->task = other.task;
			}
			else
			{
				this->event_type = other.event_type;
			}

			return *this;
		}

		~ScriptCommand() {}

		std::vector<ScriptCommandParam> param_list;				//!< list of params
	};

	/**
	* class responsible for running a script
	*	-logic for running a script:
	*	1. Script::Run is called
	*	2. if everything is ok, Script::RunNext is called
	*	3. if the current step is a task, it calls the functor in the step
	*	3. if the current step is to wait for event, it subscribes to that event at EventManager
	*	4. goes to step 2 unless it is the end of the script
	*/
	class Script
	{
	public:
		/**
		* creates a script from script file
		* @param map the pointer to map
		* @param script_path the path to script file
		* @param should_run_script a function that Script::ShouldRunScript will call, this function should always return immediately
		*/
		Script(Map* map, const std::string & script_path, const std::function<bool(void)> & should_run_script);

		/**
		* default constructor, does nothing
		*/
		Script() {}

		~Script() {}

		/**
		 * copy constructor
		 */
		Script(const Script& other);

		/**
		 * assignment operator
		 */
		//Script& operator=(const Script& other);

		/**
		* @return true if the script is running
		*/
		bool IsRunning()	const
		{
			return is_running_;
		}
		/**
		* @return the current step the script is on
		*/
		int GetCurrentStep()	const
		{
			return current_step_;
		}

		/**
		* run the script
		*/
		void Run();

		/**
		* @return true if the script should be run
		*/
		bool ShouldRunScript();

		/**
		* pause the execution of the script
		*/
		void Pause();

		bool is_bad_ = false;			//!< true if the script has errors. In the future, details will be logged
	private:
		Map * map_;
		int current_step_ = -1;
		bool is_running_ = false;						//true if even when paused
		bool is_paused_ = false;						//only true when Pause() is called
		int run_for_;									//number of times the script can run, decreases every time the end of script is reached
		std::mutex run_for_mutex_;
		std::atomic_int run_for_a_;
		std::vector<ScriptCommand> steps_;				//stores all commands in script
		std::function<bool(void)> should_run_script_;	//function that checks if this script should be run

		Timer* current_timer_;							//if the current task is a task
														//this points to the timer used, used when pausing
																 
		void RunNext();									//run the next command, called by script only
		Functor call_run_next_;							//functor of RunNext, this is usually passed to EventManager or Timer


		/*************************script private help functions********************/
		/**
		 * calls Map::ShowHintText or DeleteHintText
		 * @param on_off
		 */
		void ShowPressAnyKeyToContinue(bool on_off);

		/**
		 * helper parsing function
		 * @return true for success
		 */
		bool ParseCommand(ScriptCommand& command, const Functor & f, const std::string & line, ScriptCommandType type,
			std::initializer_list<ScriptCommandParamType> target_types, unsigned int line_count /*used for error message*/);

		/**
		 * initializes call_run_next_
		 */
		void InitFunctors();
		/**************************************************************************/
	};
}