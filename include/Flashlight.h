#pragma once
#include "GameItem.h"

namespace Fengine
{
	class Map;
	class Object;

	/**
	 * a data structure that holds info loaded from a flashlight ObjDef
	 */
	struct FlashlightDef
	{
		float def_Range;		//!< the radius of the light (how far the light travels)
		GLColor def_Color;		//the color of the light
		Angle def_Angle;			//how wide the light travels, stored in Radians
	};

	/**
	 * a class that represents a Flashlight, inherits from GameItem
	 */
	class Flashlight :
		public GameItem
	{
	private:
		/**
		 * @todo make Flashlight::light_color_ and Flashlight::casting_angle_ work,
		 * get rid of Flashlight::map_points_, at least make it static first
		 * @references <a href="https://www.geeksforgeeks.org/program-for-point-of-intersection-of-two-lines/" /> <a href="https://www.redblobgames.com/articles/visibility/" />
		 */
		GLColor light_color_;					//!< the color of the light
		float casting_angle_;					//!< how wide the light travels, stored in Radians
		float cast_side_len_;					//!< the casting range of this Flashlight
		Rect4F cast_square_;					//!< square boundary of light
		Sprite light_sprite_;					//!< the texture for 2D light effect
		GLShape shape_;							//!< geometry for when the flashlight is on

		static std::map<std::string, const FlashlightDef> def_map_;			//!< holds all ObjDef info of Flashlight

		std::vector<Point2F>* map_points_;		//!< stores all vertices and line intersections in the map
												//!< @todo udpate map_points_ (bug fix)
		std::vector<Line>* map_lines_;					//!< pointer to map

		std::vector<Point2F> near_by_direct_points; 	//!< stores all points that are inside cast_square
		std::vector<Line> line_in_range_;					//!< stores all lines that affect lighting


		std::vector<Line> lines_;						//!< stores all the rays used by shape

		/**
		 * @return the global Flashlight shader
		 */
		static GLuint GetFlashlightShader()
		{
			static GLuint FlashLightShader;
			if (!FlashLightShader)
			{
				Graphics::GetInstance()->GenShaderProgram("Resources/Shaders/Map Rendering/flashlight.vert", "Resources/Shaders/Map Rendering/flashlight.frag", FlashLightShader, "Flashlight Rendering");
			}
			GLErrorVal();
			return FlashLightShader;
		}

		/**
		 * @return the FlashLightTopLeft uniform location in shader
		 */
		static GLuint TopLeftUniLoc()
		{
			static GLuint loc;
			if (!loc)
			{
				loc = glGetUniformLocation(GetFlashlightShader(), "FlashLightTopLeft");
			}
			return loc;
		}

		/**
		 * @return the CameraMateix uniform location in shader
		 */
		static GLuint CameraMatrixLoc()
		{
			static GLuint loc;
			if (!loc)
			{
				loc = glGetUniformLocation(GetFlashlightShader(), "CameraMatrix");
			}
			return loc;
		}

		/**
		 * @return the FlashlightRange uniform location in shader
		 */
		static GLuint RangeLoc()
		{
			static GLuint loc;
			if (!loc)
			{
				loc = glGetUniformLocation(GetFlashlightShader(), "FlashlightRange");
			}
			return loc;
		}

		/**
		* create a flashlight
		* @warning this is deprecated & should not be used other than by Flashlight class itself
		* @param map the map that owns this flashlight
		* @param shader_program the map's shader program
		* @param owner the owner of this object, usually a player
		* @param cast_range the range of the flashlight
		* @param cast_angle the angle of the flashlight in Radians
		* @param dropped_image_path the image path of the dropped flashlight
		* @param light_image_path the image path of the turned on flashlight
		* @param location the location of the flashlight in the map
		*/
		Flashlight(Map*map, GLuint shader_program, int cast_range, float cast_angle, const std::string & dropped_image_path, const std::string & light_image_path, Point2F location, int id = 0, Object* owner = nullptr);

	public:

		/**
		 * the best way to create a flashglight
		 * @param type the name of the ObjDef used to create this Flashlight
		 * @param location the location of this flashlight
		 * @param shader_program the shader program used by the map
		 * @param map the map
 		 * @param id unique object id issued by Tiled Map Editor
		 * @param owner the owner of this item, can be nullptr
		 */
		Flashlight(const std::string & type, Point2F location, GLuint shader_program, Map* map, int id = 0, Object* owner = nullptr);
		~Flashlight() final;

		void Update(Uint32 time_passed, Map* map = nullptr) final;

		//packs all the points in Lines into Shape
		void PackShape();
		void GenPointsFromSys();
		//generate lines from points<>
		void GenLinesFromPoints();

		/**
		 * not intended to be used outside the class, this is strictly for internal use
		 * inits the flashlight with necessary info
		 * @param info
		 */
		void Init(Map* info);

		/**
		 * loads a flashlight object def
		 * @param def_path the path of the ObjDef
		 */
		static void LoadDef(const std::string & def_path);

		//find and pushback 2 points each +/- 0.0001 radians into ExtendedPoints
		void FindNeighboringLines();

		virtual void Draw();
		virtual void Use(void* info) {}
		virtual void Move(Point2F delta_xy);
		virtual void MoveTo(Point2F destination);
		virtual void Activate(bool on_off);

		virtual void ConfigureZOrder();

		//copy constructor
		Flashlight(const Flashlight & other);

		virtual Flashlight* Clone();
	};
}
