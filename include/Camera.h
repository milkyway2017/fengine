#pragma once
#include "Graphics.h"
#include "Flashlight.h"
#include "Functor.h"
#include <glm/glm.hpp>

namespace Fengine
{
	/**
	 * a function that sets the position of input x and y, and returns nothing
	 */
	typedef std::function<void (float* x, float* y)> PositionSetter;

	/**
	 * A Camera corresponds to a shader program, usually the map owns its own camera(Map::camera_) and shader program(Map::shader_program_).
	 */
	class Camera
	{
	public:

		Camera();
		
		/**
		 * initialize the camera, store the program that the camera is used in, this needs to be called before the camera is used for the first time
		 * @param program_id the id of the shader program this camera is used in
		 * @param name_in_program the name in the shader that stores the camera matrix, this is usually located in vertex shader
		 */
		void Init(GLuint program_id, std::string name_in_program);

		~Camera();

		/**
		 * gets the position of the camera
		 * @return the glm::vec2 of position
		 */
		glm::vec2 GetPostion();

		/**
		 * set the position of the camera
		 * @param position position
		 */
		void SetPosition(glm::vec2 position);

		/**
		 * get the scale of the camera
		 * @return the scale of the camera: 1.f for the original size
		 */
		float GetScale()	const;

		/**
		 * set the position of the camera
		 * @param get_position_function a function that can be called to get an updated position, can ba a lambda function
		 */
		void SetPositionCallback(PositionSetter get_position_function);

		/**
		 * Get the field of view
		 */
		Rect4F GetFieldOfView();

		/**
		 * informs the camera about window resizing
		 */
		void NewRes();

		/**
		 * move the Camera
		 * @param delta how much to move by
		 */
		void Move(glm::vec2 delta)
		{
			this->camera_position_ -= delta;
		}

		/**
		 * set the scale of camera
		 * @param scale 1.f for original, increase to zoom in, decrease to zoom out
		 */
		void SetScale(float scale)
		{
			this->camera_scale_ = scale;
		}

		/**
		 * change the scale of camera in a period of time, uses SineInterpolation
		 * @param to_scale the target scale
		 * @param from_scale the scale to start with, default is current scale
		 * @param time the amount of time to change, default is one second
		 * @param callback callback to call after the zoom is complete
		 */
		void ZoomToScale(float to_scale, float from_scale = -1.f, Uint32 time = 1000, const Functor & callback = Functor());

		void MoveTo(const Point2F & position, Uint32 time = 1000, const Functor & callback = Functor());

		/**
		 * apply the camera by making OpenGl calls
		 */
		void Apply();

		/**
		 * update function, basically informs OpenGL about the state of this camera, this must be called only if SetPositionCallback is called
		 */
		void Update();

		/**
		 * friend function because when flashlight draws, it needs to access private member Camera::camera_matrix_
		 */
		friend void Flashlight::Draw();
	private:

		//Camera internal stuff...
		
		glm::mat4 camera_matrix_;
		glm::vec2 camera_position_;				//!< the position of the center of camera in pixel coordinate
		float camera_scale_;					//!< the scale of camera

		glm::mat4 camera_ortho_matrix_;

		GLuint shader_program_;			//!< the shader program this camera applies to
		GLint camera_location_;					//!< camera matrix location in the shader program
		Point2F screen_wh_;				//!< width and height of screen

		PositionSetter get_position_;		//!< function the camera uses to set the position, set by SetPositionCallback
	};
}
