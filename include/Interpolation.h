//base class of all interpolations
#pragma once
#include <vector>
#include <SDL_hints.h>
#include "Functor.h"

namespace Fengine {

	/**
	 * The Interpolation class solves the problem of interpolating a value between 2 values: an initial and a final.
	 * This class saves the trouble of having to declare tons of states to track information and update it every frame;
	 * it does all that for you and it also allows callback when the interpolation is done. 
	 * All you need to do is to pick an interpolation pattern and...:
	 * @code 
	 * 
	 * float value;							//stores interpolation
	 * float initial = 5.f, final = 10.f;	//values to interpolate between
	 * int duration = 5000;					//5 seconds
	 * Functor callback;
	 * //callback = ...(you decide)
	 * Interpolation::AddInterpolationToPool(new LinearInterpolation(&value, initial, final, duration, callback));
	 * //done!
	 */
	class Interpolation
	{
	private:
		static std::vector<Interpolation*> interpolation_pool;

	protected:
		Uint32 time_began_;			//stores the time this interplation started
		Uint32 total_time_passed_;	
		float initial_value_, final_value_;
		float* value_;			//value to interpolate
		Functor callback_;
		Uint32 duration_;


		/**
		 * create a new interpolation
		 */
		Interpolation(float * value, const float & initial_value, const float & final_value, const Uint32 & duration, const Functor & callback = Functor());
	public:

		virtual ~Interpolation() = default;

		/**
		 * add a new interpolation to the pool;
		 * the pool will automatically manage all interpolations added.
		 * @param interpolation a new interpolation 
		 * @note any interpolation added to the pool must be allocated in the heap (using 'new')
		 */
		static void AddInterpolationToPool(Interpolation * interpolation);

		/**
		 * updates all interpolations in the pool
		 * @param time_passed the amount of time passed since last update
		 */
		static void UpdatePool(Uint32 time_passed);

		/**
		 * update the value
		 * @return true if the interpolation is complete
		 */
		virtual bool Update(Uint32 time_passed);
	};

	class LinearInterpolation: public Interpolation
	{
	public:
		LinearInterpolation(float * value, const float & initial_value, const float & final_value, const Uint32 & duration, const Functor & callback = Functor());

		bool Update(Uint32 time_passed) override;

		~LinearInterpolation() override = default;
	};

	class SineInterpolation : public Interpolation
	{
	public:
		SineInterpolation(float * value, const float & initial_value, const float & final_value, const Uint32 & duration, const Functor & callback = Functor());

		bool Update(Uint32 time_passed) override;

		~SineInterpolation() override = default;
	};
}
