#pragma once
#include "Bot.h"

namespace Fengine
{
	/**
	 * internal enum used by Lycan to store animation
	 */
    enum class LycanAnimations
    {
        ATTACK_ANIMATION
    };

    /**
     * A kind of Bot-Lycan, the most common type of monster
     * 	- High speed
     * 	- Med-Low health
     * 	- High stamina
     * 	- medium damage
     * 	reference: https://mm.tt/998060193?t=NDvPGBBKxX
     */
	class Lycan : public Bot
	{
	private:
		Point2F reference_point_;					//!< the point Lycan patrols around

		/**
		 * call back for actions in lycan_actions_
		 * @param e a pointer to an action code, for lycan, it can be any of the LycanAnimations
		 * @warning the function is not responsible for <e>'s garbage collection, so please do not use "new" operator when calling this!
		 */
		void HandleAnimationComplete(void * e);
	protected:
		void InitAnimation(int TimeToUpdate);
		void Patrol() final;
		void Follow() final;
		void Attack() final;
		void Freeze() final;
		void SetNPCStatus(NPCStatus s) final;
		void HandleSpecialEvent(Event e) final;
        bool CanMakeDamage(Object* other) final;
        void ReceiveAttack(int Damage) final;
		void Die() final;
		bool IsPlayerInVision() final;
		Animation lycan_actions_;

		/******************************static ObjDef defaults*******************************/
		/**
		* these are the default (initial) stats for a new Lycan, loaded from the LycanDef.json
		*/

		static Rect4F def_SpriteToBody;				//a rect that can yield the Body rect when added to def_SpriteRect
		static Rect4F def_SpriteRect;				//stores the default SpriteRect with top-left located at (0,0)
		static bool def_Loaded;						//if LycanDef has been loaded
		static float def_Damage;						//damage of 1 normal attack 
		static float def_Detection;						//detection range for Lycan
		static float def_Speed;							//initial speed of Lycan in pixel/ms
		static float def_Health;						//initial health of Lycan
		static float def_Stamina;						//initial stamina of Lycan
		static float def_HealthRegen;				//initial health regen/ms
		static Angle def_DetectionAngle;			//initial detection angle
		static float def_DetectionRadius;			//initial detection radius

		/************************************************************************************/
	public:
		/**
		 *  initialize the Lycan
		 * @param map the pointer to the map object
		 * @param position the position of the Lycan
		 * @param shader_program map's shader program
		 * @param id unique object id issued by Tiled Map Editor
		 */
		void Init(Map * map, Point2F position, GLuint shader_program, int id);
		/**
		* loads the ObjDef for Lycan
		*@param DefPath the path to LycanDef.json
		*/
		static void LoadDef(const std::string & DefPath);
		Obj_ptr Clone(Point2F Location) final;
		Lycan();
		~Lycan();
	};

}