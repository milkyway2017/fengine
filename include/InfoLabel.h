#pragma once
#include <SDL_ttf.h>
#include <string>
#include "Sprite.h"
#include <vector>
#include <tuple>

namespace Fengine
{
	class Sprite;

	/**
	 * a singleton class that can display plain text on top left corner of game window
	 * this can be used for debugging purpose.
	 * @note to display a string, all you need to do is to call InfoLabel::DisplayString
	 * @code
	 * InfoLabel::GetInstance()->DisplayString("random name", "random string", Graphics::GetInstance()->GetFont("Consola"));
	 * //displays:
	 * //random string
	 * @endcode
	 */
	class InfoLabel
	{
	private:
		InfoLabel();

		TTF_Font* default_font_;
		
		std::vector<std::tuple<std::string/*string name*/, std::string/*string*/, TTF_Font*/*font*/, Sprite/*texture*/,bool/*is being used current frame*/,std::string/*last drawn string*/>> StringTable;		//stores all the strings that need to be rendered

		void Update();				//updates the texture and deletes unused strings
	public:

		static InfoLabel* GetInstance()
		{
			static InfoLabel* inst = new InfoLabel();
			return inst;
		}
		
		/**
		 * display string at top left corner of window
		 * @param name of the string
		 * @param text the content of the strong
		 * @param font the font to render the text in, fonts are available in Graphics::LoadFont or Graphics::GetFont, the default font is consola(size 30)
		 */
		void DisplayString(const std::string& name,const std::string & text, TTF_Font* font = nullptr);

		/**
		 * do not call this!
		 */
		void Draw();
		~InfoLabel();
		 
	};

}