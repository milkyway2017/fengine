#pragma once
#include <SDL.h>
#include "Graphics.h"


namespace Fengine
{
	/**
	 * A sprite is an image.
	 * To load an image: Sprite::Init
	 * To draw an image after loading: Sprite::Draw
	 * 
	 * @note A sprite in Fengine is available in two modes: reference mode and non-reference mode\n
	 * the two modes result in different Sprite::Draw's 3rd parameter\n
	 *		-reference mode: no destination rect(location on screen) is required to be passed in, the location is retrieved from Sprite::ref_dest_rect_\n
	 *		-non-ref. mode: destination rect must be updated every update, otherwise the sprite will draw the sprite at the last updated position\n
	 * @todo write sample code for using Sprite class, show how to use both reference and non-reference mode
	 */
	class Sprite
	{
	private:
		GLColor highlight_color_;			//!< RGBA color used when the sprite is highlighted
		bool is_drawable_;					//!< only true after Init (except for the Init that takes in no parameter)
	public:
		Sprite();
		~Sprite();

		/**
		 * set the highlight color used by the sprite
		 * @param color the color, can be obtained by Graphics::GetColor()
		 */
		void SetHighlightColor_Sprite(const GLColor &color);

		/**
		 * init function, inits with an image file path and dest_rect passed in as a value
		 * @param path path of the image
		 * @param dest_rect the rect on screen where the texture is displayed
		 */
		void Init(const std::string & path, Rect4F dest_rect);

		/**
		 * init with an SDL_Surface* (Init() is not in charge of the destruction of the surface)
		 * @param surface the surface containing the texture used by this sprite
		 * @param dest_rect the rect on screen where the texture is displayed
		 * @param gen_VAO whether to generate a new VAO for this initialization,
		 * true if this is the first Init call of this Sprite,
		 * false if this is not
		 */
		void Init(SDL_Surface* surface, Rect4F dest_rect, bool gen_VAO);

		/**
		* preferred init function, inits with an image file path and dest_rect pointer
		* @param path path of the image
		* @param dest_rect the pointer to a rect on screen where the texture is displayed
		*/
		void Init(const std::string & path, Rect4F * dest_rect);

		/**
		 * initializes the spirte with a new shape with VAO, VBO and 6 empty indices
		 */
		void Init();

		/**
		 * check if point is inside the sprite's rect
		 * @param p the point to check
		 * @return true if the point is inside
		 */
		bool IsPointInShape(const Point2F & p) const;

		/**
		 * get the destination rect used for the rendering of the sprite
		 * @return the pointer to the destination rect 
		 */
		Rect4F* GetReferenceDestinationRect()	const
		{
			return ref_dest_rect_;
		}

		/**
		 * set a Rect4F reference to use to render
		 * @param ref_dest_rect the pointer to the Rect4F reference
		 * @note passing nullptr will turn off reference mode, to turn on reference mode, simply pass in a pointer to a Rect4F
		 */
		void SetReferenceDestinationRect(Rect4F * ref_dest_rect)
		{
			ref_dest_rect_ = ref_dest_rect;
		}

		/**
		 * draw the sprite
		 * @param shader_program the id of the shader program to draw this sprite in
		 * @param src_rect a rectangle in the image that specify the area of the image to draw, when not specified, the last used source rect will be drawn
		 * @param dest_rect the place to draw this sprite on screen, this is not required for Sprite in reference mode, \n
		 * if the sprite is in non-reference mode and dest_rect is not provided, the sprite will be drawn at the last updated position
		 * @note to switch between reference mode and non-reference mode, use SetReferenceDestinationRect()
		 */
		void Draw(GLuint shader_program, const SDL_Rect & src_rect = {}, const Rect4F & dest_rect = Rect4F());

		/**
		 * draw the sprite with a GLShape ready, VERY RARELY USED
		 * @param shader_program the id of the shader program to draw this sprite in
		 * @param shape the GLShape generated to draw
		 * @note only Flashlight::Draw use this funtion
		 */
		void Draw(GLuint shader_program, const GLShape & shape);

		/**
		 * clean up the VAO and VBO (vertex data) used to render the sprite
		 */
		void CleanUp();

		/**
		 * move by a distance
		 * @param delta the distance to move
		 */
		void MoveSprite(Point2F delta);

		/**
		 * move the top-left corner of the sprite to
		 * @param destination the new location
		 */
		void MoveSpriteTo(Point2F destination);

		/**
		 * move the center of the sprite to
		 * @param destination the new location
		 */
		void MoveSpriteCenterTo(Point2F destination);

		/**
		 * set the transparency of the sprite
		 * @param transparency 0.f for transparent, 1.f for opaque
		 */
		void SetTransparency(float transparency);

		/**
		 * get the pointer to the transparency variable
		 * @note used for interpolation purposes, see Interpolation class
		 */
		float* GetTransparencyPtr();

		/**
		 * set which part of the image is rendered
		 * @param rect the area to render, default sets to full image
		 */
		void SetSourceRect(const SDL_Rect & rect = {});

		/**
		 * set which part of the screen the sprite is rendered
		 * @param rect the area to render
		 */
		void SetDestRect(const Rect4F & rect);

		/**
		 * get width on screen
		 * @return width
		 * @note To change this, call Sprite::SetDestRect
		 */
		float GetWidth() const;

		/**
		 * get height on screen
		 * @return height
		 * @note To change this, call Sprite::SetDestRect
		 */
		float GetHeight() const;

		/**
		 * check if this sprite is ready to draw
		 * @return true if the sprite is ready
		 */
		bool IsDrawable()	const;

		GLuint sprite_sheet_;			//!< the id that holds the texture in OpenGL
		GLShape shape_;					//!< Shape used for drawing, should not be modified outside of sprite class
		bool highlighted_;			//!< used for debugging, reset to false after every time drawn with highlight
	protected:
		float sprite_width_, sprite_height_;	//<! the width and height on screen
		SDL_Point wh_;						//!< width and height of this texture
		Rect4F* ref_dest_rect_;				//!< points to a destination rectangle, set to nullptr if the sprite does not have one
											//!< @note can be set using Sprite::SetReferenceDestinationRect
		float transparency_ = 1.0f;			//transparency of the sprite
	};
}