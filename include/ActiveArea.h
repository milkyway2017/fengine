#pragma once
#include "Primitive.h"
#include <string>
#include <vector>
#include <memory>

namespace Fengine
{
	//forward declaration
	class Map;
	class EventManager;

	/**
	 * a teleportation can be activated when a player / object enters an ActiveArea
	 * @note right now this only supports player teleporting to a new map
	 */
	struct TeleportInfo/*CastInfo/Spell*/
	{
		//TeleportType/CastType/SpellType type;
		//enum{
		std::string next_map;			//!< name of the map to enter
		//}
	};

	/**
	 * ActiveArea is a class that can monitor a region in Map.
	 * 		It notifies the map when an object entered it's assigned area by:
	 * 		- firing an MapEventManagerEventType::ACTIVEAREA_FILLED event when an object enters empty ActiveArea region, and
	 * 		- firing an MapEventManagerEventType::ACTIVEAREA_EMPTIED event when the last object in the region exits
	 *
	 * ActiveArea can be added to a map using the map's Map::AddActiveArea
	 *
	 * ActiveArea can be retrieved from the map using ActiveArea::ID
	 */
	class ActiveArea
	{
	private:
		/**
		 * the region in Map this ActiveArea monitors
		 * @todo support more shapes, such as polygon
		 */
		Rect4F area_;

		/**
		 * number of objects in this area, updated in Update function
		 */
		int object_count_;
		/**
		 * pointer to the map's event manager
		 */
		EventManager* event_manager_;
		/**
		 * the pointer to the map this area belongs to
		 */
		Map* map_;

		std::vector<TeleportInfo> enter_spells_;	//!< activated when desired type of object enter

		bool no_trig_;								//!< if this is true, no event will be fired when an object enters / exits
													//!< reset after once

		/**
		 * helper function, fires an event and trigger spells
		 * @param filled true if the ActiveArea is just filled, false otherwise
		 */
		void FireEventAndTrigger(bool filled);
	public:

		/**
		 * create an ActiveArea
		 * @param area the area to monitor
		 * @param map the map this area is in
		 * @param id the unique object id, issued by Tiled Map Editor, anonymous ActiveArea would not have one
		 * (such as the one that comes with sliding door)
		 * @param name the name assigned in Tiled, will be empty for anonymous ActiveArea
		 */
		ActiveArea(Rect4F area,  Map* map, int id = 0, const std::string & name = "");
		~ActiveArea();

		/**
		 * unique id of this ActiveArea, can be used to retrieve the ActiveArea using Map::GetActiveAreaByID
		 */
		const int id_;

		/**
		 * named assigned in Tiled
		 */
		const std::string name_;

		/**
		 *  update function, called by the map every Map::Update(), this will check if any new Object entered the region
		 */
		void Update();

		/**
		 * check if the ActiveArea is empty
		 * @note this function is not recommended, as the best way is to subscribe to the events using EventManager::SubscribeEvent
		 * @return true if empty, false if not
		 */
		bool IsEmpty();

		/**
		 * get the area monitored
		 * @return the rectangular region this ActiveArea monitors
		 */
		const Rect4F &GetArea() const;

		/**
		 * set a new area to monitor
		 * @param area the new area this ActiveArea need to monitor
		 */
		void SetArea(const Rect4F &area);

		/**
		 * add spell that activates when the player enters
		 * @param spell the spell to cast
		 */
		void AddSpell(TeleportInfo spell);

		/**
		 * teleport an entry, do not trigger anything
		 */
		void NoTrig();
	};

	/**
	 * an alias for an active area pointer wrapped in a shared_ptr
	 * @todo replace this with std::unique_ptr, the map will take ownership and other objects can only have the plain pointer to ActiveArea
	 */
	typedef std::shared_ptr<ActiveArea> Area_ptr;
}