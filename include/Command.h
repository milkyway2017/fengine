#pragma once
#include <string>
#include <vector>

#include "CommandDumpLog.h"
#include "CommandDef.h"


namespace Fengine
{

	/**
	 * a Command structure holds a command (obviously...), the advantage of Command is it gives you flexibility and does not result in bad design, for example:
	 * 		- let's say you changed resolution using UI
	 * 		- UI needs to inform the rest of the system
	 * 		- it doesn't have access to other class's, say, NewResolution() function, because the function is private OR the it does not hold a reference to other classes
	 * 		- with Command system, UI class can:
	 * 			-# construct a Command, with the command level set to PROGRAM
	 * 			-# add the command to the system using AddCommand() //this is a function every level has, from the top most PROGRAM to MAP (check CommandLevel for levels available)
	 * 			-# the command is passed to the PROGRAM level, and the PROGRAM level's CommandProc() will process the command
	 * 		- this is great because:
	 * 			-# the receiver of the command always gets to decide what to do with it, no function calls needed on the sender's side except for AddCommand()
	 * 			-# member function does not need to be public
	 *
	 * 		-another use of Command is known as Event, which is just an alias of Command, extensive use of Event can be found in EventManager, where the map's object can fire/subscribe/unsubscribe
	 * 		to a type of event. The main difference between an Event and Command (usage-wise, they are absolutely the same in memory) is that Event is used to INFORM others about something rather
	 * 		than to COMMAND. For example, an Object in Map can fire an MapEventManagerEventType::ITEM_PICKEDUP event. This is done by calling the map's eventmanager's EventManager::FireEvent. Now,
	 * 		a monster may be informed about this if it subscribed to this event type. What does the monster do about this? It's up to the monster itself.
	 *
	 * 	TLDR: a Command is LIKE a function, except as the sender of the Command you do not know what happens in the execution of the Command.
	 *
	 *  @todo sample code to show how to construct and use Command
	 *  @todo sample code to show how to use Event (link to EventManager's documentation)
	 */
	struct Command
	{
		/**
		 * constructor of Command, usually used when creating a Command with user input command in the following format "[commandLevel]<commandType>(paramString[0])(paramtString[1])".
		 * For the string representation of every command, please refer to CommandTypeMap enums in CommandDef.h
		 * @warning this constructor may throw a std::exception, if the commandLevel or commandType is not known to the constructor.
		 */
		Command(const std::string & command_string);

		/**
		 * default constructor
		 */
		Command();

		/**
		 * constructor of Command, used when creating a Command with paramString
		 * @param command_level the level this command should be processed in (check CommandLevel enum CommandDef.h)
		 * @param command_type the type of the command (check CommandType enums in CommandDef.h)
		 * @param commander the commander class (e.g. Object, Figure, Level, Map, &...)
		 * @param param_string list of parameters enclosed in '(' & ')', for example, to pass in '2' and 'user_id', the corresponding param_string will be "(2)(user_id)"
		 */
		Command(CommandLevel command_level, int command_type, const std::string & commander, const char* param_string = "");

		/**
		 * constructor of Command, used when creating a Command with target
		 * @param command_level the level this command should be processed in (check CommandLevel enum CommandDef.h)
		 * @param command_type the type of the command (check CommandType enums in CommandDef.h)
		 * @param commander the commander class (e.g. Object, Figure, Level, Map, &...)
		 * @param target the target of the command
		 */
		Command(CommandLevel command_level, int command_type, const std::string & commander, void* target);

		/**
		 * constructor of Command, used when creating a Command with paramList
		 * @param command_level the level this command should be processed in (check CommandLevel enum CommandDef.h)
		 * @param command_type the type of the command (check CommandType enums in CommandDef.h)
		 * @param command the commander class (e.g. Object, Figure, Level, Map, &...)
		 * @param param_string list of parameters enclosed in '(' & ')', for example, to pass in '2' and 'user_id', the corresponding param_string will be "(2)(user_id)"
		 */
		Command(CommandLevel command_level, int command_type, const std::string & command, std::vector<void*> param_list, std::string param_string = "");

		/**
		 * informs the command that it's resolved,
		 * delete self if is in heap
		 */
		void Resolve();

		~Command();

		/**
		 * informs the command that it's irresolvable,
		 * @note this will delete this command if it is in heap
		 */
		void CannotResolve();

		/**
		 * log the invalid command into a logger (with CEGUI formatting tags)
		 * @note this will delete this command if it is in heap
		 */
		void CannotResolve(Logger* log);

		/**
		 * custom new operator, this will mark the created Command's Command::in_heap_ as true.
		 * @note no heap allocated Command can be copied using '=', Command::Clone must be used
		 */
		void* operator new(size_t size)
		{
			Command* t = (Command*)(std::malloc(size));

			t->in_heap_ = true;

			return (void*)t;
		}

		void operator delete(void* p)
		{
			std::free(p);
		}

		/**
		 * check if the Command is allocated in heap
		 * @return true if is in heap
		 */
		inline bool IsInHeap() const
		{
			return in_heap_;
		}

		/**
		 * copy constructor and operator
		 */
		Command(const Command & other);
		Command & operator=(const Command& other);

		/**
		 * call Clone() to copy a heap allocated command
		 */
		Command Clone();

		/**
		 * convert a Command to string
		 * @param with_commander true if you want Command::commander_ in the returned string
		 * @return the string representation of the Command
		 * @note Command::target_ and Command::param_list_ are not converted as they are pointers to objects
		 */
		std::string ToString(bool with_commander)	const;

		void* target_;								//!< the target of the command, may be nullptr

		CommandLevel command_level_;				//!< holds which level the command should be executed at
		int command_type_;							//!< holds the type of the command
		std::string commander_;						//!< the object that calls this command

		std::vector<std::string> param_string_list_;		//!< param_string split into strings
		std::vector<void*> param_list_;						//!< holds a list of null pointers, may be in either stack or heap, @note the Command is not responsible for any garbage collection!
	private:
		bool resolved_;
		bool irresolvable_;
		bool in_heap_;
	};
	typedef Command Event;				//for Map use	

}
