#pragma once
#include "Sprite.h"
#include "Label.h"
#include <iomanip>
#include <sstream>

namespace Fengine
{
    /**
     * displays stats with a ratio
     */
    class StatusBar
    {
    public:
        /**
         * default constructor, does nothing
         */
        StatusBar();

        /**
         * StatusBar constructor initialize the bars
         * @param filler_path a path to the filler
         * @param frame_path a path to the frame of the bar
         * @param location the coordinates for the top left corner of the bar
         */
        StatusBar(const std::string & filler_path, const std::string & frame_path, Point2F location, LabelStyle label_style);

        /**
         * scale the status bar
         * @param scale the scale
         */
        void SetScale(float scale);

        /**
         * Draws the status bar
         * @param shader_program a shader program
         */
        void Draw(GLuint shader_program);

        /**
         * set the amount this bar is filled up graphically
         * @param value amount filled up, 0-1, 0 for empty
         */
        void SetValue(float value);

        /**
         * Update the text displayed
         * @param text text to display on bar
         */
        void SetText(const std::string &text);
    private:
        Sprite bar_;			//!< sprite of bar
        Sprite bar_frame_;		//!< sprite of bar frame
		SDL_Point bar_frame_wh_;	//!< width and height of bar frame texture
		SDL_Point bar_wh_;			//!< width and height of bar texture
        Label label_;
        LabelStyle label_style_;	//!< style of label_
        Rect4F bar_src_rect_;		//!< source rect for bar
        Rect4F bar_dest_rect_;		//!< dest rect for bar
        Rect4F bar_frame_src_rect_;	//!< source rect for bar frame
        Rect4F bar_frame_dest_rect_;//!< dest rect for bar frame
        float scale_;				//!< ratio of dest : source, e.g. a scale of 2 means the bar is twice as large on screen
        float value_;				//!< 0-1, the amount filled of this bar
    };
}