#pragma once
#include "Sprite.h"
#include "Primitive.h"
#include "Functor.h"

namespace Fengine
{
	/**
	 * contains information for animation callback, passed in when using Animation::PlayAnimation\n
	 * 0th element: frame_of_callback, this specifies at the beginning of which frame the callback will be called.\nSet this to -1 if the callback is to be called after the action is played once. Set this to -2 if the callback is to be called at the end of this PlayAction (after playing the action times_to_play of times)\n
	 * 1st element: functor, when the frame_of_callback is reached, functor will be called with a AnimationCallbackFunctorParam passed in as void*\n
	 * @note its first element is an int that specifies the frame at which the second element(a functor as a callback) will be called
	 */
	typedef std::tuple<int, Functor> AnimationCallback;

	/**
	 * provides information for callback functions, this is always passed into the callback functor as a void*
	 */
	struct AnimationCallbackFunctorParam
	{
		int action_code_;				//!< the action code of this animation
		int frame_of_callback_;			//!< the frame in the action that triggers this functor callback
	};

	/**
	 * a class that can render continuous animation from sprite sheets
	 */
	class Animation : public Sprite
	{
	public:
		/**
		 * default constructor, creates an empty animation, Animation::Init must be called to initialize the created animation
		 */
		Animation();
		~Animation();
		/**
		 * Initialize an animation with local dest rect, use this Init when creating Animation for object that does not move
		 * @param path the path to the sprite sheet file
		 * @param refresh_time the rate at which the animation refreshes
		 * @param dest_rect the rectangle at which this animation will be displayed
		 */
		void Init(const std::string & path, int refresh_time, Rect4F dest_rect);

		/**
		 * Initialize an animation with a pointer to desk rect, use this Init when creating Animation for movable object
		 * @param path the path to the sprite sheet file
		 * @param refresh_time the rate at which the animation refreshes
		 * @param dest_rect the THE POINTER TO A rectangle at which this animation will be displayed
		 */
		void Init(const std::string & path, int refresh_time, Rect4F * dest_rect);

		/**
		 * update function for Animation
		 * @param time_passed the time passed since the last update
		 */
		void Update(int time_passed);

		/**
		 * draw the animation
		 * @param shader_program the shader program to draw in, this is usually the game shader program
		 */
		void Draw(GLuint shader_program);

		/**
		 * Add an action, an action is a series of coordinates in a sprite sheet that specifies the animation
		 * @note please document the action_code used, the action_code will be required when calling Animation::PlayAction
		 * @param action_code an integer/enum that can be used to identify the action added
		 * @param start_x x-position of the top-left corner of the first sprite
		 * @param start_y y-position of the top-left corner of the first sprite
		 * @param sprite_width the width of each individual sprite (this is not the dimension the action will have on screen!)
		 * @param sprite_height the height of each individual sprite (this is not the dimension the action will have on screen!)
		 * @param frame_count the number of frames this action contains
		 */
		void AddAction(int action_code, int start_x, int start_y, int sprite_width, int sprite_height, int frame_count);

		/**
		 * play an action
		 * @param action_code the action_code used in AddAction
		 */
		void PlayAction(int action_code);

		/**
		 * play an action with call back when certain frame is reached
		 * @param action_code the action_code used in AddAction
		 * @param callback a functor callback, the function will be receiving a void* pointing toward Animation::current_action_ as the parameter
		 * @param frame_of_callback the frame that triggers the callback
		 * @param callbacks the Animation callbacks put into an initializer_list, check AnimationCallback
		 * @param times_to_play number of times to play this animation, set to 1 by default
		 */
		void PlayAction(int action_code, const std::initializer_list<AnimationCallback> &callbacks, int times_to_play = 1);

		/**
		 * @return the currect action code
		 */
		int GetCurrentAction()	const;
	private:

		/************************callback action stuff***************/

		std::vector<AnimationCallback> callbacks_;	//!< callbacks passed in when calling Animation::PlayAimation

		int times_remaining;						//!< how many times the callback action needs to be played before stopping
													//!< set to -1 when no callback action is playing
		/************************************************************/
	protected:
		/**
		 * the amount of time that has passed since last action refresh
		 */
		int time_since_refresh_;

		/**
		 * The proper time for an action refresh
		 */
		int refresh_time_;

		/**
		 * the current frame in current action min: 0, max: frame count of this action
		 */
		int current_frame_;

		/**
		 * the action code of the currently played action, a current_action_ of -1 means no action is playing-call Animation::PlayAnimation to play
		 */
		int current_action_;

		/**
		 * action added
		 */
		std::map<int, std::vector<SDL_Rect>> actions_;					//map that stores all the sprites of the object
	};
}