#pragma once
#include <iostream>
#include <iterator>
#include <algorithm>
#include <SDL.h>
#include "Primitive.h"
#include <CEGUI/CEGUI.h>


namespace Fengine
{
	enum MouseButtonState
	{
		BUTTON_UP,				//not pressed
		BUTTON_DOWN,			//held down
		BUTTON_PRESSED,			//pressed down this frame
		BUTTON_RELEASED			//released this frame
	};

	/**
	 * a class(not singleton) that contains user input information
	 * usually this class is filled by Project1's inject input handler and passed around the system
	 */
	class Input
	{
	public:
		Input();
		~Input() 
		{
		}
		/***************************setters**************/
		/**
		 * record Key down event
		 */
		void KeyDownEvent(SDL_Scancode Key);

		/**
		 * handles the key up event
		 */
		void KeyUpEvent(SDL_Scancode Key);

		/**
		 * resets the pressedkeys and releasedkeys every new frame
		 */
		void BeginNewFrame();

		/**
		 * updates the status of mouse buttons
		 */
		void CursorUpdate(SDL_Event& CursorEvent);

		/**
		 * update the position of the cursor
		 */
		void CursorPositionUpdate();

		/**
		 * records wheel event, positive for rolling up, vice versa
		 */
		void WheelEvent(int Magnitude);									
		/************************************************/


		/***************************getters**************/

		/**
		 * @return how much the wheel is rolled, positive for rolling up, negative for rolling down
		 */
		int GetWheelEvent();

		/**
		 * @param Key the key to check
		 * @return true if a key is pressed
		 */
		bool IsKeyPressed(SDL_Scancode Key);

		/**
		 * @param Key the key to check
		 * @return true if a key is currently being held
		 */
		bool IsKeyHeld(SDL_Scancode Key);

		/**
		 * @param Key the key to check
		 * @return true if a key is released
		 */
		bool IsKeyReleased(SDL_Scancode Key);

		/**
		 * @return the status of left mouse button, return true for down false for up
		 */
		MouseButtonState GetLButtonState()	const;

		/**
		 * @return the status of right mouse button, return true for down false for up
		 */
		MouseButtonState GetRButtonState()	const;

		/**
		 * @return get the new-->old WASD pressing key vector, used in Player class
		 */
		std::vector<SDL_Scancode> GetPlayerMovementKeySet();
		/************************************************/

		SDL_Point cursor_loc_;									//!< stores the cursor SDL_Point on the screen

		bool is_key_pressed_;									//!< if any key is pressed in the current frame
		bool is_key_released_;									//!< if any key is released in the current frame

		std::map<SDL_Keycode, CEGUI::Key::Scan> SDL_CEGUI_keymap_;
		void Init();
	private:
		std::vector<SDL_Scancode> latest_key_press_;			//{later pressed-->earlier pressed}
		std::map<SDL_Scancode, bool> held_keys_;				//keys held in current frame
		std::map<SDL_Scancode, bool> pressed_keys_;				//keys pressed in current frame
		std::map<SDL_Scancode, bool> released_keys_;			//keys released in current frame
		MouseButtonState l_button_;								//stores the status of left button
		MouseButtonState r_button_;								//stores the status of right button
		int wheel_scroll_;										//stores wheel scroll, + for scrolling up, - for scrolling down
	};

}