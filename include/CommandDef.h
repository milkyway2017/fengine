#pragma once
#include <string>
#include <boost/bimap.hpp>
#include <boost/assign.hpp>

//file that defines every kind of Command in the program (Fengine + Project1)
namespace Fengine
{

	template <class T>
	using EnumStringMap = boost::bimap<T, std::string>;
	typedef EnumStringMap<int> IntEnumStringMap;

	/**
	* convert an enum to string
	* @param Enum an enum to convert
	* @param map the map used to convert
	* @return the corresponding string representation of the enum according to the map, when conversion not found, returns empty string
	*/
	static std::string EnumToString(int Enum, IntEnumStringMap map)
	{
		auto it = map.left.find(Enum);
		//check if exists
		if (it != map.left.end())
		{
			return it->second;
		}
		return "";
	}

	/**
	 * converts a string representation of enum into an enum (int)
	 * @param string the string representation 
	 * @param map the conversion map
	 * @return the int representation of that enum, returns -1 if no enum is found
	 */
	static int StringToEnum(const std::string string, IntEnumStringMap map)
	{
		auto it = map.right.find(string);
		//check if exists
		if(it != map.right.end())
		{
			return it->second;
		}
		return -1;
	}

	enum CommandLevel
	{
		PROGRAM = 1,
		FRAME,
		LEVEL,
		GAMEWORLD,
		MAP,
		MAP_EVENTMANAGER,
		NO_LEVEL = 0
	};
	typedef CommandLevel EventLevel;				//alias

	/**
	 * contains mapping from CommandLevel enum to string
	 */
	static IntEnumStringMap CommandLevelMap = boost::assign::list_of<IntEnumStringMap::relation>
		(PROGRAM, "PROGRAM")
		(FRAME, "FRAME")
		(LEVEL, "THREE")
		(GAMEWORLD, "GAMEWORLD")
		(MAP, "MAP")
		(MAP_EVENTMANAGER, "MAP_EVENTMANAGER")
		(NO_LEVEL, "NO_LEVEL");

	/**
	 * converts a command level to string
	 */
	static std::string CommandLevelToString(CommandLevel commandLevel)
	{
		std::string result = EnumToString(commandLevel, CommandLevelMap);
		if (result.empty())		//if empty
		{
			return "NO_LEVEL";
		}
		return result;
	}

	/**
	*  convert a string representation of a command level to enum
	* @param level the string representation of the command level
	* @return the enum, if not found, returns NO_LEVEL
	*/
	static int CommandLevelToEnum(std::string level)
	{
		auto it = CommandLevelMap.right.find(level);
		//check if exists
		if (it != CommandLevelMap.right.end())
		{
			return it->second;
		}
		//if not return NO_LEVEL
		return NO_LEVEL;
	}

	/**
	 *  the command type at PROGRAM level
	 */
	enum class ProgramCommandType
	{
		//program level
		NO_COMMAND = 0,
		/**
		 *  exit program
		 */
		EXP,
		/**
		 *  command to change the active frame
		 * @param paramString[0] the WindowFrameType to switch to
		 */
		CGF,
		/**
		 *  informs about resolution change
		 */
		NEW_RES
	};

	//contains mapping from ProgramCommanType
	static IntEnumStringMap ProgramCommandTypeMap = boost::assign::list_of<IntEnumStringMap::relation>
		((int)ProgramCommandType::NO_COMMAND, "NO_COMMAND")
		((int)ProgramCommandType::EXP, "EXP")
		((int)ProgramCommandType::CGF, "CGF");

	enum class FrameCommandType
	{
		//frame level
		NO_COMMAND = 100,
		/**
		 *  load a level
		 * @param paramString[0] the path of the level to laod
		 */
		LOAD_LEVEL,
		/**
		 *  toggle the console on/off
		 * @param paramString[0] '1' to turn on, '0' to turn off
		 */
		TOGGLE_CONSOLE,
		/**
		 *  toggle the shadow on/off
		 * @param paramString[0] '1' to turn on, '0' to turn off
		 */
		TOGGLE_SHADOW,
		/**
		 *  toggle a widget on/off
		 * @param paramString[0] the widgetID of the widget
		 * @param paramString[1] '1' to turn on, '0' to turn off
		 */
		TOGGLE_WIDGET,
		/**
		 *  add a widget to frame, supports PromptWidget & MessageBoxWidget
		 * @paramString[0] the type of the widget to add can be one of PromptWidget & MessageBoxWidget
		 * @note 
		 * -adding PromptWidget:
		 * @param paramString[1] title of the prompt
		 * @param paramString[2] prompt body text
		 * @param paramString[3] option 1 text
		 * @param paramString[4] option 2 text
		 * @param target a functor that can handle the user decision
		 * 
		 * -adding MessgaeBoxWidget:
		 * @param paramString[1] title of the message
		 * @param paramString[2] message body
		 * @param target a functor that can handle when OK is clicked
		 */
		ADD_WIDGET
	};
	
	static IntEnumStringMap FrameCommandTypeMap = boost::assign::list_of<IntEnumStringMap::relation>
		((int)FrameCommandType::NO_COMMAND, "NO_COMMAND")
		((int)FrameCommandType::LOAD_LEVEL, "LOAD_LEVEL")
		((int)FrameCommandType::TOGGLE_CONSOLE, "TOGGLE_CONSOLE")
		((int)FrameCommandType::TOGGLE_SHADOW, "TOGGLE_SHADOW")
		((int)FrameCommandType::TOGGLE_WIDGET, "TOGGLE_WIDGET")
		((int)FrameCommandType::ADD_WIDGET, "ADD_WIDGET");
		
	enum class LevelCommandType
	{
		NO_COMMAND = 200,
		/**
		 *  delete the current level
		 */
		DELLVL,
		/**
		 *  informs new resolution
		 */
		NEW_RES
	};
	
	static IntEnumStringMap LevelCommandTypeMap = boost::assign::list_of<IntEnumStringMap::relation>
		((int)LevelCommandType::NO_COMMAND, "NO_COMMAND")
		((int)LevelCommandType::DELLVL, "DELLVL")
		((int)LevelCommandType::NEW_RES, "NEW_RES");
		
	enum class GameWorldCommandType
	{
		NO_COMMAND = 300
		//TODO: LOAD_MAP
	};
	
	static IntEnumStringMap GameWorldCommandTypeMap = boost::assign::list_of<IntEnumStringMap::relation>
		((int)GameWorldCommandType::NO_COMMAND, "NO_COMMAND");
	
	enum class MapCommandType
	{
		NO_COMMAND = 400,
		/**
		 *  informs about new resolution
		 */
		NEW_RES
	};

	static IntEnumStringMap MapCommandTypeMap = boost::assign::list_of<IntEnumStringMap::relation>
		((int)MapCommandType::NO_COMMAND, "NO_COMMAND")
		((int)MapCommandType::NEW_RES, "NEW_RES");

	enum class MapEventManagerEventType
	{
		NO_COMMAND = 500,
		/**
		 * @note Event
		 *  fired when active area is filled
		 * @param target the active area that calls this
		 */
		ACTIVEAREA_FILLED,
		/**
		 * @note Event
		 *  fired when active area is emptied
		 * @param target the active area that calls this
		 */
		ACTIVEAREA_EMPTIED,
		/**
		 * @note Command
		 * @note can be sent to any kind of bot
		 *  set Bot's status to FOLLOWING
		 * @param target the Object to follow
		 */
		FOLLOW,
		/**
		 * @note Command
		 *  set Bot's status to FREEZING
		 */
		FREEZE,
		/**
		 * @note Command
		 *  set Bot's status to PATROLLING
		 */
		PATROL,
		/**
		 * @note Command/Event
		 *  informs that debug mode has been turned on/off, object should respond correspondingly
		 * @param paramStringList[0] true for on, false for off
		 */
		DEBUGMODE,
		/**
		 * @note Event
		 *  informs that an item is picked up by an object
		 * @param target the object that picked up the item
		 */
		ITEM_PICKEDUP,
		/**
		 * @note Event
		 *  informs that an item is dropped by an object
		 * @param target the object that dropped the item
		 */
		ITEM_DROPPED,
		/**
		 * @note Event
		 *  informs that an attack has happened
		 * @param paramStringList[0] the amount of damage dealt
		 * @param paramList[0] the attacker object
		 * @param paramList[1] the attacked object
		 */
		ATTACK,
		/**
		 * @note Event
		 *  informs that an object has died
		 * @param target the object dying
		 */
		DIE,
		/**
		 * @note Event
		 * informs that a key is pressed
		 * @param target the Input object
		 */
		KEY_PRESSED
	};

	static IntEnumStringMap MapEventManagerEventTypeMap = boost::assign::list_of<IntEnumStringMap::relation>
		((int)MapEventManagerEventType::NO_COMMAND, "NO_COMMAND")
		((int)MapEventManagerEventType::ACTIVEAREA_FILLED, "ACTIVEAREA_FILLED")
		((int)MapEventManagerEventType::ACTIVEAREA_EMPTIED, "ACTIVEAREA_EMPTIED")
		((int)MapEventManagerEventType::FOLLOW, "FOLLOW")
		((int)MapEventManagerEventType::FREEZE, "FREEZE")
		((int)MapEventManagerEventType::PATROL, "PATROL")
		((int)MapEventManagerEventType::DEBUGMODE, "DEBUGMODE")
		((int)MapEventManagerEventType::ITEM_PICKEDUP, "ITEM_PICKEDUP")
		((int)MapEventManagerEventType::ITEM_DROPPED, "ITEM_DROPPED")
		((int)MapEventManagerEventType::ATTACK, "ATTACK")
		((int)MapEventManagerEventType::DIE, "DIE");
};