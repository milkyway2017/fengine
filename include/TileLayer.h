#pragma once
#include <unordered_map>
#include "Graphics.h"
#include "Tile.h"
#include "Input.h"

namespace Fengine
{
	class Map;
	class EventManager;
	/**
	 * a class that represents a layer of tiles
	 */
	class TileLayer
	{
	protected:
		std::unordered_map<GLuint, GLShape> render_list_;

		GLuint shader_program_;
		int column_count_;
		EventManager* event_manager_;				//enables the TileLayer to subscribe to events
		Map* map_;

		/**
		 * used as a Functor callback to subscribe to DEBUGMODE event
		 * @param e pointer to Event struct
		 */
		void ToggleDebugMode(void* e);

		bool is_debugging_;
		
		Tile* tile_selected_;						//!< tile selected by cursor

	public:
		//LayerRenderMethod render_method_;
		std::vector<Tile> TileList;					//!< during each Draw call, each tile's Tile::Draw is called,
													//!< however most tiles don't do anything there as the rendering is done by TileLayer's Draw

		/**
		 * create a TileLayer, for a TileLayer to be usable, TileLayer::Init must be called
		 * @param map the pointer to map
		 */
		TileLayer(Map* map);

		/**
		 * @todo deprecate IsGroundLayer, TileLayer class should only care about Tiles and their rendering, not navigation
		 * initialize: tell the class what shader program to use when rendering
		 * @param shaderProgram the shader program to draw in
		 * @param columnCount the number of columns
		 * @param tile_count the number of tile that will be added to this layer
		 */
		void Init(GLuint shaderProgram, int columnCount, bool IsGroundLayer, unsigned int tile_count);

		/**
		 * update the Tiles in the layer
		 * @param input the user Input structure
		 * @todo deprecate the use of Input structure as a param, instead let the map do the work of selecting tile in a layer during DEBUGMODE
		 */
		void Update(Input * input);

		/**
		 * draw the TileLayer
		 */
		void Draw();

		/**
		 * Add a tile
		 * @param source_rect the rectangle area in an image file that has the texture of the TileLayer
		 * @param dest_rect the tile's location in map
		 * @param texture the loaded texture
		 * @param tileset_gid the gid read from the tile map editor
		 * @param z_order_offset the amount of offset of this tile's z_order, a default of 0 means this tile is drawn with the layer(no z_order)
		 */
		void AddTile(SDL_Rect source_rect, SDL_Rect dest_rect, GLuint texture, unsigned int tileset_gid, int z_order_offset = 0);

		/**
		 * activates the Tile
		 * @note this must be called before calling TileLayer::Draw, otherwise nothing will be draw on screen
		 */
		void Activate();

		/**
		 * gets the adjacent tile of a tile in specified direction
		 * @param t the tile to get adjacent from
		 * @param dir the direction, must use one of predefined Angles (check struct Angle)
		 * @return nullptr if no tile exists in specified direction
		 */
		Tile* GetAdjacentTile(Tile* t, Angle dir);

		/**
		 * gets the adjacent tile of a tile in specified direction
		 * @param tile_id the tileID of the tile to get adjacent from
		 * @param dir the direction, must use idle version Status
		 * @return nullptr if no tile exists in specified direction
		 */
		Tile* GetAdjacentTile(int tile_id, Angle dir);
		
		/**
		 * if true then the layer is used for navigation purposes (its tiles mark the availability of an area)
		 * @todo deprecate the use of TileLayer::IsGroundLayer
		 */
		bool is_ground_layer_;

		/**
		 * @return the selected tile, only available when TileLayer::is_debugging_ is true
		 */
		Tile* GetSelectedTile();

		/**
		 * returns the tile id of the next added tile
		 */
		unsigned int GetNextAvailableTileID() const;

		/**
		 * get a tile
		 * @param tile_id the tile's id
		 */
		Tile* GetTile(unsigned int tile_id);
	private:
		unsigned int tile_id_counter_;
	};
}