#pragma once
#include "Object.h"
#include "Input.h"

namespace Fengine
{
	class Player :
		public Object
	{
	protected:
		void InitAnimation(int TimeToUpdate);
		void HandleSpecialEvent(Event e) final{}
		void UpdateStats(int time_passed) final;
		bool CanMakeDamage(Object* other) final;
		void ReceiveAttack(int Damage) final;
		void Die() final;

	public:
		Player();
		~Player() final;

		static void LoadDef(const std::string & DefPath);


		/**************************************ObjDef defaults*******************************/
		/**
		 * these are the default (initial) stats for a new player, loaded from the PlayerDef.json
		 */

		static Rect4F def_SpriteToBody;				//a rect that can yield the Body rect when added to def_SpriteRect
		static Rect4F def_SpriteRect;				//stores the default SpriteRect with top-left located at (0,0)
		static bool def_Loaded;						//if PlayerDef has been loaded
		static float def_Health;					//initial health of player
		static float def_HealthRegen;				//health regen/ms
		static float def_Stamina;					//initial stamina of player
		static float def_StaminaRegen;				//stamina regen/ms
		static float def_Speed;						//default/initial terminal_speed in px/ms
		static float def_Agility;					//default agility, stealth and strength
		static float def_Stealth;
		static float def_Strength;

		const static int baseDamage = 10;		//this is the base damage for player, the actual damage is obtained by multiplying baseDamage and Strength

		/************************************************************************************/

		/**
		 * convert a scancode to a Point2F pointing in the corresponding direction
		 * @param code the scancode, sent by the key the user pressed
		 * @return a vector pointing in the correct direction; for example, SDL_SCANCODE_W typically yields (0,1)
		 */
		static Point2F ScancodeToVector(SDL_Scancode code);

		/**
		 * update the player
		 * @param time_passed the time passed in ms since last update
		 * @param Data a pointer to an Input class
		 */
		void Update(int time_passed, void * Data) final;
		/**
		 * initialize the player
		 * @param map the pointer to the map
		 * @param position the position of the Player
		 * @ShaderProgram the shader program
 		 * @param id unique object id issued by Tiled Map Editor
		 */
		void Init(Map* map, Point2F position, GLuint shader_program, int id);

		int GetHealth();

		int GetMaxHealth();

		int GetStamina();

		int GetMaxStamina();

		Obj_ptr Clone(Point2F Location) final;

		/**
		 * checks if an object is Player
		 * @param obj the pointer to the object to be checked
		 * @return true if obj is an instance of Player class
		 */
		static bool IsPlayer(Object* obj);

		/**
		 * checks if the player is alive
		 */
		bool IsAlive() const;

		int max_inventory_size_;					//!< max number of items this player can hold
	private:

		/********************************Player Attributes********************************/
		/**
		 * An attribute of 1 means default.
		 * For example, if the player has an agility of 1, the terminal_speed is 100% of the player's original(default) terminal_speed
		 * similarly, if the player has an agility of 1.2, the terminal_speed is 120% of the player's original(default) terminal_speed
		 */

        float Agility;						//determines the terminal_speed at which the player moves
		float Stealth;						//determines how hard it is for the NPC to detect the player
		float Strength;						//determines how hard the player attacks

		float Health;						//determines player's health
		float HealthRegen;					//determines health regen/ms
		float Stamina;						//determines player's stamina
		float StaminaRegen;					//determines stamina regen/ms

		float maxHealth;					//determines max health
		float maxStamina;					//determines max stamina

		Point2F spawn_position_;			//!< the past spawn position passed in from Player::Init
		/*********************************************************************************/

		std::vector<Timer*> scheduled_timers_;			//!< a list of scheduled timers

		/***********************Player internal functions*********************************/

		/**
		*  respawn the player at a new location (queried from Map), this functionality is special to Player, no other object has Respawn()
		*/
		void Respawn();
		//version of Respawn that can be used as a functor
		void Respawn(void* v);

		//helper function for player to determine which direction to go
		void DecideDirection(Input & input);
		/*********************************************************************************/
	}; 
}