#pragma once
#include <functional>
#include <map>
#include <vector>
#include <algorithm>
#include <memory>

namespace Fengine
{
	/**
	 * a function that takes in void* and return void
	 * @note do not use this when creating a Functor, intead just use plain function pointer,
	 * this is just a way to show the appropriate type of function for functor
	 */
	typedef std::function<void(void*)> FunctorFunction;

	/**
	 * class used for callbacks in the program (mainly used for event driven parts, e.g. EventManager)
	 * 	- supports multiple callback functions
	 * 	- supports global/static functions
	 * 	@note all callback functions must be in the form of FunctorFunction
	 * 	@code
	 * 	//sample code
	 *
	 * 	//a function that takes in void* and returns void
	 * 	void DoSomething(void* param)
	 * 	{
	 * 		//cast it to a string and print it
	 * 		std::cout<< (*(std::string*)param) << std::endl;
	 * 	}
	 *
	 * 	int main()
	 * 	{
	 * 		//create a functor using DoSomething
	 * 		Functor f(&DoSomething);
	 *
	 * 		std::string string_to_print = "hello Functor";
	 * 		//calls the functor by passing a pointer to string
	 * 		f(&string_to_print);
	 *
	 * 		return 0;
	 * 	}
	 *
	 *
	 * 	//output:
	 * 	//hello Functor
	 * 	@endcode
	 */
	class Functor
	{
	public:
		//use functor
		void operator()(void* p) const
		{
			std::for_each(std::get<1>(functions_).begin(), std::get<1>(functions_).end(), [=](FunctorFunction f) {f(p); });
		}

		/**
		 * compare two functors
		 * @return true if the functions linked are the same and are from the same object
		 */
		bool operator==(const Functor & other) const
		{
			if (std::get<0>(functions_) != std::get<0>(other.functions_))
				return false;
			if (std::get<1>(functions_).size() != std::get<1>(other.functions_).size())
				return false;
			for(unsigned int i = 0; i < std::get<1>(functions_).size(); i++)
			{
				if (std::get<1>(functions_)[i].target<void(*)(void*)>() !=
					std::get<1>(other.functions_)[i].target<void(*)(void*)>())
					return false;
			}
			return true;
		}

		/**
		 * create a callback with an object and one of its functions
		 * @tparam T any type of object
		 * @param t any instance of the type T
		 * @param memfunc the member function used for the functor
		 */
		template<class T>
		Functor(T* t, void(T::*memfunc)(void*)) :
			is_default_(false)
		{
			std::get<0>(functions_) = t;
			std::get<1>(functions_).push_back(std::bind(memfunc, t, std::placeholders::_1));
		}

		/**
		 * create a callback with an object and a list of its functions/methods
		 * @tparam T any type of object
		 * @param t any instance of the type T
		 * @param list a list of member functions
		 */
		template<class T>
		Functor(T* t, std::initializer_list<void(T::*)(void*)> list) :
			is_default_(false)
		{
			std::get<0>(functions_) = t;
			std::get<1>(functions_).reserve(list.size());
			std::for_each(list.begin(), list.end(), [=](void(T::*func)(void*)) {std::get<1>(functions_).push_back(std::bind(func,t,std::placeholders::_1)); });
		}

		/**
		 * create a callback with a static/global function
		 * @param func pointer to the static/global function
		 */
		Functor(void(*func)(void*)):
			is_default_(false)
		{
			std::get<0>(functions_) = nullptr;
			std::get<1>(functions_) = { func };
		}

		/**
		 * create a functor with a lambda expression
		 * @param lambd the lambda expression
		 */
		Functor(const FunctorFunction & lambd):
			is_default_(false)
		{
			std::get<0>(functions_) = nullptr;
			std::get<1>(functions_) = { lambd };
		}

		Functor():
			is_default_(true)			//true because default ctor used to construct
		{
			std::get<0>(functions_) = nullptr;
			std::get<1>(functions_) = {};
		}

		bool is_default_;				//!< if the functor is created by the default constructor
									//!< if true, this functor is probably not usable (because it holds no function)

		Functor(const Functor & other):
			Functor()
		{
			if(other.is_default_)
				return;
			*this = other;
		}

		Functor& operator=(const Functor & other)
		{
			//clear stuff
			std::get<0>(functions_) = nullptr;
			std::get<1>(functions_) = {};

			this->is_default_ = other.is_default_;
			if(this->is_default_)
				return *this;
			std::get<1>(functions_).reserve(std::get<1>(other.functions_).size());
			functions_ = other.functions_;
			return *this;
		}

		~Functor() 
		{
		}

	private:
		std::tuple<void*, std::vector<FunctorFunction>> functions_;			//stores object and its member function pointers
	};
}
