#pragma once
#include "Animation.h"
#include "Utility.h"
#include "Figure.h"
#include "Flashlight.h"
#include "EventManager.h"
#include "Timer.h"
#include "Tile.h"
#include <nlohmann/json.hpp>


namespace Fengine
{

	enum Status : int
	{
		IDLE = 1,
		WALKING = 7
	};

	enum Attribute
	{
		STEALTH,
		AGILTITY,
		STRENTH,
		HEALTH,
		STAMINA
	};

	typedef std::unique_ptr<Object>	Obj_ptr;

	//class used to manage a series of object movements, mainly used for navigation
	class Path
	{
	private:
		std::map<float, Angle> PathInfo;		//contains: a series of {distance, direction}
	public:
		/**
		 * add move 
		 * @param Distance the amount of distance in a direction
		 * @param dir the direction to go
		 */
		void AddMove(float Distance, Angle dir);

		/**
		 * get the direction to go
		 * @param Distance the current distance traveled
		 */
		Angle GetDirection(float Distance);

		/**
		 * get the index of the current step
		 * @return index, -1 if out of range(nagivation should have stopped at this point)
		 */
		int GetStepInPath(float Distance);

		/**
		 * clear all path info
		 */
		void ResetPath();

		Path();
		~Path();


	};

	class Map;

	class Object
	{
	public:
		int id_;							//!< id of this object issued by Tiled Map Editor
											//!< 0 if the object is created during runtime

		virtual Obj_ptr Clone(Point2F Location) = 0;

		/**
		 * basic constructor, Init() needs to be called after this
		 * @param objType
		 */
		Object(ObjectType objType);


		/**
		 * Initializes the object
		 * @note prior to calling this function, the caller should have already set up:
		 * 	1. Object::body_animation (by setting sprite_rect_, initializing the animation, and adding necessary actions to the animation)
		 * @param map the map this object belongs to
		 * @param collision_rect the rectangle that represents the object's collision body
		 * @param shader_program the shader program of the map
		 * @param id unique object id issued by Tiled Map Editor
		 */
		void Init_Obj(Map *map, Rect4F collision_rect, GLuint shader_program, int id = 0);

		/**
		 *  this is called by the map when there is an ATTACK event,
		 * this is pure virtual as every kind of Object may respond to attacks differently
		 * @param Damage the damage of this attack
		 */
		virtual void ReceiveAttack(int Damage) = 0;

		virtual ~Object();

		/**
		 * pick up an item
		 * @param item the pointer to the item to pickup
		 * @param index the inventory index (slot) to put the item
		 * @return true if pick up succeeded
		 */
		virtual bool PickUpItem(GameItem* item, unsigned int index);

		/**
		 * drop the item at an index
		 * @param i the index (slot)
		 * @return true if item dropped
		 */
		virtual bool DropItem(unsigned int i);

		/**
		 * drop the currently selected item
		 * @return true if item dropped
		 */
		virtual bool DropItem();

		//Move the object by time*DX and time*DY, and play animation
		//<Data>: respond to the map or player's input
		virtual void Update(int ElapsedTime, void * Data);

		//Update object statistics
		virtual void UpdateStats(int ElapsedTime) = 0;

		//draw the Object
		void Draw();

		void StopWalking();

		bool IsMoving();

		/*************************Setters*****************************************/

		/**
		 * set object to face a direction
		 * @param direction the direction the object faces (& moves), has to be one of the predefined Angles
		 */
		void SetMovementState(Angle direction, Status status = WALKING);

		//set the Object's visibility
		inline void SetVisible(bool vis)			//sets the Object's visibility
		{
			is_visible_ = vis;
		}

		void SetPosition(const Point2F & position);
		/*************************************************************************/

		/*************************Getters*****************************************/
		/**
		 * get the terminal_speed of current object
		 */
		Speed& GetSpeed();

		/** 
		 * a list of event subscribed by this object, this is public
		 * because it will be used by the map when the object is destroyed
		 */
		std::map<int, Functor> subscribedEvents;

		/**
		 * get the ObjectType enum of this Object
		 */
		ObjectType GetType()	const;

		/**
		 * get the object's visibility
		 * @return the object's visibility
		 */
		inline bool IsVisible() const
		{
			return is_visible_;
		}

		/**
		 * check if a point lies inside the sprite_rect_
		 * @param point the point to check
		 * @return true if the point lies inside
		 */
		bool IsPointInSprite(const Point2F & point) const;

		/**
		 * @return if the object is following another object
		 */
		bool IsFollowing() const
		{
			return is_following_path_;
		}

		/**
		 * @return the index of the item selected in the object's item_list
		 */
		int GetItemSelected()	const;

		/**
		 * @return reference to the object's item_list_
		 */
		std::vector<GameItem*>& GetItemList();

		Z_order* GetZOrder();

		Body* GetBody();

		/**
		 * get the geometric center of this object's collision body
		 */
		Point2F GetCenter();
		/*************************************************************************/


		//moves the object
		//<move>: pixels to move
		inline void MoveObject(Point2F move)
		{
			//move the body
			body_.MoveCenter(move);
		}

		/**
		 * let the object walk to a location in map
		 * @param point the xy-coordinate of the destination
		 */
		void GoTo(const Point2F & point);

		/**
		 * moves the object to a new location
		 * @param dest the new location in map coordinates
		 */ 
		inline void MoveObjectCenterTo(Point2F dest)
		{
			//calculate the distance to move, and call MoveObject
			Point2F distance_to_move = dest - body_.GetCenter();
			MoveObject(distance_to_move);
		}

		bool is_init_;

		Map* map;					//the pointer to the map that holds this obj

		/***************Object Event Handling*************************************************************/
		/*
		Logic:
		1. HandleEvent() is called every Update()
		2. HandleEvent() processes all event that Object class is aware of,
		the rest is sent to HandleSpecialEvent(), which is implemented by the derived class
		*/

		//implemented by the derived class
		//handles special events only known to the NPC types (such as changing NPCstatus to PATROLLING...)
		//a command is passed to this Object
		virtual void HandleSpecialEvent(Event c) = 0;
		//function that handles all Events in EventQueue
		//EventQueue will be cleared after an HandleEvent call
		virtual void HandleEvent();

		//adds an event to EventQueue
		//can be used as a Functor
		void AddEvent(void * e)
		{
			//if the Functor is allocated using new operator, clone it and delete the original
			if (((Event*)e)->IsInHeap())
			{
				EventQueue.push_back(((Event*)(e))->Clone());
				delete (Event*)(e);
			}
			//otherwise just copy
			else
			{
				AddEvent(*(Event*)e);
			}
		}
		//adds an event to EventQueue
		void AddEvent(const Event& c)
		{
			EventQueue.push_back(c);
		}
		/*************************************************************************************************/

	private:
		bool is_visible_;						//if the object is visible, can be set by derived using SetVisible()
		const ObjectType obj_type_;				//the type of the object


		/****************************debug mode stuff****************/
		static bool is_debugging_;				//whether the map is in debugmode
		GLColor highlight_color_;				//the color used to display highlight (debug) shape
		/****************************debug mode stuff****************/


		/****************************navigation****************/
		Path obj_path_;							//contains the steps for nagivation
		Object* target_following_;				//the target this Object is following, may be nullptr 
		Tile* target_tile_;						//the tile the target is at
		bool is_following_path_;				//is the Object in nagivation mode
		
		/**
		 * helper function, given the destination in map, generate a path to it
		 * @param dest the point of destination
		 */
		void GenPath(const Point2F & dest);

		/**
		 * helper function, handles navigation logic in Update()
		 */
		void HandleNavigation();

		/****************************navigation****************/
	protected:
		Body body_;							//!< the physical body of this object

		GLuint shader_program_;				//!< the shader program to draw the object in
		Z_order z_order_;					//!< determines the order all objects are rendered in the system 
		Rect4F sprite_rect_;				//!< the rectangle of the Object's body sprite
		Animation body_animation_;					//!< animation that draws the body of the object
		std::vector<Animation*> animation_list;		//!< a list of Animations that is owned by this Object and its derived class
													//!<	- all animations in this list will be updated and drawn every frame
													//!<	- derived classes are welcomed to add animations to this list, however, it is the adder's responsibility to manage the added animations

		std::vector<Event> EventQueue;		//!< a list of Events that haven't been processed by the Object

		std::vector<GameItem*> item_list_;	//!< a list of GameItems owned by this Object

		unsigned int item_selected_;				//!< index of the item that is selected by the object

		/**
		*  a wrapper around the EventManager::SubscribeEvent
		* this will automatically add the subscribed event to subscribeEvents,
		* should be used all the time
		* @param eventType the type of event to subscribe to, please refer to CommandDef.h
		* @param functor the functor used to subscribe
		*/
		void SubscribeEvent(MapEventManagerEventType eventType, Functor functor);

		/**
		*  a wrapper around the EventManager::UnsubscribeEvent,
		* this should always be used, as it will check if the event is subscribed,
		* it will also remove the event subscription from subscribeEvents when unsubscibing
		* @param eventType the type of event to unsubscribe from, please refer to CommandDef.h
		* @param functor the functor used to subscribe
		*/
		void UnsubscribeEvent(MapEventManagerEventType eventType, Functor functor);

		const GLColor& getHighlightColor() const;
		void setHighlightColor(const GLColor &highlightColor);
		/**
		 * toggle the debugmode on/off
		 * @param e the event
		 */
		static void ToggleDebugMode(void* e);

		/**
		 * starts to follow a Target
		 * @param target the target to follow
		 */
		void BeginFollowing(Object* target);

		/**
		 * stop following the target
		 */
		void EndFollowing();

		virtual void ConfigureZOrder();

		/**
		 * checks if THIS object is able to damage ANOTHER object, depending on the type of Object, the implementation can vary,
		 * usually, this checks if the other Object is in range
		 * @note if an object cannot make damage, it does not mean that the object cannot attack (for example, an object can swing the weapon without making any damange)
		 * @param other the object to be attacked
		 * @return true @if the object can be attacked
		 * 		   false @if the object cannot
		 */
		virtual bool CanMakeDamage(Object* other) = 0;

		/**
		 * attacks another Object
		 * @param other the object to attack
		 * @param Damage the damage of this attack
		 */
		void SendAttack(Object *other, float Damage);

		/**
		 * die-this will drop all the items & fire a DIE event(for map to log)
		 * @warning any custom de-allocation/clean up/reset should be done in the derived classes before the base Die() is called
		 */
		virtual void Die();

	};

}