#pragma once
#include <map>
#include <vector>

#include "Command.h"

namespace Fengine
{
	class Functor;
	/**
	 * EventManager is able to fire event and store subscribers of events,\n
	 * currently EventManager is used in Map, available to Map objects such as Object and GameItem. \n
	 * @note To fire an event call EventManager::FireEvent, to Subscribe to a type of event call SubscribeEvent.\n
	 * When a subscriber gets destroyed, make sure the events subscribed are unsubscribed using EventManager::UnsubscribeEvent
	 */
	class EventManager
	{
	private:
		std::map<int/*event type*/, std::vector<Functor>/*subscriber*/> subscriber_table_;		//!< stores the event type and the subscribers
		Logger* ref_logger_;							//!< pointer to the owner's logger, this is used for logging when error occurs 
	public:
		/**
		 * create an EventManager
		 * @param log the logger of the owner of this EventManager
		 */
		explicit EventManager(Logger* log);
		~EventManager();

		/**
		 * fire an event, for the types of event available please refer to CommandDef.h
		 * @param e the event to fire
		 */
		void FireEvent(const Event &e);

		/**
		 * subscribe to a type of event, for the types of event available please refer to CommandDef.h
		 * @param command_type the type of the event to subscribe to, enum must be cast into int
		 * @param f the functor called when the event is fired
		 */
		void SubscribeEvent(int command_type, const  Functor& f);

		/**
		 *  unsubscribe from an event
		 * @param command_type the type of event to unsubscribe from
		 * @param f the functor used to subscribe this event
		 */
		void UnsubscribeEvent(int command_type, const Functor& f);
	};
}