#pragma once
#include "Kinematics.h"
#include "Z_order.h"
#include "Sprite.h"

namespace Fengine
{
	//forward decl
	class Map;
	class EventManager;

	/**
	 * Map object that represents an entity, may be a wall/decoration/...
	 * this class provides collision detection utilities
	 */
	class Figure
	{
	protected:
		Z_order z_order_;						//!< z-order of this Figure, used to sort rendering index
		EventManager* event_manager_;			//!< pointer to map's event manager
		Body body_;						//!< body that handles collision
		Sprite sprite_;							//!< used when the figure is an image (storefront, street lamp...)
		Map* map_;								//!< pointer to the map this figure is in
	public:

		int id_;								//!< unique object id issued by Tiled Map Editor

		/**
		 * default constructor
		 * @note if you choose to use default ctor, be sure to initialize it with Figure::Init
		 */
		Figure(){};

		/**
		 * rectangle Figure constructor, preferred
		 * @param rect a rectangle representing the collision body
		 * @param is_solid true if the figure is solid (responds to collision)
		 * @param is_movable true if the figure moves when colliding with another solid figure
		 * @param id unique object id issued by Tiled Map Editor
		 * @param map the pointer to the map
		 * @param image_path the path to the texture
		 * @param source_rect the area of the texture to render
		 */
		Figure(const Rect4F & rect, bool is_solid, bool is_movable, int id = 0, Map* map = nullptr, const std::string & image_path= "", const SDL_Rect & source_rect = {});

		Figure(const Figure & other);

		/**
		 * initialize the figure (alternative to construtor)
		 */
		bool Init(Map * map, GLShape shape, GeometryType type, int id, bool is_solid, bool is_movable);

		virtual ~Figure();


		virtual void ConfigureZOrder();

		/**
		 * @return the solidity of this figure
		 */
		inline bool IsSolid()
		{
			return body_.is_solid_;
		}

		/**
		 * set the solidity of this figure
		 * @param is_solid true if the figure collides
		 */
		inline void SetSolidity(bool is_solid)
		{
			body_.is_solid_ = is_solid;
		}

		/**
		 * @return the collision rect of this figure
		 */
		inline Point2F GetCenter()
		{
			return body_.GetCenter();
		}

		inline Z_order* GetZOrder()
		{
			return &z_order_;
		}

		/**
		 * get the position,
		 * @return the position of the top left corner if the shape is a rect, otherwise returns the center (same with GetCenter)
		 */
		inline Point2F GetPosition()	const
		{
			if (body_.type_ == RECT_SHAPE)
				return body_.collision_rect_.GetXY();
			return body_.GetCenter();
		}

		/**
		 * move the Figure by some distance
		 * @param delta distance to move
		 */
		void Move(Point2F delta);

		/**
		 * Move the figure to new location
		 * @param destination the new location of the top left corner of the Figure
		 */
		void MoveTo(Point2F destination);

		/**
		 * Move the figure's center to one location
		 * @param destination the new location of the center of the Figure
		 */
		void MoveCenterTo(Point2F destination);

		/**
		 * get the physical body of this Figure
		 */
		Body* GetBody();

		/**
		 * get the sprite of this figure
		 */
		Sprite* GetSprite();

		/**
		 * virtual update function for derived classes
		 * @param time_passed the time passed since last update in ms
		 */
		virtual void Update(int time_passed) { }

		/**
		 * can be implemented by derived classes
		 */
		virtual void Draw();

		/**
		 * draws the Figure with highlight, only rect figure is supported for now,
		 * used for debugging purposes
		 * @param color the color used for debugging
		 * @param shader_program the shader program to draw the figure
		 */
		void Draw_Fig_Debug(const GLColor & color, GLuint shader_program);
	};
	typedef std::unique_ptr<Figure> Figure_ptr;
}
