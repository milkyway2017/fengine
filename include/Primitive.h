#pragma once
#include <GL/glew.h>
#include <SDL_rect.h>
#include <vector>

/**
 * defines all "primitive" data types of Fengine
 */

namespace Fengine
{
	/**
	 * constant
	 */
	const float pi = 3.1415926535f;

	/**
	 * types of geometry in Fengine
	 */
	enum GeometryType
	{
		POINT_SHAPE,
		LINE_SHAPE,
		RECT_SHAPE,
		CIRCLE_SHAPE,
		POLYGON_SHAPE
	};

	/**
	 * structure that represents a point on a 2d plane, can also be used to represent a 2d vector
	 */
	struct Point2F
	{
		float x;			//!< x component, set to NaN by default
		float y;			//!< y component, set to NaN by default

		explicit Point2F(float x, float y);
		explicit Point2F();
		explicit Point2F(const SDL_Point & other);
		/*************************************
		 * vector arithmetics  ***************
		 *************************************/

		Point2F operator-()	const;
		Point2F operator+()	const;
		Point2F operator*(const float & scale)	const;		//!< vector-scalar product
		Point2F operator/(const float & scale)	const;		//!< vector-scalar division
		Point2F operator+(const Point2F & other)	const;
		Point2F operator-(const Point2F & other)	const;
		float operator*(const Point2F & other)	const;		//!< dot product
		float Norm()	const;
		Point2F Normalize()	const;
		float PsuedoNorm()	const;							//!< norm^2 of the vector
		float Angle(const Point2F & other)	const;			//!< get the internal angle formed with another vector
		void operator+=(const Point2F & other);
		void operator-=(const Point2F & other);
		bool operator==(const Point2F & other)	const;
		bool operator!=(const Point2F & other)	const;
		bool operator<(const Point2F& other)	const;		//!< compares based on the magnitudes
		bool operator>(const Point2F& other)	const;

		/**
		 * explicit conversion to point
		 */
		explicit operator SDL_Point()	const;

		/*************************************
		 ** point arithmetics  ***************
		 *************************************/

		float Dist(const Point2F & other)	const;			//!< finds the distance between 2 points

		/**
		 *
		 * @return if the vector is at default (or valid)
		 */
		bool IsDefault()	const;
		operator bool()	const;								//!< if this vector is not a null vector
		std::string ToString();								//!< string representation
	};
	typedef Point2F Vector2F;
	Point2F operator*(float scale, const Point2F & vector);

	/**
	 * represents a rectangle on a 2d plane or a 4d vector
	 */
	struct Rect4F
	{
		/*********************************************
		 * components, initialized to NaN by defualt *
		 *********************************************/

		float x;
		float y;
		float w;
		float h;

		Rect4F();
		Rect4F(Point2F xy, Point2F wh);
		Rect4F(float x, float y, float w, float h);
		Rect4F(Point2F xy, float w, float h);
		explicit Rect4F(const SDL_Rect & other);

		/*************************************
		 ** rectangle arithmetics  ***********
		 *************************************/

		/**
		 * move the rectangle so that it centers at the position given
		 */
		void SetCenter(const Point2F& pos);
		Point2F& GetXY() const;
		Point2F GetCenter() const;

		explicit operator SDL_Rect()	const;

		/**
		 *
		 * @return if the rect is at default (or valid)
		 */
		bool IsDefault()	const;
		bool operator==(const Rect4F & other)	const;
		
		/*************************************
		 ** vector arithmetics  **************
		 *************************************/

		Rect4F operator+(const Rect4F & other)	const;
		Rect4F operator-(const Rect4F & other)	const;

	};

	typedef float Radian;
	typedef int Degree;
	
	/**
	 * structure that describes angles, allows easy conversion between degrees and radians
	 */
	struct Angle
	{
	private:
		union
		{
			float radian = 0.f;
			int degree;
		};

		bool is_radian = true;
		bool is_undefined = true;

	public:
		Angle() = default;			//!an angle is by default undefined

		/**
		 * create a radian angle
		 * @param radians
		 */
		explicit Angle(Radian radians);

		/**
		 * create a degree angle
		 * @param degrees
		 */
		explicit Angle(Degree degrees);

		/***************************************************
		 *************  basic angle operations *************
		 ***************************************************/

		Radian GetRadian()	const;

		Degree GetDegree()	const;

		Angle& ToRadian();

		Angle& ToDegree();

		Angle operator+(const Angle& other)	const;

		Angle operator-(const Angle& other)	const;

		Angle operator*(const float& scale)	const;

		/**
		 * fix a negative angle or angle that is greater than 2pi or 360deg to the range 0~2pi or 0~360deg
		 */
		Angle& Simplify();

		/**
		 * flip the angle around the x axis, also simpilfies the angle
		 */
		Angle& FlipX();

		/**
		 * set the angle to undefined
		 */
		void SetUndefined();

		/**
		 * check if the angle is undefined
		 */
		bool IsUndefined() const;

		/***************************************************
		 *************  logical op. ************************
		 ***************************************************/

		bool operator>(const Angle& other)	const;

		bool operator<(const Angle& other)	const;

		bool operator==(const Angle& other)	const;

		bool operator>=(const Angle& other)	const;

		bool operator<=(const Angle& other)	const;

		bool operator!=(const Angle& other)	const;
	};


	/**
	 * a structure that represents a color, values have a range of 0~1
	 */
	struct GLColor
	{
		float r;
		float g;
		float b;
		float a;

		GLColor(const SDL_Color& sdlcolor);

		GLColor(float r, float g, float b, float a);

		GLColor();

		operator SDL_Color()	const;

		/**
		 * return a transparent version of this GLColor
		 * @param t transparency [0,1]
		 */
		GLColor ToTransparent(float t)	const;
	};

	/**
	 * represents a vertex
	 */
	struct GLVertex
	{
		Point2F position;
		GLColor color;
		Point2F texture_coordinate;

		GLVertex() = default;
		GLVertex(Point2F location, GLColor color = GLColor(), Point2F tex_coord = Point2F());

		bool operator ==(const GLVertex& other)	const;
		bool operator !=(const GLVertex& other)	const;
	};

	/**
	 * represents any polygon, used by Graphics class to render
	 */
	struct GLShape
	{
		GLuint vao_id;
		GLuint vbo_id;
		std::vector<GLVertex> vertices;

		/**
		 * shift the GLShape by some amount 
		 * @param delta the amount to move by
		 */
		void Move(const Point2F& delta);

		/**
		 * move the first vertex to a location, bringing all other vertices over the same delta distance moved
		 * @param destination the new location for the first vertex
		 */
		void MoveTo(const Point2F& destination);

		void AddVertex(GLVertex vertex);

		void SetTransparency(float transparency);

		bool operator==(const GLShape & other)	const;
		bool operator!=(const GLShape & other)	const;
	};


	/**
	 * represents a line, mainly used by flashlight's visibility polygon algorithm
	 */
	struct Line
	{
		Point2F P1;
		Point2F P2;

		Line() = default;
		Line(Point2F p1, Point2F p2);

		bool IsDefault()	const;

		//if theses two lines are connected
		const Point2F* operator^(const Line& other)	const;

		Point2F ToVector()	const;
	};

	struct Circle
	{
		Point2F xy;					
		float radius = 0;

		Circle() = default;
		Circle(Point2F center, float radius);

		bool IsDefault()	const;
	};
}