#pragma once
#include "Graphics.h"
#include "Z_order.h"

namespace Fengine
{
	class Map;
	/**
	 * represents a tile in Map
	 */
	class Tile
	{
	public:
		Tile();

		/**
		 * create a tile
		 * @param texture_ the texture used by the tile
		 * @param source_rect the coordinate in the texture this tile renders
		 * @param dest_rect the coordinate in the map this tile renders
		 * @param tile_id the tile's number in map's json file
		 * @param tileset_gid tile gid read from map file
		 * @param z_order_offset the amount of z to plus or minus when configuring z_order
		 * @param map the map
		 * @param shader_program the map's shader program
		 */
		Tile(GLuint texture_, SDL_Rect source_rect, SDL_Rect dest_rect, Map* map, GLuint shader_program, unsigned int tile_id, unsigned int tileset_gid, int z_order_offset = 0);
		~Tile();

		void Update() {}
		void Draw();						//draws the tile

		GLuint texture_;					//the pointer to a texture that it will be using to render
		bool no_collision_;					//true if there is no wall/collider on this tile i.e.: this tile is open for path finding //!< @todo deprecate the use of Tile::no_collision_

		SDL_Rect source_rect_;
		SDL_Rect dest_rect_;				//the position of the tile on screen
		GLShape shape_;					//!< shape used to render

		unsigned int tile_id_;					//!< issued by TileLayer, ranges from 0 to row*col-1
		unsigned int tileset_gid_;				//!< tile gid read from map file

		//gets the center of this tile
		Point2F GetCenter();	

		bool highlighted_;				//!< there will be a shade over this tile if Highlighted
		GLColor hightlight_color_;		//!< color used to render highlight square
		GLuint shader_program_;			//!< used when rendering
		Z_order z_order_;				//!< the Z_order

		Map* map_;						//!< pointer to map
	};

}