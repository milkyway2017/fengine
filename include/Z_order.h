#pragma once
#include <memory>
#include <functional>

namespace Fengine
{
	//forward declaration
	class Object;
	class Figure;
	class GameItem;

	//an object that helps sorting the drawable game elements in the z axis
	class Z_order
	{
	public:
		/**
		 * default constructor
		 * @note Make sure this object is not used before initialzation
		 */
		Z_order()
		{}

		~Z_order(){}

		bool is_ref_used = false;
		union
		{
			float z_pos_;
			float* z_pos_ref_;
		};
		float z_offset_ = 0;

		std::function<void()> draw_function_;				//!< the draw function of the sorted object(owner)

		/**
		 * draw the owner
		 */
		void Draw() const;

		/*
		 * initialize with a constant z position
		 * @param z_pos the z position, usually the y position of the object
		 * @param draw_function the function used to draw the owner of this z_order
		 * @param z_offset a value to add to the z_position when comparing z_order, this is usually
		 * the object's height
		 */
		void Init(float z_pos, std::function<void()> draw_function, float z_offset = 0);

		/*
		* initialize with a pointer to a z position
		* @param z_pos_ref the z position, usually the y position of the object
		* @param draw_function the function used to draw the owner of this z_order
		* @param z_offset a value to add to the z_position when comparing z_order, this is usually
		* the object's height
		*/
		void Init(float* z_pos_ref, std::function<void()> draw_function, float z_offset = 0);

		/**
		 * get the z position of this z_order
		 */
		float GetZPosition() const;
	};

	/**
	 * @param a
	 * @param b
	 * @return true if a should be drawn before b
	 */
	extern bool CompareZOrder(Z_order* a, Z_order* b);
}

