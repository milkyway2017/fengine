#pragma once
#include <string>
#include <fstream>

namespace Fengine
{

	class Logger
	{
	public:
		Logger();
		std::string log_;			//!< string that holds the log

		/**
		 * write the content of log to a file specified
		 * @param file_path the path of the file, including the file name and extension
		 */
		void WriteToFile(const std::string & file_path);

		/**
		 * add a new line to logger
		 * @param line the string to add, without '\n'
		 */
		void AddLine(const std::string & line);

		//todo: write without CEGUI's tags
		//void WriteToFile_NoTag();
	};
}