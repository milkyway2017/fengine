#pragma once
#include <string>
#include <SDL_ttf.h>

#include "Graphics.h"
#include "Functor.h"
#include "Sprite.h"

namespace Fengine
{
	/**
	 * data structure used to create a label, default LabelStyle is consolas-20-red-no-effect
	 */
	struct LabelStyle
	{
		TTF_Font* font = Graphics::GetInstance()->GetFont("consola.ttf", 20);	//!< default consola
		GLColor color = GLColor(1.f,0,0,1.f);		//!< default red
		Uint32 fade_in_time = 0;		//!< time in ms required by the opacity to go from 0 to 1, set to 0 for no effect
		Uint32 fade_out_time = 0;		//!< time in ms required by the opacity to go from 1 to 0, set to 0 for no effect
	};

	/**
	 * a class that provides on screen text rendering with various styles
	 */
	class Label
	{
	public:
		/**
		 * default constructor, no effect
		 */
		Label() = default;

		/**
		 * constructor
		 * @param text text to display
		 * @param position the position of label's top left on the screen
		 * @param style the style, go to LabelStyle for more details
		 */
		Label(const std::string & text, const Point2F & position, const LabelStyle & style = {});

		/********************************Getters*********************/

		/**
		 * @return the style of this label
		 */
		LabelStyle GetStyle() const;

		/************************************************************/

		/*********************************Setters********************/

		/**
		 * set a new style, this will automatically update the texture
		 * @param style a new style
		 */
		void SetStyle(const LabelStyle & style);

		/**
		 * set a new text to display, this will automatically update the texture
		 * @param text new text
		 */
		void SetText(const std::string& text);
		/************************************************************/

		void Draw(GLuint shader_program);

		/**
		 * clean up all the resources used, the textures
		 */
		void CleanUp();

		Point2F position_;				//!< the top left coordinate of the label
	private:
		std::string text_;
		LabelStyle style_;
		Uint32 start_time = 0;
		bool is_valid_;					//!< if the texture is up to date with the label

		std::vector<Sprite> sprites_;

		/**
		 * private helper functions
		 */

		void UpdateTexture();
		float GetCurrentTransparency();
	};
}
