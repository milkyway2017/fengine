#pragma once
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <SDL.h>
#include "SDLClipboardProvider.h"
#include "Exception.h"

namespace Fengine
{
	/**
	 * class that manages GUI resources, uesd by Project1's widget and frame classes
	 */
	class GUI
	{
	public:
		static GUI* GetGUIClass()
		{
			static GUI* gui = nullptr;
			if (gui == nullptr)
			{
				gui = new GUI;
			}
			return gui;
		}
		CEGUI::OpenGL3Renderer* GetRenderer()
		{
			return opengl_renderer_;
		}
		CEGUI::GUIContext& GetGUIContext()
		{
			return  CEGUI::System::getSingleton().createGUIContext(opengl_renderer_->getDefaultRenderTarget());
		}

		/**
		 * load a window, it is okay to load from the same file multiple times, GUI class has its own window counter that handles this
		 * @param path the path to the .layout file
		 * @return the loaded window
		 */
		CEGUI::Window* LoadWindow(const std::string & path);
	private:
		unsigned long name_counter_;			//variable that counts the unique name of cloned window
		//cegui renderer
		CEGUI::OpenGL3Renderer* opengl_renderer_;
		std::map<std::string, CEGUI::Window*> loaded_windows_;		//list of windows loaded from layout file
		
		GUI();
		~GUI()
		{
			CEGUI::System::destroy();
			CEGUI::OpenGL3Renderer::destroy(static_cast<CEGUI::OpenGL3Renderer&>(*opengl_renderer_));
		}
	};
}