#pragma once
#include <string>
#include "Logger.h"

namespace Fengine
{
	/**
	 * CommandDumpLog is a singleton class where Command destructors write at when they are destroyed
	 */
	class CommandDumpLog
	{
	private:
		CommandDumpLog();
		
	public:
		/**
		 * get the singleton class
		 * @return the pointer to the singleton CommandDumpLog
		 */
		static CommandDumpLog* GetInstance()
		{
			static CommandDumpLog* log = new CommandDumpLog();

			return log;
		}

		~CommandDumpLog();

		Logger logger_;					//!< logger, used to log Commands' destruction
	};
}

