#pragma once
#include "Primitive.h"
#include <SDL.h>
#include "Z_order.h"
#include "EventManager.h"
#include "Sprite.h"


namespace Fengine
{
	class Map;
	class Object;

	enum GameItemType
	{
		CELLPHONE,
		FLASHLIGHT
	};

	/**
	 * base class for all items in game
	 */
	class GameItem
	{
	protected:
		GLuint shader_program_;		//!< shader program of the map
		Map* map_;					//!< a pointer to the map
		Object* owner_;				//!< a pointer to the owner of this item
		bool is_active_;				//!< if the item is activated, different items have different behaviors for this

		EventManager* event_manager_;		//!< the pointer to the event manager of the map
		Z_order z_;							//!< the z_order of current item

		Point2F location_;					//!<the location of this GameItem

		Sprite icon_;					//!< the sprite of the icon of this icon
	public:
		/**
		 * constructor for GameItem
		 * @param Type the type of the Item
		 * @param map the pointer to the map
		 * @param ShaderProgram the shader program of the map
		 * @param Owner the owner of this item
		 * @param id unique object id issued by Tiled Map Editor
		 * @param ItemLocation the location of this item
		 */
		GameItem(GameItemType Type, Map*map, GLuint ShaderProgram, Object* Owner = nullptr, int id = 0, Point2F ItemLocation = Point2F());

		/**
		 * @return the pointer to the icon sprite
		 */
		Sprite* GetIcon();

		/**
		 * @return the location of the item in xy coordinate
		 */
		virtual Point2F& GetLocation();

		virtual void Update(Uint32 ElapsedTime, Map* MapInfo) = 0;

		/**
		 * render function
		 */
		virtual void Draw() = 0;

		/**
		 * use the item, this is different from GameItem::Activate. For example, a gun is not used but activated as it's being picked up. Using the gun means shooting.
		 * @param Info supplementary info
		 */
		virtual void Use(void* Info) = 0;
		virtual void Move(Point2F deltaXY) = 0;
		virtual void MoveTo(Point2F Location) = 0;

		/**
		 * virtual function, there may be special logic for a specific item when it is being activated/deactivated
		 * @param OnOff true to activate, false to deactivate
		 */
		virtual void Activate(bool OnOff)
		{
			is_active_ = OnOff;
		}
		/*
		 * @return true for activated
		 */
		virtual bool IsActive()	const
		{
			return is_active_;
		}
		//switch states
		inline void Toggle()
		{
			Activate(!is_active_);
		}

		inline Z_order* GetZOrder()		//get the z_order
		{
			return &z_;
		}

		virtual void ConfigureZOrder() 		//function that forces the derived class to configure Z_order within init
		{}

		/**
		 * Tells the GameItem that it is now owned by an object, get ready for use
		 * @param owner the object that now owns the object
		 */
		virtual void Own(Object* owner);

		/**
		 * Tells the GameItem that it is now dropped,
		 * this will deactivate the item and reset the owner to nullptr
		 */
		virtual void Drop();

		/**
		 * @return true if the GameItem is owned by an object
		 */
		bool HasOwner();
		virtual ~GameItem() {}

		//copy constructor
		GameItem(const GameItem & other);

		const GameItemType Type;				//!< the type of this item

		/**
		 * pure virtual function, this should be implemented by the derived class.
		 * @return a cloned item
		 */
		virtual GameItem* Clone() = 0;

		const int id_;						//!< unique object id issued by Tiled Map Editor
	};
	typedef std::unique_ptr<GameItem> Item_ptr;
}
