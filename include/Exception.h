#pragma once
#include <exception>
#include <vector>
#include <string>

namespace Fengine
{
	//forward decl
	struct Command;
	/**
	 * an exception class in Fengine, rules for using exception:
	 * 	- only throw when you have to, otherwise use error code or return value
	 * 	- no exception should be thrown by anything below Map in hierarchy (all Map entities such as Object, GameItem, ActiveArea and ...)
	 * 	- read post https://milkyway-project.000webhostapp.com/2018/06/exceptions-in-fengine-and-project1 \n
	 * exceptions will appear in the form of message boxes if not caught (this is preferred, there really isn't a good way to deal with them exception for displaying)
	 */
	class Exception : public std::exception
	{
	public:
		/**
		 * simple constructor, not preferred
		 */
		explicit Exception(const std::string & msg, bool is_fatal = false) noexcept:
			is_fatal_(is_fatal)
		{
			message_queue_.push_back("Exception Thrown");
			message_queue_.push_back(msg);

			JoinMessage();
		}

		/**
		 * complex constructor (kind of)
		 * @param command a command that explains whats wrong, this can be used when an important command cannot be resolved
		 * @param is_fatal if this exception is fatal
		 */
		explicit Exception(const Command & command, bool is_fatal = false)	noexcept;

		/**
		 * the most common constructor
		 * @param msg a list of error messages
		 * @param is_fatal if this exception is fatal
		 */
		explicit Exception(std::initializer_list<std::string> msg, bool is_fatal = false)	noexcept;

		Exception(const Exception & other)	noexcept;

		std::vector<std::string> message_queue_;				//!< this message queue contains all information that shows what goes wrong

		virtual ~Exception(){}

		bool is_fatal_;											//!< is this exception fatal (requires the stopping of program execution)

		/**
		 * @return the error message queue joined by '\n'
		 */
		const char* what() const noexcept override;

	private:
		std::string joined_message_;					//message_queue joined by Exception::JoinMessage

		/**
		 * helper function that joins the message_queue_ into joined_message
		 */
		void JoinMessage();
	};
}