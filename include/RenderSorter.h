#pragma once
#include "Z_order.h"
#include <vector>
#include "GameItem.h"
#include "Figure.h"
#include "Object.h"

namespace Fengine
{
	/**
	 * A class that sort all the objects in the map in z-order
	 * given the lists of figures and their types. This class is only owned by Map.\n
	 * In Fengine, Map objects' Draw functions are not directly called by the map, but called by RenderSorter.
	 */
	class RenderSorter
	{
	private:
		std::vector<Z_order*> z_order_list_;					//!< list of Z_orders
	public:
		RenderSorter();
		~RenderSorter();

		void SortAndDraw();							//!< sort the list and draw

		/**
		 * remove an added z_order
		 * @param z pointer to the z_order to remove
		 */
		void RemoveZOrder(Z_order* z);

		/**
		 * add an initialized z_order
		 * @param z pointer to the z_order to add
		 */
		void AddZOrder(Z_order* z);
	};
}
