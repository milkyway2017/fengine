#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <map>
#include <vector>
#include <nlohmann/json.hpp>

#include "Utility.h"

namespace Fengine
{
	const std::map<std::string, SDL_Point> Resolutions = { std::pair<std::string,SDL_Point>("ancient",{640,480}),
														   std::pair<std::string,SDL_Point>("720p",{1280,720}) };					//sets of resolutions available for this game

	struct AutoDrawShape;

	/**
	 * class responsible for the most basic graphics functions
	 */
	class Graphics
	{
	public:
		~Graphics();
		void Close();

		/**
		 * @param title the title of the created window, only need to pass in the first time called (to initialize)
		 * @return the singleton of this class
		 */
		static Graphics* GetInstance(const std::string& title = "")
		{
			static Graphics* graphics;
			if (!graphics)
			{
				graphics = new Graphics();
				graphics->Init(title);
			}
			return graphics;
		}

		//makes Graphics not copyable
		Graphics(const Graphics&) {};
		Graphics& operator= (const Graphics&) {};

		/**
		 * retrieve a texture from image file
		 * @param path the path to the image file, this can be either a file's full path or a file name under Graphics::image_directory_
		 * @return the texture id of the texture, this id can be used to render textures
		 */
		GLuint GetTexture(const std::string & path);


		/**
		 * delete a texture
		 * @param texture the texture's id, issued by GetTexture
		 */
		void DeleteTexture(GLuint texture);

		/**
		 * retrieve a font
		 * @param path file path of the font, this can be either a file's full path or a file name under Graphics::image_directory_
		 * @param font_size font size of the loaded font
		 */
		TTF_Font* GetFont(const std::string & path, unsigned int font_size);

		/**
		 * load all Colors in the target file into the map
		 * @param path the path to the color file
		 * @return false on success, true on error
		 */
		bool LoadColor(const std::string & path);

		/**
		 * get a color
		 * @param color_name the name of the color in the color file
		 * @return the copy of that color
		 */
		GLColor GetColor(std::string color_name);

		/**
		 * get the width and height of a texture in pixels
		 * @param texture_id the texture id issued bu GetTexture
		 * @return the width & height of the texture (x -> width, y -> height)
		 */
		SDL_Point GetTextureWH(GLuint texture_id);

		/**
		 * @return the window's size in pixels
		 */
		SDL_Point GetWindowSize();

		/**
		 * sets the window size with p
		 * @param p the struct containing window's width as x and height as y
		 */
		void SetWindowSize(const SDL_Point & p);

		void DisplayDebugShape(const Rect4F &s, const GLColor& color, GLuint shader_program);
		void DisplayDebugShape(const GLShape & s, const GLColor& color, GLuint shader_program);

		/**
		 * draw a texture to screen
		 * @param shader_program the shader program to use
		 * @param texture the texture id to use, images can be loaded from Graphics::GetTexture
		 * @param shape the shape used to draw this texture
		 */
		void BlitSurface(GLuint shader_program, GLuint texture, const GLShape& shape);

		/**
		 * draw a series of lines
		 * @param shader_program the shader_program to use
		 * @param shape the GLShape struct containing the vertices of the lines, these lines are assumed to be connected
		 */
		void DrawLines(GLuint shader_program, const GLShape& shape);

		/**
		 * Generate a shader program
		 * @param vertpath the path to the vertex shader
		 * @param fragpath the path to fragment shader
		 * @param ProgramID_Holder the place to put the loaded shader
		 * @param name the name of this shader program, which will be use to refer to the program in the future
		 */
		void GenShaderProgram(std::string vertpath/*vertex shader path*/, std::string fragpath/*fragment shader path*/, GLuint& ProgramID_Holder, const std::string & name);

		/**
		 * present buffer to the window, end frame, no drawing should be done after this call and before the next BeginDrawing call
		 */
		void EndDrawing();

		/**
		 * clear the screen
		 */
		void BeginDrawing();

		/*********************************************
		 *****binding functions***not for beginners***
		 *********************************************/

		void BindTexture(GLuint texture);
		void BindVAO(GLuint vao_id);
		void BindVBO(GLuint vbo_id);
		void BindProgram(GLuint program);
		void UnbindAll();
		/*********************************************/

		/**
		 * convert an SDL_Surface* to an Opengl Texture
		 * @param surface the pointer to SDL_Surface created by SDL
		 * @param TextureID_Holder the place to put the converted texture
		 * @warning the graphics class is not responsible for the clean up of generated texture nor the SDL_Surface used to generate
		 * the caller has to do the clean up
		 */
		void SDLSurfaceToGLTexture(SDL_Surface *surface, GLuint &TextureID_Holder);

		/**
		 * returns a loaded shader program. To load a shader program, call Graphics::GenShaderProgram
		 * @param name the name given to the shader program
		 */
		GLuint GetShaderProgram(const std::string & name);

		/**
		 * set default directories for resource loading
		 * @param image_directory the default dir used to load images when Graphics::GetTexture is called
		 * @param font_directory the default dir used to load fonts when Graphics::GetFont is called
		 */
		void SetPaths(const std::string & image_directory, const std::string & font_directory);
	private:
		/*********************************************************
		 ***************private helper functions******************
		 *********************************************************/

		Graphics();
		//initialize SDL & Opengl
		void Init(const std::string& title);
		//set GL version
		void SetGLVersion(int major, int minor);
		//draws the shape's in debug_shapes_, and set them to not InUse
		void DrawDebugShapes();
		//link the program and detach and clean all the shaders associated with this program
		void LinkandCleanShaders(const GLuint &program_id, std::vector<GLuint>* shader_ids);
		//attach a shader to a program
		void AttachShader(GLuint shader, GLuint program_id);
		//load and compile a shader
		void CompileShader(std::string path, GLuint shaderid);



		/*********************************************************
		 ****************private data members*********************
		 *********************************************************/

		SDL_Window* window_;
		SDL_GLContext gl_context_;

		//containers
		std::unordered_map<std::string, SDL_Color> color_map_;													//!< colors loaded, <name (all caps), color> pairs
		std::unordered_map<std::string, GLuint> texture_map_;													//!< textures loaded
		std::unordered_map<std::string, std::unordered_map<unsigned int, TTF_Font*>> font_map_;					//!< fonts loaded by font file name -> font size -> font
		std::map<std::string, GLuint> shader_program_map_;															//!< shader programs with their names
		std::vector<AutoDrawShape> debug_shapes_;																//!< all debugshapes
		std::string image_directory_;							//!< default path when given image file names to load, this can be set by calling Graphics::SetPaths
		std::string font_directory_;							//!< default path when given font file names to load, this can be set by calling Graphics::SetPaths
	};

	/**
	 * a kind of GLShape that Graphics class uses to draw debugging shapes
	 */
	struct AutoDrawShape : GLShape
	{
		bool in_use_;			//!< true if this shape is in use
		unsigned int times_unused_;		//!< stores the number of times Draw() is called when not InUse,
										//!< when this exceeds limit_, Draw() will return true, causing the shape
										//!< to be garbage collected

		unsigned int limit_;				//!< stores the number

		GLuint shader_program;

		/*
		 * draws the shape
		 * @return true if the shape should be garbage collected
		 */
		bool Draw();
		friend void Graphics::DisplayDebugShape(const Rect4F & s, const GLColor& color, GLuint ShaderProgram);
		friend void Graphics::DisplayDebugShape(const GLShape & s, const GLColor& color, GLuint shader_program);
	private:
		AutoDrawShape() :
			in_use_(0),
			times_unused_(0),
			limit_(2)
		{}
	};
}
