var struct_fengine_1_1_sliding_door_def =
[
    [ "delta", "struct_fengine_1_1_sliding_door_def.html#aaba9351e46d2be32d3e48964af7b661f", null ],
    [ "distance", "struct_fengine_1_1_sliding_door_def.html#a7278e0e9ce4c6ed6bfde004cb722dfdc", null ],
    [ "duration_ms", "struct_fengine_1_1_sliding_door_def.html#aad490c526cfd05dee0106cb154af0094", null ],
    [ "isDouble", "struct_fengine_1_1_sliding_door_def.html#aadfeb473387eae43417b0930ed8199b7", null ],
    [ "isHorizontal", "struct_fengine_1_1_sliding_door_def.html#aedc28e7e2b08e37cecff988ffd21df7f", null ],
    [ "isNegative", "struct_fengine_1_1_sliding_door_def.html#a1aa335d7d2671fade4d250908d30fec7", null ],
    [ "source_rect_1", "struct_fengine_1_1_sliding_door_def.html#a75d7015b4cd020164312033d7cabd03a", null ],
    [ "source_rect_2", "struct_fengine_1_1_sliding_door_def.html#a7b181f1edbb7dae86270d84eff5e639e", null ],
    [ "spriteRect", "struct_fengine_1_1_sliding_door_def.html#a97ef4404c9ff35c358d18dbcacb7c7eb", null ],
    [ "spriteToActiveArea", "struct_fengine_1_1_sliding_door_def.html#a48887feb1605de0917841311f24e05ee", null ],
    [ "spriteToBody1", "struct_fengine_1_1_sliding_door_def.html#aa00540fe74e4581be2f9f17b15b9e03e", null ],
    [ "spriteToBody2", "struct_fengine_1_1_sliding_door_def.html#aba946c60faa6c69c72b2f81073253581", null ],
    [ "texture_path", "struct_fengine_1_1_sliding_door_def.html#a0a7009e07c40058727a0f35eaaf6b006", null ]
];