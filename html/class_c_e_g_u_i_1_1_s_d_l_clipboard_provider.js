var class_c_e_g_u_i_1_1_s_d_l_clipboard_provider =
[
    [ "SDLClipboardProvider", "class_c_e_g_u_i_1_1_s_d_l_clipboard_provider.html#ac1cbf7a6ff2c4513dde90b0b102ebe70", null ],
    [ "~SDLClipboardProvider", "class_c_e_g_u_i_1_1_s_d_l_clipboard_provider.html#a4845347bfeef50ffe7eb13fbc95d2cd4", null ],
    [ "Deallocate", "class_c_e_g_u_i_1_1_s_d_l_clipboard_provider.html#a91a3fa0453174753d46ef0c014c79941", null ],
    [ "retrieveFromClipboard", "class_c_e_g_u_i_1_1_s_d_l_clipboard_provider.html#ad15b85f54d1ab3a4e0ece8f312f21a6c", null ],
    [ "sendToClipboard", "class_c_e_g_u_i_1_1_s_d_l_clipboard_provider.html#a7b51d934d4cc70a7c8e70e6061d7eccd", null ],
    [ "text", "class_c_e_g_u_i_1_1_s_d_l_clipboard_provider.html#a08933e35e9fcc7bb35c0ed6a90cd6aee", null ]
];