var _command_def_8h =
[
    [ "EnumStringMap", "_command_def_8h.html#a5a65b7ac24868003bab1e463f213219e", null ],
    [ "EventLevel", "_command_def_8h.html#a16b7f565329f8e2ae0ecc87db26b990a", null ],
    [ "IntEnumStringMap", "_command_def_8h.html#a5b5f08b3ac9ba3cd73aec39b73d27395", null ],
    [ "CommandLevel", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacb", [
      [ "PROGRAM", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacba1a1dab7d911e74cc254938bb98991b24", null ],
      [ "FRAME", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacbae20c7c11396431aef294057834211446", null ],
      [ "LEVEL", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacbaaf8bcd7896138682ebcd28b47e7cafc5", null ],
      [ "GAMEWORLD", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacba2661af2e43f9636088b0ef0987a0c743", null ],
      [ "MAP", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacbabfc796957b5615bd4762629401ef2ba9", null ],
      [ "MAP_EVENTMANAGER", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacba482e50df7edc7aaf7787851aa3c11a69", null ],
      [ "NO_LEVEL", "_command_def_8h.html#af537995cd149aa270ebc87ad5fbbeacbac001a8ad97c50a41bffbc19937e62532", null ]
    ] ],
    [ "FrameCommandType", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068", [
      [ "NO_COMMAND", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068a7c855892af458b76d7cce2dd38d746f0", null ],
      [ "LOAD_LEVEL", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068a07f0d0c92987a1e1433ece8c99d8f122", null ],
      [ "TOGGLE_CONSOLE", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068a01cb85db7a0be8e1b1e1d63b55300443", null ],
      [ "TOGGLE_SHADOW", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068a0e9abba025137aca4399b322967be6a0", null ],
      [ "TOGGLE_WIDGET", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068ae21fb4ed2e9a3bc9308db34c5c6094d6", null ],
      [ "ADD_WIDGET", "_command_def_8h.html#a2bb82f0b0e933cd62b13883433815068ae6dcea2d36ebb532b9bc0e2fd6e2b36c", null ]
    ] ],
    [ "GameWorldCommandType", "_command_def_8h.html#a00df5a39dcf29405fca26d79cebd11e6", [
      [ "NO_COMMAND", "_command_def_8h.html#a00df5a39dcf29405fca26d79cebd11e6a7c855892af458b76d7cce2dd38d746f0", null ]
    ] ],
    [ "LevelCommandType", "_command_def_8h.html#a4a4c86a40c8253457b7b19f04e2bc36e", [
      [ "NO_COMMAND", "_command_def_8h.html#a4a4c86a40c8253457b7b19f04e2bc36ea7c855892af458b76d7cce2dd38d746f0", null ],
      [ "DELLVL", "_command_def_8h.html#a4a4c86a40c8253457b7b19f04e2bc36ea4c2939198f56e8d583708a3ee49b86a2", null ],
      [ "NEW_RES", "_command_def_8h.html#a4a4c86a40c8253457b7b19f04e2bc36ea6f5a130458e65cd3daa62a302d2794b2", null ]
    ] ],
    [ "MapCommandType", "_command_def_8h.html#af9167d1730ceab5c9d8ae340624f095b", [
      [ "NO_COMMAND", "_command_def_8h.html#af9167d1730ceab5c9d8ae340624f095ba7c855892af458b76d7cce2dd38d746f0", null ],
      [ "NEW_RES", "_command_def_8h.html#af9167d1730ceab5c9d8ae340624f095ba6f5a130458e65cd3daa62a302d2794b2", null ]
    ] ],
    [ "MapEventManagerEventType", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485", [
      [ "NO_COMMAND", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a7c855892af458b76d7cce2dd38d746f0", null ],
      [ "ACTIVEAREA_FILLED", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a877b25d84c1ae4afe357f56e3696a0da", null ],
      [ "ACTIVEAREA_EMPTIED", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485aa5fa9e1a8455aeccdbf9bc1b29f940b8", null ],
      [ "FOLLOW", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a578d83c6d15737d0c46bd80f0a19263f", null ],
      [ "FREEZE", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a1da4c7af6c0e106e2aec1c483252b329", null ],
      [ "PATROL", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a5383e862b017e18e1087d61e9f2684d9", null ],
      [ "DEBUGMODE", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485ae1327efd2b5e94ee34b36cdae9d23272", null ],
      [ "ITEM_PICKEDUP", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a5aec6a4f3b7995eafac76e0478569ae1", null ],
      [ "ITEM_DROPPED", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a84951df51a6fbd9fa948a92021b62107", null ],
      [ "ATTACK", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485ac6ddd0f72ff2fd344693b9ca8d483871", null ],
      [ "DIE", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a83ebd7c89f4cc1a85396fba7d6f7e900", null ],
      [ "KEY_PRESSED", "_command_def_8h.html#ad29993ea8320ac85ffe1a403061c0485a7ef8949a37c02f94d7975401b455b7f0", null ]
    ] ],
    [ "ProgramCommandType", "_command_def_8h.html#a2d185b197d1380f2bdc3971c79fa095b", [
      [ "NO_COMMAND", "_command_def_8h.html#a2d185b197d1380f2bdc3971c79fa095ba7c855892af458b76d7cce2dd38d746f0", null ],
      [ "EXP", "_command_def_8h.html#a2d185b197d1380f2bdc3971c79fa095ba8c670f8c37b95e1ed14a0ce414b049c7", null ],
      [ "CGF", "_command_def_8h.html#a2d185b197d1380f2bdc3971c79fa095ba892e87ed17a9b41700a29c4faabcc470", null ],
      [ "NEW_RES", "_command_def_8h.html#a2d185b197d1380f2bdc3971c79fa095ba6f5a130458e65cd3daa62a302d2794b2", null ]
    ] ]
];